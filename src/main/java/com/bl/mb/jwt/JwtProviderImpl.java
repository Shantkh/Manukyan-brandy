package com.bl.mb.jwt;

import com.bl.mb.security.UserPrincipal;
import com.bl.mb.utils.SecurityUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PublicKey;
import java.security.spec.ECGenParameterSpec;
import java.util.Arrays;
import java.util.Date;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class JwtProviderImpl implements JwtProvider {

    @Value("${app.jwt.expiration}")
    private Long JWT_EXPIRATION;

    private final KeyPair keyPair;

    /**
     * Initializes the JwtProviderImpl by generating a key pair for signing and verifying JWTs.
     */
    public JwtProviderImpl() {
        try {
            ECGenParameterSpec ecSpec = new ECGenParameterSpec("secp521r1");
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("EC");
            keyPairGenerator.initialize(ecSpec);
            keyPair = keyPairGenerator.generateKeyPair();
        } catch (Exception e) {
            throw new RuntimeException("Error initializing key pair", e);
        }
    }

    /**
     * Generates a JWT token for the given UserPrincipal.
     *
     * @param auth The UserPrincipal representing the user details.
     * @return The generated JWT token.
     */
    @Override
    public String generateToken(UserPrincipal auth) {
        String authorities = auth.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","));

        JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject(auth.getUsername())
                .claim("roles", authorities)
                .claim("userId", auth.getId())
                .setExpiration(new Date(System.currentTimeMillis() + JWT_EXPIRATION));

        // Sign with the private key
        jwtBuilder.signWith(SignatureAlgorithm.ES512, keyPair.getPrivate());

        return jwtBuilder.compact();
    }

    /**
     * Extracts authentication information from the JWT token present in the HttpServletRequest.
     *
     * @param request The HttpServletRequest object.
     * @return Authentication object containing user details.
     */
    @Override
    public Authentication getAuthentication(HttpServletRequest request) {
        Claims claims = extractClaims(request);
        if (claims == null) return null;

        String username = claims.getSubject();
        UUID userId = UUID.fromString(claims.get("userId", String.class));

        Set<GrantedAuthority> authorities = Arrays.stream(claims.get("roles", String.class).split(","))
                .map(SecurityUtils::convertRoleToAuthority)
                .collect(Collectors.toSet());

        UserDetails userDetails = UserPrincipal.builder()
                .username(username)
                .authorities(authorities)
                .id(userId)
                .build();

        return new UsernamePasswordAuthenticationToken(userDetails, null, authorities);
    }

    /**
     * Checks if the JWT token in the HttpServletRequest is valid.
     *
     * @param request The HttpServletRequest object.
     * @return True if the token is valid, false otherwise.
     */
    @Override
    public boolean isTokenValid(HttpServletRequest request) {
        Claims claims = extractClaims(request);
        return claims != null && claims.getExpiration().after(new Date());
    }

    private Claims extractClaims(HttpServletRequest request) {
        String token = SecurityUtils.extractAuthTokenFromRequest(request);
        if (token == null) return null;

        try {
            PublicKey publicKey = keyPair.getPublic();
            return Jwts.parserBuilder()
                    .setSigningKey(publicKey)
                    .build()
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            return null;
        }
    }
}
