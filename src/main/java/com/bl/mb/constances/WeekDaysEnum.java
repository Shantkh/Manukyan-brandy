package com.bl.mb.constances;

public enum WeekDaysEnum {
    SUNDAY("Կիրակի"),
    MONDAY("Երկուշաբթի"),
    TUESDAY("Երեքշաբթի"),
    WEDNESDAY("Չորեքշաբթի"),
    THURSDAY("Հինգշաբթի"),
    FRIDAY("Ուրբաթ"),
    SATURDAY("Շաբաթ");

    private final String armenianTranslation;

    WeekDaysEnum(String armenianTranslation) {
        this.armenianTranslation = armenianTranslation;
    }

    public String getArmenianTranslation() {
        return armenianTranslation;
    }

    //Locale armenianLocale = new Locale("hy", "AM");
    //        ResourceBundle bundle = ResourceBundle.getBundle("translations", armenianLocale);
    //
    //        System.out.println("\nDays of the week:");
    //        for (WeekDaysEnum day : WeekDaysEnum.values()) {
    //            System.out.println(day + " / " + bundle.getString(day.name()));
    //        }
}
