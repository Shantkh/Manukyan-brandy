package com.bl.mb.constances;

public enum MonthOfYear {
    JANUARY("Հունվար"),
    FEBRUARY("Փետրվար"),
    MARCH("Մարտ"),
    APRIL("Ապրիլ"),
    MAY("Մայիս"),
    JUNE("Հունիս"),
    JULY("Հուլիս"),
    AUGUST("Օգոստոս"),
    SEPTEMBER("Սեպտեմբեր"),
    OCTOBER("Հոկտեմբեր"),
    NOVEMBER("Նոյեմբեր"),
    DECEMBER("Դեկտեմբեր");

    private final String armenianTranslation;

    MonthOfYear(String armenianTranslation) {
        this.armenianTranslation = armenianTranslation;
    }

    public String getArmenianTranslation() {
        return armenianTranslation;
    }
}

