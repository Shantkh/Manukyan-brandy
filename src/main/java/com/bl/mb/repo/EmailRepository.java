package com.bl.mb.repo;


import com.bl.mb.models.EmailDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface EmailRepository extends JpaRepository<EmailDetails, UUID> {
    EmailDetails findAllByCode(String code);

    EmailDetails findAllByRecipient(String email);
}