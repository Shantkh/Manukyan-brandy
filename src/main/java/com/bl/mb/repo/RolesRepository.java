package com.bl.mb.repo;

import com.bl.mb.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface RolesRepository extends JpaRepository<Role, UUID> {
    Role findByName(String name);

    @Override
    void delete(Role role);
}