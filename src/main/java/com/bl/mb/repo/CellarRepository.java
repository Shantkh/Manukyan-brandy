package com.bl.mb.repo;

import com.bl.mb.models.Cellar;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface CellarRepository extends JpaRepository<Cellar, UUID> {
    Optional<Cellar> findCellarByCellarSection(String cellarSection);
}
