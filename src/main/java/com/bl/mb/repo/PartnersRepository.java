package com.bl.mb.repo;

import com.bl.mb.models.Partners;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PartnersRepository extends JpaRepository<Partners, Long> {
}
