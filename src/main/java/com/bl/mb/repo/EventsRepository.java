package com.bl.mb.repo;

import com.bl.mb.models.Events;
import com.bl.mb.models.MonthEventsDTO;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

public interface EventsRepository extends JpaRepository<Events, UUID> {
    Events findByEventStartDate(Timestamp eventStartDate);

    @Query("SELECT e FROM Events e " +
            "JOIN FETCH e.localizations l " +
            "WHERE l.locale = :locale AND e.isEventVisible = true")
    List<Events> findLatestByLocale(@Param("locale") String locale,
                                    PageRequest pageReq);

    @Query("SELECT e FROM Events e " +
            "JOIN FETCH e.localizations l " +
            "WHERE e.isEventVisible = true AND (e.eventStartDate >= :startDate AND e.eventEndDate <= :endDate)")
    List<Events> findAllByEventStartDateAndEventEndDate(PageRequest pageReq,
                                                        @Param("startDate") Timestamp startDate,
                                                        @Param("endDate") Timestamp endDate);

    @Query("SELECT e FROM Events e " +
            "JOIN FETCH e.localizations l " +
            "WHERE l.locale = :locale AND e.isEventVisible = true " +
            "AND (e.eventStartDate >= :startDate AND e.eventEndDate <= :endDate)")
    List<Events> findAllByEventStartDateAndEventEndDateAndLocalizations(PageRequest pageReq,
                                                                        @Param("startDate") Timestamp startDate,
                                                                        @Param("endDate") Timestamp endDate,
                                                                        @Param("locale") String locale);

    @Query("SELECT e FROM Events e " +
            "JOIN FETCH e.localizations l " +
            "WHERE l.locale = :locale AND e.isEventVisible = true " +
            "AND EXTRACT(YEAR FROM e.eventStartDate) = :year " +
            "AND EXTRACT(MONTH FROM e.eventStartDate) = :month " +
            "ORDER BY e.eventStartDate ASC")
    List<Events> findEventsInNextMonth(@Param("locale") String locale, @Param("year") int year, @Param("month") int month, PageRequest pageReq);

    @Query("SELECT e FROM Events e " +
            "JOIN FETCH e.localizations l " +
            "WHERE l.locale = :locale AND e.isEventVisible = true " +
            "AND EXTRACT(YEAR FROM e.eventStartDate) = :year " +
            "AND EXTRACT(MONTH FROM e.eventStartDate) = :month " +
            "ORDER BY e.eventStartDate ASC")
    List<Events> findEventsInPreMonth(@Param("locale") String locale, @Param("year") int year, @Param("month") int month, PageRequest pageReq);

    @Query(value = "SELECT e FROM Events e  " +
            "WHERE EXTRACT(MONTH FROM e.eventStartDate) > EXTRACT(MONTH FROM CAST(:targetDate AS timestamp))")
    List<Events> findNextNearestEventFuture(@Param("targetDate") Timestamp targetDate);

    @Query(value = "SELECT e FROM Events e  " +
            "WHERE EXTRACT(MONTH FROM e.eventStartDate) < EXTRACT(MONTH FROM CAST(:targetDate AS timestamp))")
    List<Events> findPreNearestEventPast(@Param("targetDate") Timestamp targetDate);

    @Query("SELECT e FROM Events e " +
            "JOIN FETCH e.localizations l " +
            "WHERE e.id = :id AND l.locale = :locale AND e.isEventVisible = true")
    List<Events> findByIdAndLocalizations(@Param("id") UUID id, @Param("locale") String locale);

    @Query("SELECT e FROM Events e " +
            "JOIN FETCH e.localizations l " +
            "WHERE l.locale = :locale AND e.isEventVisible = true " +
            "AND EXTRACT(YEAR FROM e.eventStartDate) = :year " +
            "AND EXTRACT(MONTH FROM e.eventStartDate) = :month " )
    List<Events> findEventsInThisMonth(@Param("locale") String locale,
                                       @Param("year") int year,
                                       @Param("month") int month,
                                       PageRequest pageReq);

    @Query("SELECT NEW com.bl.mb.models.MonthEventsDTO(" +
            "TO_CHAR(e.eventStartDate, 'Month'), " +
            "COUNT(e), " +
            "CASE WHEN COUNT(e) > 0 THEN true ELSE false END) " +
            "FROM Events e " +
            "JOIN e.localizations l " +
            "WHERE l.locale = :locale AND e.isEventVisible = true " +
            "AND EXTRACT(YEAR FROM e.eventStartDate) = :year " +
            "GROUP BY TO_CHAR(e.eventStartDate, 'Month')" +
            "ORDER BY EXTRACT(MONTH FROM MIN(e.eventStartDate))")
    List<MonthEventsDTO> findEventsInThisYearGroupByMonth(@Param("locale") String locale,
                                                          @Param("year") int year);



}
