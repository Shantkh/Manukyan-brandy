package com.bl.mb.repo;

import com.bl.mb.models.SectionLocalization;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BannerLocalizationRepository extends JpaRepository<SectionLocalization,Long> {
}
