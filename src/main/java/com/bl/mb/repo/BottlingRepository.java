package com.bl.mb.repo;

import com.bl.mb.models.Bottling;
import com.bl.mb.models.Cellar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BottlingRepository extends JpaRepository<Bottling, UUID> {

    Optional<Bottling> findBottlingByBottlingSection(String cellarSection);

    @Query("SELECT b FROM Bottling b JOIN FETCH b.images")
    List<Bottling> findAllWithImages();

}
