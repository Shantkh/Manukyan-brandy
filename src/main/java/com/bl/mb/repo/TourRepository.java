package com.bl.mb.repo;

import com.bl.mb.models.Tours;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface TourRepository extends JpaRepository<Tours, UUID> {

    @Query("SELECT s FROM Tours s " +
            "JOIN FETCH s.tourLocalization l " +
            "WHERE l.locale = :locale " +
            "ORDER BY s.createdOn DESC")
    List<Tours> findLatestByPageNameAndLocale(@Param("locale") String locale, PageRequest pageable);

    @Query("SELECT s FROM Tours s JOIN FETCH s.tourLocalization l ORDER BY s.dateOfTour ASC ")
    List<Tours> findAllPagination(PageRequest pageable);

    @Query("SELECT s FROM Tours s " +
            "JOIN FETCH s.tourLocalization l " +
            "WHERE l.locale = :locale " +
            "AND s.dateOfTour BETWEEN :startDate AND :endDate " +
            "ORDER BY s.dateOfTour DESC, s.id ASC")
    List<Tours> findToursBetweenDates(@Param("locale") String locale,
                                      @Param("startDate") String startDate,
                                      @Param("endDate") String endDate);

    @Query("SELECT e FROM Tours e " +
            "JOIN FETCH e.tourLocalization l " +
            "WHERE l.locale = :locale AND e.id =:bookingId")
    Tours findByIdAndLocal(@Param("bookingId") UUID bookingId,
                           @Param("locale") String locale);
}
