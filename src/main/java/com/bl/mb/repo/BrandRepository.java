package com.bl.mb.repo;

import com.bl.mb.models.Brand;
import com.bl.mb.models.BrandName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface BrandRepository extends JpaRepository<Brand, UUID> {
    Optional<Brand> findBrandByBrandSectionAndBrandName(String brandSection, BrandName brandNameId);

    @Query("SELECT e FROM Brand e WHERE e.brandName.id =?1")
    List<Brand> findAllByBrandNameId(UUID id);
}
