package com.bl.mb.repo;

import com.bl.mb.models.Subscribers;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface SubscribeRepository extends JpaRepository<Subscribers, UUID> {
    boolean existsByEmail(String email);
}
