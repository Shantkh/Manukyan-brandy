package com.bl.mb.repo;

import com.bl.mb.constances.PagesEnum;
import com.bl.mb.constances.SliderEnum;
import com.bl.mb.models.Sections;
import jakarta.validation.Valid;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface SectionsRepository extends JpaRepository<Sections, UUID> {
    long count();

    @Query("SELECT s FROM Sections s " +
            "JOIN FETCH s.localizations l " +
            "WHERE l.locale = :locale " +
            "AND s.pagesEnum = :pageName " +
            "ORDER BY s.createdOn DESC limit 1")
    Optional<Sections> findLatestByPageNameAndLocale(@Param("pageName") PagesEnum pageName,
                                                     @Param("locale") String locale);

    List<Sections> findAllByPagesEnum(@Param("pagesEnum") PagesEnum pagesEnum);

    @Query("SELECT COUNT(b) FROM Sections b WHERE b.pagesEnum = :pagesEnum")
    Integer countByPageEnum(@Param("pagesEnum") PagesEnum pagesEnum);

    Optional<Sections> findSectionsByPagesEnum(@Param("pagesEnum") PagesEnum pagesEnum);
}

