package com.bl.mb.repo;

import com.bl.mb.models.TourBooking;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

public interface ToursBookingRepository extends JpaRepository<TourBooking, UUID> {

    @Query("SELECT tb FROM TourBooking  tb WHERE tb.createdOn >= ?1 ")
    List<TourBooking> findAllToursWithinTimeRange(Timestamp startDate, PageRequest pageReq);

    @Query("SELECT tb FROM TourBooking tb WHERE tb.tourId = :eventId")
    List<TourBooking> findParticipantsByEventId(@Param("eventId") UUID eventId);

}
