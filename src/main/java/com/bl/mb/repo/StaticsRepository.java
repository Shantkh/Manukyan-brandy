package com.bl.mb.repo;

import com.bl.mb.models.Statics;
import com.bl.mb.models.StatisticsResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface StaticsRepository extends JpaRepository<Statics, UUID> {
    @Query("SELECT new com.bl.mb.models.StatisticsResult(" +
            "COUNT(CASE WHEN v.createdOn >= :thirtyDaysAgo THEN 1 END) AS usersLast30Days, " +
            "COUNT(CASE WHEN v.createdOn >= :fifteenDaysAgo THEN 1 END) AS usersLast15Days, " +
            "COUNT(CASE WHEN v.createdOn >= :sevenDaysAgo THEN 1 END) AS usersLast7Days, " +
            "COUNT(CASE WHEN v.createdOn >= :today THEN 1 END) AS usersToday) " +
            "FROM Statics v")
    StatisticsResult getVisitorCounts(@Param("thirtyDaysAgo") LocalDateTime thirtyDaysAgo,
                                      @Param("fifteenDaysAgo") LocalDateTime fifteenDaysAgo,
                                      @Param("sevenDaysAgo") LocalDateTime sevenDaysAgo,
                                      @Param("today") LocalDateTime today);

    @Query("SELECT v.country AS country, COUNT(*) AS visitors " +
            "FROM Statics v " +
            "WHERE v.createdOn >= :thirtyDaysAgo " +
            "GROUP BY v.country " +
            "ORDER BY visitors DESC " +
            "LIMIT 10")
    List<Map<String, Object>> getTopCountries(@Param("thirtyDaysAgo") LocalDateTime thirtyDaysAgo);

    @Query("SELECT v.page AS page, COUNT(*) AS pageCount " +
            "FROM Statics v " +
            "WHERE v.createdOn >= :thirtyDaysAgo " +
            "GROUP BY v.page " +
            "ORDER BY pageCount DESC " +
            "LIMIT 10")
    List<Map<String, Object>> getTopPages(@Param("thirtyDaysAgo") LocalDateTime thirtyDaysAgo);

    List<Statics> findAllByCreatedOnBefore(Timestamp createdOn);

}
