package com.bl.mb.repo;

import com.bl.mb.models.InfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface InfoRepository extends JpaRepository<InfoEntity, UUID> {
    Optional<InfoEntity> findInfoEntitiesByShowOn(String showOn);

}
