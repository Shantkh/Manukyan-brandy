package com.bl.mb.repo;

import com.bl.mb.constances.PagesEnum;
import com.bl.mb.constances.SliderEnum;
import com.bl.mb.models.Sections;
import com.bl.mb.models.SliderMedia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface SlidersRepository extends JpaRepository<SliderMedia, UUID> {

    @Query("SELECT COUNT(b) FROM SliderMedia b WHERE b.sliderEnum = :sliderEnum")
    Integer countBySliderEnum(@Param("sliderEnum") SliderEnum sliderEnum);

    @Query("SELECT s FROM SliderMedia s " +
            "JOIN FETCH s.sliderLocalizations l " +
            "WHERE l.locale = :locale ")
    List<SliderMedia> findSlidesPageNameAndLocale(@Param("locale") String lang);

    List<SliderMedia> findAllBySliderEnum(SliderEnum pagesEnum);

    Optional<SliderMedia> findSliderMediaBySliderEnum(SliderEnum pagesEnum);
}
