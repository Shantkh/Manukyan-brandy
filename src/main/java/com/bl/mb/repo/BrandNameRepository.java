package com.bl.mb.repo;

import com.bl.mb.models.Brand;
import com.bl.mb.models.BrandName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.UUID;

public interface BrandNameRepository extends JpaRepository<BrandName, UUID> {

    Optional<BrandName> findByBrandNameEnglish(String brandName);
    Optional<BrandName> findBrandNamesByBrandNameArmenianContains(String brandName);

}
