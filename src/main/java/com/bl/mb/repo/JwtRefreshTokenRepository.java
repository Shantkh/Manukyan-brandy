package com.bl.mb.repo;

import com.bl.mb.models.JwtRefreshToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface JwtRefreshTokenRepository extends JpaRepository<JwtRefreshToken, UUID> {

}