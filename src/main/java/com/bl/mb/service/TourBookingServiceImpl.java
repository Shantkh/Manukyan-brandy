package com.bl.mb.service;

import com.bl.mb.constances.LanguageEnum;
import com.bl.mb.kafka.utils.KafkaEmailUtils;
import com.bl.mb.models.*;
import com.bl.mb.repo.TourRepository;
import com.bl.mb.repo.ToursBookingRepository;
import com.bl.mb.utils.PagePagination;
import com.bl.mb.utils.ResponseUtil;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static com.bl.mb.utils.ResponseUtil.ResponseError;
import static com.bl.mb.utils.ResponseUtil.ResponseSuccess;

@Service
@RequiredArgsConstructor
public class TourBookingServiceImpl implements TourBookingService {

    private final ToursBookingRepository toursBookingRepository;
    private final PagePagination pagePagination;
    private final TourRepository tourRepository;
    private final KafkaEmailUtils kafkaEmailUtils;


    @Override
    public ResponseBase<?> bookTour(@Valid final TourBooking tourBooking) {
        try {
            var result = toursBookingRepository.save(tourBooking);
            EmailDetails emailDetails = EmailDetails.builder()
                    .recipient(tourBooking.getEmail())
                    .name(tourBooking.getName())
                    .phone(tourBooking.getPhone())
                    .build();
//            kafkaEmailUtils.userTourBookingActivity(emailDetails);
            return ResponseUtil.ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> bookedAllTours(@Valid final PaginationWithSorting paginationWithSorting) {
        try {
            var pageReq = pagePagination.pagePagination(paginationWithSorting);
            var data = toursBookingRepository.findAll(pageReq);
            Map<UUID, List<TourBooking>> groupedByTourId = data.getContent().stream()
                    .collect(Collectors.groupingBy(TourBooking::getTourId));

            var result = groupedByTourId.entrySet().stream()
                    .map(entry -> {
                        UUID tourId = entry.getKey();
                        List<TourBooking> bookingsForTourId = entry.getValue();
                        Tours tour = getTourById(tourId); // Fetch the tour details

                        // Create TourBookingResponse object for each group
                        return TourBookingResponse.builder()
                                .tour(tour)
                                .bookings(bookingsForTourId)
                                .build();
                    })
                    .toList();

            return ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    private Tours getTourById(UUID tourId) {
        return tourRepository.findById(tourId).get();
    }

    @Override
    public ResponseBase<?> bookedAllToursStartedLastWeek(@Valid final PaginationWithSorting paginationWithSorting) {
        try {
            PageRequest pageReq = pagePagination.pagePagination(paginationWithSorting);
            Timestamp startDate = Timestamp.from(Instant.now().minus(7, ChronoUnit.DAYS));
            var result = toursBookingRepository.findAllToursWithinTimeRange(startDate, pageReq);
            return ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> toursById(@Valid final String id) {
        try {
            var result = toursBookingRepository.findById(UUID.fromString(id));
            return ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> customersToursById(@Valid final String id) {
        try {
            var result = getParticipantsByLanguage(id);
            return ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    public Map<LanguageEnum, List<TourBooking>> getParticipantsByLanguage(String eventId) {
        var participants = toursBookingRepository.findParticipantsByEventId(UUID.fromString(eventId));
        Map<LanguageEnum, List<TourBooking>> participantsByLanguage = new HashMap<>();

        for (TourBooking participant : participants) {
            LanguageEnum language = LanguageEnum.valueOf(participant.getTourLanguage().toUpperCase());
            participantsByLanguage.computeIfAbsent(language, k -> new ArrayList<>()).add(participant);
        }

        return participantsByLanguage;
    }
}
