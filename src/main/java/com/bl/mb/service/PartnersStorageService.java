package com.bl.mb.service;

import com.bl.mb.models.ResponseBase;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.util.stream.Stream;

public interface PartnersStorageService {
   void init();

   void save(final MultipartFile file) throws IOException;

   String uploadFiles(final MultipartFile file,final HttpServletRequest request);

   void deleteFile(final String filename);
}