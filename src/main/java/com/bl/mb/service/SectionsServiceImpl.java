package com.bl.mb.service;

import com.bl.mb.constances.LinksEnum;
import com.bl.mb.constances.PagesEnum;
import com.bl.mb.constances.SliderEnum;
import com.bl.mb.models.*;
import com.bl.mb.repo.BannerLocalizationRepository;
import com.bl.mb.repo.SectionsRepository;
import com.bl.mb.repo.SlidersRepository;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.bl.mb.utils.ResponseUtil.*;
import static com.bl.mb.utils.ResponseUtil.ResponseSuccess;

/**
 * Service implementation for managing banners and their localizations.
 *
 * <p>
 * This class provides methods for creating, updating, deleting, and retrieving banner information.
 * The implementation adheres to the SOLID principles and encapsulates banner-related operations
 * while interacting with repositories to persist and retrieve data.
 * </p>
 *
 * <p>
 * <strong>Author:</strong> Shant Khayalian<br>
 * <strong>Version:</strong> 1.0<br>
 * <strong>Since:</strong> 2024-01-13
 * </p>
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class SectionsServiceImpl implements SectionsService {

    private final SectionsRepository sectionsRepository;
    private final SlidersRepository slidersRepository;
    private final BannerLocalizationRepository bannerLocalizationRepository;

    /**
     * Saves a new banner along with its localizations.
     *
     * @param sections  The banner entity to be saved.
     * @param pagesEnum The page enum associated with the banner.
     * @return A response indicating the success or failure of the operation.
     */
    @Override
    public ResponseBase<?> saveSections(@Valid final Sections sections,
                                        @Valid final PagesEnum pagesEnum) {
        try {
            var bannersList = sectionsRepository.findSectionsByPagesEnum(pagesEnum);
            bannersList.ifPresent(value -> sectionsRepository.deleteById(value.getId()));

            var bannerObject = Sections.builder()
                    .sliderMedia(sections.getSliderMedia())
                    .pagesEnum(pagesEnum)
                    .localizations(new ArrayList<>())
                    .link(sections.getLink() == null ? LinksEnum.HOME : sections.getLink())
                    .build();

            var localizations = sections.getLocalizations();
            if (localizations != null && !localizations.isEmpty()) {
                localizations.stream().map(e -> SectionLocalization.builder()
                        .locale(e.getLocale())
                        .htmlContent(e.getHtmlContent())
                        .sections(bannerObject)
                        .build()).forEach(localization -> bannerObject.getLocalizations().add(localization));
            }

            var savedBanner = sectionsRepository.save(bannerObject);

            var response = SectionsDto.builder()
                    .id(savedBanner.getId())
                    .pagesEnum(pagesEnum)
                    .sliderMedia(savedBanner.getSliderMedia())
                    .localizations(savedBanner.getLocalizations())
                    .createdOn(savedBanner.getCreatedOn())
                    .build();

            return ResponseSuccess(response);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> saveSlideSections(final @Valid SliderMedia sliderMedia,
                                             final @Valid SliderEnum sliderEnum) {
        try {
            var sliderData = slidersRepository.findSliderMediaBySliderEnum(sliderEnum);
            if (sliderData.isPresent()) {
                slidersRepository.deleteById(sliderData.get().getId());
            }

            var bannerObject = SliderMedia.builder()
                    .sliderMedia(sliderMedia.getSliderMedia())
                    .sliderEnum(sliderEnum)
                    .sliderLocalizations(new ArrayList<>())
                    .link(sliderMedia.getLink())
                    .build();

            var localizations = sliderMedia.getSliderLocalizations();
            if (localizations != null && !localizations.isEmpty()) {
                localizations.stream().map(e -> SliderLocalization.builder()
                        .locale(e.getLocale())
                        .htmlContent(e.getHtmlContent())
                        .sliderMedia(bannerObject)
                        .build()).forEach(localization -> bannerObject.getSliderLocalizations().add(localization));
            }

            return saveSliderMedia(sliderEnum, bannerObject);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    /**
     * Updates an existing banner along with its localizations.
     *
     * @param sections  The banner entity to be updated. Must not be {@code null}.
     * @param pagesEnum The page enum associated with the banner. Must not be {@code null}.
     * @return A response indicating the success or failure of the operation.
     * If successful, the response contains the updated banner information.
     * If unsuccessful, the response contains an error message.
     */
    @Override
    public ResponseBase<?> updateSections(@Valid final Sections sections,
                                          @Valid final PagesEnum pagesEnum) {
        try {
            var isBanner = sectionsRepository.findById(sections.getId());
            for (SectionLocalization localization : sections.getLocalizations()) {
                var isLocal = bannerLocalizationRepository.findById(localization.getId());
                if (isLocal.isEmpty()) {
                    return ResponseNotFound(localization + " not found.");
                }
            }

            if (isBanner.isEmpty()) {
                return ResponseNotFound("Banner not found.");
            }

            var bannerObject = Sections.builder()
                    .id(isBanner.get().getId())
                    .createdOn(isBanner.get().getCreatedOn())
                    .pagesEnum(sections.getPagesEnum())
                    .sliderMedia(sections.getSliderMedia())
                    .localizations(new ArrayList<>())
                    .build();

            var localizations = sections.getLocalizations();
            if (localizations != null && !localizations.isEmpty()) {
                localizations.stream().map(e -> SectionLocalization.builder()
                        .id(e.getId())
                        .locale(e.getLocale())
                        .htmlContent(e.getHtmlContent())
                        .sections(bannerObject)
                        .build()).forEach(localization -> bannerObject.getLocalizations().add(localization));
            }

            var savedBanner = sectionsRepository.save(bannerObject);

            var response = getResponseDto(savedBanner);

            return ResponseSuccess(response);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> updateMedia(@Valid final SliderMedia sliderMedia,
                                       @Valid final SliderEnum sliderEnum) {
        try {
            var isSlider = slidersRepository.findById(sliderMedia.getId());
            for (SliderLocalization localization : sliderMedia.getSliderLocalizations()) {
                var isLocal = bannerLocalizationRepository.findById(localization.getId());
                if (isLocal.isEmpty()) {
                    return ResponseNotFound(localization + " not found.");
                }
            }
            if (isSlider.isEmpty()) {
                return ResponseNotFound("Slider not found.");
            }

            var sliderObject = SliderMedia.builder()
                    .id(isSlider.get().getId())
                    .createdOn(isSlider.get().getCreatedOn())
                    .sliderEnum(sliderMedia.getSliderEnum())
                    .sliderMedia(sliderMedia.getSliderMedia())
                    .link(sliderMedia.getLink())
                    .sliderLocalizations(new ArrayList<>())
                    .build();

            var localizations = sliderMedia.getSliderLocalizations();
            if (localizations != null && !localizations.isEmpty()) {
                localizations.stream().map(e -> SliderLocalization.builder()
                        .id(e.getId())
                        .locale(e.getLocale())
                        .htmlContent(e.getHtmlContent())
                        .sliderMedia(sliderObject)
                        .build()).forEach(localization -> sliderObject.getSliderLocalizations().add(localization));
            }
            return saveSliderMedia(sliderEnum, sliderObject);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    private ResponseBase<?> saveSliderMedia(@Valid SliderEnum sliderEnum, SliderMedia sliderObject) {
        var savedBanner = slidersRepository.save(sliderObject);

        Map<Object, Object> mapResponse = new HashMap<>();
        mapResponse.put("slide", savedBanner.getSliderMedia());
        mapResponse.put("locale", savedBanner.getSliderLocalizations());
        mapResponse.put("sliderEnum", savedBanner.getSliderEnum());

        var response = SliderDto.builder()
                .id(savedBanner.getId())
                .data(mapResponse)
                .createdOn(savedBanner.getCreatedOn())
                .link(savedBanner.getLink())
                .build();

        return ResponseSuccess(response);
    }

    /**
     * Deletes a banner based on its ID and page enum.
     *
     * @param id        The ID of the banner to be deleted. Must not be {@code null}.
     * @param pagesEnum The page enum associated with the banner. Must not be {@code null}.
     * @return A response indicating the success or failure of the operation.
     * If successful, the response contains a success message.
     * If unsuccessful, the response contains an error message.
     */
    @Override
    public ResponseBase<?> deleteSections(@Valid final UUID id,
                                          @Valid final PagesEnum pagesEnum) {
        try {
            long bannerCount = sectionsRepository.countByPageEnum(pagesEnum);
            return bannerCountCheckAndDelete(bannerCount, id);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }


    /**
     * Retrieves a list of banners based on the specified page enum.
     *
     * @param pagesEnum The page enum for which banners are retrieved.
     * @return A response containing the list of banners for the specified page.
     */
    @Override
    public ResponseBase<?> sections(@Valid final PagesEnum pagesEnum) {
        try {
            var response = sectionsRepository.findAllByPagesEnum(pagesEnum);
            return ResponseSuccess(response);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> slides(@Valid final SliderEnum pagesEnum) {
        try {
            var response = slidersRepository.findAllBySliderEnum(pagesEnum);
            return ResponseSuccess(response);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    /**
     * Retrieves a banner and its localizations based on the specified language and page enum.
     *
     * @param lang      The language for which localizations are retrieved.
     * @param pagesEnum The page enum associated with the banner.
     * @return A response containing the banner and its localizations.
     */
    @Override
    public ResponseBase<?> sections(@Valid final String lang, @Valid final PagesEnum pagesEnum) {
        try {

            var result = sectionsRepository.findLatestByPageNameAndLocale(pagesEnum, lang);
            if (!result.isPresent()) {
                return ResponseError("No data yet in the " + pagesEnum + " section. ");
            }
            var response = getResponseDto(result.get());
            log.info("response banner {}", response);
            return ResponseSuccess(response);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }


    /**
     * Helper method to check banner count and perform deletion based on specified conditions.
     *
     * @param bannerCount The count of banners based on the specified page enum.
     * @param id          The ID of the banner to be deleted.
     * @return A response indicating the success or failure of the banner deletion.
     * If successful, the response contains a success message.
     * If unsuccessful, the response contains an error message.
     */
    private ResponseBase<?> bannerCountCheckAndDelete(final long bannerCount, final UUID id) {
        return (bannerCount == 1) ?
                ResponseError("There is only 1 banner. Consider creating another before deleting a banner from the home page.") :
                (bannerCount == 0) ?
                        ResponseError("No banners found for the specified page name.") :
                        deleteBannerById(id);
    }

    /**
     * Helper method to delete a banner by its ID.
     *
     * @param id The ID of the banner to be deleted.
     * @return A response indicating the success or failure of the banner deletion.
     * If successful, the response contains a success message.
     * If unsuccessful, the response contains an error message.
     */
    private ResponseBase<?> deleteBannerById(final UUID id) {
        try {
            sectionsRepository.deleteById(id);
            return ResponseSuccess("Successfully deleted.");
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    private SectionsDto getResponseDto(Sections savedBanner) {
        return SectionsDto.builder()
                .id(savedBanner.getId())
                .pagesEnum(savedBanner.getPagesEnum())
                .sliderMedia(savedBanner.getSliderMedia())
                .localizations(savedBanner.getLocalizations())
                .createdOn(savedBanner.getCreatedOn())
                .link(savedBanner.getLink().name())
                .build();
    }
}
