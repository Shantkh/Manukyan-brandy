package com.bl.mb.service;

import com.bl.mb.models.PaginationWithSorting;
import com.bl.mb.models.Products;
import com.bl.mb.models.ResponseBase;
import jakarta.validation.Valid;

import java.util.UUID;

public interface ProductsService {
    ResponseBase<?> product(@Valid final Products products);

    ResponseBase<?> update(@Valid final Products products);

    ResponseBase<?> deleteProduct(@Valid final UUID id);

    ResponseBase<?> getProductById(@Valid final UUID id);

    ResponseBase<?> getProducts();

    ResponseBase<?> getAllPagination(@Valid final PaginationWithSorting paginationWithSorting);

    ResponseBase<?> getOtherWines(@Valid final String acceptLanguage);
}
