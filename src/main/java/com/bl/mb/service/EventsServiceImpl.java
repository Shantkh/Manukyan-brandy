package com.bl.mb.service;

import com.bl.mb.models.*;
import com.bl.mb.repo.EventsRepository;
import com.bl.mb.utils.PagePagination;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;

import static com.bl.mb.utils.ResponseUtil.*;

@Service
@RequiredArgsConstructor
public class EventsServiceImpl implements EventsService {

    private final EventsRepository eventsRepository;
    private final PagePagination pagePagination;

    /**
     * Creates a new event and saves it in the database. Checks for date validity,
     * existing events on the same date, and saves associated localizations.
     *
     * @param events    The Events object containing event details.
     * @param startDate The start date of the event in seconds since epoch.
     * @param endDate   The end date of the event in seconds since epoch.
     * @return A ResponseBase containing the saved event details or an error message.
     */
    @Override
    public ResponseBase<?> events(final Events events,
                                  final Long startDate,
                                  final Long endDate) {
        try {
            Timestamp start = new Timestamp(startDate);
            Timestamp end = new Timestamp(endDate);

            var checkEndEvent = checkEventTimes(end);
            var checkStartEvent = checkEventTimes(start);

            if (!checkEndEvent || !checkStartEvent) {
                return ResponseError("The date is in the past.");
            }

            var isExists = eventsRepository.findByEventStartDate(events.getEventStartDate());
            if (isExists != null) {
                return ResponseError("You have event in the save date.");
            }
            var eventObject = Events.builder()
                    .eventStartDate(start)
                    .eventEndDate(end)
                    .localizations(new ArrayList<>())
                    .sliderMedia(events.getSliderMedia())
                    .isEventVisible(events.isEventVisible())
                    .build();

            var localizations = events.getLocalizations();
            if (localizations != null && !localizations.isEmpty()) {
                localizations.stream().map(e -> EventsLocalization.builder()
                                .locale(e.getLocale())
                                .htmlTitle(e.getHtmlTitle())
                                .htmlLongContent(e.getHtmlLongContent())
                                .htmlShortContent(e.getHtmlShortContent())
                                .events(eventObject)
                                .build())
                        .forEach(localization -> eventObject.getLocalizations().add(localization));
            }

            var savedEvent = eventsRepository.save(eventObject);

            var response = EventsDto.builder()
                    .id(savedEvent.getId())
                    .createdOn(savedEvent.getCreatedOn())
                    .sliderMedia(savedEvent.getSliderMedia())
                    .localizations(savedEvent.getLocalizations())
                    .eventStartDate(savedEvent.getEventStartDate())
                    .eventEndDate(savedEvent.getEventEndDate())
                    .isEventVisible(savedEvent.isEventVisible())
                    .build();

            return ResponseCreated(response);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    /**
     * Retrieves all events from the database using pagination and sorting.
     *
     * @param paginationWithSorting The pagination and sorting parameters.
     * @return A ResponseBase containing a list of events or an error message.
     */
    @Override
    public ResponseBase<?> allEvents(final PaginationWithSorting paginationWithSorting) {
        try {
            PageRequest pageReq = pagePagination.pagePagination(paginationWithSorting);
            var result = eventsRepository.findAll(pageReq).getContent();
            return ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    /**
     * Retrieves all events in a specific language from the database using pagination and sorting.
     *
     * @param lang                  The language code.
     * @param paginationWithSorting The pagination and sorting parameters.
     * @return A ResponseBase containing a list of events or an error message.
     */
    @Override
    public ResponseBase<?> allEvents(final String lang,
                                     final PaginationWithSorting paginationWithSorting) {
        try {
            var pageReq = pagePagination.pagePagination(paginationWithSorting);
            var result = eventsRepository.findLatestByLocale(lang, pageReq);
            return ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    /**
     * Retrieves events within a date range from the database using pagination and sorting.
     *
     * @param paginationWithSorting The pagination and sorting parameters.
     * @param startDate             The start date of the range in seconds since epoch.
     * @param endDate               The end date of the range in seconds since epoch.
     * @return A ResponseBase containing a list of events or an error message.
     */
    @Override
    public ResponseBase<?> allEventsByDate(final PaginationWithSorting paginationWithSorting,
                                           final Long startDate,
                                           final Long endDate) {
        try {
            var start = new Timestamp(startDate * 1000);
            var end = new Timestamp(endDate * 1000);
            var pageReq = pagePagination.pagePagination(paginationWithSorting);
            var result = eventsRepository.findAllByEventStartDateAndEventEndDate(pageReq, start, end);
            return ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }

    }

    /**
     * Retrieves events within a date range and a specific language from the database
     * using pagination and sorting.
     *
     * @param paginationWithSorting The pagination and sorting parameters.
     * @param startDate             The start date of the range in seconds since epoch.
     * @param endDate               The end date of the range in seconds since epoch.
     * @param lang                  The language code.
     * @return A ResponseBase containing a list of events or an error message.
     */
    @Override
    public ResponseBase<?> allEventsByDateAndLocale(final PaginationWithSorting paginationWithSorting,
                                                    final Long startDate,
                                                    final Long endDate,
                                                    final String lang) {
        try {
            var start = new Timestamp(startDate * 1000);
            var end = new Timestamp(endDate * 1000);
            var pageReq = pagePagination.pagePagination(paginationWithSorting);
            var result = eventsRepository.findAllByEventStartDateAndEventEndDateAndLocalizations(pageReq, start, end, lang);
            return ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    /**
     * Retrieves events occurring in the next month from a target date, in a specific language,
     * using pagination and sorting.
     *
     * @param lang                  The language code.
     * @param paginationWithSorting The pagination and sorting parameters.
     * @param month                 The target month.
     * @param year                  The target year.
     * @return A ResponseBase containing a list of events or an error message.
     * @throws Exception if an error occurs during execution.
     */
    @Override
    public ResponseBase<?> nextEvents(final String lang,
                                      final PaginationWithSorting paginationWithSorting,
                                      final Long month,
                                      final Long year) throws Exception {
        try {
            Timestamp targetDate = getTimeStamp(month, year);
            var nextNearestEvent = getNextNearestEventFuture(targetDate);

            if (nextNearestEvent.isPresent()) {
                LocalDateTime localDateTime = nextNearestEvent.get().toLocalDateTime();
                var nextEventYear = localDateTime.getYear();
                var nextEventMonth = localDateTime.getMonthValue();
                var pageReq = pagePagination.pagePagination(paginationWithSorting);
                var result = eventsRepository.findEventsInNextMonth(lang, nextEventYear, nextEventMonth, pageReq);
                return ResponseSuccess(result);
            } else {
                return ResponseError("No next nearest event found");
            }
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    /**
     * Retrieves events occurring in the previous month from a target date, in a specific language,
     * using pagination and sorting.
     *
     * @param lang                  The language code.
     * @param paginationWithSorting The pagination and sorting parameters.
     * @param month                 The target month.
     * @param year                  The target year.
     * @return A ResponseBase containing a list of events or an error message.
     * @throws Exception if an error occurs during execution.
     */
    @Override
    public ResponseBase<?> preEvents(final String lang,
                                     final PaginationWithSorting paginationWithSorting,
                                     final Long month,
                                     final Long year) throws Exception {
        try {
            Timestamp targetDate = getTimeStamp(month, year);
            var nextNearestEvent = getNextNearestEventPast(targetDate);

            if (nextNearestEvent.isPresent()) {
                LocalDateTime localDateTime = nextNearestEvent.get().toLocalDateTime();
                var nextEventYear = localDateTime.getYear();
                var nextEventMonth = localDateTime.getMonthValue();
                var pageReq = pagePagination.pagePagination(paginationWithSorting);
                var result = eventsRepository.findEventsInPreMonth(lang, nextEventYear, nextEventMonth, pageReq);
                return ResponseSuccess(result);
            } else {
                return ResponseError("No next nearest event found");
            }
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    /**
     * Retrieves events occurring in the current month from a target date, in a specific language,
     * using pagination and sorting.
     *
     * @param lang                  The language code.
     * @param paginationWithSorting The pagination and sorting parameters.
     * @param month                 The target month.
     * @param year                  The target year.
     * @return A ResponseBase containing a list of events or an error message.
     * @throws Exception if an error occurs during execution.
     */
    @Override
    public ResponseBase<?> thisMonthEvents(final String lang,
                                           final PaginationWithSorting paginationWithSorting,
                                           final Long month,
                                           final Long year) throws Exception {
        try {
            var pageReq = pagePagination.pagePagination(paginationWithSorting);
            var result = eventsRepository.findEventsInThisMonth(lang, Math.toIntExact(year), Math.toIntExact(month), pageReq);
            return ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    /**
     * Retrieves event details by ID and language.
     *
     * @param lang The language code.
     * @param id   The ID of the event.
     * @return A ResponseBase containing the event details or an error message.
     */
    @Override
    public ResponseBase<?> eventById(final String lang,
                                     final UUID id) {
        var response = eventsRepository.findByIdAndLocalizations(id, lang);
        if (response.isEmpty()) {
            ResponseError("Event not found.");
        }
        return ResponseSuccess(response);
    }


    @Override
    public ResponseBase<?> oneEvent(final UUID id) {
        var event = eventsRepository.findById(id);

        if (event.isEmpty()) {
            ResponseError("Event not found.");
        }
        var result = event.get();
        return ResponseSuccess(result);
    }

    /**
     * Retrieves and formats events grouped by month for a specified year.
     *
     * @param lang The language code for localization.
     * @param year The year for which events are to be retrieved.
     * @return A {@link ResponseBase} containing the formatted month-wise event data.
     */
    @Override
    public ResponseBase<?> eventEventsByYear(final String lang,
                                             final Long year) {
        try {
            var existingData = eventsRepository.findEventsInThisYearGroupByMonth(lang, Math.toIntExact(year));
            List<MonthEventsDTO> modifiedData = new ArrayList<>();

            Arrays.stream(Month.values()).forEach(month -> {
                String monthName = getMonthName(month);
                boolean hasEvents = false;
                int eventCount = 0;

                Optional<MonthEventsDTO> existingMonthData = existingData.stream()
                        .filter(data -> data.getMonth().trim().equalsIgnoreCase(monthName.toLowerCase()))
                        .findFirst();

                if (existingMonthData.isPresent()) {
                    hasEvents = existingMonthData.get().isHasEvents();
                    eventCount = existingMonthData.get().getEventCount();
                }

                MonthEventsDTO modifiedMonthData = new MonthEventsDTO(monthName, hasEvents, eventCount);
                modifiedData.add(modifiedMonthData);

                if ("hy".equalsIgnoreCase(lang)) {
                    modifiedData.forEach(monthData -> {
                        String updatedMonthName = getArmenianMonthName(monthData.getMonth().toUpperCase());
                        monthData.setMonth(updatedMonthName);
                    });
                }
            });

            return ResponseSuccess(modifiedData);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    /**
     * Deletes an event with the specified ID.
     *
     * @param id The unique identifier of the event to be deleted.
     * @return A {@link ResponseBase} indicating the success or failure of the deletion.
     * If the event is not found, returns an error response with a corresponding message.
     * If the deletion is successful, returns a success response with a confirmation message.
     */
    @Override
    public ResponseBase<?> deleteEvent(UUID id) {
        try {
            var isExists = eventsRepository.findById(id);
            if (isExists.isEmpty()) {
                return ResponseError("Event not found.");
            }
            eventsRepository.deleteById(id);
            return ResponseSuccess("Event Deleted successfully");
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    /**
     * Updates an existing event with the provided details.
     *
     * @param events The {@link Events} object containing the updated event details.
     * @return A {@link ResponseBase} containing the result of the update operation.
     *         If the update is successful, it returns a success response with the updated event details.
     *         If the event is not found, it returns an error response with a message indicating that the event was not found.
     *         If an exception occurs during the update process, it returns an error response with the exception message.
     */
    @Override
    public ResponseBase<?> updateEvent(Events events) {
        try {
            var isExists = eventsRepository.findById(events.getId());
            if (isExists.isEmpty()) {
                return ResponseError("Event not found.");
            }

            var eventObject = Events.builder()
                    .id(isExists.get().getId())
                    .createdOn(isExists.get().getCreatedOn())
                    .eventStartDate(events.getEventStartDate())
                    .eventEndDate(events.getEventEndDate())
                    .localizations(new ArrayList<>())  // Create a new list for localizations
                    .sliderMedia(events.getSliderMedia())
                    .isEventVisible(events.isEventVisible())
                    .build();

            var localizations = events.getLocalizations();

            if (localizations != null && !localizations.isEmpty()) {
                var modifiedLocalizations = localizations.stream()
                        .map(e -> EventsLocalization.builder()
                                .id(e.getId())
                                .locale(e.getLocale())
                                .htmlTitle(e.getHtmlTitle())
                                .htmlLongContent(e.getHtmlLongContent())
                                .htmlShortContent(e.getHtmlShortContent())
                                .events(eventObject)
                                .build())
                        .toList();

                eventObject.getLocalizations().addAll(modifiedLocalizations);
            }

            var savedEvent = eventsRepository.save(eventObject);

            var response = EventsDto.builder()
                    .id(savedEvent.getId())
                    .createdOn(savedEvent.getCreatedOn())
                    .sliderMedia(savedEvent.getSliderMedia())
                    .localizations(savedEvent.getLocalizations())
                    .eventStartDate(savedEvent.getEventStartDate())
                    .eventEndDate(savedEvent.getEventEndDate())
                    .isEventVisible(savedEvent.isEventVisible())
                    .build();

            return ResponseSuccess(response);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }


    /**
     * Retrieves the English name of a month.
     *
     * @param month The {@link Month} enum representing the month.
     * @return The English name of the month.
     */
    private String getMonthName(Month month) {
        return month.toString();
    }

    /**
     * Retrieves the Armenian name of a month based on its English name.
     *
     * @param englishMonthName The English name of the month.
     * @return The Armenian name of the month.
     */
    private String getArmenianMonthName(String englishMonthName) {
        return switch (englishMonthName) {
            case "JANUARY" -> "Հունվար";
            case "FEBRUARY" -> "Փետրվար";
            case "MARCH" -> "Մարտ";
            case "APRIL" -> "Ապրիլ";
            case "MAY" -> "Մայիս";
            case "JUNE" -> "Հունիս";
            case "JULY" -> "Հուլիս";
            case "AUGUST" -> "Օգոստոս";
            case "SEPTEMBER" -> "Սեպտեմբեր";
            case "OCTOBER" -> "Հոկտեմբեր";
            case "NOVEMBER" -> "Նոյեմբեր";
            case "DECEMBER" -> "Դեկտեմբեր";
            default -> englishMonthName; // Return as is if not found in mapping
        };
    }

    /**
     * Finds the next nearest future event from a target date.
     *
     * @param targetDate The target date.
     * @return An Optional containing the event start date or empty if not found.
     */
    public Optional<Timestamp> getNextNearestEventFuture(final Timestamp targetDate) {
        return eventsRepository.findNextNearestEventFuture(targetDate)
                .stream()
                .limit(1)
                .findFirst()
                .map(Events::getEventStartDate);
    }

    /**
     * Finds the next nearest past event from a target date.
     *
     * @param targetDate The target date.
     * @return An Optional containing the event start date or empty if not found.
     */
    public Optional<Timestamp> getNextNearestEventPast(final Timestamp targetDate) {
        return eventsRepository.findPreNearestEventPast(targetDate)
                .stream()
                .sorted(Comparator.comparing(Events::getEventStartDate).reversed())
                .limit(1)
                .findFirst()
                .map(Events::getEventStartDate);
    }


    /**
     * Converts month and year to a Timestamp representing the start of the month.
     *
     * @param month The month.
     * @param year  The year.
     * @return A Timestamp representing the start of the specified month.
     */
    private Timestamp getTimeStamp(final Long month,
                                   final Long year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Math.toIntExact(year));
        calendar.set(Calendar.MONTH, Math.toIntExact(month - 1));
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return new Timestamp(calendar.getTimeInMillis());
    }

    /**
     * Checks if the event date is in the future.
     *
     * @param eventDate The event date.
     * @return true if the event date is in the future, false otherwise.
     */
    private boolean checkEventTimes(final Timestamp eventDate) {
        var eventStartTimestampInSeconds = eventDate.getTime() / 1000;
        var instance = Instant.ofEpochSecond(eventStartTimestampInSeconds);
        return instance.isAfter(Instant.now());
    }

}
