package com.bl.mb.service;
import com.bl.mb.models.*;
import com.bl.mb.repo.SlidersRepository;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import static com.bl.mb.utils.ResponseUtil.ResponseError;
import static com.bl.mb.utils.ResponseUtil.ResponseSuccess;
@Service
@RequiredArgsConstructor
public class SliderServiceImpl implements SliderService{
    private final SlidersRepository slidersRepository;
    @Override
    public ResponseBase<?> slides(@Valid final String lang) {
        try {
            var result = slidersRepository.findSlidesPageNameAndLocale(lang);
            return ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }
}