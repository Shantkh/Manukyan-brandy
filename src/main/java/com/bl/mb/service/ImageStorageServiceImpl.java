package com.bl.mb.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.bl.mb.models.Image;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.repo.ImageRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

import static com.bl.mb.utils.ResponseUtil.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class ImageStorageServiceImpl implements ImageStorageService {

    @Autowired
    private final ImageRepository imageRepo;

    @Autowired
    private final AmazonS3 s3Client;


    //	@Value("${do.space.bucket}")
    private String doSpaceBucket = "manukyan";

    String FOLDER = "images/";
    String IMAGE_FOLDER = "https://manukyan.sfo3.digitaloceanspaces.com/";

    @Override
    public ResponseBase<?> saveFile(MultipartFile[] multipartFile) throws IOException {
        List<String> notSavedFiles = new ArrayList<>();
        try {
            for (MultipartFile file : multipartFile) {
                var foundImage = imageRepo.findImageByName(file.getName());
                if (foundImage.isPresent()) {
                    imageRepo.deleteById(foundImage.get().getId());
                }
                    String extension = FilenameUtils.getExtension(file.getOriginalFilename());
                    String imgName = FilenameUtils.removeExtension(file.getOriginalFilename());
                    String key = FOLDER + imgName + "." + extension;
                    saveImageToServer(file, key);
                    Image image = Image.builder()
                            .name(imgName)
                            .createdtime(new Timestamp(new Date().getTime()))
                            .ext(extension)
                            .link(IMAGE_FOLDER.concat(FOLDER).concat(imgName) + "." + extension)
                            .build();

                    imageRepo.save(image);
                }

            if (notSavedFiles.size() > 0) {
                return ResponseCreated("All saved except " + notSavedFiles + " already exists");
            } else {
                return ResponseCreated("All uploaded");
            }

        } catch (Exception e) {
            log.info("Exception {} ", e.getMessage());
            return checkResponse(e);
        }

    }

    private ResponseBase<?> checkResponse(Exception e) {
        log.info("checkResponse ");
        return e.getMessage().contains("Could not store the file") ? ResponseError("File already exists") : ResponseError(e.getMessage());
    }

    @Override
    public ResponseBase<?> deleteFile(String fileName) throws Exception {
        var imageName = fileName.substring(fileName.lastIndexOf("/") + 1, fileName.lastIndexOf("."));
        Optional<Image> data = imageRepo.findImageByName(imageName);
        if (data.isPresent()) {
            Optional<Image> imageOpt = imageRepo.findById(data.get().getId());
            if (imageOpt.get() != null) {
                Image image = imageOpt.get();
                String key = FOLDER + image.getName() + "." + image.getExt();
                s3Client.deleteObject(new DeleteObjectRequest(doSpaceBucket, key));
                imageRepo.delete(image);
            }
        } else {
            return ResponseError("File not found");
        }
        return ResponseSuccess("File deleted successfully");
    }

    private void saveImageToServer(MultipartFile multipartFile, String key) throws IOException {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(multipartFile.getInputStream().available());
        if (multipartFile.getContentType() != null && !"".equals(multipartFile.getContentType())) {
            metadata.setContentType(multipartFile.getContentType());
        }
        var putObjectResult = s3Client.putObject(new PutObjectRequest(doSpaceBucket, key, multipartFile.getInputStream(), metadata)
                .withCannedAcl(CannedAccessControlList.PublicRead));
    }

    @Override
    public ResponseBase<?> getImage() {
        var all = imageRepo.findAll();
        log.info("all files {}", all);
        Map<String, List<String>> fileMap = new HashMap<>();

        for (Image e : all) {
            String fileExtension = getFileExtension(e.getName()+"."+e.getExt());

            if (isImage(fileExtension)) {
                fileMap.computeIfAbsent("images", k -> new ArrayList<>()).add(e.getLink());
            } else if (isDocument(fileExtension)) {
                fileMap.computeIfAbsent("documents", k -> new ArrayList<>()).add(e.getLink());
            } else if (isVideo(fileExtension)) {
                fileMap.computeIfAbsent("videos", k -> new ArrayList<>()).add(e.getLink());
            }
        }
        return ResponseSuccess(fileMap);
    }

    /**
     * Retrieves the file extension from a given filename.
     *
     * @param fileName The name of the file.
     * @return The file extension.
     */
    private String getFileExtension(final String fileName) {
        int dotIndex = fileName.lastIndexOf(".");
        if (dotIndex > 0 && dotIndex < fileName.length() - 1) {
            return fileName.substring(dotIndex + 1).toLowerCase();
        }
        return "";
    }

    /**
     * Checks if the file extension corresponds to an image format.
     *
     * @param fileExtension The file extension to be checked.
     * @return True if the file is an image, false otherwise.
     */
    private boolean isImage(final String fileExtension) {
        List<String> imageExtensions = Arrays.asList("jpg", "jpeg", "png", "gif", "bmp", "svg");
        return imageExtensions.contains(fileExtension);
    }

    /**
     * Checks if the file extension corresponds to a document format.
     *
     * @param fileExtension The file extension to be checked.
     * @return True if the file is a document, false otherwise.
     */
    private boolean isDocument(final String fileExtension) {
        List<String> documentExtensions = Arrays.asList("doc", "docx", "pdf", "txt", "xls", "xlsx");
        return documentExtensions.contains(fileExtension);
    }

    /**
     * Checks if the file extension corresponds to a video format.
     *
     * @param fileExtension The file extension to be checked.
     * @return True if the file is a video, false otherwise.
     */
    private boolean isVideo(final String fileExtension) {
        List<String> videoExtensions = Arrays.asList("avi", "mov", "mkv", "mp4", "wmv", "3gp", "flv");
        return videoExtensions.contains(fileExtension);
    }
}