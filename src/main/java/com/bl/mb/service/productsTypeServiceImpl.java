package com.bl.mb.service;

import com.bl.mb.models.ResponseBase;
import com.bl.mb.utils.ResponseUtil;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class productsTypeServiceImpl implements productsTypeService {

    /**
     * Retrieves a map containing product types categorized by locale.
     *
     * @return A {@link ResponseBase} containing a map with product types based on different locales.
     */
    @Override
    public ResponseBase<?> getTypes() {
        Map<String, Map<String, String[]>> map = new HashMap<>();
        Map<String, String[]> locale = new HashMap<>();
        String[] engList = returnList("en");
        String[] hyList = returnList("hy");
        locale.put("en", engList);
        locale.put("hy", hyList);
        map.put("types", locale);
        return ResponseUtil.ResponseSuccess(map);
    }

    /**
     * Returns a list of product types based on the provided locale.
     *
     * @param en The locale for which the product types list is requested.
     * @return A list of product types in the specified locale.
     */
    private String[] returnList(String en) {
        return "en".equals(en) ? new String[]{"Red", "White", "Rosé", "Reserved"} :  new String[]{"Կարմիր", "Սպիտակ","Ռոզե","Վերապահված"};
    }
}
