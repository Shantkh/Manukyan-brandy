package com.bl.mb.service;

import com.bl.mb.models.ResponseBase;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import jakarta.servlet.http.HttpServletRequest;

import java.io.IOException;

public interface StaticService {
    void saveStatics(HttpServletRequest request, String page) throws IOException, GeoIp2Exception;

    ResponseBase<?> usersStatics();

}
