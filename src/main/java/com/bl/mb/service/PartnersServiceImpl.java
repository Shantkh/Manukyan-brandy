package com.bl.mb.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.bl.mb.models.Image;
import com.bl.mb.models.Partners;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.repo.ImageRepository;
import com.bl.mb.repo.PartnersRepository;
import com.bl.mb.utils.DateUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import static com.bl.mb.utils.ResponseUtil.*;

/**
 * Service implementation for managing partner entities.
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class PartnersServiceImpl implements PartnersService {

    private final PartnersRepository partnersRepository;

    /**
     * Retrieves all partner entities.
     *
     * @return ResponseEntity containing the response status and data.
     */
    @Override
    public ResponseBase<?> getPartners() {
        try {
            Iterable<Partners> result = partnersRepository.findAll();
            return ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> savePartner(Partners partners) {
        try {
            Partners PObject = Partners.builder()
                    .createdOn(DateUtils.newTimeStamp())
                    .partnerNameEn(partners.getPartnerNameEn())
                    .partnerNameHy(partners.getPartnerNameHy())
                    .logo(partners.getLogo())
                    .build();
            Partners result = partnersRepository.save(PObject);
            return ResponseSuccess(result);

        } catch (Exception e) {
            return ResponseError(e.getMessage().contains("Could not store the file") ? "File already exists" : e.getMessage());
        }
    }
    /**
     * Deletes a partner entity by its ID.
     *
     * @param id The ID of the partner entity to delete.
     * @return ResponseEntity containing the response status and data.
     */
    @Override
    public ResponseBase<?> deletePartner(Long id) {
        try {
            Optional<Partners> partner = partnersRepository.findById(id);
            if (!partner.isPresent()) {
                return ResponseNotFound("Partner not found");
            }
            partnersRepository.delete(partner.get());
            return ResponseSuccess("Deleted successfully.");
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }


}
