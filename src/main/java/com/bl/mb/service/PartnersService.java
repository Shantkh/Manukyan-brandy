package com.bl.mb.service;

import com.bl.mb.models.Partners;
import com.bl.mb.models.ResponseBase;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.Part;
import jakarta.validation.Valid;
import org.springframework.web.multipart.MultipartFile;

public interface PartnersService {

    ResponseBase<?> getPartners();

    ResponseBase<?> deletePartner(final Long id);

    ResponseBase<?> savePartner(Partners partners);
}
