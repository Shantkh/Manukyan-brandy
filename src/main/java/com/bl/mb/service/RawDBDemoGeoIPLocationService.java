package com.bl.mb.service;

import com.bl.mb.models.GeoIP;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CountryResponse;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;

public class RawDBDemoGeoIPLocationService {
    String dbLocation = "GeoLite2-Country.mmdb";
    private final DatabaseReader dbReader;

    /**
     * Constructs a new RawDBDemoGeoIPLocationService and initializes the GeoIP2 database reader.
     *
     * @throws IOException if there is an issue reading the GeoIP2 database file.
     */
    public RawDBDemoGeoIPLocationService() throws IOException {
        InputStream database = new ClassPathResource(dbLocation).getInputStream();
        dbReader = new DatabaseReader.Builder(database).build();
    }

    /**
     * Retrieves the geographical location information for the given IP address.
     *
     * @param ip The IP address for which to retrieve location information.
     * @return A GeoIP object containing location information. If the information cannot be
     *         retrieved, a GeoIP object with default values is returned.
     */
    public GeoIP getLocation(String ip){
        try {
            InetAddress ipAddress = InetAddress.getByName(ip);
            CountryResponse response = dbReader.country(ipAddress);

            String cityName = response.getCountry().getName();
            return new GeoIP(ip, cityName, "", "");
        }catch (Exception e){
            return new GeoIP();
        }
    }
}