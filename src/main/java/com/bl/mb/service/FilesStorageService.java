package com.bl.mb.service;

import java.nio.file.Path;
import java.util.stream.Stream;

import com.bl.mb.models.ResponseBase;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FilesStorageService {
   void init();
   void save(final MultipartFile file);
   ResponseBase<?> load(final String filename);

   Stream<Path> loadAll();

   ResponseBase<?> uploadFiles(final MultipartFile[] files);

   ResponseBase<?> getListFiles();

   ResponseBase<?> deleteFile(final String filename);
}