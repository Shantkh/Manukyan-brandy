package com.bl.mb.service;

import com.bl.mb.models.*;
import com.bl.mb.repo.BrandNameRepository;
import com.bl.mb.repo.BrandRepository;
import com.bl.mb.utils.ResponseUtil;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BrandServiceImpl implements BrandService {

    private final BrandRepository brandRepository;
    private final BrandNameRepository brandNameRepository;

    @Override
    public ResponseBase<?> create(@Valid final BrandRequest brandRequest) {
        try {
            if(brandRequest.getBrandSection() == null || brandRequest.getBrandNameId() == null || brandRequest.getEnglishContent() == null ||brandRequest.getArmenianContent() == null) {
                return ResponseUtil.ResponseError("Please fill the filed");
            }
            var brandExists = brandRepository.findBrandByBrandSectionAndBrandName(brandRequest.getBrandSection(), brandRequest.getBrandNameId());
            brandExists.ifPresent(brand -> brandRepository.deleteById(brand.getId()));
            var brand = Brand.builder()
                    .images(brandRequest.getImages())
                    .brandName(brandRequest.getBrandNameId())
                    .brandSection(brandRequest.getBrandSection())
                    .armenianContent(brandRequest.getArmenianContent())
                    .englishContent(brandRequest.getEnglishContent())
                    .build();
            var result = brandRepository.save(brand);
            return ResponseUtil.ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> getBrands() {
        try {
            var response = brandRepository.findAll();
            var result = response.stream().map(e -> BrandResponse.builder()
                            .brandNameEnglish(e.getBrandName().getBrandNameEnglish())
                            .brandNameArmenian(e.getBrandName().getBrandNameArmenian())
                            .build())
                    .toList();
            return ResponseUtil.ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> getBrand(@Valid final String lang,
                                    @Valid final String brand) {
        try {
            var brandName = brandNameRepository.findAll();
            var id = brandName.stream()
                    .filter(e -> e.getBrandNameEnglish().contains(brand))
                    .map(BrandName::getId)
                    .findFirst()
                    .orElse(null);

            if(brandName.isEmpty()) {
                return ResponseUtil.ResponseError("The brand does not exist");
            }
            var response = brandRepository.findAllByBrandNameId(id);
            return ResponseUtil.ResponseSuccess(response);
        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> createName(@Valid final BrandName brand) {
        try {
            if (brand.getBrandNameEnglish() == null) {
                return ResponseUtil.ResponseError("PLease fill the Brand name.");
            }
            var isExists = brandNameRepository.findByBrandNameEnglish(brand.getBrandNameEnglish());
            if (isExists.isPresent()) {
                return ResponseUtil.ResponseError(" The brand already exists");
            }
            var result = brandNameRepository.save(brand);
            return ResponseUtil.ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> getBrandsName() {
        try {
            var result = brandNameRepository.findAll();
            return ResponseUtil.ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> deleteBrandName(@Valid final UUID id) {
        try {
            brandNameRepository.deleteById(id);
            return ResponseUtil.ResponseSuccess("Deleted successfully.");
        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }
}
