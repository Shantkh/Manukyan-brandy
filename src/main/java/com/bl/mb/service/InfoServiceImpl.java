package com.bl.mb.service;

import com.bl.mb.models.InfoEntity;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.repo.InfoRepository;
import com.bl.mb.utils.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class InfoServiceImpl implements InfoService{

    private final InfoRepository infoRepository;

    @Override
    public ResponseBase<?> create(InfoEntity infoEntity) {
        try{
            var result = infoRepository.save(infoEntity);
            return ResponseUtil.ResponseSuccess(result);
        }catch (Exception e){
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> info(String showOn) {
        try{
            var result = infoRepository.findInfoEntitiesByShowOn(showOn);
            return ResponseUtil.ResponseSuccess(result);
        }catch (Exception e){
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }
}
