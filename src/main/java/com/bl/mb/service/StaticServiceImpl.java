package com.bl.mb.service;

import com.bl.mb.models.GeoIP;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.models.ResponseStatics;
import com.bl.mb.models.Statics;
import com.bl.mb.repo.StaticsRepository;
import com.bl.mb.utils.DateUtils;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static com.bl.mb.utils.ResponseUtil.ResponseSuccess;

@Service
@Slf4j
public class StaticServiceImpl implements StaticService {
    private final RawDBDemoGeoIPLocationService locationService;
    private final StaticsRepository staticsRepository;

    public StaticServiceImpl(StaticsRepository staticsRepository) throws IOException {
        this.staticsRepository = staticsRepository;
        locationService = new RawDBDemoGeoIPLocationService();
    }

    /**
     * Saves statistics data based on the incoming request.
     *
     * @param request HTTP request containing client information.
     * @param page    Page associated with the request.
     */
    @Override
    public void saveStatics(HttpServletRequest request, String page){
        var remoteAddress = request.getRemoteAddr();
        log.info("remote Address {} ", remoteAddress);
        var location = new GeoIP();
        if (!"0:0:0:0:0:0:0:1".equals(remoteAddress) || locationService.getLocation(remoteAddress) != null) {
            location = locationService.getLocation(remoteAddress);
        } else {
            location = new GeoIP(remoteAddress, "Yerevan", "", "");
        }

        var statics = Statics.builder()
                .createdOn(DateUtils.newTimeStamp())
                .country(location.getCity() == null ? "Armenia" : location.getCity())
                .page(page)
                .ip(location.getIpAddress() == null ? remoteAddress : location.getIpAddress())
                .build();
        var result = staticsRepository.save(statics);
        log.info("database saved with id {} ", result.getId());
        log.info("database saved with ip {} ", result.getIp());
        log.info("database saved with city {} ", result.getCountry());
        log.info("database saved with page {} ", result.getPage());
        log.info("database saved with createdOn {} ", result.getCreatedOn());

    }

    private void checkDateExpiredData() {
        LocalDateTime thirtyTwoDaysAgo = LocalDateTime.now().minusDays(32);
        Timestamp thirtyTwoDaysAgoTimestamp = Timestamp.valueOf(thirtyTwoDaysAgo);
        var datas = staticsRepository.findAllByCreatedOnBefore(thirtyTwoDaysAgoTimestamp);
        staticsRepository.deleteAll(datas);
    }

    /**
     * Retrieves user statistics including counts, top countries, and top pages.
     *
     * @return Response containing user statistics.
     */
    @Override
    public ResponseBase<?> usersStatics() {
        checkDateExpiredData();
        LocalDateTime thirtyDaysAgo = LocalDateTime.now().minusDays(30);
        LocalDateTime fifteenDaysAgo = LocalDateTime.now().minusDays(15);
        LocalDateTime sevenDaysAgo = LocalDateTime.now().minusDays(7);
        LocalDateTime today = LocalDateTime.now().with(LocalTime.MIN);
        var statisticsResult = staticsRepository.getVisitorCounts(thirtyDaysAgo, fifteenDaysAgo, sevenDaysAgo, today);
        var countries = staticsRepository.getTopCountries(thirtyDaysAgo);
        var pages = staticsRepository.getTopPages(thirtyDaysAgo);
        var result = ResponseStatics.builder()
                .statisticsResult(statisticsResult)
                .countries(countries)
                .pages(pages)
                .build();

        return ResponseSuccess(result);
    }
}
