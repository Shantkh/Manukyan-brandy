package com.bl.mb.service;

import com.bl.mb.models.EmailDetails;
import com.bl.mb.models.EmailForm;
import com.bl.mb.models.ResponseBase;
import jakarta.validation.Valid;

public interface EmailService {
    ResponseBase<?> sendEmail(@Valid final EmailDetails formData);
}
