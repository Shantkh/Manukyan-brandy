package com.bl.mb.service;

import com.bl.mb.models.Brand;
import com.bl.mb.models.BrandName;
import com.bl.mb.models.BrandRequest;
import com.bl.mb.models.ResponseBase;
import jakarta.validation.Valid;

import java.util.UUID;

public interface BrandService {
    ResponseBase<?> create(@Valid final BrandRequest brand);

    ResponseBase<?> getBrands();

    ResponseBase<?> getBrand(@Valid final String lang, final String brand);

    ResponseBase<?> createName(@Valid final BrandName brand);

    ResponseBase<?> getBrandsName();

    ResponseBase<?> deleteBrandName(@Valid final UUID id);
}
