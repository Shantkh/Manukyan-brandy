package com.bl.mb.service;

import com.bl.mb.models.BottlingRequest;
import com.bl.mb.models.ResponseBase;
import jakarta.validation.Valid;

public interface BottlingService {
    ResponseBase<?> create(@Valid final BottlingRequest bottlingRequest);

    ResponseBase<?> getBottling();
}
