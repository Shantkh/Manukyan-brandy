package com.bl.mb.service;

import com.bl.mb.constances.PagesEnum;
import com.bl.mb.constances.SliderEnum;
import com.bl.mb.models.Sections;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.models.SliderMedia;
import jakarta.validation.Valid;

import java.util.UUID;


public interface SectionsService {
    ResponseBase<?> saveSections(@Valid final Sections sections,
                                 @Valid final PagesEnum pagesEnum);

    ResponseBase<?> updateSections(@Valid final Sections sections,
                                   @Valid final PagesEnum pagesEnum);

    ResponseBase<?> deleteSections(@Valid final UUID id,
                                   @Valid final PagesEnum pagesEnum);

    ResponseBase<?> sections(@Valid final PagesEnum pagesEnum);

    ResponseBase<?> sections(@Valid final String lang,
                             @Valid final PagesEnum pagesEnum);

    ResponseBase<?> saveSlideSections(final @Valid SliderMedia sections,
                                      final @Valid SliderEnum pagesEnum);

    ResponseBase<?> updateMedia(final @Valid SliderMedia sliderMedia,
                                final @Valid SliderEnum sliderEnum);

    ResponseBase<?> slides(final @Valid SliderEnum sliderEnum);
}
