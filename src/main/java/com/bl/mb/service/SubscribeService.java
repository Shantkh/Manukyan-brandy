package com.bl.mb.service;

import com.bl.mb.models.PaginationWithSorting;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.models.Subscribers;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.UUID;

public interface SubscribeService {
    ResponseBase<?> subscriber(String lang, Subscribers subscribers);

    ResponseBase<?> getSubscriber();

    ResponseBase<?> delete(UUID[] ids);

    ResponseBase<?> download(HttpServletResponse res) throws IOException;
}
