package com.bl.mb.service;

import com.bl.mb.models.Image;
import com.bl.mb.models.ResponseBase;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface ImageStorageService {

	ResponseBase<?> saveFile(MultipartFile[] multipartFile) throws IOException;

	ResponseBase<?> deleteFile(String fileName) throws Exception;

	ResponseBase<?> getImage();
}
