package com.bl.mb.service;

import com.bl.mb.models.FileStorage;
import com.bl.mb.models.ResponseBase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.bl.mb.utils.ResponseUtil.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class FilesStorageServiceImpl implements FilesStorageService {

    private final Path root;
    private final List<Path> paths;

    @Autowired
    public FilesStorageServiceImpl(
            @Value("${spring.servlet.multipart.location}") String path, List<Path> paths) {
//        this.root = Paths.get("/var/lib/public/images");
        this.root = Paths.get(path);
        this.paths = paths;
        log.info("Upload path in container: " + this.root);
        log.info("Upload path in container: " + this.paths);
    }

    /**
     * Initializes the file storage service by creating the root directory if it does not exist.
     *
     * @throws RuntimeException if the initialization fails.
     */
    @Override
    public void init() {
        try {
            if (!Files.exists(root)) {
                log.info("root {} ", root);
                Files.createDirectory(root);
            }
            Files.exists(root);
        } catch (IOException e) {
            log.info("Could not initialize folder for upload!");
            throw new RuntimeException("Could not initialize folder for upload!");
        }
    }

    /**
     * Saves a file to the storage location.
     *
     * @param file The file to be saved.
     * @throws RuntimeException if storing the file fails.
     */
    @Override
    public void save(final MultipartFile file) {

        try {
            Files.copy(file.getInputStream(), this.root.resolve(Objects.requireNonNull(file.getOriginalFilename())));
            log.info("file.getInputStream(), this.root.resolve(Objects.requireNonNull(file.getOriginalFilename()))");
            paths.add(Path.of(String.valueOf(this.root.resolve(Objects.requireNonNull(file.getOriginalFilename())))));
            log.info("paths.add(Path.of(String.valueOf(this.root.resolve(Objects.requireNonNull(file.getOriginalFilename())))));");

        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
    }

    /**
     * Loads a file resource by its filename.
     *
     * @param filename The name of the file to be loaded.
     * @return Resource representing the file.
     * @throws RuntimeException if loading the file fails.
     */
    @Override
    public ResponseBase<?> load(final String filename) {
        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            log.info("resource {}",resource);
            log.info("file {}",file);
            if (resource.exists() || resource.isReadable()) {
                log.info("ResponseSuccess(resource)");
                return ResponseSuccess(resource);
            } else {
                log.info("ResponseError(\"Could not read the file!\")");
                return ResponseError("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            ResponseError("\"Error: \" + e.getMessage()");
            return ResponseError("Error: " + e.getMessage());
        }
    }

    /**
     * Retrieves a stream of all files in the storage location.
     *
     * @return Stream of file paths.
     * @throws RuntimeException if loading the files fails.
     */
    @Override
    public Stream<Path> loadAll() {
        try {
            log.info("loadAll()");
            return Files.walk(this.root, 1).filter(path -> !path.equals(this.root)).map(this.root::relativize);
        } catch (IOException e) {
            log.info("Could not load the files!");
            throw new RuntimeException("Could not load the files!");
        }
    }

    /**
     * Uploads multiple files to the storage location.
     *
     * @param files Array of MultipartFile representing the files to be uploaded.
     * @return ResponseBase containing the response for the file upload request.
     * @throws RuntimeException if uploading the files fails.
     */
    @Override
    public ResponseBase<?> uploadFiles(final MultipartFile[] files) {
        init();
        log.info("init()");
        try {
            List<String> fileNames = new ArrayList<>();

            Arrays.stream(files).forEach(file -> {
                log.info("file name {} ", file);
                save(file);
                log.info("save(file)");
                fileNames.add(file.getOriginalFilename());
                log.info("uploaded path {}", paths);

            });
            log.info("Uploaded the files successfully: " + fileNames + " " + paths);
            return ResponseCreated("Uploaded the files successfully: " + fileNames + " " + paths);
        } catch (Exception e) {
            log.info("Exception {} ", e.getMessage());
            return checkResponse(e);
        }
    }

    private ResponseBase<?> checkResponse(Exception e) {
        log.info("checkResponse ");
        return e.getMessage().contains("Could not store the file") ? ResponseError("File already exists") : ResponseError(e.getMessage());
    }

    /**
     * Deletes a file from the storage location.
     *
     * @param filename The name of the file to be deleted.
     * @return ResponseBase containing the response for the file deletion request.
     * @throws RuntimeException if deleting the file fails.
     */
    @Override
    public ResponseBase<?> deleteFile(final String filename) {
        try {
            Path file = root.resolve(filename);
            log.info(" Path file = root.resolve(filename); {} ",file);
            if (Files.exists(file)) {
                Files.delete(file);
                return ResponseSuccess("File deleted successfully: " + filename);
            } else {
                return ResponseNotFound("File not found: " + filename);
            }
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    /**
     * Retrieves a list of files categorized by type (images, documents, videos).
     *
     * @return ResponseBase containing the response with the categorized list of files.
     * @throws RuntimeException if retrieving the file list fails.
     */
    @Override
    @Transactional
    public ResponseBase<?> getListFiles() {
        var all = loadAll();
        log.info(String.valueOf(all.toList()));
        Map<String, List<String>> fileMap = new HashMap<>();

        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(root.toString()))) {
            for (Path path : directoryStream) {
                String fileName = path.getFileName().toString();
                String fileExtension = getFileExtension(fileName);

                if (isImage(fileExtension)) {
                    fileMap.computeIfAbsent("images", k -> new ArrayList<>()).add(fileName);
                } else if (isDocument(fileExtension)) {
                    fileMap.computeIfAbsent("documents", k -> new ArrayList<>()).add(fileName);
                } else if (isVideo(fileExtension)) {
                    fileMap.computeIfAbsent("videos", k -> new ArrayList<>()).add(fileName);
                }
            }
        } catch (IOException e) {
            return ResponseError(e.getMessage());
        }
        return ResponseSuccess(fileMap);
    }

    /**
     * Retrieves the file extension from a given filename.
     *
     * @param fileName The name of the file.
     * @return The file extension.
     */
    private String getFileExtension(final String fileName) {
        int dotIndex = fileName.lastIndexOf(".");
        if (dotIndex > 0 && dotIndex < fileName.length() - 1) {
            return fileName.substring(dotIndex + 1).toLowerCase();
        }
        return "";
    }

    /**
     * Checks if the file extension corresponds to an image format.
     *
     * @param fileExtension The file extension to be checked.
     * @return True if the file is an image, false otherwise.
     */
    private boolean isImage(final String fileExtension) {
        List<String> imageExtensions = Arrays.asList("jpg", "jpeg", "png", "gif", "bmp", "svg");
        return imageExtensions.contains(fileExtension);
    }

    /**
     * Checks if the file extension corresponds to a document format.
     *
     * @param fileExtension The file extension to be checked.
     * @return True if the file is a document, false otherwise.
     */
    private boolean isDocument(final String fileExtension) {
        List<String> documentExtensions = Arrays.asList("doc", "docx", "pdf", "txt", "xls", "xlsx", "ppt", "pptx");
        return documentExtensions.contains(fileExtension);
    }

    /**
     * Checks if the file extension corresponds to a video format.
     *
     * @param fileExtension The file extension to be checked.
     * @return True if the file is a video, false otherwise.
     */
    private boolean isVideo(final String fileExtension) {
        List<String> videoExtensions = Arrays.asList( "avi", "mov", "mkv", "mp4", "wmv", "flv", "3gp", "mpg", "mpeg");
        return videoExtensions.contains(fileExtension);
    }

}