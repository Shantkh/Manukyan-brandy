package com.bl.mb.service;

import com.bl.mb.models.PaginationWithSorting;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.models.Subscribers;
import com.bl.mb.models.SubscribersResponse;
import com.bl.mb.repo.SubscribeRepository;
import com.bl.mb.utils.PagePagination;
import com.opencsv.CSVWriter;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.UUID;

import static com.bl.mb.config.Translator.toLocale;
import static com.bl.mb.utils.ResponseUtil.*;

@Service
@RequiredArgsConstructor
public class SubscribeServiceImpl implements SubscribeService {

    private final SubscribeRepository subscribeRepository;
    private final PagePagination pagePagination;

    /**
     * Adds a new subscriber to the database.
     *
     * @param lang
     * @param subscribers The subscriber information to be added.
     * @return ResponseBase containing the response for the subscription request.
     */
    @Override
    public ResponseBase<?> subscriber(String lang, final Subscribers subscribers) {
        try {
            var exists = subscribeRepository.existsByEmail(subscribers.getEmail());
            if (exists) {
                return ResponseError(toLocale("email_already_exists"));
            }
            var result = subscribeRepository.save(subscribers);
            return ResponseCreated(result);
        } catch (Exception e) {
            return ResponseError(toLocale("an_error_occurred_please_try_again_later"));
        }
    }

    /**
     * Retrieves subscribers with pagination and sorting parameters.
     *
     * @param paginationWithSorting The pagination and sorting parameters.
     * @return ResponseBase containing the response with the list of subscribers.
     */
    @Override
    public ResponseBase<?> getSubscriber() {
        try {
            var data = subscribeRepository.findAll();
            var result = data.stream()
                    .sorted(Comparator.comparing(Subscribers::getCreatedOn).reversed())
                    .map(e -> SubscribersResponse.builder()
                            .id(e.getId())
                            .createdOn(e.getCreatedOn())
                            .email(e.getEmail())
                            .build())
                    .toList();
            return ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    /**
     * Deletes subscribers based on their IDs.
     *
     * @param subscriberIds Array of UUIDs representing subscriber IDs to be deleted.
     * @return ResponseBase containing the response for the delete request.
     */
    @Override
    public ResponseBase<?> delete(final UUID[] subscriberIds) {
        try {
            for (UUID id : subscriberIds) {
                subscribeRepository.deleteById(id);
            }
            return ResponseSuccess("Subscribers deleted successfully.");
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    /**
     * Downloads a CSV file containing subscriber information.
     *
     * @param response The HttpServletResponse object used to manipulate the HTTP response.
     * @return A ResponseBase containing the response status and data.
     */
    @Override
    public ResponseBase<?> download(HttpServletResponse response) {
        try (CSVWriter csvWriter = new CSVWriter(response.getWriter())) {
            var subscribers = subscribeRepository.findAll();
            response.setContentType("text/csv");
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String currentDateTime = dateFormat.format(new Date());

            String filename = "subscribers_" + currentDateTime + ".csv";
            response.setHeader("Content-disposition", "attachment; filename=" + filename);

            String[] csvHeader = {"ID", "SUBSCRIPTION DATE", "EMAIL"};
            csvWriter.writeNext(csvHeader);

            for (Subscribers subscriber : subscribers) {
                String[] data = {
                        subscriber.getId().toString(),
                        subscriber.getCreatedOn().toString(),
                        subscriber.getEmail()
                };
                csvWriter.writeNext(data);
            }

            return ResponseSuccess(csvWriter);

        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }
}
