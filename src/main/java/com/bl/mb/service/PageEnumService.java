package com.bl.mb.service;

import com.bl.mb.models.ResponseBase;

public interface PageEnumService {

    ResponseBase<?> pageEnums();

    ResponseBase<?> LinkEnums();

}
