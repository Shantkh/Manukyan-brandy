package com.bl.mb.service;

import com.bl.mb.models.InfoEntity;
import com.bl.mb.models.ResponseBase;

public interface InfoService {
    ResponseBase<?> create(final InfoEntity infoEntity);

    ResponseBase<?> info(String showOn);
}
