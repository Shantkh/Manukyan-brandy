package com.bl.mb.service;

import com.bl.mb.models.Bottling;
import com.bl.mb.models.BottlingRequest;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.repo.BottlingRepository;
import com.bl.mb.utils.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BottlingServiceImpl implements BottlingService {

    private final BottlingRepository bottlingRepository;
    @Override
    public ResponseBase<?> create(BottlingRequest bottlingRequest) {
        try {
            var bottlingExists = bottlingRepository.findBottlingByBottlingSection(bottlingRequest.getBottlingSection());
            bottlingExists.ifPresent(e -> bottlingRepository.deleteById(e.getId()));
            var bottling = Bottling.builder()
                    .images(bottlingRequest.getImages())
                    .bottlingSection(bottlingRequest.getBottlingSection())
                    .armenianContent(bottlingRequest.getArmenianContent())
                    .englishContent(bottlingRequest.getEnglishContent())
                    .build();
            var result = bottlingRepository.save(bottling);
            return ResponseUtil.ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> getBottling() {
        try {
            var result = bottlingRepository.findAll();
            return ResponseUtil.ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }
}
