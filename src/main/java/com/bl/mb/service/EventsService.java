package com.bl.mb.service;

import com.bl.mb.models.Events;
import com.bl.mb.models.PaginationWithSorting;
import com.bl.mb.models.ResponseBase;
import jakarta.validation.Valid;

import java.sql.Timestamp;
import java.util.UUID;

public interface EventsService {
    ResponseBase<?> events(@Valid final Events events,
                           @Valid final Long startDate,
                           @Valid final Long endDate);

    ResponseBase<?> allEvents(@Valid final PaginationWithSorting paginationWithSorting) throws Exception;

    ResponseBase<?> allEvents(@Valid final String lang,
                              @Valid final PaginationWithSorting paginationWithSorting) throws Exception;

    ResponseBase<?> allEventsByDate(@Valid final PaginationWithSorting paginationWithSorting,
                                    @Valid final Long startDate,
                                    @Valid final Long endDate);

    ResponseBase<?> allEventsByDateAndLocale(@Valid final PaginationWithSorting paginationWithSorting,
                                             @Valid final Long startDate,
                                             @Valid final Long endDate,
                                             @Valid final String lang);

    ResponseBase<?> nextEvents(@Valid final String lang,
                               @Valid final PaginationWithSorting paginationWithSorting,
                               @Valid final Long month,
                               @Valid final Long year) throws Exception;

    ResponseBase<?> preEvents(@Valid final String lang,
                              @Valid final PaginationWithSorting paginationWithSorting,
                              @Valid final Long month,
                              @Valid final Long year) throws Exception;

    ResponseBase<?> eventById(@Valid final String lang,
                              @Valid final UUID id);

    ResponseBase<?> thisMonthEvents(@Valid final String lang,
                                    @Valid final PaginationWithSorting paginationWithSorting,
                                    @Valid final Long month,
                                    @Valid final Long year) throws Exception;

    ResponseBase<?> oneEvent(UUID id);

    ResponseBase<?> eventEventsByYear(@Valid final String lang,
                                      @Valid final Long year);

    ResponseBase<?> deleteEvent(@Valid final UUID id);

    ResponseBase<?> updateEvent(@Valid final Events events);
}
