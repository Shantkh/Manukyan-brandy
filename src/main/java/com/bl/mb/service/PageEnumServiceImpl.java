package com.bl.mb.service;

import com.bl.mb.constances.LinksEnum;
import com.bl.mb.constances.PagesEnum;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.utils.ResponseUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class PageEnumServiceImpl implements PageEnumService {
    @Override
    public ResponseBase<?> pageEnums() {
        var result = List.of(PagesEnum.values());
        log.info("result: {}", result);
        return ResponseUtil.ResponseSuccess(result);
    }

    @Override
    public ResponseBase<?> LinkEnums() {
        var result = List.of(LinksEnum.values());
        return ResponseUtil.ResponseSuccess(result);
    }
}
