package com.bl.mb.service;

import com.bl.mb.models.CellarRequest;
import com.bl.mb.models.ResponseBase;
import jakarta.validation.Valid;

public interface CellarService {
    ResponseBase<?> create(@Valid final CellarRequest cellarRequest);

    ResponseBase<?> getCellars();
}
