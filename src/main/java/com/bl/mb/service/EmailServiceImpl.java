package com.bl.mb.service;

import com.bl.mb.kafka.utils.KafkaEmailUtils;
import com.bl.mb.models.EmailDetails;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.models.Subscribers;
import com.bl.mb.repo.SubscribeRepository;
import com.bl.mb.utils.ResponseUtil;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService{

    private final KafkaEmailUtils kafkaContactUsProducer;
    private final SubscribeRepository subscribeRepository;

    /**
     * Sends an email and handles subscription if requested.
     *
     * @param formData The {@link EmailDetails} containing email information.
     * @return A {@link ResponseBase} indicating the success or failure of the email sending process.
     */
    @Override
    public ResponseBase<?> sendEmail(@Valid final EmailDetails formData) {
        try {
            if (formData.isSubscribe()) {
                handleSubscription(formData.getRecipient());
            }
//            kafkaContactUsProducer.userContactUsActivity(formData);
            return ResponseUtil.ResponseSuccess("Email Sent.");
        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }

    /**
     * Handles subscription by checking if the email recipient is already subscribed. If not, adds them to the subscribers list.
     *
     * @param recipient The email recipient to check and subscribe.
     */
    private void handleSubscription(final String recipient) {
        if (!subscribeRepository.existsByEmail(recipient)) {
            Subscribers subscribers = Subscribers.builder().email(recipient).build();
            subscribeRepository.save(subscribers);
        }
    }
}
