package com.bl.mb.service;

import com.bl.mb.jwt.JwtProvider;
import com.bl.mb.kafka.utils.KafkaEmailUtils;
import com.bl.mb.mapper.EmailMapper;
import com.bl.mb.mapper.UserMapping;
import com.bl.mb.models.*;
import com.bl.mb.repo.EmailRepository;
import com.bl.mb.repo.UserRepository;
import com.bl.mb.security.UserPrincipal;
import com.bl.mb.security.service.JwtRefreshTokenService;
import com.bl.mb.utils.EmailUtils;
import io.jsonwebtoken.Jwts;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;

import static com.bl.mb.constances.UserSendEmailActivity.FORGOT_PASSWORD;
import static com.bl.mb.utils.ResponseUtil.ResponseError;
import static com.bl.mb.utils.ResponseUtil.ResponseSuccess;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtProvider jwtProvider;

    @Autowired
    private JwtRefreshTokenService jwtRefreshTokenService;

    @Autowired
    private UserMapping userMapping;

    @Autowired
    private final KafkaEmailUtils kafkaEmailUtils;

    @Autowired
    private final EmailRepository emailRepository;

    @Autowired
    private final PasswordEncoder passwordEncoder;

    @Autowired
    private final UserRepository userRepository;

    @Autowired
    private final EmailMapper emailMapper;

    private final static String PASSWORD_ERROR_RESPONSE = "Username or the password is incorrect.";
    private final static String PASSWORD_EMPTY_RESPONSE = "Encoded password cannot be empty.";

    public AuthenticationServiceImpl(KafkaEmailUtils kafkaEmailUtils,
                                     EmailRepository emailRepository,
                                     PasswordEncoder passwordEncoder,
                                     UserRepository userRepository, EmailMapper emailMapper) {
        this.kafkaEmailUtils = kafkaEmailUtils;
        this.emailRepository = emailRepository;
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.emailMapper = emailMapper;
    }

    /**
     * Authenticates a user and returns a JWT token.
     *
     * @param signInRequest The user authentication request.
     * @return A response containing JWT token or an error message.
     */
    @Override
    public ResponseBase<?> signInAndReturnJwt(User signInRequest) {
        Authentication authentication;
        if (signInRequest.getPassword() == null || signInRequest.getPassword().isEmpty()) {
            return ResponseError(PASSWORD_ERROR_RESPONSE);
        }

        if (signInRequest.getPassword().length() < 4 || signInRequest.getPassword().length() > 20) {
            return ResponseError(PASSWORD_ERROR_RESPONSE);
        }

        try {
            authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(signInRequest.getUsername(), signInRequest.getPassword())
            );
        } catch (Exception e) {
            return ResponseError(PASSWORD_ERROR_RESPONSE);
        }

        var userPrincipal = (UserPrincipal) authentication.getPrincipal();

        if (userPrincipal.getPassword() == null || userPrincipal.getPassword().isEmpty()) {
            return ResponseError(PASSWORD_EMPTY_RESPONSE);
        }

        var jwt = jwtProvider.generateToken(userPrincipal);

        var signInUser = userPrincipal.getUser();
        signInUser.setAccessToken(jwt);
        signInUser.setRefreshToken(String.valueOf(jwtRefreshTokenService.createRefreshToken(signInUser.getId()).getTokenId()));
        var result = userMapping.toDto(signInUser);
        return ResponseSuccess(result);
    }


    /**
     * Updates the password for a user based on the provided {@code UpdatePassword} object.
     *
     * @param forgotPassword The object containing information for updating the password,
     *                       including email, verification code, old password, and new password.
     * @return A {@link ResponseBase} instance indicating the result of the password update operation.
     * If successful, the response includes a success message; otherwise, an error message is provided.
     * @see UpdatePassword
     * @see ResponseBase
     */
    @Override
    public ResponseBase<?> forgotPassword(ForgotPasswordRequest forgotPasswordRequest,
                                          HttpServletRequest request) {
        try {
            var user = userRepository.findByEmail(forgotPasswordRequest.getEmail());
            if (user == null) {
                return ResponseError("user email not found.");
            }
            var emailDetails = EmailUtils.createdForgotPasswordCode(
                    forgotPasswordRequest.getEmail(),
                    FORGOT_PASSWORD);

//            kafkaEmailUtils.userForgotPasswordActivity(emailDetails);
            var result = emailMapper.toDto(emailDetails);

            return ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    /**
     * Updates the password for a user based on the provided {@code UpdatePassword} object.
     *
     * @param updatePassword The object containing information for updating the password,
     *                       including email, verification code, old password, and new password.
     * @return A {@link ResponseBase} instance indicating the result of the password update operation.
     * If successful, the response includes a success message; otherwise, an error message is provided.
     * @see UpdatePassword
     * @see ResponseBase
     */
    @Override
    public ResponseBase<?> updatePassword(UpdatePassword updatePassword) {
        try {
            var emailCode = emailRepository.findAllByRecipient(updatePassword.getEmail());
            User user = userRepository.findByEmail(updatePassword.getEmail());

            if (!updatePassword.getCode().equals(emailCode.getCode())) {
                return ResponseError("Code does not match.");
            }

            userRepository.changeUserPassword(user.getId(), passwordEncoder.encode(updatePassword.getNewPassword()));
            emailRepository.deleteById(updatePassword.getCodeId());
            return ResponseSuccess("Password updated.");

        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }


    /**
     * Signs out the user.
     *
     * @return A response indicating successful sign-out.
     */
    @Override
    public ResponseBase<?> signOut() {
        var result = Jwts.builder()
                .setSubject(null)
                .claim("roles", new Role())
                .claim("userId", new User())
                .setExpiration(new Date(System.currentTimeMillis()))
                .compact();
        return ResponseSuccess(result);
    }
}