package com.bl.mb.service;


import com.bl.mb.constances.MonthOfYear;
import com.bl.mb.constances.WeekDaysEnum;
import com.bl.mb.models.*;
import com.bl.mb.repo.TourRepository;
import com.bl.mb.utils.PagePagination;
import com.bl.mb.utils.ResponseUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.time.temporal.WeekFields;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.bl.mb.utils.LocaleChecker.checkLocale;
import static com.bl.mb.utils.ResponseUtil.ResponseError;
import static com.bl.mb.utils.ResponseUtil.ResponseSuccess;
import static com.bl.mb.utils.ToursUtils.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class ToursServiceImpl implements ToursService {

    private final TourRepository tourRepository;
    private final PagePagination pagePagination;

    @Override
    public ResponseBase<?> save(final ToursRequest toursRequest) {
        var tourDays = toursRequest.getTourDay();
        var tourMonths = toursRequest.getTourMonth();
        var tourHour = toursRequest.getTourHour();
        int currentYear = getCurrentYear();
        try {
            var data = tourMonths.stream()
                    .flatMap(tourMonth -> tourDays.stream()
                            .flatMap(tourDay -> IntStream.rangeClosed(1, countDaysInMonth(currentYear, tourDay, tourMonth))
                                    .mapToObj(k -> {
                                        Tours tours = Tours.builder()
                                                .tourDay(tourDay)
                                                .duration(toursRequest.getDuration())
                                                .price(toursRequest.getPrice())
                                                .tourMonth(tourMonth)
                                                .tourHour(tourHour)
                                                .tourImages(toursRequest.getImage())
                                                .weekNumber(k)
                                                .dateOfTour(String.valueOf(getDateFromMonthAndDay(currentYear,
                                                        getMonthNumber(tourMonth),
                                                        tourDay,
                                                        k)
                                                ))
                                                .build();

                                        tours.setTourLocalization(toursRequest.getTourLocalization().stream()
                                                .map(e -> {
                                                    return ToursLocalization.builder()
                                                            .locale(e.getLocale())
                                                            .htmlShortContent(e.getHtmlShortContent())
                                                            .htmlLongContent(e.getHtmlLongContent())
                                                            .htmlTitle(e.getHtmlTitle())
                                                            .tours(tours) // Set reference to Tours entity
                                                            .build();
                                                })
                                                .collect(Collectors.toList())
                                        );

                                        return tours;
                                    })
                            ))
                    .peek(tourRepository::save)
                    .toList();

            var result = data.stream()
                    .map(e -> ToursResponse.builder()
                            .id(e.getId())
                            .createdOn(e.getCreatedOn())
                            .tourImages(e.getTourImages())
                            .price(e.getPrice())
                            .tourLocalization(e.getTourLocalization())
                            .dateOfTour(e.getDateOfTour())
                            .tourHour(e.getTourHour())
                            .weekNumber(e.getWeekNumber())
                            .tourImages(e.getTourImages())
                            .tourMonth(e.getTourMonth().name())
                            .tourDay(e.getTourDay().name())
                            .dateOfTour(e.getDateOfTour())
                            .duration(e.getDuration())
                            .build())
                    .collect(Collectors.toList());


            return ResponseUtil.ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> saveOne(ToursOneRequest toursRequest) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
        SimpleDateFormat outputDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = dateFormat.parse(toursRequest.getTourDate().toString());
            SimpleDateFormat dayFormat = new SimpleDateFormat("dd");
            SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
            int monthNumber = Integer.parseInt(monthFormat.format(date));

            DayOfWeek dayOfWeek = date.toInstant().atZone(java.time.ZoneId.systemDefault()).getDayOfWeek();
            String dayName = dayOfWeek.getDisplayName(TextStyle.FULL, Locale.ENGLISH);

            Calendar cal = Calendar.getInstance();
            cal.setTime(dateFormat.parse(toursRequest.getTourDate().toString()));
            int weekOfMonth = cal.get(Calendar.WEEK_OF_MONTH);

            String outputDateStr = outputDateFormat.format(date);

            Month month = Month.of(monthNumber);

            var tourHour = toursRequest.getTourHour();
            var localizations = toursRequest.getTourLocalization();
            var tour = Tours.builder()
                    .tourMonth(MonthOfYear.valueOf(String.valueOf(month)))
                    .tourDay(WeekDaysEnum.valueOf(dayName.toUpperCase()))
                    .dateOfTour(outputDateStr)
                    .tourHour(tourHour)
                    .tourImages(toursRequest.getImage())                  .weekNumber(weekOfMonth)
                    .tourLocalization(new ArrayList<>())
                    .price(toursRequest.getPrice())
                    .duration(toursRequest.getDuration())
                    .build();

            localizations.stream()
                    .map(e -> ToursLocalization.builder()
                            .locale(e.getLocale())
                            .htmlTitle(e.getHtmlTitle())
                            .htmlLongContent(e.getHtmlLongContent())
                            .htmlShortContent(e.getHtmlShortContent())
                            .tours(tour)
                            .build())
                    .forEach(localization -> tour.getTourLocalization().add(localization));

            var data = tourRepository.save(tour);
            var result = ToursResponse.builder()
                    .id(data.getId())
                    .createdOn(data.getCreatedOn())
                    .tourImages(data.getTourImages())
                    .price(data.getPrice())
                    .tourLocalization(data.getTourLocalization())
                    .dateOfTour(data.getDateOfTour())
                    .tourHour(data.getTourHour())
                    .weekNumber(data.getWeekNumber())
                    .tourMonth(data.getTourMonth().name())
                    .tourDay(data.getTourDay().name())
                    .dateOfTour(data.getDateOfTour())
                    .duration(data.getDuration())
                    .build();
            return ResponseUtil.ResponseSuccess(result);

        } catch (ParseException e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> delete(final UUID id) {
        try {
            var isExists = tourRepository.findById(id);
            if (isExists.isEmpty()) {
                return ResponseError("Tour not found.");
            }
            tourRepository.deleteById(id);
            return ResponseSuccess("Tour Deleted successfully");
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> getTourById(final UUID id) {
        try {
            var isExists = tourRepository.findById(id);
            if (isExists.isEmpty()) {
                ResponseUtil.ResponseNotFound("No Tour with the id.");
            }
            var data = tourRepository.findById(id).get();
            var result = ToursResponse.builder()
                    .id(data.getId())
                    .createdOn(data.getCreatedOn())
                    .tourImages(data.getTourImages())
                    .tourLocalization(data.getTourLocalization())
                    .dateOfTour(data.getDateOfTour())
                    .tourHour(data.getTourHour())
                    .weekNumber(data.getWeekNumber())
                    .tourMonth(data.getTourMonth().name())
                    .tourDay(data.getTourDay().name())
                    .dateOfTour(data.getDateOfTour())
                    .price(data.getPrice())
                    .duration(data.getDuration())
                    .tourImages(data.getTourImages())
                    .build();

            return ResponseUtil.ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> allTourPagination(final String local, final PaginationWithSorting paginationWithSorting) {
        String lang;
        try {
            lang = fixLanguage(local);
            var pageReq = pagePagination.pagePagination(paginationWithSorting);
            var data = tourRepository.findLatestByPageNameAndLocale(lang, pageReq);
            if (data.isEmpty()) {
                return ResponseError(new ArrayList<>());
            }
            var result = data.stream()
                    .map(e -> ToursResponse.builder()
                            .id(e.getId())
                            .createdOn(e.getCreatedOn())
                            .tourImages(e.getTourImages())
                            .tourLocalization(e.getTourLocalization())
                            .dateOfTour(e.getDateOfTour())
                            .tourHour(e.getTourHour())
                            .weekNumber(e.getWeekNumber())
                            .tourMonth(e.getTourMonth().name())
                            .tourDay(e.getTourDay().name())
                            .dateOfTour(e.getDateOfTour())
                            .duration(e.getDuration())
                            .price(e.getPrice())
                            .build())
                    .collect(Collectors.toList());

            return ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    private String fixLanguage(String local) {
        return local.contains("-") ? local.substring(0, local.indexOf('-')) : local;
    }

    @Override
    public ResponseBase<?> allTourByDates(final String locales, final String startDate, final String endDate) {
        var locale = checkLocale(locales);

        try {
            var data = tourRepository.findToursBetweenDates(locale, startDate, endDate);
            var result = data.stream()

                    .map(e -> ToursResponse.builder()
                            .id(e.getId())
                            .createdOn(e.getCreatedOn())
                            .tourImages(e.getTourImages())
                            .tourLocalization(e.getTourLocalization())
                            .dateOfTour(e.getDateOfTour())
                            .tourHour(e.getTourHour().format(String.valueOf(DateTimeFormatter.ofPattern("HH:mm"))))
                            .weekNumber(e.getWeekNumber())
                            .tourMonth(e.getTourMonth().name())
                            .tourDay(e.getTourDay().name())
                            .dateOfTour(e.getDateOfTour())
                            .price(e.getPrice())
                            .duration(e.getDuration())
                            .build())
                    .sorted(Comparator.comparing(ToursResponse::getDateOfTour))
                    .collect(Collectors.toList());
            return ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> updateTours(final ToursUpdateRequest toursRequest) {
        try {
            var isExists = tourRepository.findById(toursRequest.getTourId());
            if (isExists.isEmpty()) {
                return ResponseError("Tour not found.");
            }

            var eventObject = Tours.builder()
                    .id(isExists.get().getId())
                    .createdOn(isExists.get().getCreatedOn())
                    .tourHour(toursRequest.getTourHour())
                    .duration(toursRequest.getDuration())
                    .tourImages(toursRequest.getTourImages())
                    .dateOfTour(toursRequest.getDateOfTour())
                    .tourDay(toursRequest.getTourDay())
                    .tourMonth(toursRequest.getTourMonth())
                    .tourLocalization(new ArrayList<>())
                    .weekNumber(getWeekNumberOfMonth(toursRequest.getDateOfTour()))
                    .price(toursRequest.getPrice())
                    .tourImages(toursRequest.getTourImages())
                    .build();

            var localizations = toursRequest.getTourLocalization();

            if (localizations != null && !localizations.isEmpty()) {
                var modifiedLocalizations = localizations.stream()
                        .map(e -> ToursLocalization.builder()
                                .id(e.getId())
                                .locale(e.getLocale())
                                .htmlTitle(e.getHtmlTitle())
                                .htmlLongContent(e.getHtmlLongContent())
                                .htmlShortContent(e.getHtmlShortContent())
                                .tours(eventObject)
                                .build())
                        .toList();

                eventObject.getTourLocalization().addAll(modifiedLocalizations);
            }

            var data = tourRepository.save(eventObject);

            var result = ToursResponse.builder()
                    .id(data.getId())
                    .createdOn(data.getCreatedOn())
                    .tourImages(data.getTourImages())
                    .tourLocalization(data.getTourLocalization())
                    .dateOfTour(data.getDateOfTour())
                    .tourHour(data.getTourHour().format(String.valueOf(DateTimeFormatter.ofPattern("HH:mm"))))
                    .weekNumber(data.getWeekNumber())
                    .tourMonth(data.getTourMonth().name())
                    .tourDay(data.getTourDay().name())
                    .dateOfTour(data.getDateOfTour())
                    .price(data.getPrice())
                    .duration(data.getDuration())
                    .build();

            return ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> getAllTours(final PaginationWithSorting paginationWithSorting) {
        try {
            var pageReq = pagePagination.pagePagination(paginationWithSorting);
            var data = tourRepository.findAllPagination(pageReq);
            if (data.isEmpty()) {
                return ResponseError(new ArrayList<>());
            }
            var result = data.stream()
                    .map(e -> ToursResponse.builder()
                            .id(e.getId())
                            .createdOn(e.getCreatedOn())
                            .tourImages(e.getTourImages())
                            .tourLocalization(e.getTourLocalization())
                            .dateOfTour(e.getDateOfTour())
                            .tourHour(e.getTourHour())
                            .weekNumber(e.getWeekNumber())
                            .tourMonth(e.getTourMonth().name())
                            .tourDay(e.getTourDay().name())
                            .dateOfTour(e.getDateOfTour())
                            .duration(e.getDuration())
                            .price(e.getPrice())
                            .build())
                    .collect(Collectors.toList());

            return ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> getById(String locales, String bookingId) {
        var locale = checkLocale(locales);
        try {
            var isExists = tourRepository.findById(UUID.fromString(bookingId));
            if (isExists.isEmpty()) {
                return ResponseError("Tour not found.");
            }
            var data = tourRepository.findByIdAndLocal(UUID.fromString(bookingId), locale);
            var result = ToursIdResponse.builder()
                    .id(data.getId())
                    .createdOn(data.getCreatedOn())
                    .tourImages(data.getTourImages().get(0))
                    .htmlTitle(data.getTourLocalization().get(0).getHtmlTitle())
                    .htmlLongContent(data.getTourLocalization().get(0).getHtmlLongContent())
                    .htmlShortContent(data.getTourLocalization().get(0).getHtmlShortContent())
                    .dateOfTour(data.getDateOfTour())
                    .tourHour(data.getTourHour())
                    .weekNumber(data.getWeekNumber())
                    .tourMonth(data.getTourMonth().name())
                    .tourDay(data.getTourDay().name())
                    .dateOfTour(data.getDateOfTour())
                    .price(data.getPrice())
                    .duration(data.getDuration())
                    .build();
            return ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    public static int getWeekNumberOfMonth(String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(dateString, formatter);

        int dayOfMonth = date.getDayOfMonth();
        int weekNumber = (dayOfMonth - 1) / 7 + 1;

        return weekNumber;
    }
}
