package com.bl.mb.service;

import com.bl.mb.models.*;
import com.bl.mb.repo.ProductsRepository;
import com.bl.mb.utils.PagePagination;
import com.bl.mb.utils.ResponseUtil;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.hibernate.sql.ast.tree.expression.Collation;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.bl.mb.utils.ResponseUtil.ResponseError;
import static com.bl.mb.utils.ResponseUtil.ResponseSuccess;

@Service
@RequiredArgsConstructor
public class ProductsServiceImpl implements ProductsService {

    private final ProductsRepository productsRepository;
    private final PagePagination pagePagination;
    /**
     * Creates a new product based on the provided details.
     *
     * @param products The {@link Products} object containing product information.
     * @return A {@link ResponseBase} indicating the success or failure of the product creation process.
     */
    @Override
    public ResponseBase<?> product(@Valid final Products products) {
        try {
            var obj = Products.builder()
                    .productType(new ArrayList<>())
                    .productImages(products.getProductImages())
                    .brandName(products.getBrandName())
                    .pageTitle(new ArrayList<>())
                    .productDate(products.getProductDate())
                    .description(new ArrayList<>())
                    .build();

            return getResponseBase(products, obj);

        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }

    public ResponseBase<?> update(@Valid final Products updatedProduct) {
        try {
            UUID productId = updatedProduct.getId();
            Products existingProduct = productsRepository.findById(productId)
                    .orElseThrow(() -> new RuntimeException("No product found with ID: " + productId));

            // Update fields of existing product with values from updated product
            existingProduct.setProductImages(updatedProduct.getProductImages());
            existingProduct.setProductDate(updatedProduct.getProductDate());

            // Update localized entities (Brand, PageTitle, Description) for the existing product
            updateLocalizedEntities(existingProduct.getPageTitle(), updatedProduct.getPageTitle(), existingProduct);
            updateLocalizedEntities(existingProduct.getDescription(), updatedProduct.getDescription(), existingProduct);
            updateLocalizedEntities(existingProduct.getProductType(), updatedProduct.getProductType(), existingProduct);

            // Save the updated product
            Products savedProduct = productsRepository.save(existingProduct);

            return ResponseUtil.ResponseSuccess(savedProduct);
        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }

    private <T> void updateLocalizedEntities(List<T> existingEntities, List<T> updatedEntities, Products existingProduct) {
        existingEntities.clear(); // Clear existing entities to avoid conflicts
        existingEntities.addAll(updatedEntities); // Add updated entities

        // Update the reverse relationship in each updated entity
        for (T entity : updatedEntities) {
            if (entity instanceof ProductsTypeLocalization) {
                ((ProductsTypeLocalization) entity).setProducts(existingProduct);
            } else if (entity instanceof ProductsTitleLocalization) {
                ((ProductsTitleLocalization) entity).setProducts(existingProduct);
            } else if (entity instanceof ProductsDescriptionLocalization) {
                ((ProductsDescriptionLocalization) entity).setProducts(existingProduct);
            }
            // Add handling for other types of localized entities if needed
        }
    }

    /**
     * Updates an existing product based on the provided details.
     *
     * @param products The {@link Products} object containing updated product information.
     * @return A {@link ResponseBase} indicating the success or failure of the product update process.
     */

    private ResponseBase<?> getResponseBase(@Valid Products products, Products obj) {

        var pageTitle = products.getPageTitle();
        pageTitle.stream().map(e -> ProductsTitleLocalization.builder()
                .locale(e.getLocale())
                .id(e.getId())
                .htmlContent(e.getHtmlContent())
                .products(obj)
                .build()).forEach(productsTitleLocalization -> obj.getPageTitle().add((ProductsTitleLocalization) productsTitleLocalization));

        var description = products.getDescription();
        description.stream().map(e -> ProductsDescriptionLocalization.builder()
                .locale(e.getLocale())
                .id(e.getId())
                .htmlContent(e.getHtmlContent())
                .products(obj)
                .build()).forEach(productsDescriptionLocalization -> obj.getDescription().add((ProductsDescriptionLocalization) productsDescriptionLocalization));

        var types = products.getProductType();
        types.stream().map(e -> ProductsTypeLocalization.builder()
                .locale(e.getLocale())
                .id(e.getId())
                .htmlContent(e.getHtmlContent())
                .products(obj)
                .build()).forEach(localization -> obj.getProductType().add(localization));

        var result = productsRepository.save(obj);

        return ResponseUtil.ResponseSuccess(result);
    }

    /**
     * Deletes a product based on the provided product ID.
     *
     * @param id The unique identifier of the product to be deleted.
     * @return A {@link ResponseBase} indicating the success or failure of the product deletion process.
     */
    @Override
    public ResponseBase<?> deleteProduct(@Valid final UUID id) {
        try {
            var isProduct = productsRepository.findById(id);
            if (isProduct.isEmpty()) {
                ResponseUtil.ResponseNotFound("No product with the id.");
            }
            productsRepository.deleteById(id);
            return ResponseUtil.ResponseSuccess("deleted successfully.");
        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }

    /**
     * Retrieves a product based on the provided product ID.
     *
     * @param id The unique identifier of the product to be retrieved.
     * @return A {@link ResponseBase} containing the product details if found, or an error message if not found.
     */
    @Override
    public ResponseBase<?> getProductById(final UUID id) {
        try {
            var isProduct = productsRepository.findById(id);
            if (isProduct.isEmpty()) {
                ResponseUtil.ResponseNotFound("No product with the id.");
            }
            var result = productsRepository.findById(id);
            return ResponseUtil.ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> getAllPagination(PaginationWithSorting paginationWithSorting) {
        try {
            PageRequest pageReq = pagePagination.pagePagination(paginationWithSorting);
            var result = productsRepository.findAll(pageReq).getContent();
            return ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> getOtherWines(String language) {

        try {
            var response = productsRepository.findAll();

            var result = response.stream()
                    .limit(3)
                    .collect(Collectors.collectingAndThen(Collectors.toList(), list -> {
                        Collections.shuffle(list);
                        return list.stream();
                    }))
                    .map(res ->{
                        var pageTitle = res.getPageTitle().stream()
                                .filter(e -> e.getLocale().equals(language))
                                        .map(f -> ProductsTitleLocalization.builder()
                                                .id(f.getId())
                                                .htmlContent(f.getHtmlContent())
                                                .locale(f.getLocale())
                                                .build())
                                .toList();
                        var description = res.getDescription().stream()
                                        .filter(r -> r.getLocale().equals(language))
                                        .map(f -> ProductsDescriptionLocalization.builder()
                                                .id(f.getId())
                                                .htmlContent(f.getHtmlContent())
                                                .locale(f.getLocale())
                                                .build())
                                .toList();
                        var productType =res.getProductType().stream()
                                        .filter(r -> r.getLocale().equals(language))
                                        .map(f -> ProductsTypeLocalization.builder()
                                                .id(f.getId())
                                                .htmlContent(f.getHtmlContent())
                                                .locale(f.getLocale())
                                                .build())
                                .toList();

                        return Products.builder()
                                .id(res.getId())
                                .brandName(res.getBrandName())
                                .createdOn(res.getCreatedOn())
                                .productImages(res.getProductImages())
                                .productDate(res.getProductDate())
                                .pageTitle(pageTitle)
                                .description(description)
                                .productType(productType)
                                .build();

                    });

            return ResponseUtil.ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }

    /**
     * Retrieves all products.
     *
     * @return A {@link ResponseBase} containing the list of all products or an error message if an exception occurs.
     */
    @Override
    public ResponseBase<?> getProducts() {
        try {
            var result = productsRepository.findAll();
            return ResponseUtil.ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }
}
