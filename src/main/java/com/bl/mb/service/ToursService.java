package com.bl.mb.service;

import com.bl.mb.models.*;

import java.util.UUID;

public interface ToursService {

    ResponseBase<?> save(final ToursRequest toursRequest);
    ResponseBase<?> saveOne(final ToursOneRequest toursRequest);

    ResponseBase<?> delete(final UUID id);

    ResponseBase<?> getTourById(final UUID id);

    ResponseBase<?> allTourPagination(final String lang, final PaginationWithSorting paginationWithSorting);

    ResponseBase<?> allTourByDates(final String lang, final String startDate, final String endDate);

    ResponseBase<?> updateTours(final ToursUpdateRequest toursRequest);

    ResponseBase<?> getById(final String lang,final String bookingId);

    ResponseBase<?> getAllTours(final PaginationWithSorting paginationWithSorting);


}
