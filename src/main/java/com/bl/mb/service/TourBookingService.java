package com.bl.mb.service;

import com.bl.mb.models.PaginationWithSorting;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.models.TourBooking;
import jakarta.validation.Valid;

public interface TourBookingService {
    ResponseBase<?> bookTour(final TourBooking tourBooking);

    ResponseBase<?> bookedAllTours(@Valid final PaginationWithSorting paginationWithSorting);
    ResponseBase<?> bookedAllToursStartedLastWeek(@Valid final PaginationWithSorting paginationWithSorting);

    ResponseBase<?> toursById(@Valid final String id);

    ResponseBase<?> customersToursById(@Valid final String id);
}
