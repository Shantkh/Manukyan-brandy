package com.bl.mb.service;

import com.bl.mb.models.Brand;
import com.bl.mb.models.Cellar;
import com.bl.mb.models.CellarRequest;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.repo.CellarRepository;
import com.bl.mb.utils.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CellarServiceImpl implements CellarService{

    private final CellarRepository cellarRepository;
    @Override
    public ResponseBase<?> create(CellarRequest cellarRequest) {
        try {
            var cellarExists = cellarRepository.findCellarByCellarSection(cellarRequest.getCellarSection());
            cellarExists.ifPresent(e -> cellarRepository.deleteById(e.getId()));
            var cellar = Cellar.builder()
                    .images(cellarRequest.getImages())
                    .cellarSection(cellarRequest.getCellarSection())
                    .armenianContent(cellarRequest.getArmenianContent())
                    .englishContent(cellarRequest.getEnglishContent())
                    .build();
            var result = cellarRepository.save(cellar);
            return ResponseUtil.ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }

    @Override
    public ResponseBase<?> getCellars() {
        try {
            var result = cellarRepository.findAll();
            return ResponseUtil.ResponseSuccess(result);
        } catch (Exception e) {
            return ResponseUtil.ResponseError(e.getMessage());
        }
    }
}
