package com.bl.mb.service;

import com.bl.mb.models.ResponseBase;
import jakarta.validation.Valid;

public interface SliderService {
    ResponseBase<?> slides(@Valid final String lang);
}
