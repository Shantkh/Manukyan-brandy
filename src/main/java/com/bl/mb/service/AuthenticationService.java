package com.bl.mb.service;

import com.bl.mb.models.ForgotPasswordRequest;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.models.UpdatePassword;
import com.bl.mb.models.User;
import jakarta.servlet.http.HttpServletRequest;

import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;

public interface AuthenticationService {

    ResponseBase<?> signInAndReturnJwt(final User signInRequest) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException;

    ResponseBase<?> forgotPassword(ForgotPasswordRequest forgotPasswordRequest,
                                   HttpServletRequest request);

    ResponseBase<?> signOut();

    ResponseBase<?> updatePassword(UpdatePassword updatePassword);
}