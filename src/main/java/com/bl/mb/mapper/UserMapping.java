package com.bl.mb.mapper;

import com.bl.mb.models.Role;
import com.bl.mb.models.User;
import com.bl.mb.models.UserResponse;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserMapping {

    /**
     * Converts a User entity to a UserResponse DTO.
     *
     * @param user The User entity to be converted.
     * @return UserResponse DTO containing mapped user information.
     */
    public UserResponse toDto(User user) {
        var roleNames = getRoles(user);

        return UserResponse.builder()
                .id(user.getId())
                .username(user.getUsername())
                .email(user.getEmail())
                .accessToken(user.getAccessToken())
                .roles(roleNames)
                .refreshToken(user.getRefreshToken())
                .build();
    }

    /**
     * Retrieves the names of roles associated with a User entity.
     *
     * @param user The User entity.
     * @return List of role names associated with the user.
     */
    private List<String> getRoles(User user) {
        return user.getRoles()
                .stream()
                .map(Role::getName)
                .toList();
    }
}
