package com.bl.mb.mapper;

import com.bl.mb.models.EmailDetails;
import com.bl.mb.models.EmailDetailsDto;
import org.springframework.stereotype.Component;

@Component
public class EmailMapper {
    /**
     * Converts an {@link EmailDetails} entity to a Data Transfer Object (DTO) for external representation.
     *
     * @param emailDetails The {@link EmailDetails} entity to be converted to a DTO.
     * @return An {@link EmailDetailsDto} instance representing the DTO version of the {@code emailDetails}.
     * @see EmailDetails
     * @see EmailDetailsDto
     */
    public EmailDetailsDto toDto(EmailDetails emailDetails) {
        return EmailDetailsDto.builder()
                .id(emailDetails.getId())
                .createdDate(emailDetails.getCreatedDate())
                .recipient(emailDetails.getRecipient())
                .build();
    }
}
