package com.bl.mb.setup;

import com.bl.mb.models.Role;
import com.bl.mb.models.User;
import com.bl.mb.repo.RolesRepository;
import com.bl.mb.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;


@Component
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private boolean alreadySetup = false;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RolesRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Executed on application startup to initialize essential data.
     *
     * @param event The context refreshed event.
     */
    @Override
    @Transactional
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        if (alreadySetup) {
            return;
        }

        final Role adminRole = createRoleIfNotFound("ROLE_ADMIN");
        final Role userRole = createRoleIfNotFound("ROLE_USER");
        createUserIfNotFound("admin_One","One@test.com", "Admin one", "password123", new ArrayList<>(Collections.singletonList(adminRole)));
        createUserIfNotFound("admin_Two","Two@test.com", "Admin Two", "password123", new ArrayList<>(Collections.singletonList(adminRole)));
        createUserIfNotFound("info@manukyan.wine","info@manukyan.wine", "Administration", "mnbvcxz00A!@", new ArrayList<>(Collections.singletonList(adminRole)));
        createUserIfNotFound("user", "user@test.com", "user", "password123", new ArrayList<>(Collections.singletonList(userRole)));
        alreadySetup = true;
    }

    /**
     * Creates a role if it is not found in the repository.
     *
     * @param name The name of the role.
     * @return The created or existing role.
     */
    @Transactional
    public Role createRoleIfNotFound(final String name) {
        var role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role(name);
        }
        role = roleRepository.save(role);
        return role;
    }

    /**
     * Creates a user if not found in the repository.
     *
     * @param username The username of the user.
     * @param email    The email of the user.
     * @param fullName The full name of the user.
     * @param password The password of the user.
     * @param roles    The roles assigned to the user.
     * @return The created or existing user.
     */
    @Transactional
    public User createUserIfNotFound(final String username, final String email, final String fullName, final String password, final Collection<Role> roles) {
        var user = userRepository.findByEmail(email);
        if (user == null) {
            user = new User();
            user.setUsername(username);
            user.setPassword(passwordEncoder.encode(password));
            user.setEmail(email);
            user.setFullName(fullName);
        }
        user.setRoles(roles);
        user = userRepository.save(user);
        return user;
    }
}