package com.bl.mb.models;

import lombok.*;

import java.util.List;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CellarRequest {
    private List<String> images;
    private String englishContent;
    private String armenianContent;
    private String cellarSection;
}


