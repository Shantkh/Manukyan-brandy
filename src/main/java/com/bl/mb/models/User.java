package com.bl.mb.models;

import com.bl.mb.utils.TimeDateConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.UuidGenerator;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@Table(name = "users", schema = "manukyan_db", uniqueConstraints = {
        @UniqueConstraint(columnNames = "email"),
        @UniqueConstraint(columnNames = "username")
})
public class User {
    @Id
    @GeneratedValue
    @UuidGenerator(style = UuidGenerator.Style.TIME)
    @Column(name = "user_id")
    @JsonProperty
    private UUID id;

    @Column(name = "created_on")
    @Convert(converter = TimeDateConverter.class)
    @CreationTimestamp
    private Timestamp createdOn;

    @UpdateTimestamp
    @Column(name = "updated_on")
    @Convert(converter = TimeDateConverter.class)
    private Timestamp updatedOn;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "username", unique = true, nullable = false, length = 100)
    @JsonProperty
    private String username;

    @Column(name = "email", unique = true, nullable = false, length = 100)
    @Email
    @JsonProperty
    private String email;

    @Column(name = "password", nullable = false)
    @JsonProperty
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "user_id"),  // Update this to the correct column name if needed
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    @JsonIgnore
    private Collection<Role> roles;

    @Transient
    @JsonProperty
    private String accessToken;

    @Transient
    @JsonProperty
    private String refreshToken;


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((getEmail() == null) ? 0 : getEmail().hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User user = (User) obj;
        return getEmail().equals(user.getEmail());
    }
}