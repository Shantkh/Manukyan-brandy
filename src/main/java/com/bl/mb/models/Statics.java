package com.bl.mb.models;

import com.bl.mb.utils.TimeDateConverter;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import java.sql.Timestamp;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
@Entity
@Table(name = "statics", schema = "manukyan_db")
public class Statics {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id")
    private UUID id;

    @Column(name = "created_on")
    @Convert(converter = TimeDateConverter.class)
    @CreationTimestamp
    private Timestamp createdOn;

    @Column(name = "page")
    private String page;

    @Column(name = "country")
    private String country;

    @Column(name = "ip")
    private String ip;

}
