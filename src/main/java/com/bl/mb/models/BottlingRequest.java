package com.bl.mb.models;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BottlingRequest {
    private List<String> images;
    private String englishContent;
    private String armenianContent;
    private String bottlingSection;
}


