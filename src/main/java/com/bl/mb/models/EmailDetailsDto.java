package com.bl.mb.models;

import lombok.*;

import java.sql.Timestamp;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
public class EmailDetailsDto {
    private UUID id;
    private String recipient;
    private Timestamp createdDate;
}
