package com.bl.mb.models;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaginationWithSorting implements Serializable {

    private int pageNumber;
    private int contentSize;
    private String sortDirection;
    private String sortElement;
}