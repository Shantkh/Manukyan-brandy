package com.bl.mb.models;

import com.bl.mb.utils.TimeDateConverter;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
public class TourBookingResponse {
    private Tours tour;
    private List<TourBooking> bookings;

}
