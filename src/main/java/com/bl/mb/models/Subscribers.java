package com.bl.mb.models;

import com.bl.mb.utils.TimeDateConverter;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UuidGenerator;

import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "subscribers", schema = "manukyan_db")
public class Subscribers {

    @Id
    @GeneratedValue
    @UuidGenerator(style = UuidGenerator.Style.TIME)
    @Column(name = "subscribers_id")
    @JsonProperty
    private UUID id;

    @Column(name = "created_on")
    @Convert(converter = TimeDateConverter.class)
    @CreationTimestamp
    private Timestamp createdOn;

    @Email
    @Column(name = "email")
    private String email;

}
