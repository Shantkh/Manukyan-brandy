package com.bl.mb.models;


import com.bl.mb.constances.MonthOfYear;
import com.bl.mb.constances.WeekDaysEnum;
import lombok.*;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
public class ToursUpdateRequest {
    private UUID tourId;
    private List<String> tourImages;
    private List<ToursLocalization> tourLocalization;
    private WeekDaysEnum tourDay;
    private MonthOfYear tourMonth;
    private String tourHour;
    private String dateOfTour;
    private double duration;
    private double price;

}
