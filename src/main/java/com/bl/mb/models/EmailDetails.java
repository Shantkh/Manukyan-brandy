package com.bl.mb.models;


import com.bl.mb.constances.EmailTypesEnum;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import java.sql.Timestamp;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@Entity
@Table(name = "email_details", schema = "manukyan_db")
public class EmailDetails {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id")
    private UUID id;

    @Column(name = "recipient")
    private String recipient;

    @Column(name = "msg_bdy", columnDefinition = "TEXT")
    private String msgBody;

    @Column(name = "subject")
    private String subject;

    @Column(name = "attachment")
    private String attachment;

    @Column(name = "code")
    private String code;

    @Column(name = "created_date")
    private Timestamp createdDate;

    @Column(name = "phone")
    private String phone;

    @Column(name = "name")
    private String name;

    @Column(name = "subscribe")
    private boolean subscribe;

    @Column(name = "country")
    private String country;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private EmailTypesEnum emailTypesEnum;
}