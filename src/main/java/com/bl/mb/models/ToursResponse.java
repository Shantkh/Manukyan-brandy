package com.bl.mb.models;

import lombok.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
public class ToursResponse {
    private UUID id;
    private Timestamp createdOn;
    private List<String> tourImages;
    private List<ToursLocalization> tourLocalization;
    private String tourDay;
    private String tourMonth;
    private String tourHour;
    private String dateOfTour;
    private int weekNumber;
    private double duration;
    private double price;

}
