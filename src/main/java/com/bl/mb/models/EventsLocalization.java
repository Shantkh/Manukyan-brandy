package com.bl.mb.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.sql.Timestamp;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "events_localization")
public class EventsLocalization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "events_id", nullable = false)
    @JsonIgnore
    private Events events;

    @Column(name = "locale")
    private String locale;

    @Column(name = "html_title")
    private String htmlTitle;

    @Column(name = "html_short_content")
    private String htmlShortContent;

    @Column(name = "html_long_content", columnDefinition = "TEXT" )
    private String htmlLongContent;


}
