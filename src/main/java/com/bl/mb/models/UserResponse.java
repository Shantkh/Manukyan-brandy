package com.bl.mb.models;

import lombok.*;

import java.util.Collection;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserResponse {
    private UUID id;
    private String username;
    private String email;
    private Collection<String> roles;
    private String accessToken;
    private String refreshToken;
}
