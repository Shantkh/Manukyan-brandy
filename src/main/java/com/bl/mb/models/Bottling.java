package com.bl.mb.models;

import com.bl.mb.utils.TimeDateConverter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
@Entity
@Table(name = "bottling", schema = "manukyan_db")
public class Bottling {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id")
    private UUID id;

    @Column(name = "created_on")
    @Convert(converter = TimeDateConverter.class)
    @CreationTimestamp
    private Timestamp createdOn;

    @ElementCollection

    @CollectionTable(name = "bottling_media", joinColumns = @JoinColumn(name = "bottling_id"))
    @Column(name = "media_link")
    private List<String> images;

    @Column(name = "english_content", columnDefinition = "TEXT")
    private String englishContent;

    @Column(name = "armenian_content", columnDefinition = "TEXT")
    private String armenianContent;

    @Column(name = "bottling_section")
    private String bottlingSection;
}
