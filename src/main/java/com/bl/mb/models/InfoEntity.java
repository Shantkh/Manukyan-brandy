package com.bl.mb.models;

import com.bl.mb.constances.LinksEnum;
import com.bl.mb.utils.TimeDateConverter;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import java.sql.Timestamp;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "info_entity", schema = "manukyan_db")
public class InfoEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id")
    private UUID id;

    @Column(name = "created_on")
    @Convert(converter = TimeDateConverter.class)
    @CreationTimestamp
    private Timestamp createdOn;

    @Column(name = "page_link")
    @Enumerated(EnumType.STRING)
    private LinksEnum link;

    @Column(name = "info_hy")
    private String infoHy;

    @Column(name = "info_en")
    private String infoEn;

    @Column(name = "show_on")
    private String showOn;
}
