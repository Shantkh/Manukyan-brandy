package com.bl.mb.models;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GeoIP {
    private String ipAddress;
    private String city;
    private String latitude;
    private String longitude;
}