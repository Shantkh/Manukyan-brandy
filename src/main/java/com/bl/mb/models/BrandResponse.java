package com.bl.mb.models;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BrandResponse {
    private String  brandNameEnglish;
    private String  brandNameArmenian;
}
