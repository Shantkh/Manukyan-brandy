package com.bl.mb.models;

import lombok.*;

import java.sql.Timestamp;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SubscribersResponse {

    private UUID id;
    private Timestamp createdOn;
    private String email;

}
