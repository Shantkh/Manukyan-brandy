package com.bl.mb.models;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MonthEventsDTO {
    private String month;
    private boolean hasEvents;
    private int eventCount;

    public MonthEventsDTO(String month, Long eventCount, Boolean hasEvents) {
        this.month = month;
        this.eventCount = eventCount.intValue();
        this.hasEvents = hasEvents;
    }
}