package com.bl.mb.models;

import lombok.*;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
public class ResponseStatics {
    private StatisticsResult statisticsResult;
    private List<Map<String, Object>> countries;
    private List<Map<String, Object>> pages;
}
