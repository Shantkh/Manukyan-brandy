package com.bl.mb.models;

import com.bl.mb.utils.TimeDateConverter;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
@Entity
@Table(name = "cellar", schema = "manukyan_db")
public class Cellar {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id")
    private UUID id;

    @Column(name = "created_on")
    @Convert(converter = TimeDateConverter.class)
    @CreationTimestamp
    private Timestamp createdOn;


    @CollectionTable(name = "cellar_media", joinColumns = @JoinColumn(name = "cellar_id"))
    @Column(name = "media_link")
    private List<String> images;

    @Column(name = "english_content", columnDefinition = "TEXT")
    private String englishContent;

    @Column(name = "armenian_content", columnDefinition = "TEXT")
    private String armenianContent;

    @Column(name = "cellar_section")
    private String cellarSection;
}
