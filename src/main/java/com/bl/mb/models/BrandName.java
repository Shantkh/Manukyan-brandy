package com.bl.mb.models;

import com.bl.mb.utils.TimeDateConverter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
@Entity
@Table(name = "brand_name", schema = "manukyan_db")
public class BrandName {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id")
    private UUID id;

    @Column(name = "created_on")
    @Convert(converter = TimeDateConverter.class)
    @CreationTimestamp
    private Timestamp createdOn;

    @Column(name = "brand_name_en")
    private String brandNameEnglish;

    @Column(name = "brand_name_hy")
    private String brandNameArmenian;

    @OneToMany(mappedBy = "brandName", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Brand> brands;

    @OneToMany(mappedBy = "brandName", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Products> productsBrand;
}
