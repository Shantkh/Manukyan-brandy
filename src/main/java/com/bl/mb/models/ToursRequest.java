package com.bl.mb.models;

import com.bl.mb.constances.MonthOfYear;
import com.bl.mb.constances.WeekDaysEnum;
import lombok.*;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
public class ToursRequest {
    private UUID id;
    private List<ToursLocalization> tourLocalization;
    private List<WeekDaysEnum> tourDay;
    private List<MonthOfYear> tourMonth;
    private String tourHour;
    private double duration;
    private Integer price;
    private List<String> image;
}
