package com.bl.mb.models;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UuidGenerator;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "jwt_refresh_token", schema = "manukyan_db")
public class JwtRefreshToken {
    @Id
    @GeneratedValue
    @UuidGenerator(style = UuidGenerator.Style.TIME)
    @Column(name = "token", nullable = false)
    private UUID tokenId;
    @Column(name = "user_id", nullable = false)
    private UUID userId;
    @CreationTimestamp
    @Column(name = "create_date", nullable = false)
    private LocalDateTime createDate;
    @Column(name = "exporation_date", nullable = false)
    private LocalDateTime expirationDate;

}