package com.bl.mb.models;

import com.bl.mb.constances.PagesEnum;
import lombok.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
public class SectionsDto {
    private UUID id;
    private Timestamp createdOn;
    private List<SectionLocalization> localizations;
    private List<String> sliderMedia;
    private PagesEnum pagesEnum;
    private String link;
}
