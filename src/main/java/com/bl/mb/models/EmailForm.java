package com.bl.mb.models;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
public class EmailForm {
    private String name;
    private String email;
    private String subject;
    private String phone;
    private String message;
    private boolean subscribe;
}