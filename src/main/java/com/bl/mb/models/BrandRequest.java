package com.bl.mb.models;

import lombok.*;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BrandRequest {

    private List<String> images;
    private String englishContent;
    private String armenianContent;
    private String brandSection;
    private BrandName brandNameId;

}
