package com.bl.mb.models;

import com.bl.mb.constances.LinksEnum;
import com.bl.mb.constances.SliderEnum;
import com.bl.mb.utils.TimeDateConverter;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "slider_media", schema = "manukyan_db")
public class SliderMedia {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id")
    private UUID id;

    @Column(name = "created_on")
    @Convert(converter = TimeDateConverter.class)
    @CreationTimestamp
    private Timestamp createdOn;

    @OneToMany(mappedBy = "sliderMedia", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<SliderLocalization> sliderLocalizations;

    @Column(name = "slider_name")
    @Enumerated(EnumType.STRING)
    private SliderEnum sliderEnum;

    @Column(name = "media_link")
    private String sliderMedia;

    @Column(name = "page_link")
    @Enumerated(EnumType.STRING)
    private LinksEnum link;
}
