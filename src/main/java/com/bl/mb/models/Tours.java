package com.bl.mb.models;

import com.bl.mb.constances.MonthOfYear;
import com.bl.mb.constances.WeekDaysEnum;
import com.bl.mb.utils.TimeDateConverter;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
@Entity
@Table(name = "tours", schema = "manukyan_db")
public class Tours implements Serializable {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id")
    private UUID id;

    @Column(name = "created_on")
    @Convert(converter = TimeDateConverter.class)
    @CreationTimestamp
    private Timestamp createdOn;

    @ElementCollection
    @CollectionTable(name = "tours_media", joinColumns = @JoinColumn(name = "tours_media_id"))
    @Column(name = "image_link")
    private List<String> tourImages;

    @OneToMany(mappedBy = "tours", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ToursLocalization> tourLocalization;

    @Enumerated(EnumType.STRING)
    @Column(name = "weekday")
    private WeekDaysEnum tourDay;

    @Enumerated(EnumType.STRING)
    @Column(name = "tour_month")
    private MonthOfYear tourMonth;

    @Column(name = "tour_hour")
    private String tourHour;

    @Column(name = "date_of_the_tour")
    private String dateOfTour;

    @Column(name = "week_number")
    private int weekNumber;

    @Column(name = "duration")
    private double duration;

    @Column(name = "price")
    private double price;

}
