package com.bl.mb.models;

import lombok.*;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
public class StatisticsResult {
    private long usersLast30Days;
    private long usersLast15Days;
    private long usersLast7Days;
    private long usersToday;

}