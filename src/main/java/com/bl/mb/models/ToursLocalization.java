package com.bl.mb.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tours_localization")
public class ToursLocalization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "tours_id", nullable = false)
    @JsonIgnore
    private Tours tours;

    @Column(name = "locale")
    private String locale;

    @Column(name = "html_title", columnDefinition = "TEXT")
    private String htmlTitle;

    @Column(name = "html_short_content", columnDefinition = "TEXT")
    private String htmlShortContent;

    @Column(name = "html_long_content", columnDefinition = "TEXT")
    private String htmlLongContent;
}
