package com.bl.mb.models;

import com.bl.mb.constances.LanguageEnum;
import com.bl.mb.utils.TimeDateConverter;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import java.sql.Timestamp;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
@Entity
@Table(name = "tours_booking", schema = "manukyan_db")
public class TourBooking {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id")
    private UUID id;

    @Column(name = "created_on")
    @Convert(converter = TimeDateConverter.class)
    @CreationTimestamp
    private Timestamp createdOn;

    @Column(name = "tour_id")
    private UUID tourId;

    @Column(name = "tour_language")
    private String  tourLanguage;

    @Column(name = "name ")
    private String name;

    @Column(name = "surname ")
    private String surname;

    @Column(name = "email")
    @Email
    private String email;

    @Column(name = "phone ")
    private String phone;

    @Column(name = "country ")
    private String country;

    @Column(name = "attendees")
    private String attendees;

    @Column(name = "total_amount")
    private String totalAmount;

    @Column(name = "age_confirmation")
    private Boolean ageConfirmation = true;

}
