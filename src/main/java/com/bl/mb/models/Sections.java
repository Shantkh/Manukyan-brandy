package com.bl.mb.models;

import com.bl.mb.constances.LinksEnum;
import com.bl.mb.constances.PagesEnum;
import com.bl.mb.utils.TimeDateConverter;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "sections", schema = "manukyan_db")
public class Sections {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id")
    private UUID id;

    @Column(name = "created_on")
    @Convert(converter = TimeDateConverter.class)
    @CreationTimestamp
    private Timestamp createdOn;

    @OneToMany(mappedBy = "sections", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<SectionLocalization> localizations;

    @Column(name = "page_name")
    @Enumerated(EnumType.STRING)
    private PagesEnum pagesEnum;

    @ElementCollection
    @CollectionTable(name = "sections_media", joinColumns = @JoinColumn(name = "sections_id"))
    @Column(name = "media_link")
    private List<String> sliderMedia;

    @Column(name = "page_link")
    @Enumerated(EnumType.STRING)
    private LinksEnum link;
}
