package com.bl.mb.models;

import com.bl.mb.constances.MonthOfYear;
import com.bl.mb.constances.WeekDaysEnum;
import lombok.*;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
public class ToursOneRequest {
    private List<ToursLocalization> tourLocalization;
    private String tourHour;
    private double duration;
    private Integer price;
    private Date tourDate;
    private List<String> image;
}
