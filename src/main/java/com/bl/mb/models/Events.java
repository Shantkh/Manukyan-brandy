package com.bl.mb.models;

import com.bl.mb.utils.TimeDateConverter;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
@Entity
@Table(name = "events", schema = "manukyan_db")
public class Events {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "id")
    private UUID id;

    @Column(name = "created_on")
    @Convert(converter = TimeDateConverter.class)
    @CreationTimestamp
    private Timestamp createdOn;

    @OneToMany(mappedBy = "events", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<EventsLocalization> localizations;

    @ElementCollection
    @CollectionTable(name = "events_media", joinColumns = @JoinColumn(name = "events_id"))
    @Column(name = "media_link")
    private List<String> sliderMedia;

    @Column(name = "event_start_date")
    private Timestamp eventStartDate;

    @Column(name = "event_end_date")
    private Timestamp eventEndDate;

    @Column(name = "event_visible")
    private boolean isEventVisible = true;
}
