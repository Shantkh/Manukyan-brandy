package com.bl.mb.models;

import com.bl.mb.constances.LinksEnum;
import lombok.*;

import java.sql.Timestamp;
import java.util.Map;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
public class SliderDto<T, R> {
    private UUID id;
    private Timestamp createdOn;
    private Map<T,R> data;
    private LinksEnum link;

}
