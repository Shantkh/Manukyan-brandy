package com.bl.mb.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "slider_localization")
public class SliderLocalization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "slider_media_id", nullable = false)
    @JsonIgnore
    private SliderMedia sliderMedia;

    @Column(name = "locale")
    private String locale;

    @Column(name = "html_content", columnDefinition = "TEXT" )
    private String htmlContent;
}
