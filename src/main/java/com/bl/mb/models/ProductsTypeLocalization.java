package com.bl.mb.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "products_type_localization")
public class ProductsTypeLocalization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "prodcuts_id", nullable = false)
    @JsonIgnore
    private Products products;

    @Column(name = "locale")
    private String locale;

    @Column(name = "html_content", columnDefinition = "TEXT" )
    private String htmlContent;
}
