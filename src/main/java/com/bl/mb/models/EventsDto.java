package com.bl.mb.models;

import com.bl.mb.utils.TimeDateConverter;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import lombok.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
public class EventsDto {
    private UUID id;
    private Timestamp createdOn;
    private List<EventsLocalization> localizations;
    private List<String> sliderMedia;
    private Timestamp eventStartDate;
    private Timestamp eventEndDate;
    private boolean isEventVisible;
}
