package com.bl.mb.models;

import lombok.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Builder
@Getter
public class ToursIdResponse {
    private UUID id;
    private Timestamp createdOn;
    private String tourImages;
    private String htmlTitle;
    private String htmlShortContent;
    private String htmlLongContent;
    private String tourDay;
    private String tourMonth;
    private String tourHour;
    private String dateOfTour;
    private int weekNumber;
    private double duration;
    private double price;

}
