package com.bl.mb.utils;

import org.springframework.stereotype.Component;

@Component
public class LocaleChecker {

    public static String checkLocale(String locale) {
        return !locale.contains("hy") ? "en" : "hy";
    }
}
