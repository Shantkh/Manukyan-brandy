package com.bl.mb.utils;

import com.bl.mb.models.EmailDetails;
import com.bl.mb.templates.EmailTemplates;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.UUID;


@RequiredArgsConstructor
@Component
public class EmailUtils {

    /**
     * Utility method for creating an EmailDetails object with a randomly generated forgot password code.
     * This method uses VerificationUtils to generate a random number string as the code.
     * The created EmailDetails object includes the generated code, email message body, subject, recipient,
     * unique identifier, and creation date.
     *
     * @param email  The email address of the recipient.
     * @param action The subject or action related to the email.
     * @return An instance of {@link EmailDetails} containing details for the forgot password email.
     * @throws NullPointerException if the generated code is null (unlikely due to UUID.randomUUID() usage).
     */
    public static EmailDetails createdForgotPasswordCode(String email, String action) {
        VerificationUtils verificationUtils = new VerificationUtils();
        var code = Objects.requireNonNull(verificationUtils).getRandomNumberString();
        var emailDetail = EmailDetails.builder().code(code).build();
        return EmailDetails.builder()
                .id(UUID.randomUUID())
                .msgBody(EmailTemplates.forgotPasswordCode(emailDetail))
                .subject(action)
                .recipient(email)
                .createdDate(DateUtils.newTimeStamp())
                .code(code)
                .build();
    }
}
