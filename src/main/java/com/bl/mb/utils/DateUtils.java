package com.bl.mb.utils;

import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Date;

@Component
public class DateUtils {

    /**
     * Creates a new {@link Timestamp} representing the current date and time.
     *
     * @return A new {@link Timestamp} for the current date and time.
     */
    public static Timestamp newTimeStamp(){
        return new Timestamp(new Date().getTime());
    }

}