package com.bl.mb.utils;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
public final class AppPreconditions {

    public static <T> List<T> checkNotNullOrEmptyList(final List<T> t) throws Exception {
        List<T> checked = Objects.requireNonNull(t);
        if (checked.size() == 0) throw new Exception(AppPreconditions.class.getName() + " checkNotNullOrEmptyList");
        return checked;
    }

    public static <T> T checkNotNullOrEmpty(final T t) throws Exception {
        if (t == null)
            throw new Exception(AppPreconditions.class.getName() + " checkNotNullOrEmpty");
        return t;
    }


    public static <T> T checkLimitationOfString(final T t, final String... a) throws Exception {
        boolean isEquals = false;
        for (String s : a) {
            if (t.equals(s)) {
                isEquals = true;
                break;
            }
        }
        if (!isEquals) {
            throw new Exception(AppPreconditions.class.getName() + " checkLimitationOfString");
        }
        return t;
    }
}
