package com.bl.mb.utils;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.util.StringUtils;

public class SecurityUtils {
    public static final String ROLE_PREFIX = "ROLE_";
    public static final String AUTH_HEADER = "Authorization";
    public static final String AUTH_TOKEN_HEADER = "Bearer";
    public static final String AUTH_TOKEN_PREFIX = AUTH_TOKEN_HEADER + " ";

    /**
     * Converts a role string to a Spring Security {@link SimpleGrantedAuthority}.
     *
     * @param role The role to convert.
     * @return The corresponding {@link SimpleGrantedAuthority}.
     */
    public static SimpleGrantedAuthority convertRoleToAuthority(String role) {
        String formattedRole = role.startsWith(ROLE_PREFIX) ? role : ROLE_PREFIX + role;
        return new SimpleGrantedAuthority(formattedRole);

    }

    /**
     * Extracts the role name from a role string.
     *
     * @param roleString The complete role string.
     * @return The extracted role name.
     */
    public static String extractRoleName(String roleString) {
        int nameStartIndex = roleString.indexOf("name=");
        int nameEndIndex = roleString.indexOf("]", nameStartIndex);
        if (nameStartIndex != -1 && nameEndIndex != -1) {
            return roleString.substring(nameStartIndex + "name=".length(), nameEndIndex);
        }
        return roleString;
    }

    /**
     * Extracts the authentication token from the request's Authorization header.
     *
     * @param request The HTTP servlet request.
     * @return The extracted authentication token or null if not present.
     */
    public static String extractAuthTokenFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader(AUTH_HEADER);
        if (StringUtils.hasLength(bearerToken) && bearerToken.startsWith(AUTH_TOKEN_PREFIX)) {
            return bearerToken.substring(7);
        }
        return null;
    }

}