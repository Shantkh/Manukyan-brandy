package com.bl.mb.utils;

import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Convert;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

@Convert(disableConversion = true)
public class TimeDateConverter implements AttributeConverter<Timestamp, String> {

    private static final String DATE_FORMAT = "MM/dd/yyyy HH:mm:ss";

    /**
     * Converts a {@link Timestamp} to a formatted {@link String} for storing in the database.
     *
     * @param attribute The {@link Timestamp} attribute to convert.
     * @return The formatted {@link String} representation of the timestamp.
     */
    @Override
    public String convertToDatabaseColumn(final Timestamp attribute) {
        try {
            if (attribute == null) {
                return null;
            }

            SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
            return dateFormat.format(attribute);
        } catch (Exception e) {
            return "convertToDatabaseColumn " + e.getMessage();
        }
    }

    /**
     * Converts a formatted {@link String} from the database to a {@link Timestamp}.
     *
     * @param dbData The formatted {@link String} from the database.
     * @return The corresponding {@link Timestamp} or a default value if parsing fails.
     */
    @Override
    public Timestamp convertToEntityAttribute(final String dbData) {
        if (dbData == null) {
            return null; // or return some default value if needed
        }

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
            Date parsedDate = dateFormat.parse(dbData);
            return new Timestamp(parsedDate.getTime());
        } catch (Exception e) {
            return null;
        }
    }
}
