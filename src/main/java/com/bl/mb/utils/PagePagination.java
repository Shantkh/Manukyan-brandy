package com.bl.mb.utils;

import com.bl.mb.models.PaginationWithSorting;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

@Component
public class PagePagination {

    /**
     * Creates a Spring Data PageRequest object based on the provided pagination and sorting parameters.
     *
     * @param paginationWithSorting The object containing pagination and sorting information.
     * @return PageRequest configured with the specified page number, content size, sort direction, and sort element.
     * @throws Exception If there is an issue creating the PageRequest.
     */
    public PageRequest pagePagination(PaginationWithSorting paginationWithSorting) throws Exception {
        return PageRequest.of(
                paginationWithSorting.getPageNumber(),
                paginationWithSorting.getContentSize(),
                Sort.Direction.fromString(paginationWithSorting.getSortDirection()),
                paginationWithSorting.getSortElement()
        );
    }
}