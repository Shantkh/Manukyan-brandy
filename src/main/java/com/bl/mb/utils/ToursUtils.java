package com.bl.mb.utils;

import com.bl.mb.constances.MonthOfYear;
import com.bl.mb.constances.WeekDaysEnum;
import org.springframework.stereotype.Component;

import java.time.*;
import java.time.temporal.TemporalAdjusters;

@Component
public class ToursUtils {

    /**
     * Retrieves the LocalDate based on the specified year, month, day of the week, and week number.
     *
     * @param year           The year.
     * @param monthNumber    The month represented as a number.
     * @param weekDaysEnum   The day of the week.
     * @param weekNumber     The week number.
     * @return LocalDate object representing the specified date.
     */
    public static LocalDate getDateFromMonthAndDay(int year, int monthNumber, WeekDaysEnum weekDaysEnum, int weekNumber) {
        LocalDate firstDayOfMonth = LocalDate.of(year, monthNumber, 1);
        DayOfWeek dayOfWeek = DayOfWeek.valueOf(weekDaysEnum.name());
        LocalDate firstSpecifiedDay = firstDayOfMonth.with(TemporalAdjusters.nextOrSame(dayOfWeek));
        return firstSpecifiedDay.plusWeeks(weekNumber - 1);
    }

    /**
     * Counts the number of days in the specified month, considering the given day of the week.
     *
     * @param year           The year.
     * @param weekDaysEnum   The day of the week.
     * @param monthOfYear    The month.
     * @return The number of days in the month with the specified criteria.
     */
    public static int countDaysInMonth(int year, WeekDaysEnum weekDaysEnum, MonthOfYear monthOfYear) {
        var monthInt = getMonthNumber(monthOfYear);
        var dayOfWeek = DayOfWeek.valueOf(weekDaysEnum.name());
        YearMonth yearMonth = YearMonth.of(year, monthInt); // April is represented by the month number 4
        LocalDate firstDayOfWeek = yearMonth.atDay(1).with(TemporalAdjusters.nextOrSame(dayOfWeek));
        LocalDate lastDayOfWeek = yearMonth.atEndOfMonth().with(TemporalAdjusters.previousOrSame(dayOfWeek));

        int dayCount = 0;
        LocalDate currentDay = firstDayOfWeek;
        while (!currentDay.isAfter(lastDayOfWeek)) {
            dayCount++;
            currentDay = currentDay.plusWeeks(1);
        }

        return dayCount;
    }

    /**
     * Retrieves the month number based on the specified MonthOfYear enum.
     *
     * @param monthOfYear The MonthOfYear enum.
     * @return The month number or -1 if the enum is not recognized.
     */
    public static int getMonthNumber(MonthOfYear monthOfYear) {
        try {
            Month month = Month.valueOf(String.valueOf(monthOfYear));
            return month.getValue();
        } catch (IllegalArgumentException e) {
            return -1;
        }
    }

    /**
     * Retrieves the current year.
     *
     * @return The current year.
     */
    public static int getCurrentYear() {
        return Year.now().getValue();
    }
}
