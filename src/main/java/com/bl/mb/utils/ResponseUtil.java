package com.bl.mb.utils;

import com.bl.mb.models.ResponseBase;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class ResponseUtil {

    /**
     * Creates a response indicating an error with the provided message.
     *
     * @param t The error message.
     * @return A response containing the error message and details.
     */
    public static <T> ResponseBase<?> ResponseError(T t) {
        return ResponseBase.builder()
                .data(t)
                .httpStatus(HttpStatus.BAD_REQUEST)
                .httpStatusCode(HttpStatus.BAD_REQUEST.value())
                .date(DateUtils.newTimeStamp())
                .isSuccess(false)
                .build();
    }

    /**
     * Utility method for generating a standard response indicating a resource was not found.
     *
     * @param <T> The type of data associated with the response.
     * @param t   The data representing the resource that was not found.
     * @return ResponseBase containing the specified data, HTTP status NOT_FOUND, and additional response details.
     */
    public static <T> ResponseBase<?> ResponseNotFound(T t) {
        return ResponseBase.builder()
                .data(t)
                .httpStatus(HttpStatus.NOT_FOUND)
                .httpStatusCode(HttpStatus.NOT_FOUND.value())
                .date(DateUtils.newTimeStamp())
                .isSuccess(false)
                .build();
    }

    /**
     * Creates a successful response using the provided data.
     *
     * @param <T> The type of data to be included in the response.
     * @param t   The data to be included in the response.
     * @return A {@link ResponseBase} instance representing a successful response.
     *         It includes the provided data, an HTTP status of {@code OK (200)},
     *         the corresponding HTTP status code, the current timestamp, and a success flag.
     * @see ResponseBase
     * @see HttpStatus#OK
     * @see DateUtils#newTimeStamp()
     */
    public static <T> ResponseBase<?> ResponseSuccess(T t) {
        return ResponseBase.builder()
                .data(t)
                .httpStatus(HttpStatus.OK)
                .httpStatusCode(HttpStatus.OK.value())
                .date(DateUtils.newTimeStamp())
                .isSuccess(true)
                .build();
    }

    /**
     * Utility method for generating a standard response indicating a resource was successfully created.
     *
     * @param <T> The type of data associated with the response.
     * @param t   The data representing the created resource.
     * @return ResponseBase containing the specified data, HTTP status CREATED, and additional response details.
     */
    public static <T> ResponseBase<?> ResponseCreated(T t) {
        return ResponseBase.builder()
                .data(t)
                .httpStatus(HttpStatus.CREATED)
                .httpStatusCode(HttpStatus.CREATED.value())
                .date(DateUtils.newTimeStamp())
                .isSuccess(true)
                .build();
    }

    /**
     * Utility method for generating a standard response indicating unauthorized access.
     *
     * @param <T> The type of data associated with the response.
     * @param t   The data representing the reason for unauthorized access.
     * @return ResponseBase containing the specified data, HTTP status UNAUTHORIZED, and additional response details.
     */
    public static <T> ResponseBase<?> ResponseUnauthorized(T t) {
        return ResponseBase.builder()
                .data(t)
                .httpStatus(HttpStatus.UNAUTHORIZED)
                .httpStatusCode(HttpStatus.UNAUTHORIZED.value())
                .date(DateUtils.newTimeStamp())
                .isSuccess(true)
                .build();
    }
}