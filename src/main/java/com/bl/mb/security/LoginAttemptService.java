package com.bl.mb.security;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.cache.LoadingCache;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Service
public class LoginAttemptService {

    public static final int MAX_ATTEMPT = 10;
    private final LoadingCache<String, Integer> attemptsCache;

    @Autowired
    private HttpServletRequest request;

    /**
     * Constructs a new {@code LoginAttemptService} with a loading cache that expires after one day.
     */
    public LoginAttemptService() {
        super();
        attemptsCache = CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.DAYS).build(new CacheLoader<String, Integer>() {
            @Override
            public Integer load(final String key) {
                return 0;
            }
        });
    }

    /**
     * Increments the login attempts for the specified key.
     *
     * @param key The key associated with the login attempts (e.g., user IP address).
     */
    public void loginFailed(final String key) {
        int attempts;
        try {
            attempts = attemptsCache.get(key);
        } catch (final ExecutionException e) {
            attempts = 0;
        }
        attempts++;
        attemptsCache.put(key, attempts);
    }

    /**
     * Checks whether the client is blocked due to exceeding the maximum login attempts.
     *
     * @return {@code true} if the client is blocked, {@code false} otherwise.
     */
    public boolean isBlocked() {
        try {
            return attemptsCache.get(getClientIP()) >= MAX_ATTEMPT;
        } catch (final ExecutionException e) {
            return false;
        }
    }

    /**
     * Retrieves the client's IP address from the request headers.
     *
     * @return The client's IP address.
     */
    private String getClientIP() {
        final String xfHeader = request.getHeader("X-Forwarded-For");
        if (xfHeader != null) {
            return xfHeader.split(",")[0];
        }
        return request.getRemoteAddr();
    }
}