package com.bl.mb.security.service;


import com.bl.mb.jwt.JwtProvider;
import com.bl.mb.models.JwtRefreshToken;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.models.TokenRequest;
import com.bl.mb.models.User;
import com.bl.mb.repo.JwtRefreshTokenRepository;
import com.bl.mb.repo.UserRepository;
import com.bl.mb.security.UserPrincipal;
import com.bl.mb.utils.DateUtils;
import com.bl.mb.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Set;
import java.util.UUID;

@Service
public class JwtRefreshTokenServiceImpl implements JwtRefreshTokenService {

    @Value("${app.refreshToken.refresh}")
    private Long REFRESH_EXPIRATION;

    @Autowired
    private JwtRefreshTokenRepository jwtRefreshTokenRepository;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtProvider jwtProvider;

    /**
     * Creates a new refresh token for the specified user.
     *
     * @param userId The ID of the user for whom the refresh token is created.
     * @return The newly created JwtRefreshToken.
     */
    @Override
    public JwtRefreshToken createRefreshToken(UUID userId) {
        JwtRefreshToken jwtRefreshToken = new JwtRefreshToken();
        jwtRefreshToken.setTokenId(UUID.randomUUID());
        jwtRefreshToken.setUserId(userId);
        jwtRefreshToken.setCreateDate(LocalDateTime.now());
        jwtRefreshToken.setExpirationDate(LocalDateTime.now().plus(REFRESH_EXPIRATION, ChronoUnit.MILLIS));
        return jwtRefreshTokenRepository.save(jwtRefreshToken);
    }

    /**
     * Generates a new access token based on a valid refresh token.
     *
     * @param tokenRequest The TokenRequest containing the refresh token.
     * @return ResponseBase containing the new access token and user information.
     */
    @Override
    public ResponseBase<?> generateAccessTokenFromRefreshToken(TokenRequest tokenRequest) {
        try {
            UUID token = UUID.fromString(tokenRequest.getToken());
            JwtRefreshToken jwtRefreshToken = jwtRefreshTokenRepository.findById(token).orElseThrow();
            if (jwtRefreshToken.getExpirationDate().isBefore(LocalDateTime.now())) {
                throw new RuntimeException("JWT refresh token is not valid.");
            }

            User user = userRepository.findById(jwtRefreshToken.getUserId()).orElseThrow();

            UserPrincipal userPrincipal = UserPrincipal.builder()
                    .id(user.getId())
                    .username(user.getUsername())
                    .password(user.getPassword())
                    .authorities(Set.of(SecurityUtils.convertRoleToAuthority(user.getRoles().toString())))
                    .build();
            String accessToken = jwtProvider.generateToken(userPrincipal);
            user.setAccessToken(accessToken);
            user.setRefreshToken(String.valueOf(token));
            return ResponseBase.builder()
                    .date(DateUtils.newTimeStamp())
                    .data(user)
                    .httpStatus(HttpStatus.OK)
                    .httpStatusCode(HttpStatus.OK.value())
                    .isSuccess(true)
                    .build();
        }catch (Exception e){
            return ResponseBase.builder()
                    .date(DateUtils.newTimeStamp())
                    .data(e.getMessage())
                    .httpStatus(HttpStatus.BAD_REQUEST)
                    .httpStatusCode(HttpStatus.BAD_REQUEST.value())
                    .isSuccess(false)
                    .build();
        }
    }
}
