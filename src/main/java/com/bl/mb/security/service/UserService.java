package com.bl.mb.security.service;

import com.bl.mb.models.ResponseBase;
import com.bl.mb.models.User;

import java.util.Optional;

public interface UserService {

    Optional<User> findByUsername(String username);
    User findByEmail(String email);

    ResponseBase<?> user(String username);

    ResponseBase<?> userById(String userId);

}