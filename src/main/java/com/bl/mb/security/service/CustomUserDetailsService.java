package com.bl.mb.security.service;

import com.bl.mb.models.User;
import com.bl.mb.security.LoginAttemptService;
import com.bl.mb.security.UserPrincipal;
import com.bl.mb.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;
    @Autowired
    private LoginAttemptService loginAttemptService;

    /**
     * Loads user details by username and constructs a UserPrincipal.
     *
     * @param username The username for which user details should be loaded.
     * @return UserDetails containing the user details.
     * @throws UsernameNotFoundException If the user with the specified username is not found.
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (loginAttemptService.isBlocked()) {
            throw new RuntimeException("blocked");
        }

        User user = userService.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found with username" + username));
        Set<GrantedAuthority> authorities = Set.of(SecurityUtils.convertRoleToAuthority(user.getRoles().toString()));

        return UserPrincipal.builder()
                .user(user)
                .id(user.getId())
                .username(user.getUsername())
                .password(user.getPassword())
                .authorities(authorities)
                .build();
    }

}
