package com.bl.mb.security.service;

import com.bl.mb.models.JwtRefreshToken;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.models.TokenRequest;

import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public interface JwtRefreshTokenService {
    JwtRefreshToken createRefreshToken(UUID userId);

    ResponseBase<?> generateAccessTokenFromRefreshToken(TokenRequest refreshTokenId) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException;

}