package com.bl.mb.security.service;


import com.bl.mb.mapper.UserMapping;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.models.User;
import com.bl.mb.repo.UserRepository;
import com.bl.mb.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserMapping userMapping;


    @Autowired
    public UserServiceImpl(final UserRepository userRepository,
                           final PasswordEncoder passwordEncoder,
                           final UserMapping userMapping) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.userMapping = userMapping;
    }


    /**
     * Retrieves a user by their username.
     *
     * @param username The username of the user to retrieve.
     * @return An optional containing the user if found, empty otherwise.
     */
    @Override
    public Optional<User> findByUsername(final String username) {
        return userRepository.findByUsername(username);
    }

    /**
     * Retrieves a user by their email address.
     *
     * @param email The email address of the user to retrieve.
     * @return The user with the specified email address.
     */
    @Override
    public User findByEmail(final String email) {
        return userRepository.findByEmail(email);
    }

    /**
     * Retrieves user information by username.
     *
     * @param username The username of the user to retrieve.
     * @return A response containing user information or an error message.
     */
    @Override
    public ResponseBase<?> user(String username) {
        try {
            var user = userRepository.findByUsername(username);
            return ResponseBase.builder()
                    .data(user)
                    .date(DateUtils.newTimeStamp())
                    .isSuccess(true)
                    .httpStatus(HttpStatus.OK)
                    .httpStatusCode(HttpStatus.OK.value())
                    .build();
        } catch (Exception e) {
            return ResponseBase.builder()
                    .data(e.getMessage())
                    .date(DateUtils.newTimeStamp())
                    .isSuccess(false)
                    .httpStatus(HttpStatus.BAD_REQUEST)
                    .httpStatusCode(HttpStatus.BAD_REQUEST.value())
                    .build();
        }
    }

    /**
     * Retrieves user information by user ID.
     *
     * @param userId The UUID of the user to retrieve.
     * @return A response containing user information or an error message.
     */
    @Override
    public ResponseBase<?> userById(String userId) {
        try {
            var user = userRepository.findById(UUID.fromString(userId));
            var result = userMapping.toDto(user.get());
            return ResponseBase.builder()
                    .data(result)
                    .date(DateUtils.newTimeStamp())
                    .isSuccess(true)
                    .httpStatus(HttpStatus.OK)
                    .httpStatusCode(HttpStatus.OK.value())
                    .build();
        } catch (Exception e) {
            return ResponseBase.builder()
                    .data(e.getMessage())
                    .date(DateUtils.newTimeStamp())
                    .isSuccess(false)
                    .httpStatus(HttpStatus.BAD_REQUEST)
                    .httpStatusCode(HttpStatus.BAD_REQUEST.value())
                    .build();
        }
    }
}
