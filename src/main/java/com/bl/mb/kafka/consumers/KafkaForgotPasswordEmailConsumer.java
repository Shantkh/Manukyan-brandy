package com.bl.mb.kafka.consumers;

import com.bl.mb.constances.KafkaConstants;
import com.bl.mb.models.EmailDetails;
import com.bl.mb.repo.EmailRepository;
import com.bl.mb.service.NotificationService;
import com.bl.mb.utils.DateUtils;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import jakarta.mail.internet.MimeMessage;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import com.sendgrid.*;
import java.io.IOException;
@RequiredArgsConstructor
@Service
@Slf4j
public class KafkaForgotPasswordEmailConsumer {

    private final EmailRepository emailRepository;

    @Value("${spring.sendgrid.api-key}")
    private String apiKey;

    @Transactional
//    @KafkaListener(topics = KafkaConstants.CREDENTIAL_TOPIC,
//            groupId = KafkaConstants.GROUP_CREDENTIALS_ID)
    public void consume(EmailDetails data) {
        Email from = new Email("info@balians-it.com");
        String subject = data.getSubject();
        Email to = new Email(data.getRecipient());
        Content content = new Content("text/html", data.getMsgBody());
        Mail mail = new Mail(from, subject, to, content);

        log.info("mail P{} ", mail);
        SendGrid sg = new SendGrid(apiKey);
        Request request = new Request();

        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = sg.api(request);
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody());
            System.out.println(response.getHeaders());
        } catch (IOException e) {
            log.error("Email sending exception: {}", e.getMessage());
            throw new RuntimeException("Error sending to sendgrid", e);
        }

        try {
            data.setCreatedDate(DateUtils.newTimeStamp());
            emailRepository.save(data);
            log.info(String.format("Message received -> %s %s %s", data.getRecipient(), data.getId(), data.getCode()));
        } catch (Exception e) {
            // Handle exceptions related to database saving
            log.error("Database saving exception: {}", e.getMessage());
            throw new RuntimeException("Error saving to database", e);
        }
    }
}