package com.bl.mb.kafka.utils;

import com.bl.mb.constances.EmailTypesEnum;
import com.bl.mb.kafka.producer.KafkaContactUsProducer;
import com.bl.mb.kafka.producer.KafkaForgotPasswordEmailProducer;
import com.bl.mb.kafka.producer.KafkaTourBookingProducer;
import com.bl.mb.models.EmailDetails;
import com.bl.mb.utils.DateUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import static com.bl.mb.templates.EmailTemplates.getEmailBody;

@RequiredArgsConstructor
@Component
@Log4j2
public class KafkaEmailUtils {
//    private final KafkaForgotPasswordEmailProducer kafkaProducer;
//    private final KafkaContactUsProducer kafkaContactUsProducer;
//    private final KafkaTourBookingProducer kafkaTourBookingProducer;
//
//    public void userForgotPasswordActivity(EmailDetails u) {
//        EmailDetails emailDetails = EmailDetails.builder()
//                .id(u.getId())
//                .emailTypesEnum(EmailTypesEnum.FORGOT)
//                .recipient(u.getRecipient())
//                .subject(u.getSubject())
//                .msgBody(u.getMsgBody() == null ? getEmailMessage(EmailTypesEnum.FORGOT, u) : u.getMsgBody())
//                .code(u.getCode())
//                .build();
//
//        kafkaProducer.sendMessage(emailDetails);
//    }
//
//    public void userContactUsActivity(EmailDetails u) {
//        EmailDetails emailDetails = EmailDetails.builder()
//                .id(u.getId())
//                .createdDate(DateUtils.newTimeStamp())
//                .emailTypesEnum(EmailTypesEnum.CONTACTUS)
//                .name(u.getName())
//                .recipient(u.getRecipient())
//                .subject(u.getSubject())
//                .msgBody(u.getMsgBody() == null ? getEmailMessage(EmailTypesEnum.CONTACTUS, u) : u.getMsgBody())
//                .phone(u.getPhone())
//                .subscribe(u.isSubscribe())
//                .country(u.getCountry())
//                .build();
//
//        kafkaContactUsProducer.sendContactMessage(emailDetails);
//    }
//
//    public void userTourBookingActivity(EmailDetails u) {
//        EmailDetails emailDetails = EmailDetails.builder()
//                .createdDate(DateUtils.newTimeStamp())
//                .emailTypesEnum(EmailTypesEnum.TOUR)
//                .name(u.getName())
//                .recipient(u.getRecipient())
//                .subject("Tour Booking confirmation email.")
//                .msgBody(u.getMsgBody() == null ? getEmailMessage(EmailTypesEnum.TOUR, u) : u.getMsgBody())
//                .phone(u.getPhone())
//                .build();
//
//        kafkaTourBookingProducer.sendBookingMessage(emailDetails);
//    }
//
//    private String getEmailMessage(EmailTypesEnum emailTypesEnum, EmailDetails u) {
//        return getEmailBody(emailTypesEnum, u);
//    }
}