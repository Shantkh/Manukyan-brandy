package com.bl.mb.config;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.constraints.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

@Configuration
public class CustomLocaleResolver 
             extends AcceptHeaderLocaleResolver
             implements WebMvcConfigurer {

   List<Locale> LOCALES = Arrays.asList(
         new Locale("en"),
         new Locale("hy"));

   /**
    * Resolves the user's locale based on the "Accept-Language" header.
    *
    * @param request The HttpServletRequest object containing the "Accept-Language" header.
    * @return The resolved locale based on the header or the default locale if the header is not present.
    */
   @Override
   public @NotNull Locale resolveLocale(HttpServletRequest request) {
      String headerLang = request.getHeader("Accept-Language");
      return headerLang == null || headerLang.isEmpty()
            ? Locale.getDefault()
            : Locale.lookup(Locale.LanguageRange.parse(headerLang), LOCALES);
   }

   /**
    * Configures a ResourceBundleMessageSource for handling message localization.
    *
    * @return ResourceBundleMessageSource configured with the basename and encoding.
    */
   @Bean
   public ResourceBundleMessageSource messageSource() {
      ResourceBundleMessageSource rs = new ResourceBundleMessageSource();
      rs.setBasename("messages");
      rs.setDefaultEncoding("UTF-8");
      return rs;
   }
}