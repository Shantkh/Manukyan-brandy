package com.bl.mb.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfig {

    /**
     * Creates a ModelMapper bean.
     *
     * @return The configured ModelMapper bean.
     */
    @Bean
    public ModelMapper mapper(){
        return new ModelMapper();
    }
}