package com.bl.mb.controller;

import com.bl.mb.models.*;
import com.bl.mb.security.service.JwtRefreshTokenService;
import com.bl.mb.service.AuthenticationServiceImpl;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth/")
@CrossOrigin(origins = "*", maxAge = 3600)
public class AuthenticationController {

    private final AuthenticationServiceImpl authenticationService;

    private final JwtRefreshTokenService jwtRefreshTokenService;

    public AuthenticationController(AuthenticationServiceImpl authenticationService, JwtRefreshTokenService jwtRefreshTokenService) {
        this.authenticationService = authenticationService;
        this.jwtRefreshTokenService = jwtRefreshTokenService;
    }


    /**
     * Handles the sign-in request.
     *
     * @param user The user credentials for sign-in.
     * @return ResponseEntity containing the result of the sign-in operation.
     * @throws Exception If an error occurs during sign-in.
     */
    @PostMapping("signin")
    public ResponseEntity<ResponseBase<?>> signIn(@RequestBody final User user) throws Exception {
        var result = authenticationService.signInAndReturnJwt(user);
        return new ResponseEntity<>(result, result.getHttpStatus());
    }

    /**
     * Handles the refresh token request.
     *
     * @param tokenRequest The request containing the refresh token.
     * @return ResponseEntity containing the result of the refresh token operation.
     * @throws Exception If an error occurs during token refresh.
     */
    @PostMapping("refresh-token")
    public ResponseEntity<ResponseBase<?>> refreshToken(@RequestBody TokenRequest tokenRequest) throws Exception {
        var result = jwtRefreshTokenService.generateAccessTokenFromRefreshToken(tokenRequest);
        return new ResponseEntity<>(result, result.getHttpStatus());
    }

    /**
     * Handles the sign-out request.
     *
     * @return ResponseEntity containing the result of the sign-out operation.
     */
    @PostMapping("signout")
    private ResponseEntity<ResponseBase<?>> signOut() {
        var result = authenticationService.signOut();
        return new ResponseEntity<>(result, result.getHttpStatus());
    }

    /**
     * Initiates the process for resetting a user's password.
     *
     * @param forgotPasswordRequest The request containing necessary details for password reset.
     * @param request               The HTTP servlet request.
     * @return ResponseEntity<ResponseBase<?>> A response entity containing the result of the forgot password operation.
     */
    @PostMapping("forgot-password")
    public ResponseEntity<ResponseBase<?>> forgotPassword(@RequestBody ForgotPasswordRequest forgotPasswordRequest,
                                                             HttpServletRequest request) {
        var response = authenticationService.forgotPassword(forgotPasswordRequest,request);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    /**
     * Updates the user's password with new credentials.
     *
     * @param updatePassword The request containing the user's updated password.
     * @return ResponseEntity<ResponseBase<?>> A response entity containing the result of the password update operation.
     */
    @PostMapping("new-credentials")
    public ResponseEntity<ResponseBase<?>> updatePassword(@RequestBody UpdatePassword updatePassword) {
        var response = authenticationService.updatePassword(updatePassword);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }
}
