package com.bl.mb.controller;

import com.bl.mb.models.PaginationWithSorting;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.service.ProductsServiceImpl;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor
@RequestMapping("/api/products")
public class ProductsController {

    private final ProductsServiceImpl productsService;

    @GetMapping("/all")
    public ResponseEntity<ResponseBase<?>> getProducts() {
        var response = productsService.getProducts();
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseBase<?>> getProductById(@Valid @PathVariable UUID id) {
        var response = productsService.getProductById(id);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PostMapping()
    public ResponseEntity<ResponseBase<?>> getAllPagination(@Valid @RequestBody final PaginationWithSorting paginationWithSorting) {
        var response = productsService.getAllPagination(paginationWithSorting);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @GetMapping("/other")
    public ResponseEntity<ResponseBase<?>> getOtherWines(@Valid @RequestHeader(HttpHeaders.ACCEPT_LANGUAGE) String acceptLanguage) {
        var response = productsService.getOtherWines(acceptLanguage);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }
}
