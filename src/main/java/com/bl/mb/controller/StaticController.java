package com.bl.mb.controller;

import com.bl.mb.models.ResponseBase;
import com.bl.mb.service.StaticServiceImpl;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/api/statics/")
@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor
public class StaticController {

    private final StaticServiceImpl staticService;

    /**
     * Handles the HTTP POST request for saving statics information.
     *
     * @param request HTTP request containing client information.
     * @param page    Page associated with the request.
     * @return ResponseEntity containing the response to the client.
     * @throws IOException     if there is an issue with IO operations.
     * @throws GeoIp2Exception if there is an issue with GeoIP location lookup.
     */
    @PostMapping("save")
    public ResponseEntity<ResponseBase<?>> saveStatics(HttpServletRequest request,
                                                       @RequestParam("page") String page) {
        staticService.saveStatics(request, page);
        return null;
    }
}
