package com.bl.mb.controller;

import com.bl.mb.models.PaginationWithSorting;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.service.EventsServiceImpl;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.UUID;

@RequestMapping("/api/events")
@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class EventsController {

    private final EventsServiceImpl eventsService;

    @PostMapping("/show")
    public ResponseEntity<ResponseBase<?>> allEvents(@Valid @RequestBody final PaginationWithSorting paginationWithSorting) throws Exception {
        var response = eventsService.allEvents(paginationWithSorting);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @GetMapping("/show/dates")
    public ResponseEntity<ResponseBase<?>> allEventsByDate(@Valid @RequestBody final PaginationWithSorting paginationWithSorting,
                                                           @Valid @RequestParam("startDate") Long startDate,
                                                           @Valid @RequestParam("endDate") Long endDate) {
        var response = eventsService.allEventsByDate(paginationWithSorting, startDate, endDate);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PostMapping("/show/dates/locale")
    public ResponseEntity<ResponseBase<?>> allEventsByDateAndLocale(@RequestBody final PaginationWithSorting paginationWithSorting,
                                                                    @Valid @RequestParam("startDate") Long startDate,
                                                                    @Valid @RequestParam("endDate") Long endDate,
                                                                    @Valid @RequestHeader(HttpHeaders.ACCEPT_LANGUAGE) final String lang) {
        var response = eventsService.allEventsByDateAndLocale(paginationWithSorting, startDate, endDate, lang);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @GetMapping("/show/locale")
    public ResponseEntity<ResponseBase<?>> allEventsLocale(@Valid @RequestHeader(HttpHeaders.ACCEPT_LANGUAGE) final String lang,
                                                           @Valid @RequestBody final PaginationWithSorting paginationWithSorting) throws Exception {
        var response = eventsService.allEvents(lang, paginationWithSorting);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }


    @GetMapping("/show/next-events")
    public ResponseEntity<ResponseBase<?>> nextEvents(@Valid @RequestHeader(HttpHeaders.ACCEPT_LANGUAGE) final String lang,
                                                           @Valid @RequestBody final PaginationWithSorting paginationWithSorting,
                                                           @Valid @RequestParam("month") Long month,
                                                           @Valid @RequestParam("year") Long year ) throws Exception {
        var response = eventsService.nextEvents(lang, paginationWithSorting, month, year);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @GetMapping("/show/pre-events")
    public ResponseEntity<ResponseBase<?>> preEvents(@Valid @RequestHeader(HttpHeaders.ACCEPT_LANGUAGE) final String lang,
                                                           @Valid @RequestBody final PaginationWithSorting paginationWithSorting,
                                                           @Valid @RequestParam("month") Long month,
                                                           @Valid @RequestParam("year") Long year ) throws Exception {
        var response = eventsService.preEvents(lang, paginationWithSorting, month, year);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @GetMapping("/show/this-month")
    public ResponseEntity<ResponseBase<?>> thisMonthEvents(@Valid @RequestHeader(HttpHeaders.ACCEPT_LANGUAGE) final String lang,
                                                     @Valid @RequestBody final PaginationWithSorting paginationWithSorting,
                                                     @Valid @RequestParam("month") Long month,
                                                     @Valid @RequestParam("year") Long year ) throws Exception {
        var response = eventsService.thisMonthEvents(lang, paginationWithSorting, month, year);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @GetMapping("/show/{id}")
    public ResponseEntity<ResponseBase<?>> eventById(@Valid @RequestHeader(HttpHeaders.ACCEPT_LANGUAGE) final String lang,
                                                     @Valid @PathVariable UUID id) throws Exception {
        var response = eventsService.eventById(lang,id);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<ResponseBase<?>> oneEvent(@Valid @PathVariable UUID id) throws Exception {
        var response = eventsService.oneEvent(id);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @GetMapping("/show/year")
    public ResponseEntity<ResponseBase<?>> eventEventsByYear(@Valid @RequestHeader(HttpHeaders.ACCEPT_LANGUAGE) final String lang,
                                                             @Valid @RequestParam("year") Long year ) throws Exception {
        var response = eventsService.eventEventsByYear(lang,year);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }


}
