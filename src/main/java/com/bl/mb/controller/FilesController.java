package com.bl.mb.controller;

import com.bl.mb.models.ResponseBase;
import com.bl.mb.service.FilesStorageService;
import com.bl.mb.service.ImageStorageServiceImpl;
import jakarta.annotation.security.RolesAllowed;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor
@RequestMapping("/api")
public class FilesController {

    private final FilesStorageService storageService;
    private final ImageStorageServiceImpl imageStorageService;

    /**
     * Endpoint for retrieving a list of available files.
     *
     * @return ResponseEntity containing the response with the list of files.
     */
    @GetMapping("/files")
    public ResponseEntity<ResponseBase<?>> getListFiles() {
        var response = imageStorageService.getImage();
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    /**
     * Endpoint for downloading a specific file by its filename.
     *
     * @param filename The name of the file to be downloaded.
     * @return ResponseEntity containing the file as a Resource.
     */
    @GetMapping("/files/{filename:.+}")
    public ResponseEntity<ResponseBase<?>> getFile(@PathVariable final String filename) {
        var response = storageService.load(filename);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }
}