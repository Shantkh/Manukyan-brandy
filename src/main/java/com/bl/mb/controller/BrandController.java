package com.bl.mb.controller;

import com.bl.mb.models.ResponseBase;
import com.bl.mb.service.BrandServiceImpl;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/brand")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class BrandController {

    private final BrandServiceImpl brandService;

    @GetMapping("/{brand}")
    public ResponseEntity<ResponseBase<?>> getBrand(@Valid @RequestHeader(HttpHeaders.ACCEPT_LANGUAGE) final String lang,
                                                    @PathVariable String brand) {
        var response = brandService.getBrand(lang,brand);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }


}
