package com.bl.mb.controller;

import com.bl.mb.constances.PagesEnum;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.service.SliderServiceImpl;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/slider")
@CrossOrigin(origins = "*", maxAge = 3600)
public class SliderController {

    private final SliderServiceImpl sliderService;

    @GetMapping("/show")
    public ResponseEntity<ResponseBase<?>> Sections(@Valid @RequestHeader(HttpHeaders.ACCEPT_LANGUAGE) final String lang){
        var response = sliderService.slides(lang);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }
}
