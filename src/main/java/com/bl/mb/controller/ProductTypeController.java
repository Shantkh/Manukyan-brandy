package com.bl.mb.controller;

import com.bl.mb.models.ResponseBase;
import com.bl.mb.service.productsTypeServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/types")
@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor
public class ProductTypeController {

    private final productsTypeServiceImpl productsTypeService;

    @GetMapping("/all")
    public ResponseEntity<ResponseBase<?>> getTypes() {
        var response = productsTypeService.getTypes();
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

}
