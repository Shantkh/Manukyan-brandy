package com.bl.mb.controller;

import com.bl.mb.models.ResponseBase;
import com.bl.mb.service.CellarServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api")
@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class CellarController {

    private final CellarServiceImpl brandService;

    @GetMapping("/cellars")
    public ResponseEntity<ResponseBase<?>> getCellars() {
        var response = brandService.getCellars();
        return new ResponseEntity<>(response, response.getHttpStatus());
    }
}
