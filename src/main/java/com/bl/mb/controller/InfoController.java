package com.bl.mb.controller;

import com.bl.mb.models.InfoEntity;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.service.InfoServiceImpl;
import jakarta.validation.Valid;
import jakarta.websocket.server.PathParam;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/info")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class InfoController {

    private final InfoServiceImpl infoService;

    @PostMapping("/save")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> createInfo(@Valid @RequestBody final InfoEntity infoEntity) {
        var response = infoService.create(infoEntity);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @GetMapping("/get")
    public ResponseEntity<ResponseBase<?>> Info(@PathParam("page") String page) {
        var response = infoService.info(page);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }
}
