package com.bl.mb.controller;

import com.bl.mb.models.ResponseBase;
import com.bl.mb.models.Subscribers;
import com.bl.mb.service.SubscribeServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/subscribe/")
@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor
public class SubscribeController {

    private final SubscribeServiceImpl subscribeService;

    /**
     * Endpoint for adding a new subscriber.
     *
     * @param subscribers The subscriber information to be added.
     * @return ResponseEntity containing the response for the subscription request.
     */
    @PostMapping()
    public ResponseEntity<ResponseBase<?>> subscriber(@RequestHeader(HttpHeaders.ACCEPT_LANGUAGE) final String lang,
                                                      @RequestBody final Subscribers subscribers) {
        var response = subscribeService.subscriber(lang,subscribers);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }
}
