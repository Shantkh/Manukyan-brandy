package com.bl.mb.controller;

import com.bl.mb.models.ResponseBase;
import com.bl.mb.models.TourBooking;
import com.bl.mb.service.TourBookingServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/tours")
@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor
public class ToursBookingController {

    private final TourBookingServiceImpl tourBookingServiceImpl;

    @PostMapping("/book")
    public ResponseEntity<ResponseBase<?>> subscriber(@RequestBody final TourBooking tourBooking) {
        var response = tourBookingServiceImpl.bookTour(tourBooking);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }
}
