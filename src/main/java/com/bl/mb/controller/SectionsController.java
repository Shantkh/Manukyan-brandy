package com.bl.mb.controller;

import com.bl.mb.constances.PagesEnum;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.service.SectionsServiceImpl;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/banner")
@CrossOrigin(origins = "*", maxAge = 3600)
public class SectionsController {

    private final SectionsServiceImpl bannerService;

    @GetMapping("/show")
    public ResponseEntity<ResponseBase<?>> Sections(@Valid @RequestHeader(HttpHeaders.ACCEPT_LANGUAGE) final String lang,
                                                    @Valid @RequestParam("page") final PagesEnum pagesEnum){
        var response = bannerService.sections(lang,pagesEnum);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }
}
