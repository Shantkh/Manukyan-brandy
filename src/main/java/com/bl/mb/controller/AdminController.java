package com.bl.mb.controller;

import com.bl.mb.constances.PagesEnum;
import com.bl.mb.constances.SliderEnum;
import com.bl.mb.models.*;
import com.bl.mb.service.*;
import jakarta.annotation.security.RolesAllowed;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.util.UUID;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;


@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor
@Validated
@RequestMapping("/")
public class AdminController {

    private final PartnersServiceImpl partnersService;
    private final StaticServiceImpl staticService;
    private final SubscribeServiceImpl subscribeService;
    private final SectionsServiceImpl sectionsService;
    private final EventsServiceImpl eventsService;
    private final ProductsServiceImpl productsService;
    private final BrandServiceImpl brandService;
    private final CellarServiceImpl cellarService;
    private final BottlingServiceImpl bottlingService;
    private final ToursServiceImpl toursService;
    private final TourBookingServiceImpl tourBookingServiceImpl;
    private final ImageStorageService imageStorageService;


    /**
     * Endpoint for uploading files by the admin.
     *
     * @param files Array of MultipartFile representing the files to be uploaded.
     * @return ResponseEntity containing the response for the file upload request.
     */
    @PostMapping(value = "/upload",
            consumes = MULTIPART_FORM_DATA_VALUE,
            produces = APPLICATION_JSON_VALUE)
    @RolesAllowed("ADMIN")
    public ResponseEntity<ResponseBase<?>> uploadFiles(@RequestParam("files") final MultipartFile[] files) throws IOException {
        var response = imageStorageService.saveFile(files);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @DeleteMapping("/partner/delete/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> deletePartner(@PathVariable Long id) {
        var response = partnersService.deletePartner(id);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    /**
     * Endpoint for deleting a specific file by its filename.
     *
     * @param filename The name of the file to be deleted.
     * @return ResponseEntity containing the response for the file deletion request.
     */
    @DeleteMapping("/files/{filename:.+}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> deleteFile(@PathVariable final String filename) throws Exception {
        var response = imageStorageService.deleteFile(filename);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    /**
     * Handles the HTTP GET request for retrieving user statistics.
     *
     * @return ResponseEntity containing the response to the client.
     */
    @PostMapping("/static/home")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> statics() {
        var result = staticService.usersStatics();
        return new ResponseEntity<>(result, result.getHttpStatus());
    }


    @GetMapping("/subscribe/list")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> getSubscriber() {
        var response = subscribeService.getSubscriber();
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    /**
     * Endpoint for deleting subscribers based on their IDs.
     *
     * @param ids Array of UUIDs representing subscriber IDs to be deleted.
     * @return ResponseEntity containing the response for the delete request.
     */
    @DeleteMapping("/subscribe/ids")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> getSubscriber(@RequestBody final UUID[] ids) {
        var response = subscribeService.delete(ids);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    /**
     * Handles the GET request to download a CSV file of subscribers.
     *
     * @param res The HttpServletResponse object to manipulate the HTTP response.
     * @return A ResponseEntity containing the response status and data.
     * @throws IOException If an I/O error occurs while handling the request.
     */
    @GetMapping("/subscribe/download")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> download(final HttpServletResponse res) {
        var response = subscribeService.download(res);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PostMapping("/slider/save")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> saveSliderSections(@Valid @RequestBody final SliderMedia sliderMedia,
                                                              @Valid @RequestParam("sliderEnum") final SliderEnum sliderEnum) {
        var response = sectionsService.saveSlideSections(sliderMedia, sliderEnum);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PostMapping("/sections/save")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> saveSections(@Valid @RequestBody final Sections sections,
                                                        @Valid @RequestParam("page")  PagesEnum pagesEnum) {
        var response = sectionsService.saveSections(sections, pagesEnum);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PutMapping("/sections/update")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> updateSections(@Valid @RequestBody final Sections sections,
                                                          @Valid @RequestParam("page") final PagesEnum pagesEnum) {
        var response = sectionsService.updateSections(sections, pagesEnum);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PutMapping("/slider/update")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> updateSlider(@Valid @RequestBody final SliderMedia sliderMedia,
                                                        @Valid @RequestParam("sliderEnum") final SliderEnum sliderEnum) {
        var response = sectionsService.updateMedia(sliderMedia, sliderEnum);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @DeleteMapping("/sections/delete/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> deleteSections(@Valid @PathVariable UUID id,
                                                          @Valid @RequestParam("page") PagesEnum pagesEnum) {
        var response = sectionsService.deleteSections(id, pagesEnum);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }


    @GetMapping("/sections/all")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> sections(@Valid @RequestParam("page") PagesEnum pagesEnum) {
        var response = sectionsService.sections(pagesEnum);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @GetMapping("/slider/all")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> slides(@Valid @RequestParam("sliderEnum") SliderEnum sliderEnum) {
        var response = sectionsService.slides(sliderEnum);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PostMapping("/events/save")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> events(@Valid @RequestBody final Events events,
                                                  @Valid @RequestParam("startDate") Long startDate,
                                                  @Valid @RequestParam("endDate") Long endDate) {
        var response = eventsService.events(events, startDate, endDate);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @DeleteMapping("/events/delete/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> deleteEvent(@Valid @PathVariable UUID id) throws Exception {
        var response = eventsService.deleteEvent(id);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PutMapping("/events/update")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> updateEvent(@Valid @RequestBody Events events) throws Exception {
        var response = eventsService.updateEvent(events);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PostMapping("/product/save")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> products(@Valid @RequestBody final Products products) {
        var response = productsService.product(products);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PutMapping("/product/update")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> updateProduct(@Valid @RequestBody final Products products) {
        var response = productsService.update(products);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @DeleteMapping("/product/delete/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> deleteProduct(@Valid @PathVariable UUID id) {
        var response = productsService.deleteProduct(id);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PostMapping("/tours/save")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> tours(@Valid @RequestBody final ToursRequest toursRequest) throws ParseException {
        var response = toursService.save(toursRequest);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PostMapping("/tours/save/one")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> tours(@Valid @RequestBody final ToursOneRequest toursRequest) throws ParseException {
        var response = toursService.saveOne(toursRequest);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @DeleteMapping("/tours/delete/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> deleteTour(@Valid @PathVariable UUID id) {
        var response = toursService.delete(id);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @GetMapping("/tours/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> getTourById(@Valid @PathVariable UUID id) {
        var response = toursService.getTourById(id);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PutMapping("/tours/update")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> updateTours(@Valid @RequestBody ToursUpdateRequest toursRequest) throws Exception {
        var response = toursService.updateTours(toursRequest);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PostMapping("/tours/book/")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> bookedAllTours(@RequestBody final PaginationWithSorting paginationWithSorting) throws Exception {
        var response = tourBookingServiceImpl.bookedAllTours(paginationWithSorting);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @GetMapping("/tours/book/seven")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> bookedAllSevenTours(@RequestBody final PaginationWithSorting paginationWithSorting) throws Exception {
        var response = tourBookingServiceImpl.bookedAllToursStartedLastWeek(paginationWithSorting);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @GetMapping("/tours/book/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> bookedTours(@PathVariable String id) throws Exception {
        var response = tourBookingServiceImpl.toursById(id);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @GetMapping("/tours/book/customers/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> bookedToursCustomers(@PathVariable String id) throws Exception {
        var response = tourBookingServiceImpl.customersToursById(id);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }


    @PostMapping("/brand/save")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> brands(@Valid @RequestBody final BrandRequest brand) {
        var response = brandService.create(brand);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }


    @PostMapping("/brand/name/save")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> brandName(@Valid @RequestBody final BrandName brand) {
        var response = brandService.createName(brand);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @GetMapping("/brand/name/get")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> getBrandName() {
        var response = brandService.getBrandsName();
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @DeleteMapping("/brand/name/delete/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> deleteBrandName(@Valid @PathVariable UUID id) {
        var response = brandService.deleteBrandName(id);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PostMapping("/cellar/save")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> createCellar(@Valid @RequestBody final CellarRequest cellarRequest) {
        var response = cellarService.create(cellarRequest);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PostMapping("/bottling/save")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> createCellar(@Valid @RequestBody final BottlingRequest bottlingRequest) {
        var response = bottlingService.create(bottlingRequest);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }
}
