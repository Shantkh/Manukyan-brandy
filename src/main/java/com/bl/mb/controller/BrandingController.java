package com.bl.mb.controller;

import com.bl.mb.models.ResponseBase;
import com.bl.mb.service.BrandService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/brands")
@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class BrandingController {

    private final BrandService brandService;

    @GetMapping()
    public ResponseEntity<ResponseBase<?>> getBrands() {
        var response = brandService.getBrands();
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

}
