package com.bl.mb.controller;

import com.bl.mb.models.EmailDetails;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.service.EmailServiceImpl;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/mail")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", maxAge = 3600)
public class EmailController {

    private final EmailServiceImpl emailService;

    @PostMapping("/sendemail")
    public ResponseEntity<ResponseBase<?>> sendEmail(@Valid @RequestBody final EmailDetails formData) {
        var response = emailService.sendEmail(formData);
        return new ResponseEntity<>(response, response.getHttpStatus());
//        return ResponseEntity.ok().build();
    }
}
