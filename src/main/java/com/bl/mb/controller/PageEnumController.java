package com.bl.mb.controller;

import com.bl.mb.models.ResponseBase;
import com.bl.mb.service.PageEnumServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/enums")
@CrossOrigin(origins = "*", maxAge = 3600)
public class PageEnumController {

    private final PageEnumServiceImpl pageEnumService;

    @GetMapping("/pages")
    public ResponseEntity<ResponseBase<?>> getPageEnumsList(){
        var response = pageEnumService.pageEnums();
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @GetMapping("/links")
    public ResponseEntity<ResponseBase<?>> getLinksEnumsList(){
        var response = pageEnumService.LinkEnums();
        return new ResponseEntity<>(response, response.getHttpStatus());
    }
}
