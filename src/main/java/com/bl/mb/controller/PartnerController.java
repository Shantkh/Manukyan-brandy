package com.bl.mb.controller;

import com.bl.mb.models.Partners;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.service.PartnersServiceImpl;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/partners")
@CrossOrigin(origins = "*", maxAge = 3600)
@RequiredArgsConstructor
public class PartnerController {

    private final PartnersServiceImpl partnersService;

    /**
     * Retrieves all partner entities.
     *
     * @return ResponseEntity containing the response status and data.
     */
    @GetMapping("/all")
    public ResponseEntity<ResponseBase<?>> getPartners() {
        var response = partnersService.getPartners();
        return new ResponseEntity<>(response, response.getHttpStatus());
    }


    @PostMapping("/new")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ResponseBase<?>> savePartner(@Valid @RequestBody Partners partners) {
        var response = partnersService.savePartner(partners);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }
}
