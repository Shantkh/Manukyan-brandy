package com.bl.mb.controller;

import com.bl.mb.models.PaginationWithSorting;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.service.ToursServiceImpl;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/tours")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ToursController {

    private final ToursServiceImpl toursService;

    @PostMapping("/all/local/pagination")
    public ResponseEntity<ResponseBase<?>> allToursPagination(@Valid @RequestHeader(HttpHeaders.ACCEPT_LANGUAGE) final String lang,
                                                    @Valid @RequestBody() final PaginationWithSorting paginationWithSorting){
        var response = toursService.allTourPagination(lang,paginationWithSorting);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @GetMapping("/all/local/dates")
    public ResponseEntity<ResponseBase<?>> allToursPagination(@Valid @RequestHeader(HttpHeaders.ACCEPT_LANGUAGE) final String lang,
                                                              @Valid @RequestParam String startDate,
                                                              @Valid @RequestParam String endDate){
        var response = toursService.allTourByDates(lang,startDate, endDate);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @GetMapping("/booking/{bookingId}")
    public ResponseEntity<ResponseBase<?>> allTourById(@Valid @RequestHeader(HttpHeaders.ACCEPT_LANGUAGE) final String lang,
                                                       @Valid @PathVariable String bookingId){
        var response = toursService.getById(lang,bookingId);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PostMapping("/all/pagination")
    public ResponseEntity<ResponseBase<?>> allTourById(@RequestBody() final PaginationWithSorting paginationWithSorting){
        var response = toursService.getAllTours(paginationWithSorting);
        return new ResponseEntity<>(response, response.getHttpStatus());
    }

}
