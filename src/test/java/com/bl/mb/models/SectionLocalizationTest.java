package com.bl.mb.models;

import com.bl.mb.constances.PagesEnum;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@ExtendWith(MockitoExtension.class)
class SectionLocalizationTest {

    @Mock
    private Sections sections;

    @InjectMocks
    private SectionLocalization sectionsSectionLocalization;

    @Test
    void testBannerLocalization() {

        SectionLocalization sectionsSectionLocalization = SectionLocalization.builder()
                .id(1L)
                .sections(sections)
                .locale("en_US")
                .htmlContent("<h1>Welcome!</h1>")
                .build();

        assertEquals(1L, sectionsSectionLocalization.getId());
        assertEquals(sections, sectionsSectionLocalization.getSections());
        assertEquals("en_US", sectionsSectionLocalization.getLocale());
        assertEquals("<h1>Welcome!</h1>", sectionsSectionLocalization.getHtmlContent());

        verifyNoMoreInteractions(sections);
    }

    @Test
    public void testGettersAndSetters() {
        sectionsSectionLocalization.setId(1L);
        sectionsSectionLocalization.setSections(sections);
        sectionsSectionLocalization.setLocale("en_US");
        sectionsSectionLocalization.setHtmlContent("<p>This is the HTML content</p>");

        assertEquals(1L, sectionsSectionLocalization.getId());
        assertEquals(sections, sectionsSectionLocalization.getSections());
        assertEquals("en_US", sectionsSectionLocalization.getLocale());
        assertEquals("<p>This is the HTML content</p>", sectionsSectionLocalization.getHtmlContent());
    }

    @Test
    void testConstructor() {
        SectionLocalization actualSectionLocalization = new SectionLocalization();
        actualSectionLocalization.setHtmlContent("Not all who wander are lost");
        actualSectionLocalization.setId(1L);
        actualSectionLocalization.setLocale("en");
        Sections sections = new Sections();
        sections.setCreatedOn(mock(Timestamp.class));
        sections.setId(UUID.randomUUID());
        sections.setLocalizations(new ArrayList<>());
        sections.setPagesEnum(PagesEnum.HOME_SLIDER);
        sections.setSliderMedia(new ArrayList<>());
        actualSectionLocalization.setSections(sections);
        String actualHtmlContent = actualSectionLocalization.getHtmlContent();
        Long actualId = actualSectionLocalization.getId();
        String actualLocale = actualSectionLocalization.getLocale();
        Sections actualSections = actualSectionLocalization.getSections();
        assertEquals("Not all who wander are lost", actualHtmlContent);
        assertEquals("en", actualLocale);
        assertEquals(1L, actualId.longValue());
        assertSame(sections, actualSections);
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>
     * {@link SectionLocalization#SectionLocalization(Long, Sections, String, String)}
     *   <li>{@link SectionLocalization#setHtmlContent(String)}
     *   <li>{@link SectionLocalization#setId(Long)}
     *   <li>{@link SectionLocalization#setLocale(String)}
     *   <li>{@link SectionLocalization#setSections(Sections)}
     *   <li>{@link SectionLocalization#getHtmlContent()}
     *   <li>{@link SectionLocalization#getId()}
     *   <li>{@link SectionLocalization#getLocale()}
     *   <li>{@link SectionLocalization#getSections()}
     * </ul>
     */
    @Test
    void testConstructor2() {
        Sections sections = new Sections();
        sections.setCreatedOn(mock(Timestamp.class));
        sections.setId(UUID.randomUUID());
        sections.setLocalizations(new ArrayList<>());
        sections.setPagesEnum(PagesEnum.HOME_SLIDER);
        sections.setSliderMedia(new ArrayList<>());
        SectionLocalization actualSectionLocalization = new SectionLocalization(1L, sections, "en",
                "Not all who wander are lost");
        actualSectionLocalization.setHtmlContent("Not all who wander are lost");
        actualSectionLocalization.setId(1L);
        actualSectionLocalization.setLocale("en");
        Sections sections2 = new Sections();
        sections2.setCreatedOn(mock(Timestamp.class));
        sections2.setId(UUID.randomUUID());
        sections2.setLocalizations(new ArrayList<>());
        sections2.setPagesEnum(PagesEnum.HOME_SLIDER);
        sections2.setSliderMedia(new ArrayList<>());
        actualSectionLocalization.setSections(sections2);
        String actualHtmlContent = actualSectionLocalization.getHtmlContent();
        Long actualId = actualSectionLocalization.getId();
        String actualLocale = actualSectionLocalization.getLocale();
        Sections actualSections = actualSectionLocalization.getSections();
        assertEquals("Not all who wander are lost", actualHtmlContent);
        assertEquals("en", actualLocale);
        assertEquals(1L, actualId.longValue());
        assertSame(sections2, actualSections);
    }
}
