package com.bl.mb.models;

import com.bl.mb.models.Subscribers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class SubscribersTest {

    @Mock
    private Subscribers subscribers;
    @Test
    void testGetterAndSetter() {
        subscribers = new Subscribers();
        subscribers.setId(UUID.randomUUID());
        subscribers.setCreatedOn(new Timestamp(System.currentTimeMillis()));
        subscribers.setEmail("test@example.com");

        assertNotNull(subscribers.getId());
        assertNotNull(subscribers.getCreatedOn());
        assertNotNull(subscribers.getEmail());
    }

    @Test
    void testNoArgsConstructor() {
        Subscribers emptySubscribers = new Subscribers();

        assertNotNull(emptySubscribers);
    }

    @Test
    void testAllArgsConstructor() {
        UUID id = UUID.randomUUID();
        Timestamp createdOn = new Timestamp(System.currentTimeMillis());
        String email = "test@example.com";

        Subscribers allArgsSubscribers = new Subscribers(id, createdOn, email);

        assertNotNull(allArgsSubscribers);
        assertEquals(id, allArgsSubscribers.getId());
        assertEquals(createdOn, allArgsSubscribers.getCreatedOn());
        assertEquals(email, allArgsSubscribers.getEmail());
    }

    @Test
    void testBuilder() {
        UUID id = UUID.randomUUID();
        Timestamp createdOn = new Timestamp(System.currentTimeMillis());
        String email = "test@example.com";

        Subscribers builtSubscribers = Subscribers.builder()
                .id(id)
                .createdOn(createdOn)
                .email(email).build();


        assertNotNull(builtSubscribers);
        assertEquals(id, builtSubscribers.getId());
        assertEquals(createdOn, builtSubscribers.getCreatedOn());
        assertEquals(email, builtSubscribers.getEmail());
    }
}
