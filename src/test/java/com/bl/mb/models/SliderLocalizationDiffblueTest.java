package com.bl.mb.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.mock;

import com.bl.mb.constances.SliderEnum;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.UUID;

import org.junit.jupiter.api.Test;

class SliderLocalizationDiffblueTest {
    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link SliderLocalization#SliderLocalization()}
     *   <li>{@link SliderLocalization#setHtmlContent(String)}
     *   <li>{@link SliderLocalization#setId(Long)}
     *   <li>{@link SliderLocalization#setLocale(String)}
     *   <li>{@link SliderLocalization#setSliderMedia(SliderMedia)}
     *   <li>{@link SliderLocalization#getHtmlContent()}
     *   <li>{@link SliderLocalization#getId()}
     *   <li>{@link SliderLocalization#getLocale()}
     *   <li>{@link SliderLocalization#getSliderMedia()}
     * </ul>
     */
    @Test
    void testConstructor() {
        SliderLocalization actualSliderLocalization = new SliderLocalization();
        actualSliderLocalization.setHtmlContent("Not all who wander are lost");
        actualSliderLocalization.setId(1L);
        actualSliderLocalization.setLocale("en");
        SliderMedia sliderMedia = new SliderMedia();
        sliderMedia.setCreatedOn(mock(Timestamp.class));
        sliderMedia.setId(UUID.randomUUID());
        sliderMedia.setSliderEnum(SliderEnum.FIRST);
        sliderMedia.setSliderLocalizations(new ArrayList<>());
        sliderMedia.setSliderMedia("Slider Media");
        actualSliderLocalization.setSliderMedia(sliderMedia);
        String actualHtmlContent = actualSliderLocalization.getHtmlContent();
        Long actualId = actualSliderLocalization.getId();
        String actualLocale = actualSliderLocalization.getLocale();
        SliderMedia actualSliderMedia = actualSliderLocalization.getSliderMedia();
        assertEquals("Not all who wander are lost", actualHtmlContent);
        assertEquals("en", actualLocale);
        assertEquals(1L, actualId.longValue());
        assertSame(sliderMedia, actualSliderMedia);
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>
     * {@link SliderLocalization#SliderLocalization(Long, SliderMedia, String, String)}
     *   <li>{@link SliderLocalization#setHtmlContent(String)}
     *   <li>{@link SliderLocalization#setId(Long)}
     *   <li>{@link SliderLocalization#setLocale(String)}
     *   <li>{@link SliderLocalization#setSliderMedia(SliderMedia)}
     *   <li>{@link SliderLocalization#getHtmlContent()}
     *   <li>{@link SliderLocalization#getId()}
     *   <li>{@link SliderLocalization#getLocale()}
     *   <li>{@link SliderLocalization#getSliderMedia()}
     * </ul>
     */
    @Test
    void testConstructor2() {
        SliderMedia sliderMedia = new SliderMedia();
        sliderMedia.setCreatedOn(mock(Timestamp.class));
        sliderMedia.setId(UUID.randomUUID());
        sliderMedia.setSliderEnum(SliderEnum.FIRST);
        sliderMedia.setSliderLocalizations(new ArrayList<>());
        sliderMedia.setSliderMedia("Slider Media");
        SliderLocalization actualSliderLocalization = new SliderLocalization(1L, sliderMedia, "en",
                "Not all who wander are lost");
        actualSliderLocalization.setHtmlContent("Not all who wander are lost");
        actualSliderLocalization.setId(1L);
        actualSliderLocalization.setLocale("en");
        SliderMedia sliderMedia2 = new SliderMedia();
        sliderMedia2.setCreatedOn(mock(Timestamp.class));
        sliderMedia2.setId(UUID.randomUUID());
        sliderMedia2.setSliderEnum(SliderEnum.FIRST);
        sliderMedia2.setSliderLocalizations(new ArrayList<>());
        sliderMedia2.setSliderMedia("Slider Media");
        actualSliderLocalization.setSliderMedia(sliderMedia2);
        String actualHtmlContent = actualSliderLocalization.getHtmlContent();
        Long actualId = actualSliderLocalization.getId();
        String actualLocale = actualSliderLocalization.getLocale();
        SliderMedia actualSliderMedia = actualSliderLocalization.getSliderMedia();
        assertEquals("Not all who wander are lost", actualHtmlContent);
        assertEquals("en", actualLocale);
        assertEquals(1L, actualId.longValue());
        assertSame(sliderMedia2, actualSliderMedia);
    }
}
