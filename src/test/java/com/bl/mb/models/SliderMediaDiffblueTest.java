package com.bl.mb.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.mock;

import com.bl.mb.constances.LinksEnum;
import com.bl.mb.constances.SliderEnum;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;

class SliderMediaDiffblueTest {
    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link SliderMedia#SliderMedia()}
     *   <li>{@link SliderMedia#setCreatedOn(Timestamp)}
     *   <li>{@link SliderMedia#setId(UUID)}
     *   <li>{@link SliderMedia#setSliderEnum(SliderEnum)}
     *   <li>{@link SliderMedia#setSliderLocalizations(List)}
     *   <li>{@link SliderMedia#setSliderMedia(String)}
     *   <li>{@link SliderMedia#toString()}
     *   <li>{@link SliderMedia#getCreatedOn()}
     *   <li>{@link SliderMedia#getId()}
     *   <li>{@link SliderMedia#getSliderEnum()}
     *   <li>{@link SliderMedia#getSliderLocalizations()}
     *   <li>{@link SliderMedia#getSliderMedia()}
     * </ul>
     */
    @Test
    void testConstructor() {
        SliderMedia actualSliderMedia = new SliderMedia();
        Timestamp createdOn = mock(Timestamp.class);
        actualSliderMedia.setCreatedOn(createdOn);
        UUID id = UUID.randomUUID();
        actualSliderMedia.setId(id);
        actualSliderMedia.setSliderEnum(SliderEnum.FIRST);
        ArrayList<SliderLocalization> sliderLocalizations = new ArrayList<>();
        actualSliderMedia.setSliderLocalizations(sliderLocalizations);
        actualSliderMedia.setSliderMedia("Slider Media");
        actualSliderMedia.toString();
        Timestamp actualCreatedOn = actualSliderMedia.getCreatedOn();
        UUID actualId = actualSliderMedia.getId();
        SliderEnum actualSliderEnum = actualSliderMedia.getSliderEnum();
        List<SliderLocalization> actualSliderLocalizations = actualSliderMedia.getSliderLocalizations();
        assertEquals("Slider Media", actualSliderMedia.getSliderMedia());
        assertEquals(SliderEnum.FIRST, actualSliderEnum);
        assertSame(sliderLocalizations, actualSliderLocalizations);
        assertSame(id, actualId);
        assertSame(createdOn, actualCreatedOn);
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>
     * {@link SliderMedia#SliderMedia(UUID, Timestamp, List, SliderEnum, String)}
     *   <li>{@link SliderMedia#setCreatedOn(Timestamp)}
     *   <li>{@link SliderMedia#setId(UUID)}
     *   <li>{@link SliderMedia#setSliderEnum(SliderEnum)}
     *   <li>{@link SliderMedia#setSliderLocalizations(List)}
     *   <li>{@link SliderMedia#setSliderMedia(String)}
     *   <li>{@link SliderMedia#toString()}
     *   <li>{@link SliderMedia#getCreatedOn()}
     *   <li>{@link SliderMedia#getId()}
     *   <li>{@link SliderMedia#getSliderEnum()}
     *   <li>{@link SliderMedia#getSliderLocalizations()}
     *   <li>{@link SliderMedia#getSliderMedia()}
     * </ul>
     */
    @Test
    void testConstructor2() {
        UUID id = UUID.randomUUID();
        Timestamp createdOn = mock(Timestamp.class);
        ArrayList<SliderLocalization> sliderLocalizations = new ArrayList<>();
        SliderMedia actualSliderMedia = new SliderMedia(id, createdOn, sliderLocalizations, SliderEnum.FIRST,
                "Slider Media", LinksEnum.FACTORY);
        Timestamp createdOn2 = mock(Timestamp.class);
        actualSliderMedia.setCreatedOn(createdOn2);
        UUID id2 = UUID.randomUUID();
        actualSliderMedia.setId(id2);
        actualSliderMedia.setSliderEnum(SliderEnum.FIRST);
        ArrayList<SliderLocalization> sliderLocalizations2 = new ArrayList<>();
        actualSliderMedia.setSliderLocalizations(sliderLocalizations2);
        actualSliderMedia.setSliderMedia("Slider Media");
        actualSliderMedia.toString();
        Timestamp actualCreatedOn = actualSliderMedia.getCreatedOn();
        UUID actualId = actualSliderMedia.getId();
        SliderEnum actualSliderEnum = actualSliderMedia.getSliderEnum();
        List<SliderLocalization> actualSliderLocalizations = actualSliderMedia.getSliderLocalizations();
        assertEquals("Slider Media", actualSliderMedia.getSliderMedia());
        assertEquals(SliderEnum.FIRST, actualSliderEnum);
        assertEquals(sliderLocalizations, actualSliderLocalizations);
        assertSame(sliderLocalizations2, actualSliderLocalizations);
        assertSame(id2, actualId);
        assertSame(createdOn2, actualCreatedOn);
    }

    /**
     * Method under test: {@link SliderMedia#equals(Object)}
     */
    @Test
    void testEquals() {
        SliderMedia sliderMedia = new SliderMedia();
        sliderMedia.setCreatedOn(mock(Timestamp.class));
        sliderMedia.setId(UUID.randomUUID());
        sliderMedia.setSliderEnum(SliderEnum.FIRST);
        sliderMedia.setSliderLocalizations(new ArrayList<>());
        sliderMedia.setSliderMedia("Slider Media");
        assertNotEquals(sliderMedia, null);
    }

    /**
     * Method under test: {@link SliderMedia#equals(Object)}
     */
    @Test
    void testEquals2() {
        SliderMedia sliderMedia = new SliderMedia();
        sliderMedia.setCreatedOn(mock(Timestamp.class));
        sliderMedia.setId(UUID.randomUUID());
        sliderMedia.setSliderEnum(SliderEnum.FIRST);
        sliderMedia.setSliderLocalizations(new ArrayList<>());
        sliderMedia.setSliderMedia("Slider Media");
        assertNotEquals(sliderMedia, "Different type to SliderMedia");
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link SliderMedia#equals(Object)}
     *   <li>{@link SliderMedia#hashCode()}
     * </ul>
     */
    @Test
    void testEquals3() {
        SliderMedia sliderMedia = new SliderMedia();
        sliderMedia.setCreatedOn(mock(Timestamp.class));
        sliderMedia.setId(UUID.randomUUID());
        sliderMedia.setSliderEnum(SliderEnum.FIRST);
        sliderMedia.setSliderLocalizations(new ArrayList<>());
        sliderMedia.setSliderMedia("Slider Media");
        assertEquals(sliderMedia, sliderMedia);
        int expectedHashCodeResult = sliderMedia.hashCode();
        assertEquals(expectedHashCodeResult, sliderMedia.hashCode());
    }

    /**
     * Method under test: {@link SliderMedia#equals(Object)}
     */
    @Test
    void testEquals4() {
        SliderMedia sliderMedia = new SliderMedia();
        sliderMedia.setCreatedOn(mock(Timestamp.class));
        sliderMedia.setId(UUID.randomUUID());
        sliderMedia.setSliderEnum(SliderEnum.FIRST);
        sliderMedia.setSliderLocalizations(new ArrayList<>());
        sliderMedia.setSliderMedia("Slider Media");

        SliderMedia sliderMedia2 = new SliderMedia();
        sliderMedia2.setCreatedOn(mock(Timestamp.class));
        sliderMedia2.setId(UUID.randomUUID());
        sliderMedia2.setSliderEnum(SliderEnum.FIRST);
        sliderMedia2.setSliderLocalizations(new ArrayList<>());
        sliderMedia2.setSliderMedia("Slider Media");
        assertNotEquals(sliderMedia, sliderMedia2);
    }

    /**
     * Method under test: {@link SliderMedia#equals(Object)}
     */
    @Test
    void testEquals5() {
        SliderMedia sliderMedia = new SliderMedia();
        sliderMedia.setCreatedOn(mock(Timestamp.class));
        sliderMedia.setId(null);
        sliderMedia.setSliderEnum(SliderEnum.FIRST);
        sliderMedia.setSliderLocalizations(new ArrayList<>());
        sliderMedia.setSliderMedia("Slider Media");

        SliderMedia sliderMedia2 = new SliderMedia();
        sliderMedia2.setCreatedOn(mock(Timestamp.class));
        sliderMedia2.setId(UUID.randomUUID());
        sliderMedia2.setSliderEnum(SliderEnum.FIRST);
        sliderMedia2.setSliderLocalizations(new ArrayList<>());
        sliderMedia2.setSliderMedia("Slider Media");
        assertNotEquals(sliderMedia, sliderMedia2);
    }

    /**
     * Method under test: {@link SliderMedia#equals(Object)}
     */
    @Test
    void testEquals6() {
        SliderMedia sliderMedia = new SliderMedia();
        sliderMedia.setCreatedOn(mock(Timestamp.class));
        sliderMedia.setId(null);
        sliderMedia.setSliderEnum(SliderEnum.FIRST);
        sliderMedia.setSliderLocalizations(new ArrayList<>());
        sliderMedia.setSliderMedia("Slider Media");

        SliderMedia sliderMedia2 = new SliderMedia();
        sliderMedia2.setCreatedOn(mock(Timestamp.class));
        sliderMedia2.setId(null);
        sliderMedia2.setSliderEnum(SliderEnum.FIRST);
        sliderMedia2.setSliderLocalizations(new ArrayList<>());
        sliderMedia2.setSliderMedia("Slider Media");
        assertNotEquals(sliderMedia, sliderMedia2);
    }

    /**
     * Method under test: {@link SliderMedia#equals(Object)}
     */
    @Test
    void testEquals7() {
        SliderMedia sliderMedia = new SliderMedia();
        sliderMedia.setCreatedOn(null);
        sliderMedia.setId(null);
        sliderMedia.setSliderEnum(SliderEnum.FIRST);
        sliderMedia.setSliderLocalizations(new ArrayList<>());
        sliderMedia.setSliderMedia("Slider Media");

        SliderMedia sliderMedia2 = new SliderMedia();
        sliderMedia2.setCreatedOn(mock(Timestamp.class));
        sliderMedia2.setId(null);
        sliderMedia2.setSliderEnum(SliderEnum.FIRST);
        sliderMedia2.setSliderLocalizations(new ArrayList<>());
        sliderMedia2.setSliderMedia("Slider Media");
        assertNotEquals(sliderMedia, sliderMedia2);
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link SliderMedia#equals(Object)}
     *   <li>{@link SliderMedia#hashCode()}
     * </ul>
     */
    @Test
    void testEquals8() {
        SliderMedia sliderMedia = new SliderMedia();
        sliderMedia.setCreatedOn(null);
        sliderMedia.setId(null);
        sliderMedia.setSliderEnum(SliderEnum.FIRST);
        sliderMedia.setSliderLocalizations(new ArrayList<>());
        sliderMedia.setSliderMedia("Slider Media");

        SliderMedia sliderMedia2 = new SliderMedia();
        sliderMedia2.setCreatedOn(null);
        sliderMedia2.setId(null);
        sliderMedia2.setSliderEnum(SliderEnum.FIRST);
        sliderMedia2.setSliderLocalizations(new ArrayList<>());
        sliderMedia2.setSliderMedia("Slider Media");
        assertEquals(sliderMedia, sliderMedia2);
        int expectedHashCodeResult = sliderMedia.hashCode();
        assertEquals(expectedHashCodeResult, sliderMedia2.hashCode());
    }

    /**
     * Method under test: {@link SliderMedia#equals(Object)}
     */
    @Test
    void testEquals9() {
        SliderMedia sliderMedia = new SliderMedia();
        sliderMedia.setCreatedOn(null);
        sliderMedia.setId(null);
        sliderMedia.setSliderEnum(null);
        sliderMedia.setSliderLocalizations(new ArrayList<>());
        sliderMedia.setSliderMedia("Slider Media");

        SliderMedia sliderMedia2 = new SliderMedia();
        sliderMedia2.setCreatedOn(null);
        sliderMedia2.setId(null);
        sliderMedia2.setSliderEnum(SliderEnum.FIRST);
        sliderMedia2.setSliderLocalizations(new ArrayList<>());
        sliderMedia2.setSliderMedia("Slider Media");
        assertNotEquals(sliderMedia, sliderMedia2);
    }

    /**
     * Method under test: {@link SliderMedia#equals(Object)}
     */
    @Test
    void testEquals10() {
        SliderMedia sliderMedia = new SliderMedia();
        sliderMedia.setCreatedOn(null);
        sliderMedia.setId(null);
        sliderMedia.setSliderEnum(SliderEnum.SECOND);
        sliderMedia.setSliderLocalizations(new ArrayList<>());
        sliderMedia.setSliderMedia("Slider Media");

        SliderMedia sliderMedia2 = new SliderMedia();
        sliderMedia2.setCreatedOn(null);
        sliderMedia2.setId(null);
        sliderMedia2.setSliderEnum(SliderEnum.FIRST);
        sliderMedia2.setSliderLocalizations(new ArrayList<>());
        sliderMedia2.setSliderMedia("Slider Media");
        assertNotEquals(sliderMedia, sliderMedia2);
    }

    /**
     * Method under test: {@link SliderMedia#equals(Object)}
     */
    @Test
    void testEquals11() {
        SliderMedia sliderMedia = new SliderMedia();
        sliderMedia.setCreatedOn(mock(Timestamp.class));
        sliderMedia.setId(UUID.randomUUID());
        sliderMedia.setSliderEnum(SliderEnum.SECOND);
        sliderMedia.setSliderLocalizations(new ArrayList<>());
        sliderMedia.setSliderMedia("com.bl.mb.models.SliderMedia");

        SliderLocalization sliderLocalization = new SliderLocalization();
        sliderLocalization.setHtmlContent("Slider Media");
        sliderLocalization.setId(2L);
        sliderLocalization.setLocale("Slider Media");
        sliderLocalization.setSliderMedia(sliderMedia);

        ArrayList<SliderLocalization> sliderLocalizations = new ArrayList<>();
        sliderLocalizations.add(sliderLocalization);

        SliderMedia sliderMedia2 = new SliderMedia();
        sliderMedia2.setCreatedOn(null);
        sliderMedia2.setId(null);
        sliderMedia2.setSliderEnum(SliderEnum.FIRST);
        sliderMedia2.setSliderLocalizations(sliderLocalizations);
        sliderMedia2.setSliderMedia("Slider Media");

        SliderMedia sliderMedia3 = new SliderMedia();
        sliderMedia3.setCreatedOn(null);
        sliderMedia3.setId(null);
        sliderMedia3.setSliderEnum(SliderEnum.FIRST);
        sliderMedia3.setSliderLocalizations(new ArrayList<>());
        sliderMedia3.setSliderMedia("Slider Media");
        assertNotEquals(sliderMedia2, sliderMedia3);
    }

    /**
     * Method under test: {@link SliderMedia#equals(Object)}
     */
    @Test
    void testEquals12() {
        SliderMedia sliderMedia = new SliderMedia();
        sliderMedia.setCreatedOn(null);
        sliderMedia.setId(null);
        sliderMedia.setSliderEnum(SliderEnum.FIRST);
        sliderMedia.setSliderLocalizations(new ArrayList<>());
        sliderMedia.setSliderMedia(null);

        SliderMedia sliderMedia2 = new SliderMedia();
        sliderMedia2.setCreatedOn(null);
        sliderMedia2.setId(null);
        sliderMedia2.setSliderEnum(SliderEnum.FIRST);
        sliderMedia2.setSliderLocalizations(new ArrayList<>());
        sliderMedia2.setSliderMedia("Slider Media");
        assertNotEquals(sliderMedia, sliderMedia2);
    }

    /**
     * Method under test: {@link SliderMedia#equals(Object)}
     */
    @Test
    void testEquals13() {
        SliderMedia sliderMedia = new SliderMedia();
        sliderMedia.setCreatedOn(null);
        sliderMedia.setId(null);
        sliderMedia.setSliderEnum(SliderEnum.FIRST);
        sliderMedia.setSliderLocalizations(new ArrayList<>());
        sliderMedia.setSliderMedia("com.bl.mb.models.SliderMedia");

        SliderMedia sliderMedia2 = new SliderMedia();
        sliderMedia2.setCreatedOn(null);
        sliderMedia2.setId(null);
        sliderMedia2.setSliderEnum(SliderEnum.FIRST);
        sliderMedia2.setSliderLocalizations(new ArrayList<>());
        sliderMedia2.setSliderMedia("Slider Media");
        assertNotEquals(sliderMedia, sliderMedia2);
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link SliderMedia#equals(Object)}
     *   <li>{@link SliderMedia#hashCode()}
     * </ul>
     */
    @Test
    void testEquals14() {
        SliderMedia sliderMedia = new SliderMedia();
        sliderMedia.setCreatedOn(null);
        sliderMedia.setId(null);
        sliderMedia.setSliderEnum(null);
        sliderMedia.setSliderLocalizations(new ArrayList<>());
        sliderMedia.setSliderMedia("Slider Media");

        SliderMedia sliderMedia2 = new SliderMedia();
        sliderMedia2.setCreatedOn(null);
        sliderMedia2.setId(null);
        sliderMedia2.setSliderEnum(null);
        sliderMedia2.setSliderLocalizations(new ArrayList<>());
        sliderMedia2.setSliderMedia("Slider Media");
        assertEquals(sliderMedia, sliderMedia2);
        int expectedHashCodeResult = sliderMedia.hashCode();
        assertEquals(expectedHashCodeResult, sliderMedia2.hashCode());
    }
}
