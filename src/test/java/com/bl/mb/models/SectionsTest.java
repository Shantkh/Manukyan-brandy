package com.bl.mb.models;

import com.bl.mb.constances.PagesEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class SectionsTest {

    @Mock
    private SectionLocalization sectionsSectionLocalization;

    private Sections sections;

    @BeforeEach
    public void setup() {
        List<String> bannerMedia = new ArrayList<>();
        bannerMedia.add("banner_media");
        sections = Sections.builder()
                .id(UUID.randomUUID())
                .createdOn(new Timestamp(System.currentTimeMillis()))
                .localizations(new ArrayList<>())
                .sliderMedia(bannerMedia)
                .pagesEnum(PagesEnum.HOME_SLIDER)
                .build();
    }

    @Test
    public void testGetId() {
        UUID id = UUID.randomUUID();
        sections.setId(id);
        assertEquals(id, sections.getId());
    }

    @Test
    public void testGetCreatedOn() {
        Timestamp createdOn = new Timestamp(System.currentTimeMillis());
        sections.setCreatedOn(createdOn);
        assertEquals(createdOn, sections.getCreatedOn());
    }

    @Test
    public void testGetLocalizations() {
        List<SectionLocalization> sectionLocalizations = new ArrayList<>();
        sectionLocalizations.add(sectionsSectionLocalization);
        sections.setLocalizations(sectionLocalizations);
        assertEquals(sectionLocalizations, sections.getLocalizations());
    }

    @Test
    public void testGetBannerMedia() {
        List<String> bannerMedia = new ArrayList<>();
        bannerMedia.add("banner_media");
        sections.setSliderMedia(bannerMedia);
        assertEquals(bannerMedia, sections.getSliderMedia());
    }

    @Test
    public void testGetPagesEnum() {
        PagesEnum pagesEnum = PagesEnum.HOME_BANNER;
        sections.setPagesEnum(pagesEnum);
        assertEquals(pagesEnum, sections.getPagesEnum());
    }

    @Test
    public void testEquals() {
        Sections otherSections = Sections.builder()
                .id(sections.getId())
                .createdOn(sections.getCreatedOn())
                .localizations(sections.getLocalizations())
                .sliderMedia(sections.getSliderMedia())
                .pagesEnum(sections.getPagesEnum())
                .build();
        assertEquals(sections, otherSections);
    }

    @Test
    public void testHashCode() {
        Sections otherSections = Sections.builder()
                .id(sections.getId())
                .createdOn(sections.getCreatedOn())
                .localizations(sections.getLocalizations())
                .sliderMedia(sections.getSliderMedia())
                .pagesEnum(sections.getPagesEnum())
                .build();
        assertEquals(sections.hashCode(), otherSections.hashCode());
    }
}
