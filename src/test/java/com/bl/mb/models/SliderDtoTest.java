package com.bl.mb.models;

import com.bl.mb.constances.LinksEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.assertj.core.api.Assertions;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SliderDtoTest {

    private SliderDto<String, Integer> sliderDto;
    private UUID id;
    private Timestamp createdOn;
    private Map<String, Integer> data;

    @BeforeEach
    public void setUp() {
        id = UUID.randomUUID();
        createdOn = new Timestamp(System.currentTimeMillis());
        data = new HashMap<>();
        data.put("key1", 1);
        data.put("key2", 2);
        sliderDto = new SliderDto<>(id, createdOn, data, LinksEnum.FACTORY);
    }

    @Test
    public void testSliderDtoConstructorWhenValidParametersThenObjectCreated() {
        Assertions.assertThat(sliderDto).isNotNull();
        Assertions.assertThat(sliderDto).hasFieldOrPropertyWithValue("id", id);
        Assertions.assertThat(sliderDto).hasFieldOrPropertyWithValue("createdOn", createdOn);
        Assertions.assertThat(sliderDto).hasFieldOrPropertyWithValue("data", data);
    }

    @Test
    public void testGetIdWhenCalledThenReturnId() {
        UUID resultId = sliderDto.getId();
        Assertions.assertThat(resultId).isEqualTo(id);
    }

    @Test
    public void testGetCreatedOnWhenCalledThenReturnCreatedOn() {
        Timestamp resultCreatedOn = sliderDto.getCreatedOn();
        Assertions.assertThat(resultCreatedOn).isEqualTo(createdOn);
    }

    @Test
    public void testGetDataWhenCalledThenReturnData() {
        Map<String, Integer> resultData = sliderDto.getData();
        Assertions.assertThat(resultData).isEqualTo(data);
    }
}