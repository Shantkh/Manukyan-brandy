package com.bl.mb.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class TokenRequestTest {

    private TokenRequest tokenRequest;

    @BeforeEach
    public void setUp() {
        tokenRequest = new TokenRequest();
    }

    @Test
    public void testGetTokenWhenTokenIsSetThenReturnToken() {
        // Arrange
        String expectedToken = "testToken";
        tokenRequest.setToken(expectedToken);

        // Act
        String actualToken = tokenRequest.getToken();

        // Assert
        assertThat(actualToken).isEqualTo(expectedToken);
    }
}