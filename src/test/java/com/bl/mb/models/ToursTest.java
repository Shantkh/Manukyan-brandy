package com.bl.mb.models;

import com.bl.mb.constances.MonthOfYear;
import com.bl.mb.constances.WeekDaysEnum;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

class ToursTest {
    @Test
    void testToursConstructorWhenValidInputThenObjectCreatedWithCorrectState() {
        // Arrange
        UUID id = UUID.randomUUID();
        Timestamp createdOn = new Timestamp(System.currentTimeMillis());
        List<String> tourImages = Arrays.asList("image1.jpg", "image2.jpg");
        List<ToursLocalization> tourLocalization = List.of(new ToursLocalization());
        WeekDaysEnum tourDay = WeekDaysEnum.MONDAY;
        MonthOfYear tourMonth = MonthOfYear.JANUARY;
//        HoursOfTheDay tourHour = HoursOfTheDayDeserializer.deserialize("10:00");
        String dateOfTour = "01/01/2023";
        int weekNumber = 1;
        double duration = 2.5;
        double price = 99.99;

        // Act
        Tours tours = Tours.builder()
                .id(id)
                .createdOn(createdOn)
                .tourImages(tourImages)
                .tourLocalization(tourLocalization)
                .tourDay(tourDay)
                .tourMonth(tourMonth)
//                .tourHour(tourHour)
                .dateOfTour(dateOfTour)
                .weekNumber(weekNumber)
                .duration(duration)
                .price(price)
                .build();

        // Assert
        Assertions.assertEquals(id, tours.getId());
        Assertions.assertEquals(createdOn, tours.getCreatedOn());
        Assertions.assertEquals(tourImages, tours.getTourImages());
        Assertions.assertEquals(tourLocalization, tours.getTourLocalization());
        Assertions.assertEquals(tourDay, tours.getTourDay());
        Assertions.assertEquals(tourMonth, tours.getTourMonth());
//        Assertions.assertEquals(tourHour, tours.getTourHour());
        Assertions.assertEquals(dateOfTour, tours.getDateOfTour());
        Assertions.assertEquals(weekNumber, tours.getWeekNumber());
        Assertions.assertEquals(duration, tours.getDuration());
        Assertions.assertEquals(price, tours.getPrice());
    }

    @Test
    void testToursConstructorWhenPartialInputThenObjectCreatedWithCorrectState() {
        // Arrange
        UUID id = UUID.randomUUID();
        WeekDaysEnum tourDay = WeekDaysEnum.FRIDAY;
        double price = 49.99;

        // Act
        Tours tours = Tours.builder()
                .id(id)
                .tourDay(tourDay)
                .price(price)
                .build();

        // Assert
        Assertions.assertEquals(id, tours.getId());
        Assertions.assertNull(tours.getCreatedOn());
        Assertions.assertNull(tours.getTourImages());
        Assertions.assertNull(tours.getTourLocalization());
        Assertions.assertEquals(tourDay, tours.getTourDay());
        Assertions.assertNull(tours.getTourMonth());
        Assertions.assertNull(tours.getTourHour());
        Assertions.assertNull(tours.getDateOfTour());
        Assertions.assertEquals(0, tours.getWeekNumber());
        Assertions.assertEquals(0.0, tours.getDuration());
        Assertions.assertEquals(price, tours.getPrice());
    }

    @Test
    void testToursConstructorWhenNoInputThenObjectCreatedWithDefaultState() {
        // Act
        Tours tours = new Tours();

        // Assert
        Assertions.assertNull(tours.getId());
        Assertions.assertNull(tours.getCreatedOn());
        Assertions.assertNull(tours.getTourImages());
        Assertions.assertNull(tours.getTourLocalization());
        Assertions.assertNull(tours.getTourDay());
        Assertions.assertNull(tours.getTourMonth());
        Assertions.assertNull(tours.getTourHour());
        Assertions.assertNull(tours.getDateOfTour());
        Assertions.assertEquals(0, tours.getWeekNumber());
        Assertions.assertEquals(0.0, tours.getDuration());
        Assertions.assertEquals(0.0, tours.getPrice());
    }
}