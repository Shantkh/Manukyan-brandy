package com.bl.mb.models;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ForgotPasswordRequestTest {

    @Test
    void testConstructor() {
        ForgotPasswordRequest actualForgotPasswordRequest = new ForgotPasswordRequest();
        actualForgotPasswordRequest.setEmail("jane.doe@example.org");
        assertEquals("jane.doe@example.org", actualForgotPasswordRequest.getEmail());
    }

    @Test
    void testConstructor2() {
        ForgotPasswordRequest actualForgotPasswordRequest = new ForgotPasswordRequest("jane.doe@example.org");
        actualForgotPasswordRequest.setEmail("jane.doe@example.org");
        assertEquals("jane.doe@example.org", actualForgotPasswordRequest.getEmail());
    }
}