package com.bl.mb.models;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class StatisticsResultTest {

    @Test
    public void testGetUsersLast30Days() {
        // Arrange
        long expectedUsersLast30Days = 100;
        StatisticsResult statisticsResult = new StatisticsResult();
        statisticsResult.setUsersLast30Days(expectedUsersLast30Days);

        // Act
        long actualUsersLast30Days = statisticsResult.getUsersLast30Days();

        // Assert
        assertThat(actualUsersLast30Days).isEqualTo(expectedUsersLast30Days);
    }

    @Test
    public void testGetUsersLast15Days() {
        // Arrange
        long expectedUsersLast15Days = 50;
        StatisticsResult statisticsResult = new StatisticsResult();
        statisticsResult.setUsersLast15Days(expectedUsersLast15Days);

        // Act
        long actualUsersLast15Days = statisticsResult.getUsersLast15Days();

        // Assert
        assertThat(actualUsersLast15Days).isEqualTo(expectedUsersLast15Days);
    }

    @Test
    public void testGetUsersLast7Days() {
        // Arrange
        long expectedUsersLast7Days = 25;
        StatisticsResult statisticsResult = new StatisticsResult();
        statisticsResult.setUsersLast7Days(expectedUsersLast7Days);

        // Act
        long actualUsersLast7Days = statisticsResult.getUsersLast7Days();

        // Assert
        assertThat(actualUsersLast7Days).isEqualTo(expectedUsersLast7Days);
    }

    @Test
    public void testGetUsersToday() {
        // Arrange
        long expectedUsersToday = 10;
        StatisticsResult statisticsResult = new StatisticsResult();
        statisticsResult.setUsersToday(expectedUsersToday);

        // Act
        long actualUsersToday = statisticsResult.getUsersToday();

        // Assert
        assertThat(actualUsersToday).isEqualTo(expectedUsersToday);
    }
}