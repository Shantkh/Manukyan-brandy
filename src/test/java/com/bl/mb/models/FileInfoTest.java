package com.bl.mb.models;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class FileInfoTest {
    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link FileInfo#FileInfo()}
     *   <li>{@link FileInfo#setName(String)}
     *   <li>{@link FileInfo#setUrl(String)}
     *   <li>{@link FileInfo#getName()}
     *   <li>{@link FileInfo#getUrl()}
     * </ul>
     */
    @Test
    void testConstructor() {
        FileInfo actualFileInfo = new FileInfo();
        actualFileInfo.setName("Name");
        actualFileInfo.setUrl("https://example.org/example");
        String actualName = actualFileInfo.getName();
        assertEquals("Name", actualName);
        assertEquals("https://example.org/example", actualFileInfo.getUrl());
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link FileInfo#FileInfo(String, String)}
     *   <li>{@link FileInfo#setName(String)}
     *   <li>{@link FileInfo#setUrl(String)}
     *   <li>{@link FileInfo#getName()}
     *   <li>{@link FileInfo#getUrl()}
     * </ul>
     */
    @Test
    void testConstructor2() {
        FileInfo actualFileInfo = new FileInfo("Name", "https://example.org/example");
        actualFileInfo.setName("Name");
        actualFileInfo.setUrl("https://example.org/example");
        String actualName = actualFileInfo.getName();
        assertEquals("Name", actualName);
        assertEquals("https://example.org/example", actualFileInfo.getUrl());
    }
}
