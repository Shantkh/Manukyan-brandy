package com.bl.mb.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.mock;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.UUID;

import org.junit.jupiter.api.Test;

class ProductsLocalizationTest {
    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link ProductsDescriptionLocalization#setHtmlContent(String)}
     *   <li>{@link ProductsDescriptionLocalization#setId(Long)}
     *   <li>{@link ProductsDescriptionLocalization#setLocale(String)}
     *   <li>{@link ProductsDescriptionLocalization#setProducts(Products)}
     *   <li>{@link ProductsDescriptionLocalization#getHtmlContent()}
     *   <li>{@link ProductsDescriptionLocalization#getId()}
     *   <li>{@link ProductsDescriptionLocalization#getLocale()}
     *   <li>{@link ProductsDescriptionLocalization#getProducts()}
     * </ul>
     */
    @Test
    void testConstructor() {
        ProductsDescriptionLocalization actualProductsLocalization = new ProductsDescriptionLocalization();
        actualProductsLocalization.setHtmlContent("Not all who wander are lost");
        actualProductsLocalization.setId(1L);
        actualProductsLocalization.setLocale("en");
        Products products = new Products();
        products.setCreatedOn(mock(Timestamp.class));
        products.setDescription(new ArrayList<>());
        products.setId(UUID.randomUUID());
        products.setPageTitle(new ArrayList<>());
        products.setProductDate("2020-03-01");
        products.setProductImages(new ArrayList<>());
        products.setProductType(new ArrayList<>());
        actualProductsLocalization.setProducts(products);
        String actualHtmlContent = actualProductsLocalization.getHtmlContent();
        Long actualId = actualProductsLocalization.getId();
        String actualLocale = actualProductsLocalization.getLocale();
        Products actualProducts = actualProductsLocalization.getProducts();
        assertEquals("Not all who wander are lost", actualHtmlContent);
        assertEquals("en", actualLocale);
        assertEquals(1L, actualId.longValue());
        assertSame(products, actualProducts);
    }


    @Test
    void testConstructor2() {
        Products products = new Products();
        products.setCreatedOn(mock(Timestamp.class));
        products.setDescription(new ArrayList<>());
        products.setId(UUID.randomUUID());
        products.setPageTitle(new ArrayList<>());
        products.setProductDate("2020-03-01");
        products.setProductImages(new ArrayList<>());
        products.setProductType(new ArrayList<>());
        ProductsDescriptionLocalization actualProductsLocalization = new ProductsDescriptionLocalization(1L, products, "en",
                "Not all who wander are lost");
        actualProductsLocalization.setHtmlContent("Not all who wander are lost");
        actualProductsLocalization.setId(1L);
        actualProductsLocalization.setLocale("en");
        Products products2 = new Products();
        products2.setCreatedOn(mock(Timestamp.class));
        products2.setDescription(new ArrayList<>());
        products2.setId(UUID.randomUUID());
        products2.setPageTitle(new ArrayList<>());
        products2.setProductDate("2020-03-01");
        products2.setProductImages(new ArrayList<>());
        products2.setProductType(new ArrayList<>());
        actualProductsLocalization.setProducts(products2);
        String actualHtmlContent = actualProductsLocalization.getHtmlContent();
        Long actualId = actualProductsLocalization.getId();
        String actualLocale = actualProductsLocalization.getLocale();
        Products actualProducts = actualProductsLocalization.getProducts();
        assertEquals("Not all who wander are lost", actualHtmlContent);
        assertEquals("en", actualLocale);
        assertEquals(1L, actualId.longValue());
        assertSame(products2, actualProducts);
    }
}
