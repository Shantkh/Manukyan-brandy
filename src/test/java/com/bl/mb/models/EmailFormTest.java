package com.bl.mb.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class EmailFormTest {
    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link EmailForm#EmailForm()}
     *   <li>{@link EmailForm#setEmail(String)}
     *   <li>{@link EmailForm#setMessage(String)}
     *   <li>{@link EmailForm#setName(String)}
     *   <li>{@link EmailForm#setPhone(String)}
     *   <li>{@link EmailForm#setSubject(String)}
     *   <li>{@link EmailForm#setSubscribe(boolean)}
     *   <li>{@link EmailForm#getEmail()}
     *   <li>{@link EmailForm#getMessage()}
     *   <li>{@link EmailForm#getName()}
     *   <li>{@link EmailForm#getPhone()}
     *   <li>{@link EmailForm#getSubject()}
     *   <li>{@link EmailForm#isSubscribe()}
     * </ul>
     */
    @Test
    void testConstructor() {
        EmailForm actualEmailForm = new EmailForm();
        actualEmailForm.setEmail("jane.doe@example.org");
        actualEmailForm.setMessage("Not all who wander are lost");
        actualEmailForm.setName("Name");
        actualEmailForm.setPhone("6625550144");
        actualEmailForm.setSubject("Hello from the Dreaming Spires");
        actualEmailForm.setSubscribe(true);
        String actualEmail = actualEmailForm.getEmail();
        String actualMessage = actualEmailForm.getMessage();
        String actualName = actualEmailForm.getName();
        String actualPhone = actualEmailForm.getPhone();
        String actualSubject = actualEmailForm.getSubject();
        assertEquals("6625550144", actualPhone);
        assertEquals("Hello from the Dreaming Spires", actualSubject);
        assertEquals("Name", actualName);
        assertEquals("Not all who wander are lost", actualMessage);
        assertEquals("jane.doe@example.org", actualEmail);
        assertTrue(actualEmailForm.isSubscribe());
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>
     * {@link EmailForm#EmailForm(String, String, String, String, String, boolean)}
     *   <li>{@link EmailForm#setEmail(String)}
     *   <li>{@link EmailForm#setMessage(String)}
     *   <li>{@link EmailForm#setName(String)}
     *   <li>{@link EmailForm#setPhone(String)}
     *   <li>{@link EmailForm#setSubject(String)}
     *   <li>{@link EmailForm#setSubscribe(boolean)}
     *   <li>{@link EmailForm#getEmail()}
     *   <li>{@link EmailForm#getMessage()}
     *   <li>{@link EmailForm#getName()}
     *   <li>{@link EmailForm#getPhone()}
     *   <li>{@link EmailForm#getSubject()}
     *   <li>{@link EmailForm#isSubscribe()}
     * </ul>
     */
    @Test
    void testConstructor2() {
        EmailForm actualEmailForm = new EmailForm("Name", "jane.doe@example.org", "Hello from the Dreaming Spires",
                "6625550144", "Not all who wander are lost", true);
        actualEmailForm.setEmail("jane.doe@example.org");
        actualEmailForm.setMessage("Not all who wander are lost");
        actualEmailForm.setName("Name");
        actualEmailForm.setPhone("6625550144");
        actualEmailForm.setSubject("Hello from the Dreaming Spires");
        actualEmailForm.setSubscribe(true);
        String actualEmail = actualEmailForm.getEmail();
        String actualMessage = actualEmailForm.getMessage();
        String actualName = actualEmailForm.getName();
        String actualPhone = actualEmailForm.getPhone();
        String actualSubject = actualEmailForm.getSubject();
        assertEquals("6625550144", actualPhone);
        assertEquals("Hello from the Dreaming Spires", actualSubject);
        assertEquals("Name", actualName);
        assertEquals("Not all who wander are lost", actualMessage);
        assertEquals("jane.doe@example.org", actualEmail);
        assertTrue(actualEmailForm.isSubscribe());
    }
}
