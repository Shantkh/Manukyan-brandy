package com.bl.mb.models;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GeoIPTest {

    @Test
    public void testGetIpAddress() {
        // Arrange
        String expectedIpAddress = "192.168.0.1";
        GeoIP geoIP = new GeoIP(expectedIpAddress, "", "", "");

        // Act
        String actualIpAddress = geoIP.getIpAddress();

        // Assert
        assertThat(actualIpAddress).isEqualTo(expectedIpAddress);
    }

    @Test
    public void testGetCity() {
        // Arrange
        String expectedCity = "Yerevan";
        GeoIP geoIP = new GeoIP("", expectedCity, "", "");

        // Act
        String actualCity = geoIP.getCity();

        // Assert
        assertThat(actualCity).isEqualTo(expectedCity);
    }

    @Test
    public void testGetLatitude() {
        // Arrange
        String expectedLatitude = "40.177200";
        GeoIP geoIP = new GeoIP("", "", expectedLatitude, "");

        // Act
        String actualLatitude = geoIP.getLatitude();

        // Assert
        assertThat(actualLatitude).isEqualTo(expectedLatitude);
    }

    @Test
    public void testGetLongitude() {
        // Arrange
        String expectedLongitude = "44.503490";
        GeoIP geoIP = new GeoIP("", "", "", expectedLongitude);

        // Act
        String actualLongitude = geoIP.getLongitude();

        // Assert
        assertThat(actualLongitude).isEqualTo(expectedLongitude);
    }
}