package com.bl.mb.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import java.util.Date;
import static org.assertj.core.api.Assertions.assertThat;

public class ResponseBaseTest {

    private ResponseBase<Object> responseBase;
    private Object testData;
    private HttpStatus testHttpStatus;
    private int testHttpStatusCode;
    private Date testDate;
    private boolean testIsSuccess;

    @BeforeEach
    public void setUp() {
        testData = new Object();
        testHttpStatus = HttpStatus.OK;
        testHttpStatusCode = HttpStatus.OK.value();
        testDate = new Date();
        testIsSuccess = true;

        responseBase = ResponseBase.builder()
                .data(testData)
                .httpStatus(testHttpStatus)
                .httpStatusCode(testHttpStatusCode)
                .date(testDate)
                .isSuccess(testIsSuccess)
                .build();
    }

    @Test
    public void testGetDataWhenDataIsSetThenReturnData() {
        // Act
        Object data = responseBase.getData();

        // Assert
        assertThat(data).isEqualTo(testData);
    }

    @Test
    public void testGetHttpStatusWhenHttpStatusIsSetThenReturnHttpStatus() {
        // Act
        HttpStatus httpStatus = responseBase.getHttpStatus();

        // Assert
        assertThat(httpStatus).isEqualTo(testHttpStatus);
    }

    @Test
    public void testGetHttpStatusCodeWhenHttpStatusCodeIsSetThenReturnHttpStatusCode() {
        // Act
        int httpStatusCode = responseBase.getHttpStatusCode();

        // Assert
        assertThat(httpStatusCode).isEqualTo(testHttpStatusCode);
    }

    @Test
    public void testGetDateWhenDateIsSetThenReturnDate() {
        // Act
        Date date = responseBase.getDate();

        // Assert
        assertThat(date).isEqualTo(testDate);
    }

    @Test
    public void testIsSuccessWhenIsSuccessIsSetThenReturnIsSuccess() {
        // Act
        boolean isSuccess = responseBase.isSuccess();

        // Assert
        assertThat(isSuccess).isEqualTo(testIsSuccess);
    }
}