package com.bl.mb.models;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class UserResponseTest {

    @Test
    public void testGetId() {
        // Arrange
        UUID expectedId = UUID.randomUUID();
        UserResponse userResponse = UserResponse.builder()
                .id(expectedId)
                .build();

        // Act
        UUID actualId = userResponse.getId();

        // Assert
        assertThat(actualId).isEqualTo(expectedId);
    }

    @Test
    public void testGetUsername() {
        // Arrange
        String expectedUsername = "testUser";
        UserResponse userResponse = UserResponse.builder()
                .username(expectedUsername)
                .build();

        // Act
        String actualUsername = userResponse.getUsername();

        // Assert
        assertThat(actualUsername).isEqualTo(expectedUsername);
    }

    @Test
    public void testGetEmail() {
        // Arrange
        String expectedEmail = "test@example.com";
        UserResponse userResponse = UserResponse.builder()
                .email(expectedEmail)
                .build();

        // Act
        String actualEmail = userResponse.getEmail();

        // Assert
        assertThat(actualEmail).isEqualTo(expectedEmail);
    }

    @Test
    public void testGetRoles() {
        // Arrange
        Collection<String> expectedRoles = Arrays.asList("ROLE_ADMIN", "ROLE_USER");
        UserResponse userResponse = UserResponse.builder()
                .roles(expectedRoles)
                .build();

        // Act
        Collection<String> actualRoles = userResponse.getRoles();

        // Assert
        assertThat(actualRoles).isEqualTo(expectedRoles);
    }

    @Test
    public void testGetAccessToken() {
        // Arrange
        String expectedAccessToken = "testAccessToken";
        UserResponse userResponse = UserResponse.builder()
                .accessToken(expectedAccessToken)
                .build();

        // Act
        String actualAccessToken = userResponse.getAccessToken();

        // Assert
        assertThat(actualAccessToken).isEqualTo(expectedAccessToken);
    }

    @Test
    public void testGetRefreshToken() {
        // Arrange
        String expectedRefreshToken = "testRefreshToken";
        UserResponse userResponse = UserResponse.builder()
                .refreshToken(expectedRefreshToken)
                .build();

        // Act
        String actualRefreshToken = userResponse.getRefreshToken();

        // Assert
        assertThat(actualRefreshToken).isEqualTo(expectedRefreshToken);
    }
}