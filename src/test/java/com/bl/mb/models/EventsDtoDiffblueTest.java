package com.bl.mb.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;

class EventsDtoDiffblueTest {
    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link EventsDto#EventsDto()}
     *   <li>{@link EventsDto#setCreatedOn(Timestamp)}
     *   <li>{@link EventsDto#setEventEndDate(Timestamp)}
     *   <li>{@link EventsDto#setEventStartDate(Timestamp)}
     *   <li>{@link EventsDto#setEventVisible(boolean)}
     *   <li>{@link EventsDto#setId(UUID)}
     *   <li>{@link EventsDto#setLocalizations(List)}
     *   <li>{@link EventsDto#setSliderMedia(List)}
     *   <li>{@link EventsDto#getCreatedOn()}
     *   <li>{@link EventsDto#getEventEndDate()}
     *   <li>{@link EventsDto#getEventStartDate()}
     *   <li>{@link EventsDto#getId()}
     *   <li>{@link EventsDto#getLocalizations()}
     *   <li>{@link EventsDto#getSliderMedia()}
     *   <li>{@link EventsDto#isEventVisible()}
     * </ul>
     */
    @Test
    void testConstructor() {
        EventsDto actualEventsDto = new EventsDto();
        Timestamp createdOn = mock(Timestamp.class);
        actualEventsDto.setCreatedOn(createdOn);
        Timestamp eventEndDate = mock(Timestamp.class);
        actualEventsDto.setEventEndDate(eventEndDate);
        Timestamp eventStartDate = mock(Timestamp.class);
        actualEventsDto.setEventStartDate(eventStartDate);
        actualEventsDto.setEventVisible(true);
        UUID id = UUID.randomUUID();
        actualEventsDto.setId(id);
        ArrayList<EventsLocalization> localizations = new ArrayList<>();
        actualEventsDto.setLocalizations(localizations);
        ArrayList<String> sliderMedia = new ArrayList<>();
        actualEventsDto.setSliderMedia(sliderMedia);
        Timestamp actualCreatedOn = actualEventsDto.getCreatedOn();
        Timestamp actualEventEndDate = actualEventsDto.getEventEndDate();
        Timestamp actualEventStartDate = actualEventsDto.getEventStartDate();
        UUID actualId = actualEventsDto.getId();
        List<EventsLocalization> actualLocalizations = actualEventsDto.getLocalizations();
        List<String> actualSliderMedia = actualEventsDto.getSliderMedia();
        assertTrue(actualEventsDto.isEventVisible());
        assertEquals(actualLocalizations, actualSliderMedia);
        assertSame(localizations, actualLocalizations);
        assertSame(sliderMedia, actualSliderMedia);
        assertSame(id, actualId);
        assertSame(createdOn, actualCreatedOn);
        assertSame(eventEndDate, actualEventEndDate);
        assertSame(eventStartDate, actualEventStartDate);
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>
     * {@link EventsDto#EventsDto(UUID, Timestamp, List, List, Timestamp, Timestamp, boolean)}
     *   <li>{@link EventsDto#setCreatedOn(Timestamp)}
     *   <li>{@link EventsDto#setEventEndDate(Timestamp)}
     *   <li>{@link EventsDto#setEventStartDate(Timestamp)}
     *   <li>{@link EventsDto#setEventVisible(boolean)}
     *   <li>{@link EventsDto#setId(UUID)}
     *   <li>{@link EventsDto#setLocalizations(List)}
     *   <li>{@link EventsDto#setSliderMedia(List)}
     *   <li>{@link EventsDto#getCreatedOn()}
     *   <li>{@link EventsDto#getEventEndDate()}
     *   <li>{@link EventsDto#getEventStartDate()}
     *   <li>{@link EventsDto#getId()}
     *   <li>{@link EventsDto#getLocalizations()}
     *   <li>{@link EventsDto#getSliderMedia()}
     *   <li>{@link EventsDto#isEventVisible()}
     * </ul>
     */
    @Test
    void testConstructor2() {
        UUID id = UUID.randomUUID();
        Timestamp createdOn = mock(Timestamp.class);
        ArrayList<EventsLocalization> localizations = new ArrayList<>();
        EventsDto actualEventsDto = new EventsDto(id, createdOn, localizations, new ArrayList<>(), mock(Timestamp.class),
                mock(Timestamp.class), true);
        Timestamp createdOn2 = mock(Timestamp.class);
        actualEventsDto.setCreatedOn(createdOn2);
        Timestamp eventEndDate = mock(Timestamp.class);
        actualEventsDto.setEventEndDate(eventEndDate);
        Timestamp eventStartDate = mock(Timestamp.class);
        actualEventsDto.setEventStartDate(eventStartDate);
        actualEventsDto.setEventVisible(true);
        UUID id2 = UUID.randomUUID();
        actualEventsDto.setId(id2);
        ArrayList<EventsLocalization> localizations2 = new ArrayList<>();
        actualEventsDto.setLocalizations(localizations2);
        ArrayList<String> sliderMedia = new ArrayList<>();
        actualEventsDto.setSliderMedia(sliderMedia);
        Timestamp actualCreatedOn = actualEventsDto.getCreatedOn();
        Timestamp actualEventEndDate = actualEventsDto.getEventEndDate();
        Timestamp actualEventStartDate = actualEventsDto.getEventStartDate();
        UUID actualId = actualEventsDto.getId();
        List<EventsLocalization> actualLocalizations = actualEventsDto.getLocalizations();
        List<String> actualSliderMedia = actualEventsDto.getSliderMedia();
        assertTrue(actualEventsDto.isEventVisible());
        assertEquals(localizations, actualLocalizations);
        assertEquals(localizations, actualSliderMedia);
        assertSame(localizations2, actualLocalizations);
        assertSame(sliderMedia, actualSliderMedia);
        assertSame(id2, actualId);
        assertSame(createdOn2, actualCreatedOn);
        assertSame(eventEndDate, actualEventEndDate);
        assertSame(eventStartDate, actualEventStartDate);
    }
}
