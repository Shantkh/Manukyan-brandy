package com.bl.mb.models;

import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class EmailDetailsTest {

    @Test
    public void testGetId() {
        // Arrange
        UUID expectedId = UUID.randomUUID();
        EmailDetails emailDetails = EmailDetails.builder()
                .id(expectedId)
                .build();

        // Act
        UUID actualId = emailDetails.getId();

        // Assert
        assertThat(actualId).isEqualTo(expectedId);
    }

    @Test
    public void testGetRecipient() {
        // Arrange
        String expectedRecipient = "test@example.com";
        EmailDetails emailDetails = EmailDetails.builder()
                .recipient(expectedRecipient)
                .build();

        // Act
        String actualRecipient = emailDetails.getRecipient();

        // Assert
        assertThat(actualRecipient).isEqualTo(expectedRecipient);
    }

    @Test
    public void testGetMsgBody() {
        // Arrange
        String expectedMsgBody = "This is a test email";
        EmailDetails emailDetails = EmailDetails.builder()
                .msgBody(expectedMsgBody)
                .build();

        // Act
        String actualMsgBody = emailDetails.getMsgBody();

        // Assert
        assertThat(actualMsgBody).isEqualTo(expectedMsgBody);
    }

    @Test
    public void testGetSubject() {
        // Arrange
        String expectedSubject = "Test Email";
        EmailDetails emailDetails = EmailDetails.builder()
                .subject(expectedSubject)
                .build();

        // Act
        String actualSubject = emailDetails.getSubject();

        // Assert
        assertThat(actualSubject).isEqualTo(expectedSubject);
    }

    @Test
    public void testGetAttachment() {
        // Arrange
        String expectedAttachment = "test.pdf";
        EmailDetails emailDetails = EmailDetails.builder()
                .attachment(expectedAttachment)
                .build();

        // Act
        String actualAttachment = emailDetails.getAttachment();

        // Assert
        assertThat(actualAttachment).isEqualTo(expectedAttachment);
    }

    @Test
    public void testGetCode() {
        // Arrange
        String expectedCode = "123456";
        EmailDetails emailDetails = EmailDetails.builder()
                .code(expectedCode)
                .build();

        // Act
        String actualCode = emailDetails.getCode();

        // Assert
        assertThat(actualCode).isEqualTo(expectedCode);
    }

    @Test
    public void testGetCreatedDate() {
        // Arrange
        Timestamp expectedCreatedDate = new Timestamp(System.currentTimeMillis());
        EmailDetails emailDetails = EmailDetails.builder()
                .createdDate(expectedCreatedDate)
                .build();

        // Act
        Timestamp actualCreatedDate = emailDetails.getCreatedDate();

        // Assert
        assertThat(actualCreatedDate).isEqualTo(expectedCreatedDate);
    }
}