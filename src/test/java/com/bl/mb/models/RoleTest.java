package com.bl.mb.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

public class RoleTest {

    private Role role;
    private UUID roleId;
    private Timestamp createdOn;
    private Timestamp updatedOn;
    private Collection<User> users;
    private String roleName;

    @BeforeEach
    public void setUp() {
        roleId = UUID.randomUUID();
        createdOn = new Timestamp(System.currentTimeMillis());
        updatedOn = new Timestamp(System.currentTimeMillis());
        users = new ArrayList<>();
        roleName = "Test Role";

        role = new Role(roleId, createdOn, updatedOn, users, roleName);
    }

    @Test
    public void testGetId() {
        Assertions.assertEquals(roleId, role.getId());
    }

    @Test
    public void testGetCreatedOn() {
        Assertions.assertEquals(createdOn, role.getCreatedOn());
    }

    @Test
    public void testGetUpdatedOn() {
        Assertions.assertEquals(updatedOn, role.getUpdatedOn());
    }

    @Test
    public void testGetUsers() {
        Assertions.assertEquals(users, role.getUsers());
    }

    @Test
    public void testGetName() {
        Assertions.assertEquals(roleName, role.getName());
    }

//    @Test
//    public void testEquals() {
//        Role sameRole = new Role(roleId, createdOn, updatedOn, users, roleName);
//        Role differentRole = new Role(UUID.randomUUID(), createdOn, updatedOn, users, roleName);
//
//        Assertions.assertEquals(role, sameRole);
//        Assertions.assertNotEquals(role, differentRole);
//    }

    @Test
    public void testToString() {
        String expectedString = "Role [name=Test Role][id=" + roleId + "]";
        Assertions.assertEquals(expectedString, role.toString());
    }
}