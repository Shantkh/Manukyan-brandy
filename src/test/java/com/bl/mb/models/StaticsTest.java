package com.bl.mb.models;

import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class StaticsTest {

    @Test
    public void testGetId() {
        // Arrange
        UUID expectedId = UUID.randomUUID();
        Statics statics = Statics.builder()
                .id(expectedId)
                .build();

        // Act
        UUID actualId = statics.getId();

        // Assert
        assertThat(actualId).isEqualTo(expectedId);
    }

    @Test
    public void testGetCreatedOn() {
        // Arrange
        Timestamp expectedCreatedOn = new Timestamp(System.currentTimeMillis());
        Statics statics = Statics.builder()
                .createdOn(expectedCreatedOn)
                .build();

        // Act
        Timestamp actualCreatedOn = statics.getCreatedOn();

        // Assert
        assertThat(actualCreatedOn).isEqualTo(expectedCreatedOn);
    }

    @Test
    public void testGetPage() {
        // Arrange
        String expectedPage = "home";
        Statics statics = Statics.builder()
                .page(expectedPage)
                .build();

        // Act
        String actualPage = statics.getPage();

        // Assert
        assertThat(actualPage).isEqualTo(expectedPage);
    }

    @Test
    public void testGetCountry() {
        // Arrange
        String expectedCountry = "USA";
        Statics statics = Statics.builder()
                .country(expectedCountry)
                .build();

        // Act
        String actualCountry = statics.getCountry();

        // Assert
        assertThat(actualCountry).isEqualTo(expectedCountry);
    }

    @Test
    public void testGetIp() {
        // Arrange
        String expectedIp = "192.168.0.1";
        Statics statics = Statics.builder()
                .ip(expectedIp)
                .build();

        // Act
        String actualIp = statics.getIp();

        // Assert
        assertThat(actualIp).isEqualTo(expectedIp);
    }
}