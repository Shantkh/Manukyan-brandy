package com.bl.mb.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.mock;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.UUID;

import org.junit.jupiter.api.Test;

class EventsLocalizationDiffblueTest {
    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link EventsLocalization#EventsLocalization()}
     *   <li>{@link EventsLocalization#setEvents(Events)}
     *   <li>{@link EventsLocalization#setHtmlLongContent(String)}
     *   <li>{@link EventsLocalization#setHtmlShortContent(String)}
     *   <li>{@link EventsLocalization#setHtmlTitle(String)}
     *   <li>{@link EventsLocalization#setId(Long)}
     *   <li>{@link EventsLocalization#setLocale(String)}
     *   <li>{@link EventsLocalization#getEvents()}
     *   <li>{@link EventsLocalization#getHtmlLongContent()}
     *   <li>{@link EventsLocalization#getHtmlShortContent()}
     *   <li>{@link EventsLocalization#getHtmlTitle()}
     *   <li>{@link EventsLocalization#getId()}
     *   <li>{@link EventsLocalization#getLocale()}
     * </ul>
     */
    @Test
    void testConstructor() {
        EventsLocalization actualEventsLocalization = new EventsLocalization();
        Events events = new Events();
        events.setCreatedOn(mock(Timestamp.class));
        events.setEventEndDate(mock(Timestamp.class));
        events.setEventStartDate(mock(Timestamp.class));
        events.setEventVisible(true);
        events.setId(UUID.randomUUID());
        events.setLocalizations(new ArrayList<>());
        events.setSliderMedia(new ArrayList<>());
        actualEventsLocalization.setEvents(events);
        actualEventsLocalization.setHtmlLongContent("Not all who wander are lost");
        actualEventsLocalization.setHtmlShortContent("Not all who wander are lost");
        actualEventsLocalization.setHtmlTitle("Dr");
        actualEventsLocalization.setId(1L);
        actualEventsLocalization.setLocale("en");
        Events actualEvents = actualEventsLocalization.getEvents();
        String actualHtmlLongContent = actualEventsLocalization.getHtmlLongContent();
        String actualHtmlShortContent = actualEventsLocalization.getHtmlShortContent();
        String actualHtmlTitle = actualEventsLocalization.getHtmlTitle();
        Long actualId = actualEventsLocalization.getId();
        assertEquals("Dr", actualHtmlTitle);
        assertEquals("Not all who wander are lost", actualHtmlLongContent);
        assertEquals("Not all who wander are lost", actualHtmlShortContent);
        assertEquals("en", actualEventsLocalization.getLocale());
        assertEquals(1L, actualId.longValue());
        assertSame(events, actualEvents);
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>
     * {@link EventsLocalization#EventsLocalization(Long, Events, String, String, String, String)}
     *   <li>{@link EventsLocalization#setEvents(Events)}
     *   <li>{@link EventsLocalization#setHtmlLongContent(String)}
     *   <li>{@link EventsLocalization#setHtmlShortContent(String)}
     *   <li>{@link EventsLocalization#setHtmlTitle(String)}
     *   <li>{@link EventsLocalization#setId(Long)}
     *   <li>{@link EventsLocalization#setLocale(String)}
     *   <li>{@link EventsLocalization#getEvents()}
     *   <li>{@link EventsLocalization#getHtmlLongContent()}
     *   <li>{@link EventsLocalization#getHtmlShortContent()}
     *   <li>{@link EventsLocalization#getHtmlTitle()}
     *   <li>{@link EventsLocalization#getId()}
     *   <li>{@link EventsLocalization#getLocale()}
     * </ul>
     */
    @Test
    void testConstructor2() {
        Events events = new Events();
        events.setCreatedOn(mock(Timestamp.class));
        events.setEventEndDate(mock(Timestamp.class));
        events.setEventStartDate(mock(Timestamp.class));
        events.setEventVisible(true);
        events.setId(UUID.randomUUID());
        events.setLocalizations(new ArrayList<>());
        events.setSliderMedia(new ArrayList<>());
        EventsLocalization actualEventsLocalization = new EventsLocalization(1L, events, "en", "Dr",
                "Not all who wander are lost", "Not all who wander are lost");
        Events events2 = new Events();
        events2.setCreatedOn(mock(Timestamp.class));
        events2.setEventEndDate(mock(Timestamp.class));
        events2.setEventStartDate(mock(Timestamp.class));
        events2.setEventVisible(true);
        events2.setId(UUID.randomUUID());
        events2.setLocalizations(new ArrayList<>());
        events2.setSliderMedia(new ArrayList<>());
        actualEventsLocalization.setEvents(events2);
        actualEventsLocalization.setHtmlLongContent("Not all who wander are lost");
        actualEventsLocalization.setHtmlShortContent("Not all who wander are lost");
        actualEventsLocalization.setHtmlTitle("Dr");
        actualEventsLocalization.setId(1L);
        actualEventsLocalization.setLocale("en");
        Events actualEvents = actualEventsLocalization.getEvents();
        String actualHtmlLongContent = actualEventsLocalization.getHtmlLongContent();
        String actualHtmlShortContent = actualEventsLocalization.getHtmlShortContent();
        String actualHtmlTitle = actualEventsLocalization.getHtmlTitle();
        Long actualId = actualEventsLocalization.getId();
        assertEquals("Dr", actualHtmlTitle);
        assertEquals("Not all who wander are lost", actualHtmlLongContent);
        assertEquals("Not all who wander are lost", actualHtmlShortContent);
        assertEquals("en", actualEventsLocalization.getLocale());
        assertEquals(1L, actualId.longValue());
        assertSame(events2, actualEvents);
    }
}
