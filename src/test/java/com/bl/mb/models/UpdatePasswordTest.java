package com.bl.mb.models;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class UpdatePasswordTest {

    @Test
    public void testGetCodeId() {
        // Arrange
        UUID expectedCodeId = UUID.randomUUID();
        UpdatePassword updatePassword = new UpdatePassword();
        updatePassword.setCodeId(expectedCodeId);

        // Act
        UUID actualCodeId = updatePassword.getCodeId();

        // Assert
        assertThat(actualCodeId).isEqualTo(expectedCodeId);
    }

    @Test
    public void testGetCode() {
        // Arrange
        String expectedCode = "123456";
        UpdatePassword updatePassword = new UpdatePassword();
        updatePassword.setCode(expectedCode);

        // Act
        String actualCode = updatePassword.getCode();

        // Assert
        assertThat(actualCode).isEqualTo(expectedCode);
    }

    @Test
    public void testGetEmail() {
        // Arrange
        String expectedEmail = "test@example.com";
        UpdatePassword updatePassword = new UpdatePassword();
        updatePassword.setEmail(expectedEmail);

        // Act
        String actualEmail = updatePassword.getEmail();

        // Assert
        assertThat(actualEmail).isEqualTo(expectedEmail);
    }

    @Test
    public void testGetNewPassword() {
        // Arrange
        String expectedNewPassword = "newPassword";
        UpdatePassword updatePassword = new UpdatePassword();
        updatePassword.setNewPassword(expectedNewPassword);

        // Act
        String actualNewPassword = updatePassword.getNewPassword();

        // Assert
        assertThat(actualNewPassword).isEqualTo(expectedNewPassword);
    }
}