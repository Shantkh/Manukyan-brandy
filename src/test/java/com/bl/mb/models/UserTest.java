package com.bl.mb.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.UUID;

import org.junit.jupiter.api.Test;

class UserTest {

    @Test
    void testEquals() {
        User user = new User();
        user.setAccessToken("ABC123");
        user.setCreatedOn(mock(Timestamp.class));
        user.setEmail("jane.doe@example.org");
        user.setFullName("Dr Jane Doe");
        user.setId(UUID.randomUUID());
        user.setPassword("iloveyou");
        user.setRefreshToken("ABC123");
        user.setRoles(new ArrayList<>());
        user.setUpdatedOn(mock(Timestamp.class));
        user.setUsername("janedoe");
        assertNotEquals(user, null);
    }

    @Test
    void testEquals2() {
        User user = new User();
        user.setAccessToken("ABC123");
        user.setCreatedOn(mock(Timestamp.class));
        user.setEmail("jane.doe@example.org");
        user.setFullName("Dr Jane Doe");
        user.setId(UUID.randomUUID());
        user.setPassword("iloveyou");
        user.setRefreshToken("ABC123");
        user.setRoles(new ArrayList<>());
        user.setUpdatedOn(mock(Timestamp.class));
        user.setUsername("janedoe");
        assertNotEquals(user, "Different type to User");
    }

    @Test
    void testEquals3() {
        User user = new User();
        user.setAccessToken("ABC123");
        user.setCreatedOn(mock(Timestamp.class));
        user.setEmail("jane.doe@example.org");
        user.setFullName("Dr Jane Doe");
        user.setId(UUID.randomUUID());
        user.setPassword("iloveyou");
        user.setRefreshToken("ABC123");
        user.setRoles(new ArrayList<>());
        user.setUpdatedOn(mock(Timestamp.class));
        user.setUsername("janedoe");
        assertEquals(user, user);
        int expectedHashCodeResult = user.hashCode();
        assertEquals(expectedHashCodeResult, user.hashCode());
    }

    @Test
    void testEquals4() {
        User user = new User();
        user.setAccessToken("ABC123");
        user.setCreatedOn(mock(Timestamp.class));
        user.setEmail("jane.doe@example.org");
        user.setFullName("Dr Jane Doe");
        user.setId(UUID.randomUUID());
        user.setPassword("iloveyou");
        user.setRefreshToken("ABC123");
        user.setRoles(new ArrayList<>());
        user.setUpdatedOn(mock(Timestamp.class));
        user.setUsername("janedoe");

        User user2 = new User();
        user2.setAccessToken("ABC123");
        user2.setCreatedOn(mock(Timestamp.class));
        user2.setEmail("jane.doe@example.org");
        user2.setFullName("Dr Jane Doe");
        user2.setId(UUID.randomUUID());
        user2.setPassword("iloveyou");
        user2.setRefreshToken("ABC123");
        user2.setRoles(new ArrayList<>());
        user2.setUpdatedOn(mock(Timestamp.class));
        user2.setUsername("janedoe");
        assertEquals(user, user2);
        int expectedHashCodeResult = user.hashCode();
        assertEquals(expectedHashCodeResult, user2.hashCode());
    }

    @Test
    void testEquals5() {
        User user = new User();
        user.setAccessToken("ABC123");
        user.setCreatedOn(mock(Timestamp.class));
        user.setEmail("john.smith@example.org");
        user.setFullName("Dr Jane Doe");
        user.setId(UUID.randomUUID());
        user.setPassword("iloveyou");
        user.setRefreshToken("ABC123");
        user.setRoles(new ArrayList<>());
        user.setUpdatedOn(mock(Timestamp.class));
        user.setUsername("janedoe");

        User user2 = new User();
        user2.setAccessToken("ABC123");
        user2.setCreatedOn(mock(Timestamp.class));
        user2.setEmail("jane.doe@example.org");
        user2.setFullName("Dr Jane Doe");
        user2.setId(UUID.randomUUID());
        user2.setPassword("iloveyou");
        user2.setRefreshToken("ABC123");
        user2.setRoles(new ArrayList<>());
        user2.setUpdatedOn(mock(Timestamp.class));
        user2.setUsername("janedoe");
        assertNotEquals(user, user2);
    }
}
