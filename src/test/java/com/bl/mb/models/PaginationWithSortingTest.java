package com.bl.mb.models;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PaginationWithSortingTest {

    @Mock
    private PaginationWithSorting paginationWithSorting;

    @Test
    public void testGetPageNumber() {
        when(paginationWithSorting.getPageNumber()).thenReturn(1);
        assertEquals(1, paginationWithSorting.getPageNumber());
    }

    @Test
    public void testGetContentSize() {
        when(paginationWithSorting.getContentSize()).thenReturn(10);
        assertEquals(10, paginationWithSorting.getContentSize());
    }

    @Test
    public void testGetSortDirection() {
        when(paginationWithSorting.getSortDirection()).thenReturn("asc");
        assertEquals("asc", paginationWithSorting.getSortDirection());
    }

    @Test
    public void testGetSortElement() {
        when(paginationWithSorting.getSortElement()).thenReturn("name");
        assertEquals("name", paginationWithSorting.getSortElement());
    }

    @Test
    void testConstructor() {
        PaginationWithSorting actualPaginationWithSorting = new PaginationWithSorting();
        actualPaginationWithSorting.setContentSize(3);
        actualPaginationWithSorting.setPageNumber(10);
        actualPaginationWithSorting.setSortDirection("Sort Direction");
        actualPaginationWithSorting.setSortElement("Sort Element");
        int actualContentSize = actualPaginationWithSorting.getContentSize();
        int actualPageNumber = actualPaginationWithSorting.getPageNumber();
        String actualSortDirection = actualPaginationWithSorting.getSortDirection();
        assertEquals("Sort Direction", actualSortDirection);
        assertEquals("Sort Element", actualPaginationWithSorting.getSortElement());
        assertEquals(10, actualPageNumber);
        assertEquals(3, actualContentSize);
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>
     * {@link PaginationWithSorting#PaginationWithSorting(int, int, String, String)}
     *   <li>{@link PaginationWithSorting#setContentSize(int)}
     *   <li>{@link PaginationWithSorting#setPageNumber(int)}
     *   <li>{@link PaginationWithSorting#setSortDirection(String)}
     *   <li>{@link PaginationWithSorting#setSortElement(String)}
     *   <li>{@link PaginationWithSorting#getContentSize()}
     *   <li>{@link PaginationWithSorting#getPageNumber()}
     *   <li>{@link PaginationWithSorting#getSortDirection()}
     *   <li>{@link PaginationWithSorting#getSortElement()}
     * </ul>
     */
    @Test
    void testConstructor2() {
        PaginationWithSorting actualPaginationWithSorting = new PaginationWithSorting(10, 3, "Sort Direction",
                "Sort Element");
        actualPaginationWithSorting.setContentSize(3);
        actualPaginationWithSorting.setPageNumber(10);
        actualPaginationWithSorting.setSortDirection("Sort Direction");
        actualPaginationWithSorting.setSortElement("Sort Element");
        int actualContentSize = actualPaginationWithSorting.getContentSize();
        int actualPageNumber = actualPaginationWithSorting.getPageNumber();
        String actualSortDirection = actualPaginationWithSorting.getSortDirection();
        assertEquals("Sort Direction", actualSortDirection);
        assertEquals("Sort Element", actualPaginationWithSorting.getSortElement());
        assertEquals(10, actualPageNumber);
        assertEquals(3, actualContentSize);
    }
}
