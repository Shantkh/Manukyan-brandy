package com.bl.mb.models;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ProductsTypeLocalizationTest {

    @Test
    public void testProductsTypeLocalizationWhenCreatedThenStateIsCorrect() {
        // Arrange
        ProductsTypeLocalization productsTypeLocalization = new ProductsTypeLocalization();

        // Act
        // No action is needed as we are testing the initial state

        // Assert
        assertNull(productsTypeLocalization.getId(), "Id should be null");
        assertNull(productsTypeLocalization.getProducts(), "Products should be null");
        assertNull(productsTypeLocalization.getLocale(), "Locale should be null");
        assertNull(productsTypeLocalization.getHtmlContent(), "HtmlContent should be null");
    }

    @Test
    public void testProductsTypeLocalizationWhenStateModifiedThenStateIsCorrect() {
        // Arrange
        ProductsTypeLocalization productsTypeLocalization = new ProductsTypeLocalization();
        Products products = new Products();
        String locale = "en-US";
        String htmlContent = "<p>Product Description</p>";

        // Act
        productsTypeLocalization.setProducts(products);
        productsTypeLocalization.setLocale(locale);
        productsTypeLocalization.setHtmlContent(htmlContent);

        // Assert
        assertSame(products, productsTypeLocalization.getProducts(), "Products should be the same as set");
        assertEquals(locale, productsTypeLocalization.getLocale(), "Locale should be the same as set");
        assertEquals(htmlContent, productsTypeLocalization.getHtmlContent(), "HtmlContent should be the same as set");
    }

    @Test
    public void testProductsTypeLocalizationWhenRelatedEntitySetThenBehaviorIsCorrect() {
        // Arrange
        ProductsTypeLocalization productsTypeLocalization = new ProductsTypeLocalization();
        Products products = new Products();

        // Act
        productsTypeLocalization.setProducts(products);

        // Assert
        assertSame(products, productsTypeLocalization.getProducts(), "Products should be the same as set");
    }
}