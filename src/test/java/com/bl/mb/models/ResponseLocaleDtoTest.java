package com.bl.mb.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

class ResponseLocaleDtoTest {
    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link ResponseLocaleDto#ResponseLocaleDto()}
     *   <li>{@link ResponseLocaleDto#setHtmlContent(String)}
     *   <li>{@link ResponseLocaleDto#setSliderMedia(List)}
     *   <li>{@link ResponseLocaleDto#getHtmlContent()}
     *   <li>{@link ResponseLocaleDto#getSliderMedia()}
     * </ul>
     */
    @Test
    void testConstructor() {
        ResponseLocaleDto actualResponseLocaleDto = new ResponseLocaleDto();
        actualResponseLocaleDto.setHtmlContent("en");
        ArrayList<String> sliderMedia = new ArrayList<>();
        actualResponseLocaleDto.setSliderMedia(sliderMedia);
        String actualHtmlContent = actualResponseLocaleDto.getHtmlContent();
        assertEquals("en", actualHtmlContent);
        assertSame(sliderMedia, actualResponseLocaleDto.getSliderMedia());
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link ResponseLocaleDto#ResponseLocaleDto(List, String)}
     *   <li>{@link ResponseLocaleDto#setHtmlContent(String)}
     *   <li>{@link ResponseLocaleDto#setSliderMedia(List)}
     *   <li>{@link ResponseLocaleDto#getHtmlContent()}
     *   <li>{@link ResponseLocaleDto#getSliderMedia()}
     * </ul>
     */
    @Test
    void testConstructor2() {
        ArrayList<String> sliderMedia = new ArrayList<>();
        ResponseLocaleDto actualResponseLocaleDto = new ResponseLocaleDto(sliderMedia, "en");
        actualResponseLocaleDto.setHtmlContent("en");
        ArrayList<String> sliderMedia2 = new ArrayList<>();
        actualResponseLocaleDto.setSliderMedia(sliderMedia2);
        String actualHtmlContent = actualResponseLocaleDto.getHtmlContent();
        List<String> actualSliderMedia = actualResponseLocaleDto.getSliderMedia();
        assertEquals("en", actualHtmlContent);
        assertEquals(sliderMedia, actualSliderMedia);
        assertSame(sliderMedia2, actualSliderMedia);
    }
}
