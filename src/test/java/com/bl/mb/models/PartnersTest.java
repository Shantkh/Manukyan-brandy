package com.bl.mb.models;

import com.bl.mb.utils.TimeDateConverter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PartnersTest {

    @InjectMocks
    private Partners partners;

    @Mock
    private TimeDateConverter timeDateConverter;

    @Test
    public void testGetId() {
        Long id = 1L;
        partners.setId(id);

        Assertions.assertEquals(id, partners.getId());
    }

    @Test
    public void testGetCreatedOn() {
        Timestamp createdOn = new Timestamp(System.currentTimeMillis());
        partners.setCreatedOn(createdOn);

        Assertions.assertEquals(createdOn, partners.getCreatedOn());
    }

    @Test
    public void testGetLogo() {
        String logo = "logo.png";
        partners.setLogo(logo);

        Assertions.assertEquals(logo, partners.getLogo());
    }

    @Test
    public void testGetPartnerNameEn() {
        String partnerNameEn = "Partner Name (EN)";
        partners.setPartnerNameEn(partnerNameEn);

        Assertions.assertEquals(partnerNameEn, partners.getPartnerNameEn());
    }

    @Test
    public void testGetPartnerNameHy() {
        String partnerNameHy = "Partner Name (HY)";
        partners.setPartnerNameHy(partnerNameHy);

        Assertions.assertEquals(partnerNameHy, partners.getPartnerNameHy());
    }

    @Test
    void testConstructor() {
        Partners actualPartners = new Partners();
        Timestamp createdOn = mock(Timestamp.class);
        actualPartners.setCreatedOn(createdOn);
        actualPartners.setId(1L);
        actualPartners.setLogo("Logo");
        actualPartners.setPartnerNameEn("Partner Name En");
        actualPartners.setPartnerNameHy("Partner Name Hy");
        Timestamp actualCreatedOn = actualPartners.getCreatedOn();
        Long actualId = actualPartners.getId();
        String actualLogo = actualPartners.getLogo();
        String actualPartnerNameEn = actualPartners.getPartnerNameEn();
        Assertions.assertEquals("Logo", actualLogo);
        Assertions.assertEquals("Partner Name En", actualPartnerNameEn);
        Assertions.assertEquals("Partner Name Hy", actualPartners.getPartnerNameHy());
        Assertions.assertEquals(1L, actualId.longValue());
        assertSame(createdOn, actualCreatedOn);
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link Partners#Partners(Long, Timestamp, String, String, String)}
     *   <li>{@link Partners#setCreatedOn(Timestamp)}
     *   <li>{@link Partners#setId(Long)}
     *   <li>{@link Partners#setLogo(String)}
     *   <li>{@link Partners#setPartnerNameEn(String)}
     *   <li>{@link Partners#setPartnerNameHy(String)}
     *   <li>{@link Partners#getCreatedOn()}
     *   <li>{@link Partners#getId()}
     *   <li>{@link Partners#getLogo()}
     *   <li>{@link Partners#getPartnerNameEn()}
     *   <li>{@link Partners#getPartnerNameHy()}
     * </ul>
     */
    @Test
    void testConstructor2() {
        Partners actualPartners = new Partners(1L, mock(Timestamp.class), "Logo", "Partner Name En", "Partner Name Hy");
        Timestamp createdOn = mock(Timestamp.class);
        actualPartners.setCreatedOn(createdOn);
        actualPartners.setId(1L);
        actualPartners.setLogo("Logo");
        actualPartners.setPartnerNameEn("Partner Name En");
        actualPartners.setPartnerNameHy("Partner Name Hy");
        Timestamp actualCreatedOn = actualPartners.getCreatedOn();
        Long actualId = actualPartners.getId();
        String actualLogo = actualPartners.getLogo();
        String actualPartnerNameEn = actualPartners.getPartnerNameEn();
        Assertions.assertEquals("Logo", actualLogo);
        Assertions.assertEquals("Partner Name En", actualPartnerNameEn);
        Assertions.assertEquals("Partner Name Hy", actualPartners.getPartnerNameHy());
        Assertions.assertEquals(1L, actualId.longValue());
        assertSame(createdOn, actualCreatedOn);
    }
}
