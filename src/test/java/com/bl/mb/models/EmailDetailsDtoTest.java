package com.bl.mb.models;

import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.mock;

public class EmailDetailsDtoTest {
    @Test
    void testConstructor() {
        EmailDetailsDto actualEmailDetailsDto = new EmailDetailsDto();
        Timestamp createdDate = mock(Timestamp.class);
        actualEmailDetailsDto.setCreatedDate(createdDate);
        UUID id = UUID.randomUUID();
        actualEmailDetailsDto.setId(id);
        actualEmailDetailsDto.setRecipient("Recipient");
        Timestamp actualCreatedDate = actualEmailDetailsDto.getCreatedDate();
        UUID actualId = actualEmailDetailsDto.getId();
        assertEquals("Recipient", actualEmailDetailsDto.getRecipient());
        assertSame(id, actualId);
        assertSame(createdDate, actualCreatedDate);
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link EmailDetailsDto#EmailDetailsDto(UUID, String, Timestamp)}
     *   <li>{@link EmailDetailsDto#setCreatedDate(Timestamp)}
     *   <li>{@link EmailDetailsDto#setId(UUID)}
     *   <li>{@link EmailDetailsDto#setRecipient(String)}
     *   <li>{@link EmailDetailsDto#getCreatedDate()}
     *   <li>{@link EmailDetailsDto#getId()}
     *   <li>{@link EmailDetailsDto#getRecipient()}
     * </ul>
     */
    @Test
    void testConstructor2() {
        EmailDetailsDto actualEmailDetailsDto = new EmailDetailsDto(UUID.randomUUID(), "Recipient", mock(Timestamp.class));
        Timestamp createdDate = mock(Timestamp.class);
        actualEmailDetailsDto.setCreatedDate(createdDate);
        UUID id = UUID.randomUUID();
        actualEmailDetailsDto.setId(id);
        actualEmailDetailsDto.setRecipient("Recipient");
        Timestamp actualCreatedDate = actualEmailDetailsDto.getCreatedDate();
        UUID actualId = actualEmailDetailsDto.getId();
        assertEquals("Recipient", actualEmailDetailsDto.getRecipient());
        assertSame(id, actualId);
        assertSame(createdDate, actualCreatedDate);
    }
}