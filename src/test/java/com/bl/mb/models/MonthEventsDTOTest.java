package com.bl.mb.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class MonthEventsDTOTest {
    /**
     * Method under test:
     * {@link MonthEventsDTO#MonthEventsDTO(String, Long, Boolean)}
     */
    @Test
    void testConstructor() {
        MonthEventsDTO actualMonthEventsDTO = new MonthEventsDTO("Month", 3L, true);

        assertEquals("Month", actualMonthEventsDTO.getMonth());
        assertEquals(3, actualMonthEventsDTO.getEventCount());
        assertTrue(actualMonthEventsDTO.isHasEvents());
    }
}
