package com.bl.mb.models;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.Test;

class EventsTest {
    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link Events#Events()}
     *   <li>{@link Events#setCreatedOn(Timestamp)}
     *   <li>{@link Events#setEventEndDate(Timestamp)}
     *   <li>{@link Events#setEventStartDate(Timestamp)}
     *   <li>{@link Events#setEventVisible(boolean)}
     *   <li>{@link Events#setId(UUID)}
     *   <li>{@link Events#setLocalizations(List)}
     *   <li>{@link Events#setSliderMedia(List)}
     *   <li>{@link Events#getCreatedOn()}
     *   <li>{@link Events#getEventEndDate()}
     *   <li>{@link Events#getEventStartDate()}
     *   <li>{@link Events#getId()}
     *   <li>{@link Events#getLocalizations()}
     *   <li>{@link Events#getSliderMedia()}
     *   <li>{@link Events#isEventVisible()}
     * </ul>
     */
    @Test
    void testConstructor() {
        Events actualEvents = new Events();
        Timestamp createdOn = mock(Timestamp.class);
        actualEvents.setCreatedOn(createdOn);
        Timestamp eventEndDate = mock(Timestamp.class);
        actualEvents.setEventEndDate(eventEndDate);
        Timestamp eventStartDate = mock(Timestamp.class);
        actualEvents.setEventStartDate(eventStartDate);
        actualEvents.setEventVisible(true);
        UUID id = UUID.randomUUID();
        actualEvents.setId(id);
        ArrayList<EventsLocalization> localizations = new ArrayList<>();
        actualEvents.setLocalizations(localizations);
        ArrayList<String> sliderMedia = new ArrayList<>();
        actualEvents.setSliderMedia(sliderMedia);
        Timestamp actualCreatedOn = actualEvents.getCreatedOn();
        Timestamp actualEventEndDate = actualEvents.getEventEndDate();
        Timestamp actualEventStartDate = actualEvents.getEventStartDate();
        UUID actualId = actualEvents.getId();
        List<EventsLocalization> actualLocalizations = actualEvents.getLocalizations();
        List<String> actualSliderMedia = actualEvents.getSliderMedia();
        assertTrue(actualEvents.isEventVisible());
        assertEquals(actualLocalizations, actualSliderMedia);
        assertSame(localizations, actualLocalizations);
        assertSame(sliderMedia, actualSliderMedia);
        assertSame(id, actualId);
        assertSame(createdOn, actualCreatedOn);
        assertSame(eventEndDate, actualEventEndDate);
        assertSame(eventStartDate, actualEventStartDate);
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>
     * {@link Events#Events(UUID, Timestamp, List, List, Timestamp, Timestamp, boolean)}
     *   <li>{@link Events#setCreatedOn(Timestamp)}
     *   <li>{@link Events#setEventEndDate(Timestamp)}
     *   <li>{@link Events#setEventStartDate(Timestamp)}
     *   <li>{@link Events#setEventVisible(boolean)}
     *   <li>{@link Events#setId(UUID)}
     *   <li>{@link Events#setLocalizations(List)}
     *   <li>{@link Events#setSliderMedia(List)}
     *   <li>{@link Events#getCreatedOn()}
     *   <li>{@link Events#getEventEndDate()}
     *   <li>{@link Events#getEventStartDate()}
     *   <li>{@link Events#getId()}
     *   <li>{@link Events#getLocalizations()}
     *   <li>{@link Events#getSliderMedia()}
     *   <li>{@link Events#isEventVisible()}
     * </ul>
     */
    @Test
    void testConstructor2() {
        UUID id = UUID.randomUUID();
        Timestamp createdOn = mock(Timestamp.class);
        ArrayList<EventsLocalization> localizations = new ArrayList<>();
        Events actualEvents = new Events(id, createdOn, localizations, new ArrayList<>(), mock(Timestamp.class),
                mock(Timestamp.class), true);
        Timestamp createdOn2 = mock(Timestamp.class);
        actualEvents.setCreatedOn(createdOn2);
        Timestamp eventEndDate = mock(Timestamp.class);
        actualEvents.setEventEndDate(eventEndDate);
        Timestamp eventStartDate = mock(Timestamp.class);
        actualEvents.setEventStartDate(eventStartDate);
        actualEvents.setEventVisible(true);
        UUID id2 = UUID.randomUUID();
        actualEvents.setId(id2);
        ArrayList<EventsLocalization> localizations2 = new ArrayList<>();
        actualEvents.setLocalizations(localizations2);
        ArrayList<String> sliderMedia = new ArrayList<>();
        actualEvents.setSliderMedia(sliderMedia);
        Timestamp actualCreatedOn = actualEvents.getCreatedOn();
        Timestamp actualEventEndDate = actualEvents.getEventEndDate();
        Timestamp actualEventStartDate = actualEvents.getEventStartDate();
        UUID actualId = actualEvents.getId();
        List<EventsLocalization> actualLocalizations = actualEvents.getLocalizations();
        List<String> actualSliderMedia = actualEvents.getSliderMedia();
        assertTrue(actualEvents.isEventVisible());
        assertEquals(localizations, actualLocalizations);
        assertEquals(localizations, actualSliderMedia);
        assertSame(localizations2, actualLocalizations);
        assertSame(sliderMedia, actualSliderMedia);
        assertSame(id2, actualId);
        assertSame(createdOn2, actualCreatedOn);
        assertSame(eventEndDate, actualEventEndDate);
        assertSame(eventStartDate, actualEventStartDate);
    }
}
