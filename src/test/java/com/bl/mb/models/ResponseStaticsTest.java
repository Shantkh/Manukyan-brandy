package com.bl.mb.models;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

public class ResponseStaticsTest {

    @Test
    public void testGetStatisticsResult() {
        // Arrange
        StatisticsResult expectedStatisticsResult = new StatisticsResult();
        ResponseStatics responseStatics = new ResponseStatics();
        responseStatics.setStatisticsResult(expectedStatisticsResult);

        // Act
        StatisticsResult actualStatisticsResult = responseStatics.getStatisticsResult();

        // Assert
        assertThat(actualStatisticsResult).isEqualTo(expectedStatisticsResult);
    }

    @Test
    public void testGetCountries() {
        // Arrange
        List<Map<String, Object>> expectedCountries = new ArrayList<>();
        ResponseStatics responseStatics = new ResponseStatics();
        responseStatics.setCountries(expectedCountries);

        // Act
        List<Map<String, Object>> actualCountries = responseStatics.getCountries();

        // Assert
        assertThat(actualCountries).isEqualTo(expectedCountries);
    }

    @Test
    public void testGetPages() {
        // Arrange
        List<Map<String, Object>> expectedPages = new ArrayList<>();
        ResponseStatics responseStatics = new ResponseStatics();
        responseStatics.setPages(expectedPages);

        // Act
        List<Map<String, Object>> actualPages = responseStatics.getPages();

        // Assert
        assertThat(actualPages).isEqualTo(expectedPages);
    }

    @Test
    void testConstructor() {
        ResponseStatics actualResponseStatics = new ResponseStatics();
        ArrayList<Map<String, Object>> countries = new ArrayList<>();
        actualResponseStatics.setCountries(countries);
        ArrayList<Map<String, Object>> pages = new ArrayList<>();
        actualResponseStatics.setPages(pages);
        StatisticsResult statisticsResult = StatisticsResult.builder()
                .usersLast15Days(1L)
                .usersLast30Days(1L)
                .usersLast7Days(1L)
                .usersToday(1L)
                .build();
        actualResponseStatics.setStatisticsResult(statisticsResult);
        List<Map<String, Object>> actualCountries = actualResponseStatics.getCountries();
        List<Map<String, Object>> actualPages = actualResponseStatics.getPages();
        assertEquals(actualCountries, actualPages);
        assertSame(countries, actualCountries);
        assertSame(pages, actualPages);
        assertSame(statisticsResult, actualResponseStatics.getStatisticsResult());
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>{@link ResponseStatics#ResponseStatics(StatisticsResult, List, List)}
     *   <li>{@link ResponseStatics#setCountries(List)}
     *   <li>{@link ResponseStatics#setPages(List)}
     *   <li>{@link ResponseStatics#setStatisticsResult(StatisticsResult)}
     *   <li>{@link ResponseStatics#getCountries()}
     *   <li>{@link ResponseStatics#getPages()}
     *   <li>{@link ResponseStatics#getStatisticsResult()}
     * </ul>
     */
    @Test
    void testConstructor2() {
        StatisticsResult statisticsResult = StatisticsResult.builder()
                .usersLast15Days(1L)
                .usersLast30Days(1L)
                .usersLast7Days(1L)
                .usersToday(1L)
                .build();
        ArrayList<Map<String, Object>> countries = new ArrayList<>();
        ResponseStatics actualResponseStatics = new ResponseStatics(statisticsResult, countries, new ArrayList<>());
        ArrayList<Map<String, Object>> countries2 = new ArrayList<>();
        actualResponseStatics.setCountries(countries2);
        ArrayList<Map<String, Object>> pages = new ArrayList<>();
        actualResponseStatics.setPages(pages);
        StatisticsResult statisticsResult2 = StatisticsResult.builder()
                .usersLast15Days(1L)
                .usersLast30Days(1L)
                .usersLast7Days(1L)
                .usersToday(1L)
                .build();
        actualResponseStatics.setStatisticsResult(statisticsResult2);
        List<Map<String, Object>> actualCountries = actualResponseStatics.getCountries();
        List<Map<String, Object>> actualPages = actualResponseStatics.getPages();
        assertEquals(countries, actualCountries);
        assertEquals(countries, actualPages);
        assertSame(countries2, actualCountries);
        assertSame(pages, actualPages);
        assertSame(statisticsResult2, actualResponseStatics.getStatisticsResult());
    }
}