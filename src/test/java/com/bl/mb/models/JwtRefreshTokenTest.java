package com.bl.mb.models;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertSame;

public class JwtRefreshTokenTest {

    @Test
    void testConstructor() {
        JwtRefreshToken actualJwtRefreshToken = new JwtRefreshToken();
        LocalDateTime createDate = LocalDate.of(1970, 1, 1).atStartOfDay();
        actualJwtRefreshToken.setCreateDate(createDate);
        LocalDateTime expirationDate = LocalDate.of(1970, 1, 1).atStartOfDay();
        actualJwtRefreshToken.setExpirationDate(expirationDate);
        UUID tokenId = UUID.randomUUID();
        actualJwtRefreshToken.setTokenId(tokenId);
        UUID userId = UUID.randomUUID();
        actualJwtRefreshToken.setUserId(userId);
        LocalDateTime actualCreateDate = actualJwtRefreshToken.getCreateDate();
        LocalDateTime actualExpirationDate = actualJwtRefreshToken.getExpirationDate();
        UUID actualTokenId = actualJwtRefreshToken.getTokenId();
        assertSame(createDate, actualCreateDate);
        assertSame(expirationDate, actualExpirationDate);
        assertSame(tokenId, actualTokenId);
        assertSame(userId, actualJwtRefreshToken.getUserId());
    }

    /**
     * Methods under test:
     *
     * <ul>
     *   <li>
     * {@link JwtRefreshToken#JwtRefreshToken(UUID, UUID, LocalDateTime, LocalDateTime)}
     *   <li>{@link JwtRefreshToken#setCreateDate(LocalDateTime)}
     *   <li>{@link JwtRefreshToken#setExpirationDate(LocalDateTime)}
     *   <li>{@link JwtRefreshToken#setTokenId(UUID)}
     *   <li>{@link JwtRefreshToken#setUserId(UUID)}
     *   <li>{@link JwtRefreshToken#getCreateDate()}
     *   <li>{@link JwtRefreshToken#getExpirationDate()}
     *   <li>{@link JwtRefreshToken#getTokenId()}
     *   <li>{@link JwtRefreshToken#getUserId()}
     * </ul>
     */
    @Test
    void testConstructor2() {
        UUID tokenId = UUID.randomUUID();
        UUID userId = UUID.randomUUID();
        LocalDateTime createDate = LocalDate.of(1970, 1, 1).atStartOfDay();
        JwtRefreshToken actualJwtRefreshToken = new JwtRefreshToken(tokenId, userId, createDate,
                LocalDate.of(1970, 1, 1).atStartOfDay());
        LocalDateTime createDate2 = LocalDate.of(1970, 1, 1).atStartOfDay();
        actualJwtRefreshToken.setCreateDate(createDate2);
        LocalDateTime expirationDate = LocalDate.of(1970, 1, 1).atStartOfDay();
        actualJwtRefreshToken.setExpirationDate(expirationDate);
        UUID tokenId2 = UUID.randomUUID();
        actualJwtRefreshToken.setTokenId(tokenId2);
        UUID userId2 = UUID.randomUUID();
        actualJwtRefreshToken.setUserId(userId2);
        LocalDateTime actualCreateDate = actualJwtRefreshToken.getCreateDate();
        LocalDateTime actualExpirationDate = actualJwtRefreshToken.getExpirationDate();
        UUID actualTokenId = actualJwtRefreshToken.getTokenId();
        assertSame(createDate2, actualCreateDate);
        assertSame(expirationDate, actualExpirationDate);
        assertSame(tokenId2, actualTokenId);
        assertSame(userId2, actualJwtRefreshToken.getUserId());
    }
}