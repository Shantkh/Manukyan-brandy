package com.bl.mb.utils;

import com.bl.mb.models.PaginationWithSorting;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class PagePaginationTest {

    private final PagePagination pagePagination = new PagePagination();

    @Test
    public void testPagePaginationWhenValidParametersThenReturnPageRequest() throws Exception {
        // Arrange
        PaginationWithSorting paginationWithSorting = PaginationWithSorting.builder()
                .pageNumber(1)
                .contentSize(10)
                .sortDirection("ASC")
                .sortElement("name")
                .build();

        // Act
        PageRequest pageRequest = pagePagination.pagePagination(paginationWithSorting);

        // Assert
        assertEquals(1, pageRequest.getPageNumber());
        assertEquals(10, pageRequest.getPageSize());
        assertEquals(Sort.Direction.ASC, pageRequest.getSort().getOrderFor("name").getDirection());
    }

    @Test
    public void testPagePaginationWhenInvalidSortDirectionThenThrowException() {
        // Arrange
        PaginationWithSorting paginationWithSorting = PaginationWithSorting.builder()
                .pageNumber(1)
                .contentSize(10)
                .sortDirection("INVALID_DIRECTION")
                .sortElement("name")
                .build();

        // Act & Assert
        assertThrows(IllegalArgumentException.class, () -> pagePagination.pagePagination(paginationWithSorting));
    }

    @Test
    public void testPagePaginationWhenInvalidSortElementThenThrowException() {
        // Arrange
        PaginationWithSorting paginationWithSorting = PaginationWithSorting.builder()
                .pageNumber(1)
                .contentSize(10)
                .sortDirection("ASC")
                .sortElement("") // Assuming empty string is invalid
                .build();

        // Act & Assert
        Exception exception = assertThrows(Exception.class, () -> pagePagination.pagePagination(paginationWithSorting));
        assertEquals("Property must not be null or empty", exception.getMessage());
    }
}