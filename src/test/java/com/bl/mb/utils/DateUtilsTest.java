package com.bl.mb.utils;

import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class DateUtilsTest {

    @Test
    void newTimeStamp_ReturnsNewTimestamp() {
        // Arrange

        // Act
        Timestamp timestamp = DateUtils.newTimeStamp();

        // Assert
        assertNotNull(timestamp);
        assertEquals(new Date().getTime(), timestamp.getTime(), 1000); // Allow a difference of 1 second
    }

}