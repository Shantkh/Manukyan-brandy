package com.bl.mb.utils;

import com.bl.mb.models.EmailDetails;
import com.bl.mb.templates.EmailTemplates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class EmailUtilsTest {

    @Mock
    private VerificationUtils verificationUtils;

    @InjectMocks
    private EmailUtils emailUtils;

    private final String email = "test@example.com";
    private final String action = "Forgot Password";

    @Test
    void createdForgotPasswordCode_ReturnsValidEmailDetails() {
        // Act
        EmailDetails result = EmailUtils.createdForgotPasswordCode(email, action);
        // Assert
        assertNotNull(result);
        assertEquals(email, result.getRecipient());
        assertEquals(action, result.getSubject());
        assertNotNull(result.getId());
        assertNotNull(result.getMsgBody());
    }

    @Test
    void createdForgotPasswordCode_GeneratesNonNullCode() {
        // Act
        EmailDetails result = EmailUtils.createdForgotPasswordCode(email, action);

        // Assert
        assertNotNull(result.getCode());
        assertFalse(result.getCode().isEmpty());
    }
}