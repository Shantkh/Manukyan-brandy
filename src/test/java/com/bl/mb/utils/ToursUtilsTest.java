package com.bl.mb.utils;

import com.bl.mb.constances.MonthOfYear;
import com.bl.mb.constances.WeekDaysEnum;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.temporal.TemporalAdjusters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
public class ToursUtilsTest {

    @Test
    public void testGetDateFromMonthAndDayWhenValidInputsThenReturnCorrectDate() {
        // Arrange
        int year = 2023;
        int monthNumber = Month.MARCH.getValue();
        WeekDaysEnum weekDaysEnum = WeekDaysEnum.MONDAY;
        int weekNumber = 2;

        // Act
        LocalDate result = ToursUtils.getDateFromMonthAndDay(year, monthNumber, weekDaysEnum, weekNumber);

        // Assert
        LocalDate expectedDate = LocalDate.of(year, monthNumber, 1)
                .with(TemporalAdjusters.nextOrSame(DayOfWeek.valueOf(weekDaysEnum.name())))
                .plusWeeks(weekNumber - 1);
        assertEquals(expectedDate, result);
    }

    @Test
    public void testCountDaysInMonthWhenValidInputsThenReturnCorrectCount() {
        // Arrange
        int year = 2023;
        WeekDaysEnum weekDaysEnum = WeekDaysEnum.FRIDAY;
        MonthOfYear monthOfYear = MonthOfYear.JANUARY;

        // Act
        int result = ToursUtils.countDaysInMonth(year, weekDaysEnum, monthOfYear);

        // Assert
        int expectedCount = 4; // Assuming January 2023 has 5 Fridays
        assertEquals(expectedCount, result);
    }

    @Test
    public void testGetMonthNumberWhenValidMonthOfYearThenReturnCorrectNumber() {
        // Arrange
        MonthOfYear monthOfYear = MonthOfYear.APRIL;

        // Act
        int result = ToursUtils.getMonthNumber(monthOfYear);

        // Assert
        int expectedMonthNumber = Month.APRIL.getValue();
        assertEquals(expectedMonthNumber, result);
    }

    @Test
    public void testGetCurrentYearThenReturnCurrentYear() {
        // Act
        int result = ToursUtils.getCurrentYear();

        // Assert
        int expectedYear = Year.now().getValue();
        assertEquals(expectedYear, result);
    }
}