package com.bl.mb;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest
@TestPropertySource(properties = "logging.level.org.springframework=DEBUG")
class ManukyanBrandyApplicationTests {

	@Test
	void contextLoads() {
	}

}
