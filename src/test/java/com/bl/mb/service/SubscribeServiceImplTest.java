package com.bl.mb.service;

import com.bl.mb.config.Translator;
import com.bl.mb.models.ResponseBase;
import com.bl.mb.models.Subscribers;
import com.bl.mb.repo.SubscribeRepository;
import com.bl.mb.utils.PagePagination;
import com.bl.mb.utils.ResponseUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.HttpStatus;

import java.lang.reflect.Field;

import static com.bl.mb.config.Translator.toLocale;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SubscribeServiceImplTest {

    @Mock
    private SubscribeRepository subscribeRepository;

    @Mock
    private PagePagination pagePagination;

    @InjectMocks
    private SubscribeServiceImpl subscribeService;

    private Subscribers subscriber;

    @InjectMocks
    private Translator translator;

    @BeforeEach
    public void setUp() throws NoSuchFieldException, IllegalAccessException {
        subscriber = new Subscribers();
        subscriber.setEmail("test@example.com");

        // Initialize the ResourceBundleMessageSource
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("messages"); // Corrected basename
        messageSource.setDefaultEncoding("UTF-8");

        // Set the messageSource in Translator statically
        Field messageSourceField = Translator.class.getDeclaredField("messageSource");
        messageSourceField.setAccessible(true);
        messageSourceField.set(null, messageSource); // Since it's a static field, pass null as the object argument
    }

    @Test
    public void testSubscriberWhenSubscriberDoesNotExistThenReturnResponseCreated() {
        when(subscribeRepository.existsByEmail(subscriber.getEmail())).thenReturn(false);
        when(subscribeRepository.save(subscriber)).thenReturn(subscriber);

        ResponseBase<?> response = subscribeService.subscriber("en", subscriber);

        verify(subscribeRepository, times(1)).existsByEmail(subscriber.getEmail());
        verify(subscribeRepository, times(1)).save(subscriber);

        assertEquals(HttpStatus.CREATED, response.getHttpStatus());
        assertEquals(subscriber, response.getData());
    }

    @Test
    public void testSubscriberWhenSubscriberAlreadyExistsThenReturnResponseError() {
        when(subscribeRepository.existsByEmail(subscriber.getEmail())).thenReturn(true);

        ResponseBase<?> response = subscribeService.subscriber("en", subscriber);

        verify(subscribeRepository, times(1)).existsByEmail(subscriber.getEmail());

        assertEquals(HttpStatus.BAD_REQUEST, response.getHttpStatus());
        assertEquals(toLocale("email_already_exists"), response.getData());
    }

    @Test
    public void testSubscriberWhenExceptionOccursDuringSaveThenReturnResponseError() {
        when(subscribeRepository.existsByEmail(subscriber.getEmail())).thenReturn(false);
        when(subscribeRepository.save(subscriber)).thenThrow(new RuntimeException());

        ResponseBase<?> response = subscribeService.subscriber("en", subscriber);

        verify(subscribeRepository, times(1)).existsByEmail(subscriber.getEmail());
        verify(subscribeRepository, times(1)).save(subscriber);

        assertEquals(HttpStatus.BAD_REQUEST, response.getHttpStatus());
        assertEquals(toLocale("an_error_occurred_please_try_again_later"), response.getData());
    }

    @Test
    public void testSubscriberWhenSubscriberExistsThenReturnResponseError() {
        Subscribers subscriber = new Subscribers();
        subscriber.setEmail("test@example.com");

        when(subscribeRepository.existsByEmail(subscriber.getEmail())).thenReturn(true);

        ResponseBase<?> response = subscribeService.subscriber("en", subscriber);

        ResponseBase<?> expectedResponse = ResponseUtil.ResponseError("Email already exists");
        assertEquals(expectedResponse.getHttpStatus(), response.getHttpStatus());
        assertEquals(expectedResponse.getData().toString(), response.getData().toString());
    }

    @Test
    public void testSubscriberWhenExceptionOccursThenReturnResponseError() {
        Subscribers subscriber = new Subscribers();
        subscriber.setEmail("test@example.com");

        when(subscribeRepository.existsByEmail(subscriber.getEmail())).thenThrow(new RuntimeException());

        ResponseBase<?> response = subscribeService.subscriber("en", subscriber);

        // Compare the relevant fields instead of the whole object
        assertEquals(HttpStatus.BAD_REQUEST, response.getHttpStatus());
        assertEquals("An error occurred. Please try again later.", response.getData().toString());
    }
}