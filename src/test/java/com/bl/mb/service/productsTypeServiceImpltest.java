package com.bl.mb.service;

import com.bl.mb.models.ResponseBase;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class productsTypeServiceImpltest {

    @InjectMocks
    private productsTypeServiceImpl productsTypeService;

    @Test
    void getTypes_ReturnsCorrectStructureAndContent() {
        // Act
        ResponseBase<?> response = productsTypeService.getTypes();

        // Assert
        assertNotNull(response, "Response should not be null");
        assertEquals(HttpStatus.OK, response.getHttpStatus(), "HttpStatus should be OK");
        assertTrue(response.isSuccess(), "Response should indicate success");

        // Verify the structure and content of the response data
        @SuppressWarnings("unchecked")
        Map<String, Map<String, String[]>> responseData = (Map<String, Map<String, String[]>>) response.getData();
        assertNotNull(responseData, "Response data should not be null");
        assertNotNull(responseData.get("types"), "Types map should not be null");

        Map<String, String[]> localeMap = responseData.get("types");
        assertNotNull(localeMap.get("en"), "English locale should not be null");
        assertNotNull(localeMap.get("hy"), "Armenian locale should not be null");

        // Verify the content for each locale
        String[] engList = localeMap.get("en");
        assertEquals(4, engList.length, "English locale should contain 4 types");
        assertEquals("Red", engList[0], "First English type should be 'Red'");

        String[] hyList = localeMap.get("hy");
        assertEquals(4, hyList.length, "Armenian locale should contain 4 types");
        assertEquals("Կարմիր", hyList[0], "First Armenian type should be 'Կարմիր'");
    }
}