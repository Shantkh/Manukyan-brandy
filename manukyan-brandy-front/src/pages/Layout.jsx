
import { Outlet } from 'react-router-dom'
import Footer from '../components/footer/footer';
import NavBar from '../components/navbar/NavBar';

const Layout = () => {
    return (
        <>
            <NavBar/>
            <div style={{marginTop: '128px'}} />
            <Outlet />
            <Footer/>
        </>
    )
};

export default Layout;