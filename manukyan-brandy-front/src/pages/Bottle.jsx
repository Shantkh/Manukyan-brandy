import BottlingMainBanner from "../components/bottling/BottlingMainPageBanner/BottlingMainBanner";
import BottlingLowerBanner from "../components/bottling/BottlingLowerSection/BottlingLowerSection";
import BottlingMiddleBanner from "../components/bottling/BottlingMiddleBanner/BottlingMiddleBanner";
import BottlingUpperSection from "../components/bottling/BottlingUpperSection/BottlingUpperSection";
import StaticComponent from "../components/statics/StaticComponent";
import React from "react";
import InfoShowComponent from "../components/infoComponent/InfoShowComponent";

const Bottle = () => {
    return (
        <div>
            <BottlingMainBanner/>
            <BottlingUpperSection/>
            <BottlingMiddleBanner/>
            <BottlingLowerBanner/>
            <InfoShowComponent data='bottling'/>
            <StaticComponent data='bottling'/>
        </div>
    )
}

export default Bottle