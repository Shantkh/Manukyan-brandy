import React from 'react'
import WinesBanner from "../components/wines/WinesBanner/WinesBanner";
import WineMiddleBanner from "../components/wines/wineMiddleBanner/WineMiddleBanner";
import WinesSectionComponents from "../components/wines/winesSectionComponents/WinesSectionComponents";
import StaticComponent from "../components/statics/StaticComponent";
import InfoShowComponent from "../components/infoComponent/InfoShowComponent";


const Wines = () => {
    return (
        <div>
            <WinesBanner/>
            <WinesSectionComponents/>
            <WineMiddleBanner/>
            <InfoShowComponent data='wines'/>
            <StaticComponent data='wines'/>
        </div>
    )
}

export default Wines