import React from 'react'
import AnamorMainBanner from "../components/anamor/AnamorMainBanner/AnamorMainBanner";
import AnamorMiddleBanner from "../components/anamor/AnamorLowerBanner/AnamorMiddleBanner";
import OtherWinesComponent from "../components/OtherWines/OtherWinesComponent";
import AnamorWineComponent from "../components/anamor/AnamorWine/AnamorWineComponent";
import StaticComponent from "../components/statics/StaticComponent";

const Anamor = () => {
    return (
        <div>
            <AnamorMainBanner/>
            <AnamorWineComponent/>
            <AnamorMiddleBanner/>
            <OtherWinesComponent/>
            <StaticComponent data='anamor'/>
        </div>
    )
}

export default Anamor