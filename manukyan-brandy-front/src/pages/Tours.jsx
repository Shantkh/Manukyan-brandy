import React from 'react'
import ToursComponent from "../components/tours/ToursComponent";
import StaticComponent from "../components/statics/StaticComponent";

const Tours = () => {
    return (
        <div>
            <ToursComponent/>
            <StaticComponent data='tour'/>
        </div>
    )
}

export default Tours