import React from 'react'
import ContactUs from '../components/cotcatsUs/ContactUs'
import StaticComponent from "../components/statics/StaticComponent";

const Contact = () => {
  return (
    <div>
      <ContactUs />
        <StaticComponent data='contact'/>
    </div>
  )
}

export default Contact