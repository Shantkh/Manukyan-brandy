import React from 'react'
import Awards from "../components/awards/Awards";
import HomeBanner from '../components/homePageComponents/homeBanner/homeBanner';
import HomeFactorySection from '../components/homePageComponents/homeFactorySection/HomeFactorySection';
import SubscriptionComponent from '../components/subscribtion/subsciption-component';
import MainSlider from '../components/mainSlider/MainSlider';
import HomeEvents from '../components//homePageComponents/homeEventSection/HomeEventsSection';
import HomeTours from '../components/homePageComponents/homeToursSection/HomeToursSection';
import HomeWinesSection from '../components/homePageComponents/homeWinesSection/HomeWinesSection';
import StaticComponent from "../components/statics/StaticComponent";


const Home = () => {
  return (
  <div>
    <MainSlider />
    <HomeFactorySection />
    <HomeWinesSection />
    <HomeEvents />
    <HomeTours />
    <HomeBanner />
    <SubscriptionComponent />
    <Awards />
    <StaticComponent data='home'/>
  </div>
    
  )
}

export default Home