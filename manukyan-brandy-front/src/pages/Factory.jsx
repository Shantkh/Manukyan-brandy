import React from 'react'
import FactoryVineyards from '../components/factoryPageComponents/factoryVineyards/FactoryVineyards'
import FactoryMiddleBanner from '../components/factoryPageComponents/factoryMiddleBanner/FactoryMiddleBanner'
import FactoryFermentation from '../components/factoryPageComponents/factoryFermentation/FactoryFermentation'
import FactoryCellars from '../components/factoryPageComponents/factoryCellars/FactoryCellars'
import FactoryBottling from '../components/factoryPageComponents/factoryBottling/FactoryBottling'
import FactoryBottomBanner from '../components/factoryPageComponents/factoryBottomBanner/FactoryBottomBanner'
import FactoryMapComponent from "../components/factoryPageComponents/factoryMap/FactoryMapComponent";
import FactoryHomeBanner from "../components/factoryPageComponents/FactoryHomeBanner/FactoryHomeBanner";
import StaticComponent from "../components/statics/StaticComponent";
import InfoShowComponent from "../components/infoComponent/InfoShowComponent";


const Factory = () => {
  return (
    <div>
        <FactoryHomeBanner/>
        <FactoryMapComponent />
      <FactoryVineyards />
      <FactoryMiddleBanner />
      <FactoryFermentation />
      <FactoryCellars />
      <FactoryBottling />
      <FactoryBottomBanner />
       {/*<FactoryBookTours />*/}
        <InfoShowComponent data='factory'/>
        <StaticComponent data='factory'/>
    </div>
  )
}

export default Factory