import React from 'react'
import HolaniHomeBanner from "../components/holany/HolanyHameBanner/HolaniHomeBanner";
import HolanyInfoBanner from "../components/holany/HolanyInfoBanner/HolanyInfoBanner";
import HolanyDetailsComponent from "../components/holany/holanyDetails/HolanyDetailsComponent";
import OtherWinesComponent from "../components/OtherWines/OtherWinesComponent";
import StaticComponent from "../components/statics/StaticComponent";

const Holani = () => {
    return (
        <div>
            <HolaniHomeBanner/>
            <HolanyDetailsComponent/>
            <HolanyInfoBanner/>
            <OtherWinesComponent/>
            <StaticComponent data='holani'/>
        </div>
    )
}

export default Holani