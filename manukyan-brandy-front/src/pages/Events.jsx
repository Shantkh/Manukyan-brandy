import React from 'react'
import EventsComponent from '../components/events/Events'
import StaticComponent from "../components/statics/StaticComponent";

const Events = () => {
  return (
    <div>
      <EventsComponent />
        <StaticComponent data='event'/>
    </div>
  )
}

export default Events