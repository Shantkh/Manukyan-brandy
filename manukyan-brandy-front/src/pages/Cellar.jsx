import React from 'react'
import CellarsMainBanner from "../components/cellars/CellarsMainPageBanner/CellarsMainBanner";
import CellarUpperSectionComponent from "../components/cellars/CellarsUpperSection/CellarUpperSectionComponent";
import CellarMiddleBanner from "../components/cellars/CellarMiddleBanner/CellarMiddleBanner";
import CellarLowerSectionComponent from "../components/cellars/CellarsLowerSection/CellarLowerSectionComponent";
import StaticComponent from "../components/statics/StaticComponent";
import InfoShowComponent from "../components/infoComponent/InfoShowComponent";

const Cellar = () => {
    return (
        <div>
            <CellarsMainBanner/>
            <CellarUpperSectionComponent/>
            <CellarMiddleBanner/>
            <CellarLowerSectionComponent/>
            <InfoShowComponent data='cellars'/>
            <StaticComponent data='cellars'/>
        </div>
    )
}

export default Cellar