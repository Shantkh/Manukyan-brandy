import '../BottlingStyles.css';
import React, {useEffect, useState} from "react";
import LanguageService from "../../service/languageService";




const BottlingLowerBanner =() =>{
    const [bottling, setbottling] = useState([]);
    const [bottlingLower, setBottlingLower] = useState([]);
    let [language, setLanguage] = useState(LanguageService.getLanguage(navigator.language || 'en-US'));
    useEffect(() => {
        fetchData();
        const handleLanguageSwitch = (event) => {
            const newLanguage = event.detail.newLanguage;
            setLanguage(newLanguage);
            language = newLanguage;
        };
        window.addEventListener('languageSwitched', handleLanguageSwitch);

        return () => {
            window.removeEventListener('languageSwitched', handleLanguageSwitch);
        };
    },[language]);

    const fetchData = async () => {

        try {
            const response = await fetch( `api/bottling`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept-Language': language || 'en'
                }
            });
            const data = await response.json();
            setbottling(data.data);

        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        if (bottling) {

            const lowerSectionData = bottling.find(item => item.bottlingSection.toLowerCase() === 'lower section');
            if (lowerSectionData) {
                setBottlingLower(lowerSectionData);
            }
        }
    }, [bottling]);

    return(<>
        <div className="bottling-info-section reverse">
            <div className="main_container">
                <div className="bottling-info-content">
                    <div className="description-side">
                        <div className="paragraphs">
                            {bottlingLower && <div
                                dangerouslySetInnerHTML={{__html: language === 'hy' ? bottlingLower.armenianContent : bottlingLower.englishContent}}/>}
                        </div>
                    </div>
                    <div className="image-side">
                        {bottlingLower && bottlingLower.images && bottlingLower.images.length > 0 &&
                            <img className="main-content-image" src={bottlingLower.images[0]} alt=""/>
                        }
                    </div>
                </div>
            </div>
        </div>

</>)
}

export default BottlingLowerBanner;