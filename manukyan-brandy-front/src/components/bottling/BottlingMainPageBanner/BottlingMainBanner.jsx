import Banner from "../../banner/Banner";

const BottlingMainBanner =() =>{
    return (
        <Banner sectionEnum='BOTTLING_MAIN_BANNER'/>
    )
}

export default BottlingMainBanner;