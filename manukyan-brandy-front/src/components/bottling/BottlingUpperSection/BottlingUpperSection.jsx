import '../BottlingStyles.css';
import React, {useEffect, useState} from "react";
import LanguageService from "../../service/languageService";


const BottlingUpperSection =() =>{

    const [bottling, setbottling] = useState([]);
    const [bottlingTitle, setBottlingTitles] = useState([]);
    const [bottlingLower, setBottlingLower] = useState([]);
    const [bottlingUpper, setBottlingUpper] = useState([]);
    let [language, setLanguage] = useState(LanguageService.getLanguage(navigator.language || 'en-US'));
    useEffect(() => {
        fetchData();
        const handleLanguageSwitch = (event) => {
            const newLanguage = event.detail.newLanguage;
            setLanguage(newLanguage);
            language = newLanguage;
        };
        window.addEventListener('languageSwitched', handleLanguageSwitch);

        return () => {
            window.removeEventListener('languageSwitched', handleLanguageSwitch);
        };
    },[language]);

    const fetchData = async () => {

        try {
            const response = await fetch( `api/bottling`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept-Language': language || 'en'
                }
            });
            const data = await response.json();
            setbottling(data.data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        if (bottling) {
            const titleSectionData = bottling.find(item => item.bottlingSection.toLowerCase() === 'title');
            if (titleSectionData) {
                setBottlingTitles(titleSectionData);
            }

            const lowerSectionData = bottling.find(item => item.bottlingSection.toLowerCase() === 'lower section');
            if (lowerSectionData) {
                setBottlingLower(lowerSectionData);
            }

            const higherSectionData = bottling.find(item => item.bottlingSection.toLowerCase() === 'upper section');
            if (higherSectionData) {
                setBottlingUpper(higherSectionData);
            }
        }
    }, [bottling]);
    
    return(<><div className="bottling-info-section">
        <div className="main_container">
            <div className="bottling-info-title">
                {bottlingTitle && <div
                    dangerouslySetInnerHTML={{__html: language === 'hy' ? bottlingTitle.armenianContent : bottlingTitle.englishContent}}/>}
            </div>
            <div className="bottling-info-content">
                <div className="image-side">
                    {bottlingUpper && bottlingUpper.images && bottlingUpper.images.length > 0 &&
                        <img className="main-content-image" src={bottlingUpper.images[0]} alt=""/>
                    }
                </div>
                <div className="description-side">
                    <div className="paragraphs">
                        {bottlingUpper && <div
                            dangerouslySetInnerHTML={{__html: language === 'hy' ? bottlingUpper.armenianContent : bottlingUpper.englishContent}}/>}
                    </div>
                </div>
            </div>
        </div>
    </div></>)
}

export default BottlingUpperSection;