import Banner from "../../banner/Banner";

const HolaniHomeBanner = () => {

    return (
        <Banner sectionEnum='HOLANY_MAIN_BANNER'/>
    )
}

export default HolaniHomeBanner;