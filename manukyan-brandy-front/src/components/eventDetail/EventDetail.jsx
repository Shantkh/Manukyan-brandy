import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { fetchEventById } from '../service/eventsService';
import styles from './styles.module.scss';
import './styles.module.scss';
import { format } from 'date-fns';
import LanguageService from '../service/languageService';
import EventDetailImagePopupComponent from './EventDetailImagePopupComponent';


const EventDetail = () => {
  const { id } = useParams();
  const [event, setEvent] = useState(null);
  const [language, setLanguage] = useState(LanguageService.getLanguage(navigator.language || 'en-US'));
  const [isPopupOpen, setIsPopupOpen] = useState(false); // State to control popup visibility

  useEffect(() => {
    const fetchData = async () => {
      const data = await fetchEventById(id);
      if (data) {
        setEvent(data.data[0]);
      }
    };
    fetchData();

    const languageSwitchHandler = () => {
      fetchData();
    };
    window.addEventListener('languageSwitched', languageSwitchHandler);

    return () => {
      window.removeEventListener('languageSwitched', languageSwitchHandler);
    };
  }, [id]);

  const handlePopupOpen = () => {
    setIsPopupOpen(true); // Open the popup when the button is clicked
  };

  const getMenuText = (menuKey) => {
    const locale = language || 'en';
    const menuTexts = {
      en: {
        manuk: 'Manukyan Brandy Factory',
        fa: '2213, C. Balahovit, Kotayk Marz,',
        sa: 'Armenia, 10/1 Karmir Banakaineri str.',
        mph: 'More photos',
      },
      hy: {
        manuk: 'Մանուկյան կոնյակի գործարան',
        fa: '2213, Կոտայքի մարզ, Ք. Բալահովիտ,',
        sa: 'Հայաստան, Կարմիր Բանակայերի փող. 10/1',
        mph: 'Լրացուցիչ լուսանկարներ',
      },
    };

    return menuTexts[locale][menuKey] || menuTexts.en[menuKey];
  };

  return (
      <>
        {event && (
            <div className={styles.container}>
              <div className={styles.mediaContainer}>
                <div>
                  <img src={event.sliderMedia[0]} alt={event.sliderMedia[0]} />
                  {/* Transparent overlay with button */}
                  <div className={styles.overlay}>
                    <button className={styles.popupButton} onClick={handlePopupOpen}>
                      {getMenuText('mph')}
                    </button>
                  </div>
                </div>
                <div>
                  <img src={event.sliderMedia[1]} alt={event.sliderMedia[1]} />
                  <img src={event.sliderMedia[2]} alt={event.sliderMedia[2]} />
                </div>
              </div>
              <div className={styles.eventContentContainer}>
                <div className={styles.title} dangerouslySetInnerHTML={{ __html: event.localizations[0].htmlTitle }} />
                <div className={styles.dateAndTimeContainer}>
                  <div className={styles.date}>{format(event.eventStartDate, 'MMMM d, yyyy')}</div>
                  <div className={styles.time}>{format(event.eventStartDate, 'h:mm a')}</div>
                </div>
                <div className={styles.location}>
                  <div className={styles.locTitle}>{getMenuText('manuk')}</div>
                  <p>
                    {getMenuText('fa')}<br />
                    {getMenuText('sa')}
                  </p>
                </div>
                <div className={styles.description} dangerouslySetInnerHTML={{ __html: event.localizations[0].htmlLongContent }} />
              </div>
            </div>
        )}
        {/* Render the popup component if isPopupOpen is true */}
        {isPopupOpen && <EventDetailImagePopupComponent eventId={event.id} onClose={() => setIsPopupOpen(false)} />}
      </>
  );
};

export default EventDetail;
