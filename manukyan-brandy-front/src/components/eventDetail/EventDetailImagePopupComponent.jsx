import React, { useEffect, useState } from 'react';
import styles from './EventDetailImagePopupComponent.module.css'; // Corrected import statement

const EventDetailImagePopupComponent = ({ eventId, onClose }) => {
    const [images, setImages] = useState([]);

    useEffect(() => {
        const fetchImages = async () => {
            try {
                const response = await fetch( `api/events/get/${eventId}`);
                if (!response.ok) {
                    console.error('Error fetching images:', response.status);
                    return;
                }
                const data = await response.json();
                if (data.data && data.data.sliderMedia && Array.isArray(data.data.sliderMedia)) {
                    setImages(data.data.sliderMedia);
                } else {
                    console.error('Invalid response data:', data);
                }
            } catch (error) {
                console.error('Error fetching images:', error);
            }
        };

        fetchImages();

        // Cleanup function
        return () => {
            // Any cleanup code if needed
        };
    }, [eventId]);

    return (
        <div className={styles.popupOverlay} onClick={onClose}>
            <div className={styles.popup}>
                <div className={styles.popupInner}>
                    <div className={styles.imageContainer}>
                        {images.map((image, index) => (
                            <img key={index} src={image} alt={`Image ${index}`} className={styles.image} />
                        ))}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default EventDetailImagePopupComponent;
