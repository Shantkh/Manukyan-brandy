import React, {useEffect, useState} from 'react';
import {fetchBannerContent} from "../service/bannerService";
import styles from "./styles.module.scss";

const Banner = ({sectionEnum}) => {
    const [bannerContent, setBannerContent] = useState();

    useEffect(() => {

        const fetchData = async () => {
            const data = await fetchBannerContent(sectionEnum);
            if (data) {
                setBannerContent(data);
            }
        };
        fetchData();

        const languageSwitchHandler = () => {
            fetchData();
        };
        window.addEventListener('languageSwitched', languageSwitchHandler);

        return () => {
            window.removeEventListener('languageSwitched', languageSwitchHandler);
        };
    }, []);


    return (
        <div className={styles.banner} style={{ backgroundImage: `url(${bannerContent?.data?.sliderMedia || ''})` }}>
            <div className={styles.overlay}>
                {bannerContent && bannerContent.data.localizations && bannerContent.data.localizations.length > 0 && (
                    <div
                        className={styles.bannerText}
                        dangerouslySetInnerHTML={{ __html: bannerContent.data.localizations[0]?.htmlContent || '' }}
                    />
                )}
            </div>
        </div>
    )
}

export default Banner;