import "./ToursStyle.css";
import React, {useEffect, useState} from 'react'
import LanguageService from "../service/languageService";
import { Link } from 'react-router-dom';


const ToursComponent = () => {
    let [language, setLanguage] = useState(LanguageService.getLanguage(navigator.language || 'en-US'));
    const [toursData, setToursData] = useState([]);
    const [paginationWithSorting, setPaginationWithSorting] = useState({
        pageNumber: 0,
        contentSize: 5,
        sortDirection: 'asc',
        sortElement: 'tourMonth'
    });

    const fetchData = async () => {
        try {
            const response = await fetch(`api/tours/all/local/pagination`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept-Language': language || 'en'
                },
                body: JSON.stringify(paginationWithSorting)
            });
            const data = await response.json();
            setToursData(data.data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    const handleNextPage = () => {
        setPaginationWithSorting(prevState => ({
            ...prevState,
            pageNumber: prevState.pageNumber + 1
        }));
        scrollPageToTop();
    };

    const handlePrevPage = () => {
        if (paginationWithSorting.pageNumber > 0) {
            setPaginationWithSorting(prevState => ({
                ...prevState,
                pageNumber: prevState.pageNumber - 1
            }));
            scrollPageToTop();
        }
    };

    const scrollPageToTop = () => {
        const scrollToTop = () => {
            const c = document.documentElement.scrollTop || document.body.scrollTop;
            if (c > 0) {
                window.requestAnimationFrame(scrollToTop);
                window.scrollTo(0, c - c / 16);
            }
        };
        scrollToTop();
    };

    useEffect(() => {
        fetchData();
    }, [paginationWithSorting]);

    useEffect(() => {
        const handleLanguageSwitch = (event) => {
            const newLanguage = event.detail.newLanguage;
            setLanguage(newLanguage);
            language = newLanguage;
            fetchData()
        };
        window.addEventListener('languageSwitched', handleLanguageSwitch);

        return () => {
            window.removeEventListener('languageSwitched', handleLanguageSwitch);
        };
    }, []);


    const getMenuText = (menuKey) => {
        const locale = language || 'en';
        const menuTexts = {
            en: {
                tours: 'Our Tours',
                amd: 'AMD',
                cost: 'Cost for 1 visitor',
                month: 'Month',
                duration: 'Duration',
                time: 'Starting Time',
                day: 'Day',
                date: 'Date',
                book: 'Book Now',
                monday: 'Monday',
                tuesday: 'Tuesday',
                wednesday: 'Wednesday',
                thursday: 'Thursday',
                friday: 'Friday',
                saturday: 'Saturday',
                sunday: 'Sunday',
                previous: 'Previous',
                next: 'Next',
            },
            hy: {
                tours: 'Մեր շրջագայությունները',
                amd: 'դրամ',
                cost: 'Արժեքը 1 այցելուի համար',
                month: 'Ամիս',
                duration: 'Տեւողությունը',
                time: 'Մեկնարկային ժամ',
                day: 'Օր',
                date: 'Ամսաթիվ',
                book: 'Ամրագրեք',
                monday: 'Երկուշաբթի',
                tuesday: 'Երեքշաբթի',
                wednesday: 'Չորեքշաբթի',
                thursday: 'Հինգշաբթի',
                friday: 'Ուրբաթ',
                saturday: 'Շաբաթ',
                sunday: 'Կիրակի',
                previous: 'Նախորդ',
                next: 'Հաջորդը',
            },
        };

        return menuTexts[locale][menuKey] || menuTexts.en[menuKey];
    };


    return (
        <>
            <div className="section-heading">
                <div className="top">
                    <div className="secondary-headline">{getMenuText('tours')}</div>
                </div>
            </div>
            <div className="frame-202">
                <div className="main_container">
                    {toursData.map((tour) => (
                        <div className="frame-198" key={tour.id}>
                            <div className="frame-195">
                                <div className="frame-194">
                                    <div className="frame-193">
                                        <div className="standard-package">{tour.tourLocalization[0].htmlTitle}</div>
                                        <div className="frame-192">
                                            <div className="amd-5-000">{getMenuText('amd')} {tour.price}</div>
                                            <div className="cost-for-1-visitor">{getMenuText('cost')}</div>
                                        </div>
                                        <div className="tour-details">
                                            <div className="tour-detail">{getMenuText('month')}: {tour.tourMonth}</div>
                                            <div className="tour-detail">{getMenuText('duration')}: {tour.duration} hours</div>
                                            <div className="tour-detail">{getMenuText('time')}: {tour.tourHour}</div>
                                            <div className="tour-detail">{getMenuText('day')}: {getMenuText(tour.tourDay.toLowerCase())}</div>
                                            <div className="tour-detail">{getMenuText('date')}: {tour.dateOfTour}</div>
                                        </div>
                                    </div>
                                    <div className="tour-description">{tour.tourLocalization[0].htmlShortContent}</div>
                                </div>
                                <div className="buttons-group">
                                    <div className="button">
                                        <Link to={`/tours/booking/${tour.id}`} state={{ tourId: tour.id }}>
                                            <div className="text-container">
                                                <div className="button-text">{getMenuText('book')}</div>
                                            </div>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                            <img className="rectangle-20" src={tour.tourImages[0]} alt={`Tour ${tour.id}`} />
                        </div>
                    ))}
                </div>

            </div>
            <div className="pagination-container">
                    <span  className="pagination-text left-button" onClick={handlePrevPage} disabled={paginationWithSorting.pageNumber === 0}>
                        {/* {getMenuText('previous')} */}
                    </span >
                <span  className="pagination-text right-button" onClick={handleNextPage} disabled={toursData.length < paginationWithSorting.contentSize}>
                        {/* {getMenuText('next')} */}
                    </span >
            </div>
        </>
    );

}

export default ToursComponent;