import React, {useEffect, useState} from "react";
import './subscription.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faEnvelopeOpen
} from "@fortawesome/free-solid-svg-icons";
import LanguageService from "../service/languageService";

const SubscriptionComponent = () => {
    const [email, setEmail] = useState('');
    const [notification, setNotification] = useState(null);
    const [language, setLanguage] = useState(LanguageService.getLanguage(navigator.language || 'en-US'));

    useEffect(() => {
        const handleLanguageSwitch = (event) => {
            const newLanguage = event.detail.newLanguage;
            setLanguage(newLanguage);
        };

        window.addEventListener('languageSwitched', handleLanguageSwitch);

        return () => {
            window.removeEventListener('languageSwitched', handleLanguageSwitch);
        };
    }, []);

    const getMessage = () => {
        const locale = language || 'en';
        const messages = {
            en: 'Want to be the first to know about our new events? <br/>Just enter your email address and hit the subscribe button!',
            hy: 'Դուք ուզում եք մեկնարկել մեր նոր իրավունքների մասին։ <br/>Մոտավորապես մուտք գործեք ձեր էլ. հասցեն ու սեղմեք բաժնորդագրվելու կոճակը։',
        };

        return messages[locale] || messages.en;
    };

    const getButtonText = () => {
        const locale = language || 'en';
        const buttonTexts = {
            en: 'Subscribe',
            hy: 'Բաժանորդագրվել',
        };

        return buttonTexts[locale] || buttonTexts.en;
    };

    const getPLaceHolderText = () => {
        const locale = language || 'en';
        const placeHolderText = {
            en: 'Enter your email address here.',
            hy: 'Մուտքագրեք ձեր էլ.փոստի հասցեն:',
        };
        return placeHolderText[locale] || placeHolderText.en;
    }

    const validateEmail = () => {
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return emailRegex.test(email);
    };

    const subscribersData = {
        email: email
    };

    const handleSubmit = async () => {
        if (!validateEmail()) {
            setNotification(language === 'hy' ? 'Անվավեր էլ․ հասցե' : 'Invalid email address');
            // clearNotification();
            return;
        }

        try {
            const res = await fetch(`api/subscribe/`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                    'Accept-Language': language,
                },
                body: JSON.stringify(subscribersData),
            });
            const data = await res.json();
            if (data.httpStatusCode === 201) {
                setNotification(language === 'hy' ? 'Շնորհակալություն բաժանորդագրվելու համար:' : 'Thanks for subscribing.');
            } else {
                setNotification(data.data);
            }

            clearNotification();
        } catch (error) {
            setNotification(error);
            clearNotification();
        }
    };

    const clearNotification = () => {
        setTimeout(() => {
            setNotification(null);
        }, 2000);
    };

    return (
        <div className="subscribe-section">
            <div className="subscribtion-text-section">
                <label htmlFor="input" dangerouslySetInnerHTML={{__html: getMessage()}}/>
                <div className="submit-section">
                    <div>
                        <FontAwesomeIcon icon={faEnvelopeOpen}/>
                    </div>
                    <input
                        className="input-section"
                        placeholder={getPLaceHolderText()}
                        type="email"
                        id="input"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                </div>
                {notification && typeof notification === 'string' && (
                    <div className={`notification ${notification.includes('failed') ? 'error' : 'success'}`}>
                        {notification}
                    </div>
                )}
                    <div className="button-submit-section">
                        <div className='subscribe-section-button'>
                            <button onClick={handleSubmit}>
                                {getButtonText()}
                            </button>
                        </div>
                    </div>
            </div>
        </div>
    );
};

export default SubscriptionComponent;