import LanguageService from "../service/languageService";
import {useEffect, useState} from "react";
import './otherStyles.css';
import sanitizeHtml from 'sanitize-html';


const OtherWinesComponent =() =>{
    const [ines, setWines] = useState();
    let [language, setLanguage] = useState(LanguageService.getLanguage(navigator.language || 'en-US'));

    const fetchData = async () => {
        try {
            const response = await fetch(  `api/products/other` , {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept-Language': language || 'en'
                }
            });
            const data = await response.json();
            setWines(data.data);

        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        fetchData();
        const handleLanguageSwitch = (event) => {
            const newLanguage = event.detail.newLanguage;
            setLanguage(newLanguage);
            language = newLanguage;
        };
        window.addEventListener('languageSwitched', handleLanguageSwitch);

        return () => {
            window.removeEventListener('languageSwitched', handleLanguageSwitch);
        };
    }, []);

    const getMenuText = (menuKey) => {
        const locale = language || 'en';
        const menuTexts = {
            en: {
                most: 'Most Viewed',
            },
            hy: {
                most: 'Ամենաշատ դիտված',
            },
        };

        return menuTexts[locale][menuKey] || menuTexts.en[menuKey];
    };

    const renderHtmlContent = (htmlContent) => {
        return { __html: sanitizeHtml(htmlContent) };
    };


    return(
        <>
            <div className="main_container">
                <div className="otherWines_section">
                    <h1 className="main-title">{getMenuText('most')}</h1>
                    <div className="otherWines_list">
                        {ines && ines.map((wine, index) => (
                            <div className="otherWines_item" key={index}>
                                <div className="otherWines_item_image">
                                    <img src={wine.productImages} alt="" />
                                </div>
                                <div className="otherWines_item_name">
                                    {wine.pageTitle && wine.pageTitle.length > 0 && (
                                        <div dangerouslySetInnerHTML={renderHtmlContent(language === 'hy' ? wine.pageTitle.find(entry => entry.locale === 'hy')?.htmlContent : wine.pageTitle.find(entry => entry.locale === 'en')?.htmlContent)} />
                                    )}
                                    {wine.brandName && (
                                        <div dangerouslySetInnerHTML={renderHtmlContent(language === 'hy' ? wine.brandName.brandNameArmenian : wine.brandName.brandNameEnglish)} />
                                    )}
                                    {wine.productDate}
                                </div>
                            </div>
                        ))}
                    </div>
                </div>

            </div>
        </>
    )
}

export default OtherWinesComponent;