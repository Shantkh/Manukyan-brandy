import {useEffect, useState} from "react";
import LanguageService from "../service/languageService";


const InfoShowComponent = (page) => {
    const [info, setInfo] = useState();
    const [infoArmenian, setInfoArmenian] = useState();
    const [infoEnglish, setInfoEnglish] = useState();
    const [link, setLink] = useState();
    let [language, setLanguage] = useState(LanguageService.getLanguage(navigator.language || 'en-US'));

    const fetchData = async () => {
        try {
            const response = await fetch( `api/info/get?page=${page.data}`, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json; charset=UTF-8',
                }
            });
            if (response.ok) {
                const data = await response.json();
                setInfo(data.data);
                setLink(data.data.link.toLowerCase());
                setInfoEnglish(data.data.infoEn);
                setInfoArmenian(data.data.infoHy);
            }
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        fetchData();
        const handleLanguageSwitch = (event) => {
            const newLanguage = event.detail.newLanguage;
            setLanguage(newLanguage);
            language = newLanguage;
        };
        window.addEventListener('languageSwitched', handleLanguageSwitch);

        return () => {
            window.removeEventListener('languageSwitched', handleLanguageSwitch);
        };
    },[]);

    const getMenuText = (menuKey) => {
        const locale = language || 'en';
        const menuTexts = {
            en: {
                learn: 'Learn More'
            },
            hy: {
                learn: 'Իմացեք ավելին'
            },
        };

        return menuTexts[locale][menuKey] || menuTexts.en[menuKey];
    };

    return (
        <div className="frame-175">
            <div className="rectangle-19"></div>
            <div className="frame-13">
                <div className="frame-12">
                    <div className="wine-enthusiasts">
                        {language === 'en' ? <div dangerouslySetInnerHTML={{ __html: infoEnglish }} /> : <div dangerouslySetInnerHTML={{ __html: infoArmenian }} />}
                    </div>
                </div>
                <a href={link} className="button">
                    <div className="text-container">
                        <div className="button-text">{getMenuText('learn')}</div>
                    </div>
                </a>
            </div>
        </div>
    );
};

export default InfoShowComponent;