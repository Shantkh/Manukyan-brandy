import React from 'react';
import { fetchBannerContent } from '../../service/bannerService';
import { useEffect, useState } from 'react';
import languageService from '../../service/languageService';
import SliderContentSection from '../../sliderContentSection/SliderContentSection';

const HomeEvents = () => {
  const acceptLanguageHeader = languageService.getLanguage(navigator.language || 'en-US');

  const language = localStorage.getItem('language') || acceptLanguageHeader;


  const [homeEvents, setHomeEventsContent] = useState([]);

  const getMenuText = (menuKey) => {
    const locale = language || 'en';
    const menuTexts = {
      en: {
        learn: 'Learn More',
        event: 'Upcoming Events',
      },
      hy: {
        learn: 'Իմացեք ավելին',
        event: 'Սպասվող Միջոցառումներ',
      },
    };

    return menuTexts[locale][menuKey] || menuTexts.en[menuKey];
  };

  useEffect(() => {
    const fetchData = async () => {
      const data = await fetchBannerContent('HOME_EVENTS');
      if (data) {
        setHomeEventsContent(data);
      }
    };

    fetchData();

    const languageSwitchHandler = () => {
      fetchData();
    };
    window.addEventListener('languageSwitched', languageSwitchHandler);

    return () => {
      window.removeEventListener('languageSwitched', languageSwitchHandler);
    };
  }, []);

  return (
    <SliderContentSection
      data={homeEvents.data}
      titleText={getMenuText('event')}
      buttonText={getMenuText('learn')}
      sliderRight={false}
    />
  );
};

export default HomeEvents;
