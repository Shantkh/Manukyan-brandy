import Banner from '../../banner/Banner';

const HomeBanner = () => {

    return (
        <Banner sectionEnum='HOME_BANNER'/>
    )
}

export default HomeBanner;