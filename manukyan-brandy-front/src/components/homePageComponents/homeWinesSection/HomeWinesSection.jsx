import React, {useEffect, useState} from 'react';
import './homeWinesSection.css';
import LanguageService from "../../service/languageService";
import {Link} from "react-router-dom";



const HomeWinesSection = () => {
    const [produts, setProducts] = useState([]);
    let [language, setLanguage] = useState(LanguageService.getLanguage(navigator.language || 'en-US'));
    const fetchProducts = async () => {
        try {
            const response = await fetch( `api/products/all`);
            if (response.ok) {
                const data = await response.json();
                setProducts(data.data);

            } else {
                console.error('Failed to fetch brands');
            }
        } catch (error) {
            console.error('Error fetching brands:', error);
        }
    };

    useEffect(() => {
        fetchProducts();
        const handleLanguageSwitch = (event) => {
            const newLanguage = event.detail.newLanguage;
            setLanguage(newLanguage);
            language = newLanguage;
        };
        window.addEventListener('languageSwitched', handleLanguageSwitch);

        return () => {
            window.removeEventListener('languageSwitched', handleLanguageSwitch);
        };
    }, [language]);

    const renderBrandName = (wine) => {
        if (language === 'hy') {
            return <div dangerouslySetInnerHTML={{__html: wine.brandNameArmenian}}></div>;
        } else {
            return <div dangerouslySetInnerHTML={{__html: wine.brandNameEnglish}}></div>;
        }
    };

    const getMenuText = (menuKey) => {
        const locale = language || 'en';
        const menuTexts = {
            en: {
                wines: 'Our Wines',
                sa: 'See All',
            },
            hy: {
                wines: 'Մեր գինիները',
                sa: 'Տեսնել բոլորը',
            },
        };

        return menuTexts[locale][menuKey] || menuTexts.en[menuKey];
    };

    return (
        <div>

            <div className='wines-section'>
                <div className='secondary-headline-main'>{getMenuText('wines')}</div>
                <div className='wines-wrapper'>
                    {/*<svg*/}
                    {/*    className='down-arrow'*/}
                    {/*    width='36'*/}
                    {/*    height='36'*/}
                    {/*    viewBox='0 0 36 36'*/}
                    {/*    fill='none'*/}
                    {/*    xmlns='http://www.w3.org/2000/svg'*/}
                    {/*>*/}
                    {/*    <path*/}
                    {/*        d='M16.0289 17.8935L23.4539 10.4685C23.5972 10.3302 23.7115 10.1646 23.7901 9.98164C23.8687 9.79863 23.9101 9.6018 23.9118 9.40263C23.9135 9.20346 23.8756 9.00594 23.8002 8.8216C23.7247 8.63725 23.6134 8.46977 23.4725 8.32894C23.3317 8.1881 23.1642 8.07672 22.9799 8.0013C22.7955 7.92587 22.598 7.88792 22.3988 7.88965C22.1997 7.89138 22.0028 7.93276 21.8198 8.01138C21.6368 8.08999 21.4713 8.20426 21.3329 8.34753L12.8474 16.833C12.5662 17.1143 12.4083 17.4958 12.4083 17.8935C12.4083 18.2913 12.5662 18.6727 12.8474 18.954L21.3329 27.4395C21.4713 27.5828 21.6368 27.6971 21.8198 27.7757C22.0028 27.8543 22.1997 27.8957 22.3988 27.8974C22.598 27.8991 22.7955 27.8612 22.9799 27.7858C23.1642 27.7103 23.3317 27.599 23.4725 27.4581C23.6134 27.3173 23.7247 27.1498 23.8002 26.9655C23.8756 26.7811 23.9135 26.5836 23.9118 26.3844C23.9101 26.1853 23.8687 25.9884 23.7901 25.8054C23.7115 25.6224 23.5972 25.4569 23.4539 25.3185L16.0289 17.8935Z'*/}
                    {/*        fill='#291818'*/}
                    {/*    />*/}
                    {/*</svg>*/}
                    {produts.map((wine) => (
                        <Link to="/wines" className="wine-link" key={wine.id}>
                            <div className='wines'>
                                <div className='wine-item'>
                                    <img className='wine-picture' src={wine.productImages[0]}
                                         alt={renderBrandName(wine)}/>
                                    <div className='wine-title'>{renderBrandName(wine)} {wine.productDate}</div>
                                </div>
                            </div>
                        </Link>
                    ))}
                    {/*<svg*/}
                    {/*    className='arrow-down2'*/}
                    {/*    width='36'*/}
                    {/*    height='36'*/}
                    {/*    viewBox='0 0 36 36'*/}
                    {/*    fill='none'*/}
                    {/*    xmlns='http://www.w3.org/2000/svg'*/}
                    {/*>*/}
                    {/*    <path*/}
                    {/*        d='M19.9708 17.8935L12.5458 10.4685C12.4026 10.3302 12.2883 10.1646 12.2097 9.98164C12.1311 9.79863 12.0897 9.6018 12.0879 9.40263C12.0862 9.20346 12.1242 9.00594 12.1996 8.8216C12.275 8.63725 12.3864 8.46977 12.5272 8.32894C12.6681 8.1881 12.8355 8.07672 13.0199 8.0013C13.2042 7.92587 13.4018 7.88792 13.6009 7.88965C13.8001 7.89138 13.9969 7.93276 14.1799 8.01138C14.3629 8.08999 14.5285 8.20426 14.6668 8.34753L23.1523 16.833C23.4335 17.1143 23.5915 17.4958 23.5915 17.8935C23.5915 18.2913 23.4335 18.6727 23.1523 18.954L14.6668 27.4395C14.5285 27.5828 14.3629 27.6971 14.1799 27.7757C13.9969 27.8543 13.8001 27.8957 13.6009 27.8974C13.4018 27.8991 13.2042 27.8612 13.0199 27.7858C12.8355 27.7103 12.6681 27.599 12.5272 27.4581C12.3864 27.3173 12.275 27.1498 12.1996 26.9655C12.1242 26.7811 12.0862 26.5836 12.0879 26.3844C12.0897 26.1853 12.1311 25.9884 12.2097 25.8054C12.2883 25.6224 12.4026 25.4569 12.5458 25.3185L19.9708 17.8935Z'*/}
                    {/*        fill='#291818'*/}
                    {/*    />*/}
                    {/*</svg>*/}
                </div>
                <div className='buttons-group'>
                    <Link to="/wines" className="button">
                        <div className="text-container">
                            <div className="button-text">{getMenuText('sa')}</div>
                        </div>
                    </Link>
                </div>
            </div>
        </div>
    );
};

export default HomeWinesSection;
