import React from 'react';
import {fetchBannerContent} from '../../service/bannerService';
import {useEffect, useState} from 'react';
import languageService from '../../service/languageService';
import SliderContentSection from '../../sliderContentSection/SliderContentSection';
import {Link, useNavigate} from "react-router-dom";

const HomeFactorySection = () => {
    const acceptLanguageHeader = languageService.getLanguage(navigator.language || 'en-US');

    const language = localStorage.getItem('language') || acceptLanguageHeader;

    const [homeFactorySection, setHomeFactorySectionContent] = useState([]);
    const navigate = useNavigate();

    const getMenuText = (menuKey) => {
        const locale = language || 'en';
        const menuTexts = {
            en: {
                learn: 'Learn More',
                factory: 'FACTORY',
            },
            hy: {
                learn: 'Իմացեք ավելին',
                factory: 'ԳՈՐԾԱՐԱՆ',
            },
        };

        return menuTexts[locale][menuKey] || menuTexts.en[menuKey];
    };

    useEffect(() => {
        const fetchData = async () => {
            const data = await fetchBannerContent('HOME_FACTORY');
            if (data) {
                setHomeFactorySectionContent(data);
            }
        };
        fetchData();

        const languageSwitchHandler = () => {
            fetchData();
        };
        window.addEventListener('languageSwitched', languageSwitchHandler);

        return () => {
            window.removeEventListener('languageSwitched', languageSwitchHandler);
        };
    }, []);

    const handleButtonClick = () => {
        const route = homeFactorySection.data.link.toLowerCase();
        navigate(route);
    }

    return (
        <SliderContentSection
            data={homeFactorySection.data}
            titleText={getMenuText('factory')}
            buttonText={getMenuText('learn')}
            sliderRight={false}
            onClick={handleButtonClick}/>
    );
};


export default HomeFactorySection;
