import React from 'react';

const PopupComponent = ({ data,  language }) => {

    const getMenuText = (menuKey) => {
        const supportedLanguages = ['en', 'hy'];
        const locale = supportedLanguages.includes(language) ? language : 'en';
        const menuTexts = {
            en: {
                amd: 'AMD'
            },
            hy: {
                amd: 'դրամ'
            },
        };

        return menuTexts[locale][menuKey] || menuTexts.en[menuKey];
    };

    const getTourDay = (tourDay) => {
        const supportedLanguages = ['en', 'hy'];
        const locale = supportedLanguages.includes(language) ? language : 'en';
        if (locale === 'hy') {
            const dayTranslations = {
                MONDAY: 'Երկուշաբթի',
                TUESDAY: 'Երեքշաբթի',
                WEDNESDAY: 'Չորեքշաբթի',
                THURSDAY: 'Հինգշաբթի',
                FRIDAY: 'Ուրբաթ',
                SATURDAY: 'Շաբաթ',
                SUNDAY: 'Կիրակի',
            };
            return dayTranslations[data.tourDay];
        }else{
            return tourDay
        }
    };

    const formatDate = (date, lang) => {
        if (!(date instanceof Date && !isNaN(date))) {
            console.error('Invalid date:', date);
            return 'Invalid Date';
        }

        const options = {
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        };

        const monthNames = {
            en: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            hy: ['Հունվարի', 'Փետրվարի', 'Մարտի', 'Ապրիլի', 'Մայիսի', 'Հունիսի', 'Հուլիսի', 'Օգոստոսի', 'Սեպտեմբերի', 'Հոկտեմբերի', 'Նոյեմբերի', 'Դեկտեմբերի']
        };

        const monthIndex = date.getMonth();
        const formattedMonth = lang === 'hy' ? monthNames.hy[monthIndex] : monthNames.en[monthIndex];

        const formatter = new Intl.DateTimeFormat(lang === 'hy' ? 'hy' : 'en', options);
        let formattedDate = formatter.format(date);

        if (lang === 'hy') {
            const [month, day, year] = formattedDate.split(' ');
            formattedDate = `${day} ${formattedMonth}, ${year} թ`;
        }

        return formattedDate;
    }

    return (
        <div className="popup">
            <div className="popup-content">
                <h2>{data.tourLocalization[0].htmlTitle}</h2>
                <br/>
                <p>Date: <span className="highlight">{formatDate(new Date(data.dateOfTour), language)}</span></p>
                {/*<p>Time: <span className="highlight">{data.tourHour.toString()}</span></p>*/}
                <p>Duration: <span className="highlight">{data.duration}</span></p>
                <p>Price: <span className="highlight">{data.price} {getMenuText("amd")}</span></p>
                <p>Day: <span className="highlight">{getTourDay(data.tourDay)}</span></p>
                <br/>
                <p>{data.tourLocalization[0].htmlShortContent}</p>

            </div>
        </div>
    );
};

export default PopupComponent;
