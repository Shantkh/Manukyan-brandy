import {useLocation} from 'react-router-dom';
import React, {useEffect, useState} from 'react';
import './bookingstyle.scss';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import './CustomCalendar.css';
import LanguageService from '../service/languageService';
import {format} from 'date-fns';
import {hy} from 'date-fns/locale/hy';
import {enUS} from 'date-fns/locale/en-US';
import Counter from '../service/Counter';
import PopupComponent from './PopupComponent';
import useWindowDimensions from '../../hooks/useWindowDimensions';
import left_grape from '../../assets/leftgrape.png';
import right_grape from '../../assets/rightgrape.png';

const BookingComponent = () => {
    let prettyDate = '';
    let [language, setLanguage] = useState(LanguageService.getLanguage(navigator.language || 'en-US'));
    const location = useLocation();
    let tourId = location.state?.tourId;
    const [date, setDate] = useState(new Date());
    const weekdays = document.querySelectorAll('.react-calendar__month-view__weekdays__weekday');

    const [attendees, setAttendees] = useState(1);
    const [totalAmount, setTotalAmount] = useState(1);

    const [tourDate, setTourDate] = useState(new Date());
    const [toursData, setToursData] = useState(new Date());
    const [dateFormat, setDateFormat] = useState([]);
    const [markedDatesForMonth, setMarkedDatesForMonth] = useState([]);
    const [monthData, setMonthData] = useState([]);

    const [popupVisible, setPopupVisible] = useState(false);
    const [popupDate, setPopupDate] = useState(null);
    const [tourLanguage, setTourLanguage] = useState('hy');

    const handleTileMouseOver = (data, matchingDate, event) => {
        setPopupDate(matchingDate,language);
        setPopupVisible(true);
    };


    const handleTileMouseOut = () => {
        setPopupVisible(false);
    };

    useEffect(() => {
        const handleLanguageSwitch = (event) => {
            const newLanguage = event.detail.newLanguage;
            setLanguage(newLanguage);
            language = newLanguage;
        };
        window.addEventListener('languageSwitched', handleLanguageSwitch);

        return () => {
            window.removeEventListener('languageSwitched', handleLanguageSwitch);
        };
    }, [language]);



    const fetchData = async () => {
        try {
            const response = await fetch(  `api/tours/booking/${tourId}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept-Language': language || 'en',
                },
            });
            const responseData = await response.json();

            if (responseData && responseData.data) {
                setToursData(responseData.data);
                localStorage.setItem('dayOfTour', responseData.data.dateOfTour);
                if (language === 'en') {
                    prettyDate = format(responseData.data.dateOfTour, 'do MMMM, yyyy', {locale: enUS});
                } else {
                    prettyDate = format(responseData.data.dateOfTour, 'do MMMM, yyyy', {locale: hy});
                    const monthNameArmenian = prettyDate.split(' ')[1];
                    const capitalizedMonthName = monthNameArmenian.charAt(0).toUpperCase() + monthNameArmenian.slice(1);
                    prettyDate = prettyDate.replace(monthNameArmenian, capitalizedMonthName);
                }
                setDateFormat(prettyDate);
            } else {
                console.error('No tour data received from the API');
            }
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        fetchData();
    }, [tourId]);

    useEffect(() => {
        if (toursData && toursData.dateOfTour) {
            const tourDateParts = toursData.dateOfTour.split('-').map(Number);
            setTourDate(new Date(tourDateParts[0], tourDateParts[1] - 1, tourDateParts[2]));
        }
    }, [toursData]);

    const onChange = (newDate) => {
        setDate(newDate);
    };

    weekdays.forEach(function (weekday) {
        const textContent = weekday.textContent;
        if (textContent.length > 2) {
            weekday.textContent = textContent.substring(0, 2); // Truncate to two characters
        }
    });

    const getMenuText = (menuKey) => {
        const locale = language || 'en';
        const menuTexts = {
            en: {
                headerHeading: 'Tour Booking',
                headerText: `You are about to embark on your tour of the Manukyan Brandy Factory. Ensuring a responsible and 
                    safe experience is one of our foremost concerns to make your visit both memorable and secure. 
                    Please note that we do not support underage drinking, strongly discourage alcohol consumption 
                    while driving, and remind everyone to avoid drinking during pregnancy.`,
                tourBooking: 'Tour Booking',
                info: 'Fill the form to book a tour to Manukyan Brandy Factory.',
                detail: 'Tour Details',
                pdata: 'Personal Data',
                amount: 'TOTAL AMOUNT',
                amd: 'AMD',
                continue: 'Continue',
            },
            hy: {
                headerHeading: 'Շրջագայությունների ամրագրում',
                headerText: `You are about to embark on your tour of the Manukyan Brandy Factory. Ensuring a responsible and 
                    safe experience is one of our foremost concerns to make your visit both memorable and secure. 
                    Please note that we do not support underage drinking, strongly discourage alcohol consumption 
                    while driving, and remind everyone to avoid drinking during pregnancy.`,
                tourBooking: 'Շրջագայությունների ամրագրում',
                info: 'Մանուկյան կոնյակի գործարան էքսկուրսիա պատվիրելու համար լրացրեք ձևաթուղթը։',
                detail: 'Շրջագայության ման.',
                pdata: 'Անձնական տվյալներ',
                amount: 'ԸՆԴՀԱՆՈՒՐ ԳՈՒՄԱՐԸ',
                amd: 'դրամ',
                continue: 'Շարունակել',
            },
        };

        return menuTexts[locale][menuKey] || menuTexts.en[menuKey];
    };

    const handleAttendeesChange = (event) => {
        const newAttendees = parseInt(event.target.value);
        setAttendees(newAttendees);
    };

    useEffect(() => {
        const newTotalAmount = attendees * toursData.price;
        setTotalAmount(newTotalAmount);
        localStorage.setItem('attendees', attendees);
        localStorage.setItem('totalAmount', newTotalAmount);
    }, [attendees, toursData]);

    useEffect(() => {
        if (toursData?.dateOfTour) {
            const tourDateParts = toursData.dateOfTour.split('-').map(Number);
            setTourDate(new Date(tourDateParts[0], tourDateParts[1] - 1, tourDateParts[2]));
        }
    }, [toursData]);

    const isTourDay = (date) => {
        return (
            date.getDate() === tourDate.getDate() &&
            date.getMonth() === tourDate.getMonth() &&
            date.getFullYear() === tourDate.getFullYear()
        );
    };

    const tileClassName = ({date, view}) => {
        if (view === 'month' && isTourDay(date)) {
            return 'tour-day';
        }
        const formattedDate = date.toISOString().split('T')[0];
        if (markedDatesForMonth.includes(formattedDate)) {
            return 'marked-date';
        }
        return '';
    };

    const getLocale = () => {
        return language === 'hy' ? 'hy' : 'en-US';
    };

    const handleContinueClick = () => {
        localStorage.setItem("tourId",tourId);
        window.location.href = `/tours/booking/personal/${tourId}`;
    };

    useEffect(() => {
        const formatDate = (date) => {
            const year = date.getFullYear();
            const month = String(date.getMonth() + 1).padStart(2, '0');
            const day = String(date.getDate()).padStart(2, '0');
            return `${year}-${month}-${day}`;
        };

        if (!toursData.dateOfTour) return;

        const storedDate = localStorage.getItem('dayOfTour');
        const parsedDate = new Date(storedDate);
        const year = parsedDate.getFullYear();
        const month = parsedDate.getMonth();
        const lastDayOfMonth = new Date(year, month + 1, 0);
        const startDate = new Date(year, month, 1);
        const endDate = lastDayOfMonth;

        const fetchToursForMonth = async () => {
            const API_URL =  `api/tours/all/local/dates?startDate=${formatDate(
                startDate
            )}&endDate=${formatDate(endDate)}`;
            try {
                const response = await fetch(API_URL, {
                    method: 'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept-Language': language || 'en',
                    },
                });
                const responseData = await response.json();

                if (responseData && responseData.data) {
                    setMonthData(responseData.data);
                } else {
                    console.error('No tour data received for April');
                }
            } catch (error) {
                console.error('Error fetching data for April:', error);
            }
        };

        setTimeout(fetchToursForMonth, 1000);
    }, [toursData.dateOfTour, language]);

    useEffect(() => {
        const storedMarkedDates = localStorage.getItem('markedDatesForMonth');
        if (storedMarkedDates) {
            setMarkedDatesForMonth(JSON.parse(storedMarkedDates));
        }
    }, []);

    const markedDatesObject = {};
    markedDatesForMonth.forEach((date) => {
        markedDatesObject[date] = {color: 'red'};
    });

    const handleTileClick = async (id) => {
        try {
            tourId = id;
            await fetchData();
        } catch (error) {
            console.error('Error handling tile click:', error);
        }
    };

    useEffect(() => {
        const storedLanguage = localStorage.getItem('tourLanguage');
        if (!storedLanguage) {
            localStorage.setItem('tourLanguage', 'hy');
        } else {
            setTourLanguage(storedLanguage);
        }
    }, []);

    const handleLanguageSelectChange = (event) => {
        const selectedLanguage = event.target.value;
        localStorage.setItem('tourLanguage', selectedLanguage);
        setTourLanguage(selectedLanguage);
    };

    useEffect(() => {
        const storedTourLanguage = localStorage.getItem('tourLanguage');
        if (storedTourLanguage) {
            setTourLanguage(storedTourLanguage);
        }
    }, []);

    const {width} = useWindowDimensions();

    return (
        <div className='booking-container'>
            <div className='header'>
                <h2>
                    {getMenuText('headerHeading')}
                </h2>
                <div className='header-text'>
                    {getMenuText('headerText')}
                </div>
            </div>
            <div className='packages'>
                {/* <div>
                    <svg width={width - 208} height='12' viewBox='0 0 1316 12' fill='none'
                         xmlns='http://www.w3.org/2000/svg'>
                        <path
                            d='M0.226497 6L6 11.7735L11.7735 6L6 0.226497L0.226497 6ZM1315.77 6L1310 0.226497L1304.23 6L1310 11.7735L1315.77 6ZM6 7H1310V5H6V7Z'
                            fill='#852D23'
                            fill-opacity='0.25'
                        />
                    </svg>
                </div> */}
                <div className='packages-section'>

                    <div className="left-grape"><img src={left_grape} alt="left_grape" /></div>
                    <div className='pack'>
                        <div className='standard-packages'>{getMenuText('tourBooking')}</div>
                        <div className='standard-packages-content'>{getMenuText('info')}</div>

                        <div className='frame-206'>
                            <div className='frame-205'>
                                <div className='step'>
                                    <div className='row'>
                                        <div className='col-1'></div>
                                        <div className='col-2'>
                                            <div className='col-22'>
                                                <div className='indicator'>
                                                    <div className='item'>
                                                        <svg
                                                            className='current'
                                                            width='25'
                                                            height='25'
                                                            viewBox='0 0 25 25'
                                                            fill='none'
                                                            xmlns='http://www.w3.org/2000/svg'
                                                        >
                                                            <path
                                                                fill-rule='evenodd'
                                                                clip-rule='evenodd'
                                                                d='M12.6494 24.0859C19.2768 24.0859 24.6494 18.7134 24.6494 12.0859C24.6494 5.45852 19.2768 0.0859375 12.6494 0.0859375C6.022 0.0859375 0.649414 5.45852 0.649414 12.0859C0.649414 18.7134 6.022 24.0859 12.6494 24.0859Z'
                                                                fill='#852D23'
                                                            />
                                                            <path
                                                                fill-rule='evenodd'
                                                                clip-rule='evenodd'
                                                                d='M12.6494 18.0859C15.9631 18.0859 18.6494 15.3996 18.6494 12.0859C18.6494 8.77223 15.9631 6.08594 12.6494 6.08594C9.33571 6.08594 6.64941 8.77223 6.64941 12.0859C6.64941 15.3996 9.33571 18.0859 12.6494 18.0859Z'
                                                                fill='white'
                                                            />
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='col-3'>
                                            <div className='progress-line'>
                                                <div className='rectangle-3'></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='placeholder'>
                                        <div className='placeholder2'>{getMenuText('detail')}</div>
                                    </div>
                                </div>
                                <div className='step'>
                                    <div className='row2'>
                                        <div className='col-12'>
                                            <div className='progress-line'>
                                                <div className='rectangle-3'></div>
                                            </div>
                                        </div>
                                        <div className='col-22'>
                                            <div className='indicator'>
                                                <div className='item'>
                                                    <svg
                                                        className='current2'
                                                        width='25'
                                                        height='25'
                                                        viewBox='0 0 25 25'
                                                        fill='none'
                                                        xmlns='http://www.w3.org/2000/svg'
                                                    >
                                                        <path
                                                            fill-rule='evenodd'
                                                            clip-rule='evenodd'
                                                            d='M12.6494 24.0859C19.2768 24.0859 24.6494 18.7134 24.6494 12.0859C24.6494 5.45852 19.2768 0.0859375 12.6494 0.0859375C6.022 0.0859375 0.649414 5.45852 0.649414 12.0859C0.649414 18.7134 6.022 24.0859 12.6494 24.0859Z'
                                                            fill='white'
                                                            stroke='#852D23'
                                                            stroke-opacity='0.25'
                                                        />
                                                        <path
                                                            fill-rule='evenodd'
                                                            clip-rule='evenodd'
                                                            d='M12.6494 18.0859C15.9631 18.0859 18.6494 15.3996 18.6494 12.0859C18.6494 8.77223 15.9631 6.08594 12.6494 6.08594C9.33571 6.08594 6.64941 8.77223 6.64941 12.0859C6.64941 15.3996 9.33571 18.0859 12.6494 18.0859Z'
                                                            fill='#852D23'
                                                            fill-opacity='0.25'
                                                        />
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='col-3'></div>
                                    </div>
                                    <div className='placeholder'>
                                        <div className='placeholder2'>{getMenuText('pdata')}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="right-grape"><img src={right_grape} alt="right_grape" /></div>



                </div>
            </div>
            <div className='booking-calendar-filed'>
                <div className='react-calendar-selected'>
                    <div>
                        <Calendar
                            onMouseOver={(event) => handleTileMouseOver(date, event)}
                            onMouseOut={(event) => handleTileMouseOut(event)}
                            onChange={onChange}
                            value={tourDate}
                            tileClassName={tileClassName}
                            tileContent={({date, view}) => {
                                if (view === 'month') {
                                    const formattedDate = new Date(date.getTime() - date.getTimezoneOffset() * 60000)
                                        .toISOString()
                                        .split('T')[0];
                                    const matchingDate = monthData.find((tour) => tour.dateOfTour === formattedDate);

                                    if (matchingDate) {
                                        return (
                                            <div
                                                className='marked-date-indicator'
                                                onMouseOver={(event) => handleTileMouseOver(date, matchingDate, event)}
                                                onMouseOut={(event) => handleTileMouseOut(event)}
                                                onClick={() => handleTileClick(matchingDate.id)}
                                            />
                                        );
                                    }
                                }
                                return null;
                            }}
                            className='custom-calendar'
                            locale={getLocale()}
                        />
                        {popupVisible && <PopupComponent data={popupDate} language={language}/>}
                    </div>
                </div>
                <div className='booking-form'>
                    <div className='frame-213'>
                        <div className='frame-176'>
                            <div className='_26th-of-january-2023'>{dateFormat}</div>
                        </div>
                        <div className='frame-204'>
                            <div className='time'>{toursData.tourHour}</div>
                        </div>
                        <div className='w-100'>
                            <div className='dropdown'>
                                <select id='attendingNumber' onChange={handleAttendeesChange} value={attendees}>
                                    {[...Array(30)].map((_, index) => (
                                        <option key={index + 1} value={index + 1}>
                                            {index + 1}
                                        </option>
                                    ))}
                                </select>
                                <svg
                                    className='down-arrow'
                                    width='24'
                                    height='24'
                                    viewBox='0 0 24 24'
                                    fill='none'
                                    xmlns='http://www.w3.org/2000/svg'
                                >
                                    <path
                                        d='M12.0711 13.3139L17.0211 8.36388C17.1134 8.26837 17.2237 8.19219 17.3457 8.13978C17.4677 8.08737 17.599 8.05979 17.7317 8.05863C17.8645 8.05748 17.9962 8.08278 18.1191 8.13306C18.242 8.18334 18.3536 8.25759 18.4475 8.35149C18.5414 8.44538 18.6157 8.55703 18.666 8.67993C18.7162 8.80282 18.7415 8.9345 18.7404 9.06728C18.7392 9.20006 18.7117 9.33128 18.6592 9.45329C18.6068 9.57529 18.5307 9.68564 18.4351 9.77788L12.7781 15.4349C12.5906 15.6224 12.3363 15.7277 12.0711 15.7277C11.806 15.7277 11.5517 15.6224 11.3641 15.4349L5.70714 9.77788C5.61163 9.68564 5.53545 9.57529 5.48304 9.45329C5.43063 9.33128 5.40305 9.20006 5.40189 9.06728C5.40074 8.9345 5.42604 8.80282 5.47632 8.67993C5.5266 8.55703 5.60086 8.44538 5.69475 8.35149C5.78864 8.25759 5.90029 8.18334 6.02319 8.13306C6.14609 8.08278 6.27777 8.05748 6.41054 8.05863C6.54332 8.05979 6.67454 8.08737 6.79655 8.13978C6.91855 8.19219 7.0289 8.26837 7.12114 8.36388L12.0711 13.3139Z'
                                        fill='#291818'
                                    />
                                </svg>
                            </div>
                        </div>
                        <div className='w-100'>
                            <div className='dropdown' onChange={handleLanguageSelectChange} value={tourLanguage}>
                                <select id='languageSelect'>
                                    <option value='hy'>Հայերեն</option>
                                    <option value='en'>English</option>
                                    <option value='ru'>Русский</option>
                                </select>
                                <svg
                                    className='down-arrow'
                                    width='24'
                                    height='24'
                                    viewBox='0 0 24 24'
                                    fill='none'
                                    xmlns='http://www.w3.org/2000/svg'
                                >
                                    <path
                                        d='M12.0711 13.3139L17.0211 8.36388C17.1134 8.26837 17.2237 8.19219 17.3457 8.13978C17.4677 8.08737 17.599 8.05979 17.7317 8.05863C17.8645 8.05748 17.9962 8.08278 18.1191 8.13306C18.242 8.18334 18.3536 8.25759 18.4475 8.35149C18.5414 8.44538 18.6157 8.55703 18.666 8.67993C18.7162 8.80282 18.7415 8.9345 18.7404 9.06728C18.7392 9.20006 18.7117 9.33128 18.6592 9.45329C18.6068 9.57529 18.5307 9.68564 18.4351 9.77788L12.7781 15.4349C12.5906 15.6224 12.3363 15.7277 12.0711 15.7277C11.806 15.7277 11.5517 15.6224 11.3641 15.4349L5.70714 9.77788C5.61163 9.68564 5.53545 9.57529 5.48304 9.45329C5.43063 9.33128 5.40305 9.20006 5.40189 9.06728C5.40074 8.9345 5.42604 8.80282 5.47632 8.67993C5.5266 8.55703 5.60086 8.44538 5.69475 8.35149C5.78864 8.25759 5.90029 8.18334 6.02319 8.13306C6.14609 8.08278 6.27777 8.05748 6.41054 8.05863C6.54332 8.05979 6.67454 8.08737 6.79655 8.13978C6.91855 8.19219 7.0289 8.26837 7.12114 8.36388L12.0711 13.3139Z'
                                        fill='#291818'
                                    />
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div className='frame-2162'>

                        <div className='total-amount'>{getMenuText('amount')}</div>
                        <div className='frame-207'>
                            <div className='_0-amd'>
                                <Counter endValue={parseInt(totalAmount)}/> {getMenuText('amd')}
                            </div>
                        </div>

                        <div className='buttons-group'>
                            <div className='button'>
                                <div className='text-container' onClick={handleContinueClick}>
                                    <div className='button-text'>{getMenuText('continue')}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default BookingComponent;

