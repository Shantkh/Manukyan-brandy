import React, { useEffect, useState } from 'react';
import Slider from 'react-slick';
import styles from './awards.module.scss';
import Award from '../module/Award';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';


const Awards = () => {
  const [awardsData, setAwardsData] = useState([]);
  const lengthOfData = awardsData ? awardsData.length : 0;

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch( `api/partners/all`);
        const data = await response.json();
        setAwardsData(data.data);
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []);

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: Math.ceil(lengthOfData / 2),
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
  };

  return (
    <div className={styles.awardsSection}>
      <div className={styles.awardsContainer}>
        {Array.isArray(awardsData) && awardsData.length > 0 && (
          <Slider {...settings}>
            {awardsData.map((award) => (
              <div key={award.id} className={styles.awardItem}>
                <Award award={award} />
              </div>
            ))}
          </Slider>
        )}
      </div>
    </div>
  );
};

export default Awards;
