import React from 'react';
import Slider from 'react-slick';
import styles from './sliderContentSection.module.scss';

const SliderContentSection = ({ data, titleText, buttonText, sliderRight , onClick}) => {
  const settings = {
    dots: false,
    infinite: true,
    speed: 2500,
    fade: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 3000,
  };

  return (
      <div className={styles.homeContentSliderSection}>
        <div className={styles.homeContentSliderWrapper}>
          {sliderRight ? (
              <>
                <div className={styles.contentContainerRight}>
                  <div className={styles.contentWrapper}>
                    {data?.localizations && data?.localizations?.length > 0 && (
                        <div>
                          <div className={styles.contentSectionMenuTitle}>{titleText}</div>
                          <div className={styles.contentTitle}>
                            <div dangerouslySetInnerHTML={{ __html: data?.localizations[0]?.htmlContent }} />
                          </div>
                        </div>
                    )}
                    <div className={styles.contentButton} onClick={onClick}>
                      <div className={styles.buttonText} className={onClick}>{buttonText}</div>
                    </div>
                  </div>
                </div>
                {data?.sliderMedia && data?.sliderMedia.length > 0 && (
                    <div className={styles.imageSlider}>
                      <Slider {...settings}>
                        {data?.sliderMedia.map((imageUrl, index) => (
                            <div key={index}>
                              <img className={styles.sliderImage} src={imageUrl} alt={`Slide ${index + 1}`}
                                   style={{ width: '100%', height: '1000px' }}/>
                            </div>
                        ))}
                      </Slider>
                    </div>
                )}
              </>
          ) : (
              <>
                {data?.sliderMedia && data?.sliderMedia.length > 0 && (
                    <div className={styles.imageSlider}>
                      <Slider {...settings}>
                        {data?.sliderMedia.map((imageUrl, index) => (
                            <div key={index}>
                              <img className={styles.sliderImage} src={imageUrl} alt={`Slide ${index + 1}`}
                                   style={{ width: '100%', height: '1000px' }}/>
                            </div>
                        ))}
                      </Slider>
                    </div>
                )}
                <div className={styles.contentContainer}>
                  <div className={styles.contentWrapper}>
                    {data?.localizations && data?.localizations?.length > 0 && (
                        <div>
                          <div className={styles.contentSectionMenuTitle}>{titleText}</div>
                          <div className={styles.contentTitle}>
                            <div dangerouslySetInnerHTML={{ __html: data?.localizations[0]?.htmlContent }} />
                          </div>
                        </div>
                    )}
                    <div className={styles.contentButton} onClick={onClick}>
                      <div className={styles.buttonText} >{buttonText}</div>
                    </div>
                  </div>
                </div>
              </>
          )}
        </div>
      </div>
  );
};

export default SliderContentSection;
