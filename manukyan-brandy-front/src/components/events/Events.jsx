import React, { useEffect, useState } from 'react';
import EventPreview from '../eventPreview/eventsPreview';
import styles from './events.module.scss';
import { format } from 'date-fns';
import EventCalendar from '../eventsCalendar/EvenrtsCalendarView';
import { fetchEvents } from '../service/eventsService';
import LanguageService from "../service/languageService";

const EventsComponent = () => {
  const [eventsView, setEventsView] = useState(true);
  const [dateTimestamp, setDateTimestamp] = useState({ startDate: Math.floor(Date.now()/1000), endDate: Math.floor(Date.now()/1000) + 31622400 });
  const [events, setEvents] = useState([]);
  const currentDate = new Date();
  let [language, setLanguage] = useState(LanguageService.getLanguage(navigator.language || 'en-US'));

  useEffect(() => {
    const fetchData = async () => {
      const data = await fetchEvents(`dates/locale?startDate=${dateTimestamp?.startDate}&endDate=${dateTimestamp?.endDate}`);
      if (data) {
        setEvents(data.data);
      }
    };
    fetchData();

    const languageSwitchHandler = () => {
      fetchData();
    };
    window.addEventListener('languageSwitched', languageSwitchHandler);

    return () => {
      window.removeEventListener('languageSwitched', languageSwitchHandler);
    };
  }, [dateTimestamp]);

  const getMenuText = (menuKey) => {
    const locale = language || 'en';
    const menuTexts = {
      en: {
        upe: 'Upcoming Events',
        pe: 'Past Events',
        oe: 'Our Events',
        list: 'List',
        cal: 'Calendar'
      },
      hy: {
        upe: 'Գալիք իրադարձություններ',
        pe: 'Անցյալ իրադարձություններ',
        oe: 'Մեր իրադարձությունները',
        list: 'Ցուցակ',
        cal: 'Օրացույց'
      },
    };

    return menuTexts[locale][menuKey] || menuTexts.en[menuKey];
  };

  return (
      <>
        <div className={styles.eventHeading}>{getMenuText('pe')}</div>
        <div className={styles.controlsSectionContainer}>
          <div className={styles.controlsButtonsGroup}>
            <div className={styles.pastUpcomingGroup}>
              <div className={styles.past} onClick={() => setDateTimestamp({ startDate: Math.floor(Date.now()/1000) - 31622400, endDate: Math.floor(Date.now()/1000) })}>{getMenuText('pe')}</div>
              <div className={styles.upcoming}  onClick={() => setDateTimestamp({ startDate: Math.floor(Date.now()/1000), endDate: Math.floor(Date.now()/1000) + 31622400 })}>{getMenuText('upe')}</div>
            </div>
            <div className={styles.eventsViewSelector}>
              {eventsView ? (
                  <div className={styles.viewSelectorActive}>
                    <div>{getMenuText('list')}</div>
                    <svg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
                      <path d='M8 6H21' stroke='white' strokeWidth='2' strokeLinecap='round' strokeLinejoin='round' />
                      <path d='M8 12H21' stroke='white' strokeWidth='2' strokeLinecap='round' strokeLinejoin='round' />
                      <path d='M8 18H21' stroke='white' strokeWidth='2' strokeLinecap='round' strokeLinejoin='round' />
                      <path d='M3 6H3.01' stroke='white' strokeWidth='2' strokeLinecap='round' strokeLinejoin='round' />
                      <path d='M3 12H3.01' stroke='white' strokeWidth='2' strokeLinecap='round' strokeLinejoin='round' />
                      <path d='M3 18H3.01' stroke='white' strokeWidth='2' strokeLinecap='round' strokeLinejoin='round' />
                    </svg>
                  </div>
              ) : (
                  <div onClick={() => {
                    setEventsView(true)
                    setDateTimestamp({ startDate: Math.floor(Date.now()/1000), endDate: Math.floor(Date.now()/1000) + 31622400 })
                  }} className={styles.viewSelector}>
                    <div>List</div>
                    <svg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
                      <path
                          d='M8 6H21'
                          stroke='#291818'
                          strokeOpacity='0.5'
                          strokeWidth='2'
                          strokeLinecap='round'
                          strokeLinejoin='round'
                      />
                      <path
                          d='M8 12H21'
                          stroke='#291818'
                          strokeOpacity='0.5'
                          strokeWidth='2'
                          strokeLinecap='round'
                          strokeLinejoin='round'
                      />
                      <path
                          d='M8 18H21'
                          stroke='#291818'
                          strokeOpacity='0.5'
                          strokeWidth='2'
                          strokeLinecap='round'
                          strokeLinejoin='round'
                      />
                      <path
                          d='M3 6H3.01'
                          stroke='#291818'
                          strokeOpacity='0.5'
                          strokeWidth='2'
                          strokeLinecap='round'
                          strokeLinejoin='round'
                      />
                      <path
                          d='M3 12H3.01'
                          stroke='#291818'
                          strokeOpacity='0.5'
                          strokeWidth='2'
                          strokeLinecap='round'
                          strokeLinejoin='round'
                      />
                      <path
                          d='M3 18H3.01'
                          stroke='#291818'
                          strokeOpacity='0.5'
                          strokeWidth='2'
                          strokeLinecap='round'
                          strokeLinejoin='round'
                      />
                    </svg>
                  </div>
              )}
              {eventsView ? (
                  <div onClick={() => setEventsView(false)} className={styles.viewSelector}>
                    <div>{getMenuText('cal')}</div>
                    <svg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
                      <path
                          d='M19 4H5C3.89543 4 3 4.89543 3 6V20C3 21.1046 3.89543 22 5 22H19C20.1046 22 21 21.1046 21 20V6C21 4.89543 20.1046 4 19 4Z'
                          stroke='#291818'
                          strokeOpacity='0.5'
                          strokeWidth='2'
                          strokeLinecap='round'
                          strokeLinejoin='round'
                      />
                      <path
                          d='M16 2V6'
                          stroke='#291818'
                          strokeOpacity='0.5'
                          strokeWidth='2'
                          strokeLinecap='round'
                          strokeLinejoin='round'
                      />
                      <path
                          d='M8 2V6'
                          stroke='#291818'
                          strokeOpacity='0.5'
                          strokeWidth='2'
                          strokeLinecap='round'
                          strokeLinejoin='round'
                      />
                      <path
                          d='M3 10H21'
                          stroke='#291818'
                          strokeOpacity='0.5'
                          strokeWidth='2'
                          strokeLinecap='round'
                          strokeLinejoin='round'
                      />
                    </svg>
                  </div>
              ) : (
                  <div className={styles.viewSelectorActive}>
                    <div>Calendar</div>
                    <svg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
                      <path
                          d='M19 4H5C3.89543 4 3 4.89543 3 6V20C3 21.1046 3.89543 22 5 22H19C20.1046 22 21 21.1046 21 20V6C21 4.89543 20.1046 4 19 4Z'
                          stroke='white'
                          strokeWidth='2'
                          strokeLinecap='round'
                          strokeLinejoin='round'
                      />
                      <path d='M16 2V6' stroke='white' strokeWidth='2' strokeLinecap='round' strokeLinejoin='round' />
                      <path d='M8 2V6' stroke='white' strokeWidth='2' strokeLinecap='round' strokeLinejoin='round' />
                      <path d='M3 10H21' stroke='white' strokeWidth='2' strokeLinecap='round' strokeLinejoin='round' />
                    </svg>
                  </div>
              )}
            </div>
          </div>
          {eventsView && <div className={styles.monthTitle}>{format(currentDate, 'MMMM yyyy')}</div>}
        </div>

        {eventsView ? (
            <>
              <div className={styles.eventsListWrapper}>
                {events.length ? events.map((event) =>{
                      return <EventPreview key={event.id + '_prev'} event={event}/>
                    }
                ) : null}
              </div>
              <div className={styles.pastUpcomingBottomGroup}>
                <div className={styles.past} onClick={() => setDateTimestamp({ startDate: Math.floor(Date.now()/1000) - 31622400, endDate: Math.floor(Date.now()/1000) })}>{getMenuText('pe')}</div>
                <div className={styles.upcoming}  onClick={() => setDateTimestamp({ startDate: Math.floor(Date.now()/1000), endDate: Math.floor(Date.now()/1000) + 31622400 })}>{getMenuText('upe')}</div>
              </div>
            </>
        ) : (
            <EventCalendar />
        )}
      </>
  );
};

export default EventsComponent;
