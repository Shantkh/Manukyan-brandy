import Banner from '../../banner/Banner';

const FactoryBottomBanner = () => {

    return (
        <Banner sectionEnum='FACTORY_BOTTOM_BANNER'/>
    )
}

export default FactoryBottomBanner;