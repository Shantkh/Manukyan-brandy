import React from 'react';
import { fetchBannerContent } from '../../service/bannerService';
import { useEffect, useState } from 'react';
import languageService from '../../service/languageService';
import SliderContentSection from '../../sliderContentSection/SliderContentSection';

const FactoryVineyards = () => {
  const acceptLanguageHeader = languageService.getLanguage(navigator.language || 'en-US');

  const language = localStorage.getItem('language') || acceptLanguageHeader;

  const [factoryVineyards, setFactoryVineyardsContent] = useState([]);

  const getMenuText = (menuKey) => {
    const locale = language || 'en';
    const menuTexts = {
      en: {
        learn: 'Learn More',
        tour: 'Vineyards',
      },
      hy: {
        learn: 'Իմացեք ավելին',
        tour: 'Գինու Այգիներ',
      },
    };

    return menuTexts[locale][menuKey] || menuTexts.en[menuKey];
  };

  useEffect(() => {
    const fetchData = async () => {
      const data = await fetchBannerContent('FACTORY_VINEYARDS');
      if (data) {
        setFactoryVineyardsContent(data);
      }
    };
    fetchData();

    const languageSwitchHandler = () => {
      fetchData();
    };
    window.addEventListener('languageSwitched', languageSwitchHandler);

    return () => {
      window.removeEventListener('languageSwitched', languageSwitchHandler);
    };
  }, []);

  return (
    <SliderContentSection
      data={factoryVineyards.data}
      titleText={getMenuText('tour')}
      buttonText={getMenuText('learn')}
      sliderRight={true}
    />
  );
};

export default FactoryVineyards;
