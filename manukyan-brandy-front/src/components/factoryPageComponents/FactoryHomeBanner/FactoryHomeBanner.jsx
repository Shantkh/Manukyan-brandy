import Banner from "../../banner/Banner";

const FactoryHomeBanner = () => {

    return (
        <Banner sectionEnum='FACTORY_MAIN_BANNER'/>
    )
}

export default FactoryHomeBanner;