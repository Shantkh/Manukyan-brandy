import Banner from '../../banner/Banner';

const FactoryMiddleBanner = () => {

    return (
        <Banner sectionEnum='FACTORY_MIDDLE_BANNER'/>
    )
}

export default FactoryMiddleBanner;