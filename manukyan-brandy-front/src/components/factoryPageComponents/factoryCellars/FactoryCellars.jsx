import React from 'react';
import { fetchBannerContent } from '../../service/bannerService';
import { useEffect, useState } from 'react';
import languageService from '../../service/languageService';
import SliderContentSection from '../../sliderContentSection/SliderContentSection';

const FactoryCellars = () => {
  const acceptLanguageHeader = languageService.getLanguage(navigator.language || 'en-US');

  const language = localStorage.getItem('language') || acceptLanguageHeader;

  const [factoryCellars, setFactoryCellarsContent] = useState([]);

  const getMenuText = (menuKey) => {
    const locale = language || 'en';
    const menuTexts = {
      en: {
        learn: 'Learn More',
        tour: 'Cellars',
      },
      hy: {
        learn: 'Իմացեք ավելին',
        tour: 'Մառաններ',
      },
    };

    return menuTexts[locale][menuKey] || menuTexts.en[menuKey];
  };

  useEffect(() => {
    const fetchData = async () => {
      const data = await fetchBannerContent('FACTORY_CELLARS');
      if (data) {
        setFactoryCellarsContent(data);
      }
    };
    fetchData();

    const languageSwitchHandler = () => {
      fetchData();
    };
    window.addEventListener('languageSwitched', languageSwitchHandler);

    return () => {
      window.removeEventListener('languageSwitched', languageSwitchHandler);
    };
  }, []);

  return (
    <SliderContentSection
      data={factoryCellars.data}
      titleText={getMenuText('tour')}
      buttonText={getMenuText('learn')}
      sliderRight={true}
    />
  );
};

export default FactoryCellars;
