import React from 'react';
import { fetchBannerContent } from '../../service/bannerService';
import { useEffect, useState } from 'react';
import languageService from '../../service/languageService';
import SliderContentSection from '../../sliderContentSection/SliderContentSection';

const FactoryFermentation = () => {
  const acceptLanguageHeader = languageService.getLanguage(navigator.language || 'en-US');

  const language = localStorage.getItem('language') || acceptLanguageHeader;

  const [factoryFermentation, setFactoryFermentationContent] = useState([]);

  const getMenuText = (menuKey) => {
    const locale = language || 'en';
    const menuTexts = {
      en: {
        learn: 'Learn More',
        tour: 'Fermentation',
      },
      hy: {
        learn: 'Իմացեք ավելին',
        tour: 'Ֆերմենտացիա',
      },
    };

    return menuTexts[locale][menuKey] || menuTexts.en[menuKey];
  };

  useEffect(() => {
    const fetchData = async () => {
      const data = await fetchBannerContent('FACTORY_FERMENTATION');
      if (data) {
        setFactoryFermentationContent(data);
      }
    };
    fetchData();

    const languageSwitchHandler = () => {
      fetchData();
    };
    window.addEventListener('languageSwitched', languageSwitchHandler);

    return () => {
      window.removeEventListener('languageSwitched', languageSwitchHandler);
    };
  }, []);

  return (
    <SliderContentSection
      data={factoryFermentation.data}
      titleText={getMenuText('tour')}
      buttonText={getMenuText('learn')}
      sliderRight={false}
    />
  );
};

export default FactoryFermentation;
