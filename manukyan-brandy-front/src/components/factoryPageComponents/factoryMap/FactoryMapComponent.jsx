import '../FactoryMapStyle.css';
import Banner from "../../banner/Banner";

const FactoryMapComponent =() =>{
    return (
        <Banner sectionEnum='FACTORY_SECOND_BANNER'/>
    )
}

export default FactoryMapComponent;