import React, {useEffect, useState} from 'react'
import LanguageService from "../service/languageService";
import './bookingpersonal.css';
import './filed.scss';
import BookingForm from "./BookingForm";

const BookingToursPersonalComponent = () => {
    let tourId = '';
    const [timeOfTheEvent, setTimeOfTheEvent] = useState('');
    const [totalAmount, setTotalAmount] = useState('');
    const [guideLanguage, setGuideLanguage] = useState('');
    const [attendees, setAttendees] = useState('');
    let [language, setLanguage] = useState(LanguageService.getLanguage(navigator.language || 'en-US'));
    const [toursData, setToursData] = useState(new Date());
    let armenianFormattedDate = '';
    let isArmenian = false;

    const fetchData = async () => {
        tourId = localStorage.getItem("tourId");
        const storedLanguageBooking = localStorage.getItem("tourLanguage");
        const storedTotalAmount = localStorage.getItem("totalAmount");
        const storedAttendees = localStorage.getItem("attendees");
        setTotalAmount(storedTotalAmount);
        setAttendees(storedAttendees);
        setGuideLanguage(storedLanguageBooking);


        try {
            const response = await fetch( `api/tours/booking/${tourId}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept-Language': language || 'en'
                },
            });
            const responseData = await response.json();

            if (responseData && responseData.data) {
                setToursData(responseData.data);
                setTimeOfTheEvent(toursData.tourHour);
            } else {
                console.error('No tour data received from the API');
            }
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        fetchData();

    }, [tourId]);

    const getMenuText = (menuKeys) => {
        const supportedLanguages = ['en', 'hy'];
        const locale = supportedLanguages.includes(language) ? language : 'en';
        const menuTexts = {
            en: {
                tourbooking: 'Tour Booking',
                info: 'Fill the form to book a tour to Manukyan Brandy Factory.',
                detail: 'Tour Details',
                pdata: 'Personal Data',
                amount: 'TOTAL AMOUNT',
                amd: 'AMD',
                continue: 'Continue',
                totalamount: 'TOTAL AMOUNT',
                pl: 'PREFERRED LANGUAGE',
                nov: 'NUMBER OF VISITORS',
                time: 'TIME',
            },
            hy: {
                tourbooking: 'Շրջագայությունների ամրագրում',
                info: 'Մանուկյան կոնյակի գործարան էքսկուրսիա պատվիրելու համար լրացրեք ձևաթուղթը։',
                detail: 'Շրջագայության ման.',
                pdata: 'Անձնական տվյալներ',
                amount: 'ԸՆԴՀԱՆՈՒՐ ԳՈՒՄԱՐԸ',
                amd: 'դրամ',
                continue: 'Շարունակել',
                totalamount: 'ԸՆԴՀԱՆՈՒՐ ԳՈՒՄԱՐԸ',
                pl: 'ՆԱԽԸՆՏՐԵԼԻ ԼԵԶՈՒ',
                nov: 'ԱՅՑԵԼՈՑՆԵՐԻ ԹԻՎ',
                time: 'ԺԱՄԱՆԱԿ',
            },
        };

        return menuTexts[locale][menuKeys] || menuTexts.en[menuKeys];
    };

    useEffect(() => {
        fetchData();
        const handleLanguageSwitch = (event) => {
            const newLanguage = event.detail.newLanguage;
            setLanguage(newLanguage);
            fetchData();
        };
        window.addEventListener('languageSwitched', handleLanguageSwitch);

        return () => {
            window.removeEventListener('languageSwitched', handleLanguageSwitch);
        };
    },);

    const formattedDate = new Date(toursData.dateOfTour).toLocaleDateString(language, {
        day: 'numeric',
        month: 'long',
        year: 'numeric',
    });

    if (language === 'hy') {
        armenianFormattedDate = formattedDate.replace(/April/g, 'Ապրիլ')
            .replace(/May/g, 'Մայիս')
            .replace(/June/g, 'Հունիս')
            .replace(/July/g, 'Հուլիս')
            .replace(/August/g, 'Օգոստոս')
            .replace(/September/g, 'Սեպտեմբեր')
            .replace(/October/g, 'Հոկտեմբեր')
            .replace(/November/g, 'Նոյեմբեր')
            .replace(/December/g, 'Դեկտեմբեր')
            .replace(/January/g, 'Հունվար')
            .replace(/February/g, 'Փետրվար')
            .replace(/March/g, 'Մարտ')
            .replace(/st|nd|rd|th/g, '');

        isArmenian = true;
    }

    return (
        <>
            <div className="booking-container">
                <div className="booking-heading">
                    <div className="booking-secondary-headline">{toursData.htmlTitle}</div>
                </div>
                <div className="booking-main-content">
                    {toursData.htmlLongContent}
                </div>
                <div className="packages">
                    {/* <div>
                        <svg
                            width="1316"
                            height="12"
                            viewBox="0 0 1316 12"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path
                                d="M0.226497 6L6 11.7735L11.7735 6L6 0.226497L0.226497 6ZM1315.77 6L1310 0.226497L1304.23 6L1310 11.7735L1315.77 6ZM6 7H1310V5H6V7Z"
                                fill="#852D23"
                                fill-opacity="0.25"
                            />
                        </svg>
                    </div> */}
                    <div>
                        <div className="pack">
                            <div className="standard-packages">{getMenuText('tourbooking')}</div>
                            <div className="standard-packages-content">{getMenuText("info")}</div>
                            <div className="frame-206">
                                <div className="frame-205">
                                    <div className="step">
                                        <div className="row">
                                            <div className="col-1"></div>
                                            <div className="col-2">
                                                <div className="col-22">
                                                    <div className="indicator">
                                                        <div className="item">
                                                            <svg
                                                                className="current"
                                                                width="25"
                                                                height="25"
                                                                viewBox="0 0 25 25"
                                                                fill="none"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                            >
                                                                <path
                                                                    fill-rule="evenodd"
                                                                    clip-rule="evenodd"
                                                                    d="M12.6494 24.0859C19.2768 24.0859 24.6494 18.7134 24.6494 12.0859C24.6494 5.45852 19.2768 0.0859375 12.6494 0.0859375C6.022 0.0859375 0.649414 5.45852 0.649414 12.0859C0.649414 18.7134 6.022 24.0859 12.6494 24.0859Z"
                                                                    fill="#852D23"
                                                                />
                                                            </svg>
                                                            <svg
                                                                className="combined-shape"
                                                                width="14"
                                                                height="15"
                                                                viewBox="0 0 14 15"
                                                                fill="none"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                            >
                                                                <path
                                                                    fill-rule="evenodd"
                                                                    clip-rule="evenodd"
                                                                    d="M13.0761 6.48541C13.3744 6.18712 13.3744 5.7035 13.0761 5.40521L12.4594 4.78852C12.1611 4.49024 11.6775 4.49024 11.3792 4.78852L5.13026 11.0375L2.2774 8.18463C1.97912 7.88634 1.49549 7.88634 1.1972 8.18463L0.580516 8.80132C0.282227 9.09961 0.282227 9.58323 0.580516 9.88152L4.59098 13.892C4.88927 14.1903 5.37289 14.1903 5.67118 13.892L6.28787 13.2753C6.29796 13.2652 6.3077 13.2549 6.31711 13.2444L13.0761 6.48541Z"
                                                                    fill="white"
                                                                />
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-3">
                                                <div className="progress-line">
                                                    <div className="rectangle-3"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="placeholder">
                                            <div className="placeholder2">{getMenuText("detail")}</div>
                                        </div>
                                    </div>
                                    <div className="step">
                                        <div className="row2">
                                            <div className="col-12">
                                                <div className="progress-line">
                                                    <div className="rectangle-3"></div>
                                                </div>
                                            </div>
                                            <div className="col-22">
                                                <div className="indicator">
                                                    <div className="item">
                                                        <svg
                                                            className="current"
                                                            width="25"
                                                            height="25"
                                                            viewBox="0 0 25 25"
                                                            fill="none"
                                                            xmlns="http://www.w3.org/2000/svg"
                                                        >
                                                            <path
                                                                fill-rule="evenodd"
                                                                clip-rule="evenodd"
                                                                d="M12.6494 24.0859C19.2768 24.0859 24.6494 18.7134 24.6494 12.0859C24.6494 5.45852 19.2768 0.0859375 12.6494 0.0859375C6.022 0.0859375 0.649414 5.45852 0.649414 12.0859C0.649414 18.7134 6.022 24.0859 12.6494 24.0859Z"
                                                                fill="#852D23"
                                                            />
                                                            <path
                                                                fill-rule="evenodd"
                                                                clip-rule="evenodd"
                                                                d="M12.6494 18.0859C15.9631 18.0859 18.6494 15.3996 18.6494 12.0859C18.6494 8.77223 15.9631 6.08594 12.6494 6.08594C9.33571 6.08594 6.64941 8.77223 6.64941 12.0859C6.64941 15.3996 9.33571 18.0859 12.6494 18.0859Z"
                                                                fill="white"
                                                            />
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-3"></div>
                                        </div>
                                        <div className="placeholder">
                                            <div className="placeholder2">{getMenuText("pdata")}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className='details-container'>
                            <div className="image-container">
                                {/* <img className="background-image" src={`/images/vector.png`} alt=''/> */}
                                <div className="text-overlay">
                                    <div className="tour-info">
                                        <div className="tour-title">{toursData.htmlTitle}</div>
                                        <img className="tour-small-logo" src={`/images/android-chrome-192x192.png`}/>
                                        <div className="tour-date">{isArmenian ? armenianFormattedDate : formattedDate}</div>
                                        <div className="tour-time">{getMenuText('time')}: <p className="tour-time-value">{toursData.tourHour}</p>
                                        </div>
                                        <div className="visitor-count">{getMenuText('nov')}: <p
                                            className="tour-time-value">{attendees}</p></div>
                                        {/*<div class="visitor-count">{getMenuText('pl')}: <p*/}
                                        {/*    class="tour-time-value">{guideLanguage.charAt(0).toUpperCase() + guideLanguage.slice(1)}</p></div>*/}
                                        <div className="total-amount">{getMenuText('totalamount')}: <p
                                            className="tour-time-value">{totalAmount} {getMenuText('amd')}</p></div>

                                    </div>
                                </div>
                            </div>
                            <BookingForm language={language}/>
                        </div>

                    </div>
                </div>
            </div>
        </>
    )
}

export default BookingToursPersonalComponent;