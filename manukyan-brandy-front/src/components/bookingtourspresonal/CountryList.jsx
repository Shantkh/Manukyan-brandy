import React, {useEffect} from 'react';


const CountryList = ({language}) => {
    const countries = [
        {value: 'Armenia', label: {en: 'Armenia', hy: 'Հայաստան'}},
        {value: 'USA', label: {en: 'United States of America', hy: 'ԱՄՆ'}},
        {value: 'UK', label: {en: 'United Kingdom', hy: 'Մեծ Բրիտանիա'}},
        {value: 'Canada', label: {en: 'Canada', hy: 'Կանադա'}},
        {value: 'Mexico', label: {en: 'Mexico', hy: 'Մեքսիկա'}},
        {value: 'Argentina', label: {en: 'Argentina', hy: 'Արգենտինա'}},
        {value: 'Brazil', label: {en: 'Brazil', hy: 'Բրազիլիա'}},
        {value: 'Egypt', label: {en: 'Egypt', hy: 'Եգիպտոս'}},
        {value: 'Saudi Arabia', label: {en: 'Saudi Arabia', hy: 'Սաուդյան Արաբիա'}},
        {value: 'United Arab Emirates', label: {en: 'United Arab Emirates', hy: 'Ապարակյան Արաբական Էմիրատներ'}},
        {value: 'Russia', label: {en: 'Russia', hy: 'Ռուսաստան'}}
    ];


    useEffect(() => {
        localStorage.setItem("selectedCountry", 'Armenia');
    })

    const handleCountryChange = (e) => {
        const selectedCountry = e.target.value;
        localStorage.setItem("selectedCountry", selectedCountry);
    };

    return (
        <select id="country" className="dropdown-select" onChange={handleCountryChange}>>
            {countries.map(country => (
                <option key={country.value} value={country.value}>
                    {country.label[language]}
                </option>
            ))}
        </select>
    );
};

export default CountryList;