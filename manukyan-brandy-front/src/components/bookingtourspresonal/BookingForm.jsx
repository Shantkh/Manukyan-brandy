import React, {useEffect, useState} from "react";
import CountryList from "./CountryList";
import './bookingpersonal.css';
import PopupSuccess from "./PopupSuccess";

const BookingForm = ({language}) => {
    const [showPopup, setShowPopup] = useState(false);
    const [popupData, setPopupData] = useState(false);
    const [formData, setFormData] = useState({
        tourId: "",
        tourLanguage: "",
        attendees: "",
        totalAmount: "",
        name: "",
        surname: "",
        email: "",
        phone: "",
        ageConfirmation: false,
        country: "",
    });

    const [errors, setErrors] = useState({
        name: {en: "", hy: ""},
        surname: {en: "", hy: ""},
        email: {en: "", hy: ""},
        phone: {en: "", hy: ""},
        ageConfirmation: {en: "", hy: ""},
        country: {en: "", hy: ""},
    });

    useEffect(() => {
        // Update the formData state with data from localStorage
        setFormData(prevData => ({
            ...prevData,
            tourId: localStorage.getItem('tourId'),
            country: localStorage.getItem('selectedCountry'),
            tourLanguage: localStorage.getItem('tourLanguage'),
            attendees: localStorage.getItem("attendees"),
            totalAmount: localStorage.getItem("totalAmount"),
        }))
    }, []);

    const handleChange = (e) => {
        const {name, value, type, checked} = e.target;
        let errorMessage = "";

        if (type === "checkbox") {
            setFormData(prevData => ({...prevData, [name]: checked}));
        } else {
            setFormData(prevData => ({...prevData, [name]: value}));
            if (name === "email") {
                const isValidEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(value);
                errorMessage = isValidEmail ? "" : "Invalid email format";
            }
        }
        setErrors(prevErrors => ({
            ...prevErrors,
            [name]: {...prevErrors[name], [language]: errorMessage},
        }));
    };

    const handleSubmit = async (e) => {
        e.preventDefault(); // Prevent the default form submission behavior

        // Validate the form data
        const formErrors = validateForm(formData);
        setErrors(formErrors);

        // Check if there are any errors in the form
        const hasErrors = Object.values(formErrors).some(errorObj => Object.values(errorObj).some(error => error !== ""));

        // If there are no errors, submit the form data
        if (!hasErrors) {
            try {
                const response = await fetch(`api/tours/book`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(formData) // Send the formData as JSON string in the request body
                });
                if (response.ok) {
                    const responseData = await response.json();
                    setPopupData(responseData.data);
                    setShowPopup(true);
                } else {
                    console.error('Failed to send form data:', response.statusText); // Log an error if the request fails
                }
            } catch (error) {
                console.error('Error while sending form data:', error); // Log an error if there's an exception during the request
            }
        }
    };


    const validateForm = (formData) => {
        const {name, surname, email, phone, ageConfirmation, country} = formData;
        return {
            name: !name ? {en: "Name is required.", hy: "Անունը պարտադիր է։"} : {en: "", hy: ""},
            surname: !surname ? {en: "Surname is required.", hy: "Ազգանունը պարտադիր է"} : {en: "", hy: ""},
            email: !email ? {en: "Email is required.", hy: "էլփոստը պարտադիր է:"} : {en: "", hy: ""},
            phone: !phone ? {en: "Phone is required.", hy: "Հեռախոսը պարտադիր է։"} : {en: "", hy: ""},
            ageConfirmation: !ageConfirmation ? {
                en: "Please confirm that you are above the age of 21.",
                hy: "Խնդրում ենք հաստատել"
            } : {en: "", hy: ""},
            country: !country ? {en: "Please select your country.", hy: "Խնդրում ենք ընտրել ձեր երկիրը:"} : {
                en: "",
                hy: ""
            },
        };
    };

    const getMenuText = (menuKeys) => {
        const menuTexts = {
            en: {
                name: "NAME",
                surname: "SURNAME",
                email: "EMAIL",
                phone: "PHONE",
                confirm21: "  I confirm that I’m above the age of 21.",
                cb: "Confirm Booking",
            },
            hy: {
                name: "ԱՆՈՒՆ",
                surname: "ԱԶԳԱՆՈՒՆ",
                email: "էլ Փոստ",
                phone: "ՀԵՌԱԽՈՍ",
                confirm21: " Ես 21 տարեկանից բարձր եմ",
                cb: "Ամրագրել",
            },
        };

        return menuTexts[language][menuKeys] || menuTexts.en[menuKeys];
    };

    return (
        <form className="booking-confirm-form" onSubmit={handleSubmit}>
            <div className="booking-confirm">
                <div>
                    <div className="form__group field">
                        <input
                            type="text"
                            className="form__field"
                            placeholder={getMenuText("name")}
                            name="name"
                            value={formData.name}
                            onChange={handleChange}
                        />
                        <label htmlFor="name" className="form__label">
                            {getMenuText("name")} *
                        </label>
                        <div>
                            {errors.name && <span className="error-message">{errors.name[language]}</span>}
                        </div>
                    </div>
                    <div className="form__group field">
                        <input
                            type="text"
                            className="form__field"
                            placeholder={getMenuText("surname")}
                            name="surname"
                            value={formData.surname}
                            onChange={handleChange}
                        />
                        <label htmlFor="surname" className="form__label">
                            {getMenuText("surname")} *
                        </label>
                        <div>
                            {errors.surname && <span className="error-message">{errors.surname[language]}</span>}
                        </div>
                    </div>
                    <div className="form__group field">
                        <input
                            type="email"
                            className="form__field"
                            placeholder={getMenuText("email")}
                            name="email"
                            value={formData.email}
                            onChange={handleChange}
                        />
                        <label htmlFor="email" className="form__label">
                            {getMenuText("email")} *
                        </label>
                        <div>
                            {errors.email && <span className="error-message">{errors.email[language]}</span>}
                        </div>
                    </div>
                    <div className="form__group field">
                        <input
                            type="tel"
                            className="form__field"
                            placeholder={getMenuText("phone")}
                            name="phone"
                            value={formData.phone}
                            onChange={handleChange}
                        />
                        <label htmlFor="phone" className="form__label">
                            {getMenuText("phone")} *
                        </label>
                        <div>
                            {errors.phone && <span className="error-message">{errors.phone[language]}</span>}
                        </div>
                    </div>
                    <div className="custom-dropdown language-dropdown">
                        <CountryList language={language}/>
                        {/* <img className="icon-jam-icons" src={`${BASE_URL}images/chevron-down.svg`}
                                 alt="Dropdown Icon"/> */}
                    </div>
                    <div className="div-5">
                        <input
                            type="checkbox"
                            className={`checkbox-normal ${formData.ageConfirmation ? 'checked' : ''}`}
                            id="ageConfirmation"
                            name="ageConfirmation"
                            checked={formData.ageConfirmation}
                            onChange={handleChange}
                        />
                        <label htmlFor="ageConfirmation"
                               className={`${errors.ageConfirmation[language] ? 'checkbox-error-message' : 'checkbox-lable-message'}`}>
                            {getMenuText("confirm21")}
                        </label>
                    </div>
                </div>
                <div className="buttons-group">
                    <button type="submit" className="button">
                        <div className="text-container">
                            <div className="button-text">{getMenuText("cb")}</div>
                        </div>
                    </button>
                </div>
            </div>
            {popupData && (
                <PopupSuccess data={popupData}/>
            )}
        </form>
    );
};


export default BookingForm;
