import React, { useEffect, useState } from 'react';
import './popup.css';
import './bookingpersonal.css';
import LanguageService from "../service/languageService"; // Import the CSS for the popup

const PopupSuccess = ({data }) => {
    const [timeOfTheEvent, setTimeOfTheEvent] = useState('');
    let [language, setLanguage] = useState(LanguageService.getLanguage(navigator.language || 'en-US'));
    let armenianFormattedDate = '';
    let isArmenian = false;

    const getMenuText = (menuKeys) => {
        const supportedLanguages = ['en', 'hy'];
        const locale = supportedLanguages.includes(language) ? language : 'en';
        const menuTexts = {
            en: {
                tourbooking: 'Tour Booking',
                info: 'Fill the form to book a tour to Manukyan Brandy Factory.',
                detail: 'Tour Details',
                pdata: 'Personal Data',
                amount: 'TOTAL AMOUNT',
                amd: 'AMD',
                continue: 'Continue',
                totalamount: 'TOTAL AMOUNT',
                pl: 'PREFERRED LANGUAGE',
                nov: 'NUMBER OF VISITORS',
                time: 'TIME',
                fullname : 'FULL NAME',
                cong : 'Congratulations',
                yhbyt : 'You have booked your tour.',
                dihsbstyea : ' Detailed information has been sent to your e-mail address.',
            },
            hy: {
                tourbooking: 'Շրջագայությունների ամրագրում',
                info: 'Մանուկյան կոնյակի գործարան էքսկուրսիա պատվիրելու համար լրացրեք ձևաթուղթը։',
                detail: 'Շրջագայության ման.',
                pdata: 'Անձնական տվյալներ',
                amount: 'ԸՆԴՀԱՆՈՒՐ ԳՈՒՄԱՐԸ',
                amd: 'դրամ',
                continue: 'Շարունակել',
                totalamount: 'ԸՆԴՀԱՆՈՒՐ ԳՈՒՄԱՐԸ',
                pl: 'ՆԱԽԸՆՏՐԵԼԻ ԼԵԶՈՒ',
                nov: 'ԱՅՑԵԼՈՑՆԵՐԻ ԹԻՎ',
                time: 'ԺԱՄԱՆԱԿ',
                fullname : 'ԱՄԲՈՂՋ ԱՆՈՒՆԸ',
                cong : 'Շնորհավոր',
                yhbyt : 'Դուք ամրագրել եք ձեր շրջագայությունը:',
                dihsbstyea : ' Մանրամասն տեղեկություններն ուղարկվել են Ձեր էլեկտրոնային փոստի հասցեին:',
            },
        };

        return menuTexts[locale][menuKeys] || menuTexts.en[menuKeys];
    };

    const fetchData = async () => {
        const storedTurDate = localStorage.getItem("dayOfTour");
        setTimeOfTheEvent(storedTurDate);
    };

    useEffect(() => {
        fetchData();
        document.body.classList.add('popup-open');
        return () => {
            document.body.classList.remove('popup-open');
        }
    }, []);

    const formattedDate = new Date(timeOfTheEvent).toLocaleDateString(language, {
        day: 'numeric',
        month: 'long',
        year: 'numeric',
    });
    if (language === 'hy') {
        armenianFormattedDate = formattedDate.replace(/April/g, 'Ապրիլ')
            .replace(/May/g, 'Մայիս')
            .replace(/June/g, 'Հունիս')
            .replace(/July/g, 'Հուլիս')
            .replace(/August/g, 'Օգոստոս')
            .replace(/September/g, 'Սեպտեմբեր')
            .replace(/October/g, 'Հոկտեմբեր')
            .replace(/November/g, 'Նոյեմբեր')
            .replace(/December/g, 'Դեկտեմբեր')
            .replace(/January/g, 'Հունվար')
            .replace(/February/g, 'Փետրվար')
            .replace(/March/g, 'Մարտ')
            .replace(/st|nd|rd|th/g, '');

        isArmenian = true;
    }

    const handleContinueButtonClick = () => {
        window.location.href = '/';
    };

    return (
        <div className="content-success-popup">
            <div className="content-popup-background">

                <div className="popup-content-header">
                    <div className="close-button" onClick={handleContinueButtonClick}>
                        <svg className="x-square" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <circle cx="12" cy="12" r="11" stroke="currentColor" strokeWidth="2"/>
                            <path d="M16 8L8 16M8 8L16 16" stroke="currentColor" strokeWidth="2"/>
                        </svg>
                    </div>
                    <div className="congratulations">{getMenuText('cong')}!</div>
                    <div className="congratulations-content">
                        {getMenuText('yhbyf')}
                        <br/>
                        {getMenuText('dihsbstyea')}
                        <br/><br/><br/>
                        {/* <img className="background-image-success" src="/images/vector.png" alt=''/> */}
                        <div className="text-overlay pop-up-text-overlay">
                            <div className="tour-info-tour">
                                <img className="tour-small-logo-tour" src="/images/android-chrome-512x512.png"/>
                                <div className="tour-date-tour">{isArmenian ? armenianFormattedDate : formattedDate}</div>
                                <div className="tour-time-tour">{getMenuText('fullname')} <p
                                    className="tour-time-value-tour">{data.name + " " + data.surname}</p>
                                </div>
                                <div className="visitor-count-tour">{getMenuText('nov')} <p
                                    className="tour-time-value-tour">{data.attendees}</p></div>
                                <div className="total-amount-tour">{getMenuText('totalamount')} <p
                                    className="tour-time-value-tour">{data.totalAmount} {getMenuText('amd')}</p></div>

                                <div className="buttons-group-tour" onClick={handleContinueButtonClick}>
                                    <div className="button-tour">
                                        <div className="text-container-tour">
                                            <div className="button-text-tour">{getMenuText('continue')}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    );
};

export default PopupSuccess;
