import '../CellarUperSection.css';
import LanguageService from "../../service/languageService";
import React, {useEffect, useState} from 'react';



const CellarUpperSectionComponent = () => {

    const [cellars, setCellars] = useState([]);
    const [cellarsTitle, setCellarsTitles] = useState([]);
    const [cellarsLower, setCellarsLower] = useState([]);
    const [cellarsUpper, setCellarsUpper] = useState([]);
    let [language, setLanguage] = useState(LanguageService.getLanguage(navigator.language || 'en-US'));
    useEffect(() => {
        fetchData();
        const handleLanguageSwitch = (event) => {
            const newLanguage = event.detail.newLanguage;
            setLanguage(newLanguage);
            language = newLanguage;
        };
        window.addEventListener('languageSwitched', handleLanguageSwitch);

        return () => {
            window.removeEventListener('languageSwitched', handleLanguageSwitch);
        };
    },[language]);

    const fetchData = async () => {

        try {
            const response = await fetch( `api/cellars`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept-Language': language || 'en'
                }
            });
            const data = await response.json();
            setCellars(data.data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        if (cellars) {
            const titleSectionData = cellars.find(item => item.cellarSection.toLowerCase() === 'title');
            if (titleSectionData) {
                setCellarsTitles(titleSectionData);
            }

            const lowerSectionData = cellars.find(item => item.cellarSection.toLowerCase() === 'lower section');
            if (lowerSectionData) {
                setCellarsLower(lowerSectionData);
            }

            const higherSectionData = cellars.find(item => item.cellarSection.toLowerCase() === 'upper section');
            if (higherSectionData) {
                setCellarsUpper(higherSectionData);
            }
        }
    }, [cellars]);


    return (
        <div className="main-page">
            <div className="title">
                <div className="main-title-section">
                    {cellarsTitle && <div
                        dangerouslySetInnerHTML={{__html: language === 'hy' ? cellarsTitle.armenianContent : cellarsTitle.englishContent}}/>}
                </div>
                <div className="title-end-line">
                    <img src={`images/line1.png`} alt=""/>
                </div>
                <div className="main-content">
                    <div className="main-content-image">
                        {cellarsUpper && cellarsUpper.images && cellarsUpper.images.length > 0 &&
                            <img className="main-content-image" src={cellarsUpper.images[0]} alt=""/>
                        }
                        {cellarsUpper && cellarsUpper.images && cellarsUpper.images.length > 0 &&
                            <img className="main-content-image-over" src={cellarsUpper.images[1]}
                                 alt=""/>
                        }
                    </div>
                    <div className="main-content-text">
                        {cellarsUpper &&
                            <div className="info-title"
                                 dangerouslySetInnerHTML={{__html: language === 'hy' ? cellarsUpper.armenianContent : cellarsUpper.englishContent}}/>
                        }
                    </div>
                    <div className="info-bottom">
                        <div className="image">
                            <img className="symbol-image" src={`images/symbol.png`}
                                 alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CellarUpperSectionComponent;