import Banner from "../../banner/Banner";

const CellarsMainBanner = () => {

    return (
        <Banner sectionEnum='CELLAR_MAIN_BANNER'/>
    )
}

export default CellarsMainBanner;