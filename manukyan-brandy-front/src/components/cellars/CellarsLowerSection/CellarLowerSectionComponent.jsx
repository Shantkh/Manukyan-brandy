import '../CellarUperSection.css';
import React, {useEffect, useState} from "react";
import LanguageService from "../../service/languageService";



const CellarLowerSectionComponent = () => {
    const [cellars, setCellars] = useState([]);
    const [cellarsTitle, setCellarsTitles] = useState([]);
    const [cellarsLower, setCellarsLower] = useState([]);
    const [cellarsUpper, setCellarsUpper] = useState([]);
    let [language, setLanguage] = useState(LanguageService.getLanguage(navigator.language || 'en-US'));
    useEffect(() => {
        fetchData();
        const handleLanguageSwitch = (event) => {
            const newLanguage = event.detail.newLanguage;
            setLanguage(newLanguage);
            language = newLanguage;
        };
        window.addEventListener('languageSwitched', handleLanguageSwitch);

        return () => {
            window.removeEventListener('languageSwitched', handleLanguageSwitch);
        };
    },[language]);

    const fetchData = async () => {

        try {
            const response = await fetch( `api/cellars`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept-Language': language || 'en'
                }
            });
            const data = await response.json();
            setCellars(data.data);

        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        if (cellars) {
            const titleSectionData = cellars.find(item => item.cellarSection.toLowerCase() === 'title');
            if (titleSectionData) {
                setCellarsTitles(titleSectionData);
            }

            const lowerSectionData = cellars.find(item => item.cellarSection.toLowerCase() === 'lower section');
            if (lowerSectionData) {
                setCellarsLower(lowerSectionData);
            }

            const higherSectionData = cellars.find(item => item.cellarSection.toLowerCase() === 'upper section');
            if (higherSectionData) {
                setCellarsUpper(higherSectionData);
            }
        }
    }, [cellars]);

    return (
        <div className="main-page">
            <div className="info-section">
                <div className="image-section">
                    {cellarsLower && cellarsLower.images && cellarsLower.images.length > 0 &&
                        <img className="main-content-image" src={cellarsLower.images[0]} alt=""/>
                    }
                </div>
                {cellarsLower &&
                    <div className="text-section"
                         dangerouslySetInnerHTML={{__html: language === 'hy' ? cellarsLower.armenianContent : cellarsLower.englishContent}}/>
                }
                <br/> <br/>
            </div>

        </div>

    )
}

export default CellarLowerSectionComponent;