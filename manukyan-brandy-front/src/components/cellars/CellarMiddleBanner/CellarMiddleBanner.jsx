import Banner from "../../banner/Banner";

const CellarMiddleBanner =() =>{
    return (
        <Banner sectionEnum='CELLAR_MIDDLE_BANNER'/>
    )
}

export default CellarMiddleBanner;
