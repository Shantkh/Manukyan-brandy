import Banner from "../../banner/Banner";

const AnamorMiddleBanner = () => {

    return (
        <Banner sectionEnum='ANAMOR_BOTTOM_BANNER'/>
    )
}

export default AnamorMiddleBanner;