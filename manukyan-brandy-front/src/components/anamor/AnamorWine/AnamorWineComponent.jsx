import LanguageService from "../../service/languageService";
import {useEffect, useState} from "react";



const AnamorWineComponent =() =>{
    const [brand, setBrand] = useState();
    let [language, setLanguage] = useState(LanguageService.getLanguage(navigator.language || 'en-US'));
    const [titleSection,setTitleSection] = useState([]);
    const [upperSection,setUpperSection] = useState([]);
    const [lowerSection,setLowerSection] = useState([]);

    useEffect(() => {
        const handleLanguageSwitch = (event) => {
            const newLanguage = event.detail.newLanguage;
            setLanguage(newLanguage);
            language = newLanguage;
        };
        window.addEventListener('languageSwitched', handleLanguageSwitch);

        return () => {
            window.removeEventListener('languageSwitched', handleLanguageSwitch);
        };
    });

    const fetchData = async () => {
        try {
            const response = await fetch( `api/brand/Anamor`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept-Language': language || 'en'
                }
            });
            const data = await response.json();
            setBrand(data.data);

        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        if (brand) {
            const titleSectionData = brand.find(item => item.brandSection.toLowerCase() === 'title');
            if (titleSectionData) {
                setTitleSection(titleSectionData);
            }

            const lowerSectionData = brand.find(item => item.brandSection.toLowerCase() === 'lower section');
            if (lowerSectionData) {
                setLowerSection(lowerSectionData);
            }

            const higherSectionData = brand.find(item => item.brandSection.toLowerCase() === 'upper section');
            if (higherSectionData) {
                setUpperSection(higherSectionData);
            }
        }
    }, [brand]);

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="wine-container">
            <div className="wine-main-container">
                <div className="wine-main-container-title">
                    {titleSection && titleSection.brandName && <div dangerouslySetInnerHTML={{ __html: language === 'hy' ? titleSection.brandName.brandNameArmenian : titleSection.brandName.brandNameEnglish }} />}
                </div>
                <div className="wine-main-container-info">
                    {titleSection && <div dangerouslySetInnerHTML={{ __html: language === 'hy' ? titleSection.armenianContent : titleSection.englishContent }} />}
                </div>
            </div>
            <div className="upper-section">
                <div className="left-image">
                    {upperSection && upperSection.images && upperSection.images.length > 0 &&
                        <img  src={upperSection.images[0]} alt="Upper Section First Image" />
                    }
                </div>
                <div className="upper-section-right">
                    <div className="upper-section-info">
                        {upperSection && <div dangerouslySetInnerHTML={{ __html: language === 'hy' ? upperSection.armenianContent : upperSection.englishContent }} />}
                    </div>
                    <div className="upper-section-second-image">
                        {upperSection && upperSection.images && upperSection.images.length > 1 &&
                            <img src={upperSection.images[1]} alt="Upper Section Second Image" />
                        }
                    </div>
                </div>
            </div>
            <div className="lower-Section">
                <div className="lower-content">
                    <div className="left-lower-content">
                        {lowerSection && <div dangerouslySetInnerHTML={{ __html: language === 'hy' ? lowerSection.armenianContent : lowerSection.englishContent }} />}
                    </div>
                    <div className="right-lower-content">
                        {lowerSection && lowerSection.images &&
                            <img src={lowerSection.images[0]} alt="Upper Section Second Image" />
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AnamorWineComponent;