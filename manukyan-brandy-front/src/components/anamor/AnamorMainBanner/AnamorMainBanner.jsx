import Banner from "../../banner/Banner";

const AnamorMainBanner = () => {

    return (
        <Banner sectionEnum='ANAMOR_MAIN_BANNER'/>
    )
}

export default AnamorMainBanner;