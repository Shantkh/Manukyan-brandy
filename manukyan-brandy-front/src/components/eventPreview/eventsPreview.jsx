import React, {useEffect, useState} from 'react';
import styles from './eventPreview.module.scss';
import useWindowDimensions from '../../hooks/useWindowDimensions';
import { format } from 'date-fns';
import { Link } from 'react-router-dom';
import LanguageService from "../service/languageService";

const EventsPreview = ({event}) => {
  let [language, setLanguage] = useState(LanguageService.getLanguage(navigator.language || 'en-US'));

  useEffect(() => {
    const handleLanguageSwitch = (event) => {
      const newLanguage = event.detail.newLanguage;
      setLanguage(newLanguage);
      language = newLanguage;
    };
    window.addEventListener('languageSwitched', handleLanguageSwitch);

    return () => {
      window.removeEventListener('languageSwitched', handleLanguageSwitch);
    };
  }, []);

  const { width } = useWindowDimensions();

  const getMenuText = (menuKey) => {
    const locale = language || 'en';
    const menuTexts = {
      en: {
        learn: 'Learn More',
      },
      hy: {
        learn: 'Իմացեք ավելին',
      },
    };

    return menuTexts[locale][menuKey] || menuTexts.en[menuKey];
  };

  return (
      <div className={styles.eventPreviewContainer}>
        {/* <svg width={width - 208} viewBox='0 0 1316 12' fill='none' xmlns='http://www.w3.org/2000/svg'>
        <path
          d='M0.226497 6L6 11.7735L11.7735 6L6 0.226497L0.226497 6ZM1315.77 6L1310 0.226497L1304.23 6L1310 11.7735L1315.77 6ZM6 7H1310V5H6V7Z'
          fill='#852D23'
          fillOpacity='0.25'
        />
      </svg> */}
        <div className={styles.wrapper}>
          <div className={styles.contentWrapper}>
            <div className={styles.contentContainer}>
              <div className={styles.titleDateContainer}>
                <div className={styles.title} dangerouslySetInnerHTML={{ __html: event?.localizations[0]?.htmlTitle }} />
                <div className={styles.dateAndTimeContainer}>
                  <div className={styles.date}>{format(event.eventStartDate, 'MMMM d, yyyy')}</div>
                  <div className={styles.time}>{format(event.eventStartDate, 'h:mm a')}</div>
                </div>
              </div>
              <div className={styles.description} dangerouslySetInnerHTML={{ __html: event?.localizations[0]?.htmlShortContent }} />
              <div className={styles.contentButton}>
                <Link to={`/events/${event.id}`}>
                  <div className={styles.buttonText}>{getMenuText('learn')}</div>
                </Link>
              </div>
            </div>
            <img src={event?.sliderMedia[0]} alt={event?.sliderMedia[0]} className={styles.image} />
          </div>
        </div>
      </div>
  );
};

export default EventsPreview;
