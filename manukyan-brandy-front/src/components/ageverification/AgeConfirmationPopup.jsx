import React, {useState, useEffect} from 'react';
import './popupstyle.scss';

const AgeConfirmationPopup = ({onConfirm, onReject}) => {
    const [isOver18, setIsOver18] = useState(false);
    const [showPopup, setShowPopup] = useState(false);
    const [selectedYear, setSelectedYear] = useState('');
    const currentYear = new Date().getFullYear();
    const years = Array.from({ length: 101 }, (_, index) => currentYear - index);

    useEffect(() => {
        const storedAge = localStorage.getItem('userAge');
        if (storedAge === null) {
            setShowPopup(true); // No age data in local storage, show the popup
        } else {
            const age = parseInt(storedAge);
            if (age >= 18) {
                setIsOver18(true); // User is over 18, set flag accordingly
            } else {
                setShowPopup(false); // User is under 18, do not show the popup
            }
        }
    }, []);

    const handleConfirmAge = () => {
        const age = currentYear - parseInt(selectedYear);
        if (age >= 18) {
            localStorage.setItem('userAge', age.toString());
            setIsOver18(true);
            setShowPopup(false);
            onConfirm();
        } else {
            setShowPopup(false);
        }
    };

    return (
        <div>
            {!isOver18 && showPopup && (
                <div className="overlay">
                    <div className="frame-61">
                        <div className="rectangle-13"></div>
                        <div className="frame-28">
                            <div className="welcome-to-manukyan-brandy-factory">
                                Welcome to Manukyan Brandy Factory!
                            </div>
                            <div className="frame-27">
                                <div className="frame-26">
                                    <div className="please-enter-your-year-of-birth">
                                        Please select your year of birth
                                    </div>
                                </div>
                                <div className="frame-25">
                                    <div className="frame-24">
                                        <select
                                            className="_2010"
                                            value={selectedYear}
                                            onChange={(e) => setSelectedYear(e.target.value)}
                                            required
                                        >
                                            <option value="" disabled>Select Year</option>
                                            {years.map((year) => (
                                                <option key={year} value={year}>{year}</option>
                                            ))}
                                        </select>
                                    </div>
                                    <div className="buttons-group">
                                        <div className="content-btn" onClick={handleConfirmAge}>
                                            <div className="content-btn-text">
                                                <div className="button-text">Enter</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
            {!isOver18 && !showPopup && (
                <div className="overlay">
                    <div className="message">
                        Sorry, you are not allowed to view this web page.
                        Ներեցեք, ձեզ չի թույլատրվում դիտել այս վեբ էջը:
                    </div>
                </div>
            )}
        </div>
    );
};

export default AgeConfirmationPopup;