import React, {useEffect, useState} from 'react';
import './style.css';
import LanguageService from '../service/languageService';
import check_circle from "../../assets/check-circle.png";

const ContactUs = () => {
    const [formData, setFormData] = useState({
        name: '',
        recipient: '',
        subject: '',
        phone: '',
        msgBody: '',
        country: '',
        subscribe: false,
    });

    const [errors, setErrors] = useState({
        name: '',
        recipient: '',
        subject: '',
        phone: '',
        msgBody: '',
        country: '',
        subscribe: '',
    });
    const [successPopupVisible, setSuccessPopupVisible] = useState(false);
    const [errorPopupVisible, setErrorPopupVisible] = useState(false);

    const validateField = (fieldName) => {
        let fieldError = '';
        switch (fieldName) {
            case 'recipient':
                const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                if (formData.email && !emailRegex.test(formData.email)) {
                    fieldError = 'Invalid email address';
                }
                break;

            case 'phone':
                const phoneRegex = /^\d{10}$/; // For example, validating a 10-digit phone number
                if (formData.phone && !phoneRegex.test(formData.phone)) {
                    fieldError = 'Invalid phone number';
                }
                break;

            default:
                break;
        }
        setErrors((prevErrors) => ({
            ...prevErrors,
            [fieldName]: fieldError,
        }));
    };

    const validateForm = () => {
        let formIsValid = true;
        const newErrors = { ...errors };

        // Validate other fields
        Object.keys(formData).forEach((fieldName) => {
            if (fieldName !== 'subscribe' && !formData[fieldName]) {
                newErrors[fieldName] = getMenuText('errorMessage');
                formIsValid = false;
            }
        });

        // Validate email separately
        validateField('recipient');

        setErrors(newErrors);
        return formIsValid;
    };

    const handleInputChange = (fieldName, value) => {
        setFormData((prevData) => ({
            ...prevData,
            [fieldName]: value,
            country: selectedCountryCode,
        }));
        validateField(fieldName);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (validateForm()) {
            setErrors({});
            try {
                const response = await fetch(`api/mail/sendemail`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(formData),
                });

                // Check the response status
                if (response.ok) {
                    // Show success popup
                    showSuccessPopup();
                } else {
                    // Show error popup
                    showErrorPopup();
                }
            } catch (error) {
                // Handle network or other errors
                console.error('Error sending data:', error);
                // Show error popup
                showErrorPopup();
            }
        } else {
            console.log('Form has errors. Please fix them before submitting.');
        }
    };

    const showSuccessPopup = () => {
        setSuccessPopupVisible(true);
    };

    const showErrorPopup = () => {
        setErrorPopupVisible(true);
    };

    const closePopups = () => {
        setSuccessPopupVisible(false);
        setErrorPopupVisible(false);
    };

    const SuccessPopup = ({ onClose }) => (
        <div className="popup-frame">
            <div className="thank-you-message">
                <div className="popup-frame-toping">
                    <img className="check-circle" src={check_circle} alt='circle_icon'/>
                    <p className="popup-frame-title">{getMenuText('thankyou')}</p>
                    <p className="popup-frame-p">{getMenuText('thankyouMessage')}</p>
                </div>
                <div className="buttons-group">
                    <button className="button " onClick={onClose}>
                        <div className="text-container">
                            <p className="button-text">Close</p>
                        </div>
                    </button>
                </div>
            </div>
        </div>
    );

    const ErrorPopup = ({ onClose }) => (
        <div className="frame">
            <div className="overlap-group">
                <div className="thank-you-message">
                    <div className="div">
                        <div className="div-2">
                            <div className="div-3">
                                <img className="check-circle" src={`images/others/alert_circle_icon.svg`} alt='alert_circle_icon' />
                                <div className="wishlist">
                                    <p className="text-wrapper">{getMenuText('erroremail')}Something went wrong please try again.</p>
                                </div>
                            </div>
                            <div className="div-3">
                                <p className="p">
                                    {getMenuText('erroremailMessage')}
                                    If the error persists, please contact us with the phone number or email.
                                </p>
                            </div>
                        </div>
                        <div className="buttons-group">
                            <button className="button" onClick={onClose}>
                                <div className="text-container">
                                    <p className="button-text">Close</p>
                                </div>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );


    const [language, setLanguage] = useState(
        LanguageService.getLanguage(navigator.language || 'en-US')
    );

    useEffect(() => {
        const handleLanguageSwitch = (event) => {
            const newLanguage = event.detail.newLanguage;
            setLanguage(newLanguage);
        };
        window.addEventListener('languageSwitched', handleLanguageSwitch);
        return () => {
            window.removeEventListener('languageSwitched', handleLanguageSwitch);
        };
    }, []);

    const getMenuText = (menuKey) => {
        const locale = language || 'en';
        const menuTexts = {
            en: {
                errorMessage: 'Required field.',
                errorMessageEmail: 'Invalid email address',
                errorMessagePhone: 'Invalid phone number',
                maukyan: 'Manukyan Brandy Factory',
                address2: 'Armenia, 10/1 Karmir Banakaineri str.',
                address1: ' 2213, C. Balahovit, Kotayk Marz,',
                name: 'Full name *',
                recipient: 'E-MAIL *',
                subject: 'SUBJECT *',
                phone: 'PHONE NUMBER *',
                selectCountry: 'COUNTRY CODE *',
                msgBody: 'MESSAGE *',
                checkBoxMessage:
                    'I would like to subscribe to the Company’s news\n' +
                    '                                        and\n' +
                    '                                        updates.',
                send: 'Send',
                thankyou: 'Thank you for contacting us!',
                erroremail: 'Something went wrong please try again.',
                thankyouMessage: ' We appreciate your interest. Our team will reach out to you as soon as possible to assist with your inquiry.',
                erroremailMessage: ' If the error persists, please contact us with the phone number or email.',
            },
            hy: {
                errorMessage: 'Պարտադիր դաշտ',
                errorMessageEmail: 'Անվավեր էլ․ հասցե',
                errorMessagePhone: 'Անվավեր հեռախոսահամար',
                maukyan: 'Մանուկյանի կոնյակի գործարան',
                address2: 'Հայաստան, Կարմիր Բանակայերի փող. 10/1',
                address1: ' Կոտայքի մարզ, ք.Բալահովիտ 2213',
                name: 'Ամբողջական անուն *',
                recipient: 'Էլ. ՓՈՍՏ *',
                subject: 'ԹԵՄԱ *',
                phone: 'ՀԵՌԱԽՈՍԱՀԱՄԱՐ *',
                selectCountry: 'Երկրի կոդ *',
                msgBody: 'ՀԱՂՈՐԴԱԳՐՈՒԹՅՈՒՆ *',
                checkBoxMessage:
                    'Ես կցանկանայի բաժանորդագրվել Ընկերության նորություններին\n' +
                    'եւ\n թարմացումներ:',
                send: 'Ուղարկել',
                thankyou: 'Շնորհակալություն մեզ հետ կապվելու համար:',
                erroremail: 'Սխալ առաջացավ, խնդրում ենք նորից փորձեք:',
                thankyouMessage: 'Մենք գնահատում ենք ձեր հետաքրքրությունը: Մեր թիմը կկապվի ձեզ հետ որքան հնարավոր է շուտ՝ ձեր հարցումը օգնելու համար:',
                erroremailMessage: 'Եթե սխալը շարունակվում է, խնդրում ենք կապվել մեզ հետ հեռախոսահամարով կամ էլ.',
            },
        };

        return menuTexts[locale][menuKey] || menuTexts.en[menuKey];
    };

    const [selectedCountryCode, setSelectedCountryCode] = useState('+374');
    const countryCodes = [
        { code: '+374', country: 'ARM', flag: 'armenia.png' },
        { code: '+1', country: 'USA', flag: 'usa.png' },
    ];

    const handleCountryCodeChange = (event) => {
        setSelectedCountryCode(event.target.value);
    };

    const renderCountryOption = (country) => (
        <option key={country.code} value={country.code}>
            <img
                className="flag-icon"
                src={`images/others/flags/${country.flag}`}
                alt={country.country}
                style={{
                    width: '20px',
                    height: '20px',
                    marginRight: '5px',
                    backgroundSize: 'cover',
                    display: 'inline-block',
                }}
            />
            {country.code}
        </option>
    );



    return (
        <div className="contact-section">
            <div className="box main_container">
                <div className="contact-us_title">Contact Us</div>
                <div className="contact-form">
                    <div className="left-side">
                        <div className="frame-3">
                            <div className="text-wrapper">{getMenuText('maukyan')}</div>
                            <div className="menu-item">
                                <p className="p">{getMenuText('address1')}<br/>{getMenuText('address2')}</p>
                            </div>
                            <div className="menu-item">
                                <div className="menu-item-2">info@manukyanbrandy.am</div>
                            </div>
                            <div className="menu-item"><p className="menu-item-2">+374 93 99 99 30</p></div>
                        </div>
                        <div className="social-icons">
                            <a href='/#'>
                                <img className="icon-jam-icons"
                                     src={`images/others/youtube.svg`} alt='youtube'/>
                            </a>
                            <a href='/#'>
                                <img className="icon-jam-icons"
                                     src={`images/others/facebook.svg`} alt='facebook'/>
                            </a>
                            <a href='/#'>
                                <img className="icon-jam-icons"
                                     src={`images/others/twitter.svg`} alt='twitter'/>
                            </a>
                            <a href='/#'>
                                <img className="icon-jam-icons"
                                     src={`images/others/instagram.svg`} alt='instagram'/>
                            </a>
                            <a href='/#'>
                                <img className="img" src={`images/others/linkedin.svg`} alt='linkedin'/>
                            </a>
                        </div>
                    </div>
                    <div className="right-side">
                        <form onSubmit={handleSubmit}>
                            <div className="contact-form-right-side">
                                <div className="form-group w-50">
                                    <input
                                        type="text"
                                        id="name"
                                        name="name"
                                        value={formData.name}
                                        onChange={(e) => handleInputChange('name', e.target.value)}
                                        className='form-group-input'
                                        placeholder={getMenuText('name')}
                                    />
                                    {errors.name && <p className="error-message">{errors.name}</p>}
                                </div>

                                <div className="form-group w-50">

                                    <input
                                        type="text"
                                        id="subject"
                                        name="subject"
                                        value={formData.subject}
                                        onChange={(e) => handleInputChange('subject', e.target.value)}
                                        className='form-group-input'
                                        placeholder={getMenuText('subject')}
                                    />
                                    {errors.subject && <p className="error-message">{errors.subject}</p>}
                                </div>
                                <div className="form-group w-50">
                                    <input
                                        type="email"
                                        id="recipient"
                                        name="recipient"
                                        value={formData.recipient}
                                        onChange={(e) => handleInputChange('recipient', e.target.value)}
                                        onBlur={() => validateField('recipient')}  // Validate email on blur
                                        className='form-group-input'
                                        placeholder={getMenuText('recipient')}
                                    />
                                    {errors.recipient && <p className="error-message">{errors.recipient}</p>}
                                </div>
                                <div className="form-group w-50">
                                    <div className="phone-number-amount">
                                        <div className="country-code">
                                            <div className="input-content">
                                                <div className="frame-6">
                                                    <div className="armenia"></div>
                                                    <div className="input-content-2">
                                                        <label htmlFor="selectCountry" className="text-wrapper-3">
                                                            {/* {getMenuText('selectCountry')} */}
                                                        </label>
                                                        <div className="frame-6">
                                                            <div className="armenia"></div>
                                                            <div className="input-content-3">
                                                                <select
                                                                    id="selectCountry"
                                                                    value={selectedCountryCode}
                                                                    onChange={handleCountryCodeChange}
                                                                    required
                                                                >
                                                                    <option value="" disabled hidden>
                                                                        {getMenuText('selectCountry')}
                                                                    </option>
                                                                    {countryCodes.map(renderCountryOption)}
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="input">
                                            <div className="input-content-wrapper">
                                                <div className="input-content-3">
                                                    <input
                                                        type="number"
                                                        id="phoneNumber"
                                                        name="phoneNumber"
                                                        value={formData.phone}
                                                        onChange={(e) => handleInputChange('phone', e.target.value)}
                                                        className='form-group-input phone-number-input'
                                                        placeholder={getMenuText('phone')}
                                                    />
                                                    {errors.phone && <p className="error-message">{errors.phone}</p>}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group message-part w-100">
                                <textarea
                                    id="msgBody"
                                    name="msgBody"
                                    value={formData.msgBody}
                                    onChange={(e) => handleInputChange('msgBody', e.target.value)}
                                    className='form-group-input'
                                    placeholder={getMenuText('msgBody')}
                                />
                                    {errors.msgBody && <p className="error-message">{errors.msgBody}</p>}
                                </div>
                                <div className="form-group button-part w-100">
                                    <label htmlFor="subscribe" className="text-wrapper-5">
                                        {getMenuText('checkBoxMessage')}
                                        <input
                                            type="checkbox"
                                            id="subscribe"
                                            name="subscribe"
                                            checked={formData.subscribe}
                                            onChange={(e) => handleInputChange('subscribe', e.target.checked)}
                                        />
                                    </label>
                                    <button type="submit" className="buttons-group">
                                        <div className="button">
                                            <div className="text-container">
                                                <p className="button-text">{getMenuText('send')}</p>
                                            </div>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </form>
                        {successPopupVisible && <SuccessPopup onClose={closePopups} />}
                        {errorPopupVisible && <ErrorPopup onClose={closePopups} />}
                    </div>
                    {/* <div className="frame">
                        <div className="div">
                            <div className="frame-2">
                                <div className="text-wrapper">{getMenuText('maukyan')}</div>
                                <div className="frame-3">
                                    <div className="menu-item">
                                        <p className="p">{getMenuText('address1')}<br/>{getMenuText('address2')}</p>
                                    </div>
                                    <div className="menu-item">
                                        <div className="menu-item-2">info@manukyanbrandy.am</div>
                                    </div>
                                    <div className="menu-item"><p className="menu-item-2">+374 93 99 99 30</p></div>
                                </div>
                            </div>
                            <div className="social-icons">
                                <a href='/#'>
                                    <img className="icon-jam-icons"
                                         src={`${BASE_URL}images/others/youtube.svg`} alt='youtube'/>
                                </a>
                                <a href='/#'>
                                    <img className="icon-jam-icons"
                                         src={`${BASE_URL}images/others/facebook.svg`} alt='facebook'/>
                                </a>
                                <a href='/#'>
                                    <img className="icon-jam-icons"
                                         src={`${BASE_URL}images/others/twitter.svg`} alt='twitter'/>
                                </a>
                                <a href='/#'>
                                    <img className="icon-jam-icons"
                                         src={`${BASE_URL}images/others/instagram.svg`} alt='instagram'/>
                                </a>
                                <a href='/#'>
                                    <img className="img" src={`${BASE_URL}images/others/linkedin.svg`} alt='linkedin'/>
                                </a>
                            </div>
                        </div>
                        <div className="frame-4">
                            <div className="frame-3">
                                <div className="frame-5">
                                    <form onSubmit={handleSubmit}>
                                        <div className="frame-3">
                                            <div className="div-wrapper">
                                                <label htmlFor="name" className="text-wrapper-2">
                                                    {getMenuText('name')}*
                                                </label>
                                                <input
                                                    type="text"
                                                    id="name"
                                                    name="name"
                                                    value={formData.name}
                                                    onChange={(e) => handleInputChange('name', e.target.value)}
                                                />
                                                {errors.name && <p className="error-message">{errors.name}</p>}
                                            </div>
                                            <div className="div-wrapper">
                                                <label htmlFor="recipient" className="text-wrapper-2">
                                                    {getMenuText('recipient')}
                                                </label>
                                                <input
                                                    type="email"
                                                    id="recipient"
                                                    name="recipient"
                                                    value={formData.recipient}
                                                    onChange={(e) => handleInputChange('recipient', e.target.value)}
                                                    onBlur={() => validateField('recipient')}  // Validate email on blur
                                                />
                                                {errors.recipient && <p className="error-message">{errors.recipient}</p>}
                                            </div>
                                        </div>
                                        <div className="frame-3">
                                            <div className="div-wrapper">
                                                <label htmlFor="subject" className="text-wrapper-2">
                                                    {getMenuText('subject')}
                                                </label>
                                                <input
                                                    type="text"
                                                    id="subject"
                                                    name="subject"
                                                    value={formData.subject}
                                                    onChange={(e) => handleInputChange('subject', e.target.value)}
                                                />
                                                {errors.subject && <p className="error-message">{errors.subject}</p>}
                                            </div>
                                            <div className="phone-number-amount">
                                                <div className="country-code">
                                                    <div className="input-content">
                                                        <div className="frame-6">
                                                            <div className="armenia"></div>
                                                            <div className="input-content-2">
                                                                <label htmlFor="selectCountry" className="text-wrapper-3">
                                                                    {getMenuText('selectCountry')}
                                                                </label>
                                                                <div className="frame-6">
                                                                    <div className="armenia"></div>
                                                                    <div className="input-content-3">
                                                                        <select
                                                                            id="selectCountry"
                                                                            value={selectedCountryCode}
                                                                            onChange={handleCountryCodeChange}
                                                                            required
                                                                        >
                                                                            <option value="" disabled hidden>
                                                                                {getMenuText('selectCountry')}
                                                                            </option>
                                                                            {countryCodes.map(renderCountryOption)}
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="input">
                                                    <div className="input-content-wrapper">
                                                        <div className="input-content-3">
                                                            <label htmlFor="phoneNumber" className="text-wrapper-4">
                                                                {getMenuText('phone')}
                                                            </label>
                                                            <input
                                                                type="text"
                                                                id="phoneNumber"
                                                                name="phoneNumber"
                                                                value={formData.phone}
                                                                onChange={(e) => handleInputChange('phone', e.target.value)}
                                                            />
                                                            {errors.phone && <p className="error-message">{errors.phone}</p>}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="frame-7">
                                            <label htmlFor="msgBody" className="text-wrapper-2">
                                                {getMenuText('msgBody')}*
                                            </label>
                                            <textarea
                                                id="msgBody"
                                                name="msgBody"
                                                value={formData.msgBody}
                                                onChange={(e) => handleInputChange('msgBody', e.target.value)}
                                            />
                                            {errors.msgBody && <p className="error-message">{errors.msgBody}</p>}
                                        </div>
                                        <div className="frame-9">
                                            <label htmlFor="subscribe" className="text-wrapper-5">
                                                {getMenuText('checkBoxMessage')}
                                                <input
                                                    type="checkbox"
                                                    id="subscribe"
                                                    name="subscribe"
                                                    checked={formData.subscribe}
                                                    onChange={(e) => handleInputChange('subscribe', e.target.checked)}
                                                />
                                            </label>
                                        </div>
                                        <button type="submit" className="buttons-group">
                                            <div className="button">
                                                <div className="text-container">
                                                    <p className="button-text">{getMenuText('send')}</p>
                                                </div>
                                            </div>
                                        </button>
                                    </form>
                                    {successPopupVisible && <SuccessPopup onClose={closePopups} />}
                                    {errorPopupVisible && <ErrorPopup onClose={closePopups} />}
                                </div>
                            </div>
                        </div>
                    </div>
                    <img className="line" src="img/line-2.svg"/>
                    <img className="line-2" src="img/line-4.svg"/>
                    <img className="line-3" src="img/line-3.svg"/> */}

                </div>
            </div>
        </div>

    );
};

export default ContactUs;
