import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
import LanguageService from "../service/languageService";
import styles from './footer.module.scss';



const Footer = () => {

  const [language, setLanguage] = useState(LanguageService.getLanguage(navigator.language || 'en-US'));

  useEffect(() => {
    const handleLanguageSwitch = (event) => {
      const newLanguage = event.detail.newLanguage;
      setLanguage(newLanguage);
    };

    window.addEventListener('languageSwitched', handleLanguageSwitch);

    return () => {
      window.removeEventListener('languageSwitched', handleLanguageSwitch);
    };
  }, []);

  const getMenuText = (menuKey) => {
    const locale = language || 'en';
    const menuTexts = {
      en: {
        factory: 'Factory',
        toursRequest: 'Tours',
        events: 'Events',
        contactus: 'Contact us',
        wines: 'Wines',
        holani: 'Holani',
        anamor: 'Anamor',
        address2: 'Armenia, 10/1 Karmir Banakaineri str.',
        address1: ' 2213, C. Balahovit, Kotayk Marz,',
        copyright: ' ManukyanBrandy@2023. All rights reserved.'
      },
      hy: {
        factory: 'Գործարան',
        toursRequest: 'Շրջագայություններ',
        events: 'Իրադարձություններ',
        contactus: 'Կապվեք մեզ հետ',
        wines: 'Գինիներ',
        holani: 'Հոլանի',
        anamor: 'Անամոր',
        address2: 'Հայաստան, Կարմիր Բանակայերի փող. 10/1',
        address1: ' Կոտայքի մարզ, ք.Բալահովիտ 2213',
        copyright: ' ManukyanBrandy@2023. Բոլոր իրավունքները պաշտպանված են.'
      },
    };

    return menuTexts[locale][menuKey] || menuTexts.en[menuKey];
  };

  return (
      <div className={styles.footer}>
        <div className={styles.footerSection}>
          <div className={styles.footerContentWrapper}>
            <div className={styles.footerContent}>
              <div className={styles.firstCol}>
                <div className={styles.colItem}>
                  <Link to='/factory'>{getMenuText('factory')}</Link>
                </div>
                <div className={styles.colItem}>
                  <Link to='/toursRequest'>{getMenuText('toursRequest')}</Link>
                </div>
                <div className={styles.colItem}>
                  <Link to='/events'>{getMenuText('events')}</Link>
                </div>
                <div className={styles.colItem}>
                  <Link to='/contact'>{getMenuText('contactus')}</Link>
                </div>
              </div>
              <div className={styles.secondCol}>
                <div className={styles.colItem}>
                  <Link to='/wines'>{getMenuText('wines')}</Link>
                </div>
                <div className={styles.colItem}>
                  <Link to='/wines/holani'>{getMenuText('holani')}</Link>
                </div>
                <div className={styles.colItem}>
                  <Link to='/anamor'>{getMenuText('anamor')}</Link>
                </div>
              </div>
              <div className={styles.thirdCol}>
                <div className={styles.thirdColItem}>{getMenuText('address1')}
                  <br />
                  {getMenuText('address2')}

                </div>
                <div className={styles.thirdColItem}>info@manukyanbrandy.am</div>
                <div className={styles.thirdColItem}>+374 93 99 99 30</div>
              </div>
              <div className={styles.forthCol}>
                <a href='/#'>
                  <img className={styles.forthColItem} src="/images/youtube.png" alt='YouTube' />
                </a>
                <a href='/#'>
                  <img className={styles.forthColItem} src="/images/facebook.png" alt='Facebook' />
                </a>
                <a href='/#'>
                  <img className={styles.forthColItem} src="/images/twitter.png" alt='X' />
                </a>
                <a href='/#'>
                  <img className={styles.forthColItem} src="/images/instgram.png" alt='Instagram' />
                </a>
                <a href='/#'>
                  <img className={styles.forthColItem} src="/images/linkedin.png" alt='LinkedIn' />
                </a>
              </div>
            </div>
          </div>
          <div className={styles.footerCopyright}>
            {getMenuText('copyright')}
          </div>
        </div>
      </div>
  );
};

export default Footer;
