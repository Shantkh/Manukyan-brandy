class TourModel {
    constructor(id, createdOn, tourImages, tourLocalization, tourDay, tourMonth, tourHour, dateOfTour, weekNumber, duration, price) {
        this.id = id;
        this.createdOn = createdOn;
        this.tourImages = tourImages;
        this.tourLocalization = tourLocalization;
        this.tourDay = tourDay;
        this.tourMonth = tourMonth;
        this.tourHour = tourHour;
        this.dateOfTour = dateOfTour;
        this.weekNumber = weekNumber;
        this.duration = duration;
        this.price = price;
    }
}

export default TourModel;