import React from "react";
import './awardStyle.css';

const Award = ({award}) => {
    const {logo, partnerNameEn, partnerNameHy} = award;
    return (

        <div className="award-item">
            <img className="award-logo" src={logo} alt={navigator.language === 'hy' ? partnerNameHy : partnerNameEn}/>
        </div>
    );
}

export default Award;