import languageService from "./languageService";

const fetchBannerContent = async (pageName) => {
    try {
        const acceptLanguageHeader = languageService.getLanguage(navigator.language || 'en-US');

        const language = localStorage.getItem('language') || acceptLanguageHeader;

        const response = await fetch(`api/banner/show?page=`+pageName, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept-Language': language,
            },
        });

        const data = await response.json();
        console.log("data in getch", data);
        if (data.success) {
            return data;
        } else {
            console.error('Error fetching banner content:', data.httpStatus);
            return null;
        }
    } catch (error) {
        console.error('Error fetching banner content:', error);
        return null;
    }
};

export { fetchBannerContent };