import languageService from "./languageService";

const fetchSliderContent = async () => {
    try {
        const acceptLanguageHeader = languageService.getLanguage(navigator.language || 'en-US');

        const language = localStorage.getItem('language') || acceptLanguageHeader;

        // const response = await fetch(`${BASE_URL}api/slider/show`, {
        const response = await fetch( `api/slider/show`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept-Language': language,
            },
        });

        const data = await response.json();
        if (data.success) {
            return data;
        } else {
            console.error('Error fetching slider content:', data.httpStatus);
            return null;
        }
    } catch (error) {
        console.error('Error fetching slider content:', error);
        return null;
    }
};

export { fetchSliderContent };