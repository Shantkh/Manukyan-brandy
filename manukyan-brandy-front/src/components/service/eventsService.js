import languageService from "./languageService";

const fetchEvents = async (params) => {
    try {
        const acceptLanguageHeader = languageService.getLanguage(navigator.language || 'en-US');
        const language = localStorage.getItem('language') || acceptLanguageHeader;
        const body = {
          "pageNumber" : 0,
          "contentSize" : 15,
          "sortDirection" : "asc",
          "sortElement" : "id"
        };

        const response = await fetch(`api/events/show/${params}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept-Language': language,
            },
            body: JSON.stringify(body),
        });

        const data = await response.json();
        if (data.success) {
            return data;
        } else {
            console.error('Error fetching events:', data.httpStatus);
            return null;
        }
    } catch (error) {
        console.error('Error fetching events:', error);
        return null;
    }
};


const fetchEventById = async (id) => {

    try {
        const acceptLanguageHeader = languageService.getLanguage(navigator.language || 'en-US');
        const language = localStorage.getItem('language') || acceptLanguageHeader;

        const response = await fetch( `api/events/show/${id}`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept-Language': language,
            },
        });

        const data = await response.json();
        if (data.success) {
            return data;
        } else {
            console.error('Error fetching event:', data.httpStatus);
            return null;
        }
    } catch (error) {
        console.error('Error fetching event:', error);
        return null;
    }
};

export { fetchEvents, fetchEventById };