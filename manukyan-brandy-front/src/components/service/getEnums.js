
export const getEnums = async () => {
    try {
        console.log(`api/enums/pages`);
        const headers = new Headers();
        headers.append("Content-Type", "application/json");

        const response = await fetch(`api/enums/pages`, {
            method: 'GET',
            headers: headers,
        });

        // Ensure response is ok
        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }

        const data = await response.json();
        console.log(data.data);

        if (data.success) {
            return data;
        } else {
            console.error('Error fetching enums:', data.httpStatus);
            return null;
        }
    } catch (error) {
        console.error('Error fetching enums:', error.stack || error.message);
        return null;
    }
};