
const AGE_CONFIRMED_KEY = 'ageConfirmed';

const AgeConfirmationService = {
    isAgeConfirmed: () => {
        return localStorage.getItem(AGE_CONFIRMED_KEY) === 'true';
    },
    confirmAge: () => {
        localStorage.setItem(AGE_CONFIRMED_KEY, 'true');
    },
};

export default AgeConfirmationService;
