const LanguageService = {
    getLanguage: (acceptLanguage) => {
        const languages = acceptLanguage.split(',');

        for (const language of languages) {
            const primaryLanguage = language.split(';')[0].trim();

            if (primaryLanguage === 'en' || primaryLanguage === 'hy') {
                return primaryLanguage;
            }
        }

        const localStorageLanguage = localStorage.getItem('language');

        if (localStorageLanguage && (localStorageLanguage === 'en' || localStorageLanguage === 'hy')) {
            return localStorageLanguage;
        }

        return 'en';
    },

    saveLanguage: (newLanguage) => {
        localStorage.setItem('language', newLanguage);
    },

    updateLanguage: (newLanguage) => {
        localStorage.setItem('language', newLanguage);
    },

    clearLanguage: () => {
        localStorage.removeItem('language');
    },
};

export default LanguageService;
