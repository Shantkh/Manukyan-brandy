import React from 'react';
import { useSpring, animated } from 'react-spring';

const Counter = ({ endValue }) => {
    const { number } = useSpring({
        from: { number: 0 },
        to: { number: endValue },
        config: { duration: 1000 }, // Adjust the duration as needed
    });

    return (
        <animated.span>{number.interpolate(value => Math.floor(value).toLocaleString())}</animated.span>
    );
};

export default Counter;
