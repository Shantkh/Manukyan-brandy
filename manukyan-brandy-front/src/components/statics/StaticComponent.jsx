import {useEffect} from "react";

const StaticComponent = (event) => {
    const fetchData = async () => {
        try {
            const headers = new Headers();
            headers.append("Content-Type", "application/json");

            const response = await fetch( `api/statics/save?page=${event.data}`, {
                method: 'POST',
                headers: headers,
            });

            if (!response.ok) {
                throw new Error('Network response was not ok');
            }

            const json = await response.json();
        } catch (error) {
        }
    };


    useEffect(() => {
        fetchData();
    }, []);


    return (null)
}

export default StaticComponent;