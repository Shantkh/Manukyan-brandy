import {useEffect, useState} from "react";
import '../WinesStyle.css';
import LanguageService from "../../service/languageService";


const WinesSectionComponents = () => {
    const [brands, setBrands] = useState([]);
    let [language, setLanguage] = useState(LanguageService.getLanguage(navigator.language || 'en-US'));
    const [products, setProducts] = useState([]);
    const [pageNumber, setPageNumber] = useState(0);
    const [sortElement] = useState("id");
    const productsPerPage = 9;
    const [selectedBrand, setSelectedBrand] = useState("");
    const [selectedYear, setSelectedYear] = useState("");
    const [selectedType, setSelectedType] = useState("");

    const fetchData = async () => {
        try {
            const response = await fetch(  `api/products`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    pageNumber: pageNumber,
                    contentSize: productsPerPage,
                    sortDirection: "asc",
                    sortElement: sortElement,
                }),
            });
            if (!response.ok) {
                throw new Error("Failed to fetch products");
            }
            const data = await response.json();
            setProducts(data.data);
        } catch (error) {
            console.error("Error fetching product data:", error.message);
        }
    };
    useEffect(() => {
        fetchData();
        fetchBrands();
    }, [pageNumber, sortElement]);

    useEffect(() => {
        const handleLanguageSwitch = (event) => {
            const newLanguage = event.detail.newLanguage;
            setLanguage(newLanguage);
            language = newLanguage;
        };
        window.addEventListener('languageSwitched', handleLanguageSwitch);

        return () => {
            window.removeEventListener('languageSwitched', handleLanguageSwitch);
        };
    }, []);

    const getMenuText = (menuKey) => {
        const locale = language || 'en';
        const menuTexts = {
            en: {
                type: 'Type',
                brand: 'Brand',
                wines: 'Wines',
                years: 'Year',
                allWines: 'ALL WINES',
            },
            hy: {
                type: 'Տեսակ',
                brand: 'Ապրանքանիշ',
                wines: 'Գինիներ',
                years: 'Տարի',
                allWines: 'ԲՈԼՈՐ ԳԻՆԻՆԵՐԸ',
            },
        };

        return menuTexts[locale][menuKey] || menuTexts.en[menuKey];
    };

    const handleNextPage = () => {
        setPageNumber(pageNumber + 1);
    };

    const handlePrevPage = () => {
        setPageNumber(pageNumber - 1);
    };

    const getTotalPages = () => {
        return Math.ceil(products.length / productsPerPage);
    };

    const localeBrand = (locale) => {
        const uniqueBrands = {};
        products.forEach((product) => {
            const brandName = locale === 'hy' ? product.brandName.brandNameArmenian.replace(/<[^>]*>?/gm, '') : product.brandName.brandNameEnglish.replace(/<[^>]*>?/gm, '');
            uniqueBrands[brandName] = true;
        });
        return Object.keys(uniqueBrands).map((brandName, index) => (
            <option className="options" key={index} value={brandName}>{brandName}</option>
        ));
    };

    const years = () => {
        const uniqueYears = new Set();
        products.forEach((product) => {
            const year = parseInt(product.productDate);
            if (!isNaN(year)) {
                uniqueYears.add(year);
            }
        });
        return Array.from(uniqueYears).map((year, index) => (
            <option key={index} value={year}>
                {year}
            </option>
        ));
    };

    const localeProductType = (locale) => {
        return products
            .map((product) => product.productType.find((title) => title.locale === locale).htmlContent)
            .map((content, index) => {
                const productType = content.replace(/<[^>]*>?/gm, '');
                return <option key={index} value={productType}>{productType}</option>;
            });
    };


    // Filter products based on selected options
    const filteredProducts = products.filter(product => {
        if (selectedBrand) {
            const selectedBrandTrimmed = selectedBrand.trim().toLowerCase(); // Remove leading/trailing whitespace and convert to lowercase

            // Check if any brand name contains the selected brand
            if (
                product.brandName &&
                (
                    (product.brandName.brandNameArmenian &&
                        product.brandName.brandNameArmenian.toLowerCase().includes(selectedBrandTrimmed)) ||
                    (product.brandName.brandNameEnglish &&
                        product.brandName.brandNameEnglish.toLowerCase().includes(selectedBrandTrimmed))
                )
            ) {
                return true; // Keep the product if brand contains the selected brand
            }
        }
        if (selectedYear && parseInt(product.productDate) === parseInt(selectedYear)) {
            return true;
        }
        if (selectedType && product.productType && product.productType.some(type => type.htmlContent.replace(/<[^>]*>?/gm, '') === selectedType)) {
            return true;
        }
        // Return true if no filters are selected
        if (!selectedBrand && !selectedYear && !selectedType) {
            return true;
        }
        return false;
    });

    const fetchBrands = async () => {
        try {
            const response = await fetch(  `admin/brand/name/get`);
            if (response.ok) {
                const data = await response.json();
                setBrands(data.data);
            } else {
                console.error('Failed to fetch brands');
            }
        } catch (error) {
            console.error('Error fetching brands:', error);
        }
    };

    const localeTitleMenu = (language) => {
        const brandNameKey = language === 'hy' ? 'brandNameArmenian' : 'brandNameEnglish';
        return brands.map((brand, index) => (
            <a key={index} href={`/wines/${brand[brandNameKey].toLowerCase().replace(/<\/?[^>]+(>|$)/g, "")}`}>
                <div dangerouslySetInnerHTML={{__html: brand[brandNameKey]}}/>
            </a>
        ));
    };

    return (
        <div className="product-list-container">
            <div className="product_list_main_container">
                <div className="wine-dropdown">
                    <div className="frame-176">
                        <select onChange={(e) => setSelectedBrand(e.target.value)}>
                            <option value="">{getMenuText('brand')}</option>
                            {localeBrand(language)}
                        </select>
                    </div>
                    <div className="frame-177">
                        <select onChange={(e) => setSelectedYear(e.target.value)}>
                            <option value="">{getMenuText('years')}</option>
                            {years()}
                        </select>
                    </div>
                    <div className="frame-178">
                        <select onChange={(e) => setSelectedType(e.target.value)}>
                            <option value="">{getMenuText('type')}</option>
                            {localeProductType(language)}
                        </select>

                    </div>
                </div>
                <div className="frame-59">
                    <div className="frame-41">
                        <div className="frame-25">
                            <div className="all-wines">{getMenuText('allWines')}</div>
                        </div>
                        {localeTitleMenu(language)}
                    </div>

                    <div className="gallery-container">
                        {filteredProducts.slice(pageNumber * productsPerPage, (pageNumber + 1) * productsPerPage).map((product, index) => (
                            <div className="gallery-item" key={index}>
                                <div className="product-image-flex">
                                    <img className="product-image" src={product.productImages[0]} alt={product.brand}/>
                                    <div className="product-details">
                                        <span className="brand">
    {language === 'hy' ? (
        <span dangerouslySetInnerHTML={{__html: product.brandName.brandNameArmenian}}></span>
    ) : (
        <span dangerouslySetInnerHTML={{__html: product.brandName.brandNameEnglish}}></span>
    )}
</span>
                                        <span className="product-type"
                                              dangerouslySetInnerHTML={{__html: product.productType.find((type) => type.locale === language)?.htmlTitle}}></span>
                                        <span className="product-type"
                                              dangerouslySetInnerHTML={{__html: product.productType.find((type) => type.locale === language)?.htmlContent}}></span>
                                        <span className="product-date">,{product.productDate}</span>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>

                </div>
                <div className="pagination">
                    <button className="left-button" onClick={handlePrevPage} disabled={pageNumber === 0}></button>
                    {Array.from({length: getTotalPages()}, (_, index) => (
                        <button
                            className={`numbers-button ${index === pageNumber ? 'active' : ''}`}
                            key={index}
                            onClick={() => setPageNumber(index)}
                        >
                            {index + 1}
                        </button>
                    ))}
                    <button className="right-button" onClick={handleNextPage} disabled={pageNumber === getTotalPages() - 1}></button>
                </div>
            </div>

        </div>
    );
};


export default WinesSectionComponents;
