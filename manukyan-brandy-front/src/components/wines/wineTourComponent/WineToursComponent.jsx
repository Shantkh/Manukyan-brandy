import './wineTourStyle.css';
import LanguageService from "../../service/languageService";
import {useEffect, useState} from "react";

const WineToursComponent = () =>{
    let [language, setLanguage] = useState(LanguageService.getLanguage(navigator.language || 'en-US'));

    useEffect(() => {
        const handleLanguageSwitch = (event) => {
            const newLanguage = event.detail.newLanguage;
            setLanguage(newLanguage);
            language = newLanguage;
        };
        window.addEventListener('languageSwitched', handleLanguageSwitch);

        return () => {
            window.removeEventListener('languageSwitched', handleLanguageSwitch);
        };
    }, []);

    const getMenuText = (menuKey) => {
        const locale = language || 'en';
        const menuTexts = {
            en: {
                learn: 'Learn More',
                wine: 'Wine enthusiasts, this one\'s for you!',
                click: 'Click the button below to explore more about our upcoming events and\n' +
                    '                        join the adventure.',
            },
            hy: {
                learn: 'Իմացեք ավելին',
                wine: 'Գինու սիրահարներ, սա ձեզ համար է:',
                click: 'Սեղմեք ներքևի կոճակը՝ մեր առաջիկա իրադարձությունների մասին ավելին իմանալու համար և\n' +
                    '                         միանալ արկածախնդրությանը:',
            },
        };

        return menuTexts[locale][menuKey] || menuTexts.en[menuKey];
    };

    return(
        <div className="frame-175">
            <div className="rectangle-19"></div>
            <div className="frame-13">
                <div className="frame-12">
                    <div className="wine-enthusiasts">
                        {getMenuText('wine')}
                        <br />
                        {getMenuText('click')}
                    </div>
                </div>
                <a href="/tours" className="button">
                    <div className="text-container">
                        <div className="button-text">{getMenuText('learn')}</div>
                    </div>
                </a>
            </div>
        </div>

    )
}

export default WineToursComponent