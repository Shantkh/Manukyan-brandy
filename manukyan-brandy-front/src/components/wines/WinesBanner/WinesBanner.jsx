import Banner from "../../banner/Banner";

const WinesBanner = () => {

    return (
        <Banner sectionEnum='WINE_MAIN_BANNER'/>
    )
}

export default WinesBanner;