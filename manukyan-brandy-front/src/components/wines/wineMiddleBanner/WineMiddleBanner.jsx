import Banner from "../../banner/Banner";

const WineMiddleBanner = () => {

    return (
        <Banner sectionEnum='WINE_MIDDLE_BANNER'/>
    )
}

export default WineMiddleBanner;