import styles from './styles.module.scss';
import { startOfYear, eachDayOfInterval, endOfMonth, format, getDay, startOfMonth, subYears } from 'date-fns';
import { useEffect, useMemo, useState } from 'react';
import useWindowDimensions from '../../hooks/useWindowDimensions';
import { fetchEvents } from '../service/eventsService';
import { Link } from 'react-router-dom';

const WEEKDAYS = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

const EventCalendar = () => {
  const [events, setEvents] = useState([]);
  const [dateTimestamp, setDateTimestamp] = useState({
    startDate: Math.floor(startOfYear(subYears(new Date(), 1)).getTime() / 1000),
    endDate: Math.floor(startOfYear(new Date()).getTime() / 1000) + 31622400 * 2,
  });
  const [tooltip, setTooltip] = useState(1);

  const showTooltip = (id) => {
    setTooltip(id);
  };

  const hideTooltip = () => {
    setTooltip(false);
  };

  useEffect(() => {
    const fetchData = async () => {
      const data = await fetchEvents(
        `dates/locale?startDate=${dateTimestamp.startDate}&endDate=${dateTimestamp.endDate}`
      );
      if (data) {
        setEvents(data.data);
      }
    };
    fetchData();

    const languageSwitchHandler = () => {
      fetchData();
    };
    window.addEventListener('languageSwitched', languageSwitchHandler);

    return () => {
      window.removeEventListener('languageSwitched', languageSwitchHandler);
    };
  }, [dateTimestamp]);

  const [currentDate, setCurrentDate] = useState(new Date());
  const firstDayOfMonth = startOfMonth(currentDate);
  const lastDayOfMonth = endOfMonth(currentDate);
  const { width } = useWindowDimensions();
  const daysInMonth = eachDayOfInterval({
    start: firstDayOfMonth,
    end: lastDayOfMonth,
  });

  const startingDayIndex = getDay(firstDayOfMonth);
  const endingDayIndex = getDay(lastDayOfMonth);

  const eventsByDate = useMemo(() => {
    return events.reduce((acc, event) => {
      const dateKey = format(event.eventStartDate, 'yyyy-MM-dd');
      if (!acc[dateKey]) {
        acc[dateKey] = [];
      }
      acc[dateKey].push(event);
      return acc;
    }, {});
  }, [events]);

  return (
      <div className={styles.container}>
        {/* <svg width={width - 208} viewBox='0 0 1316 12' fill='none' xmlns='http://www.w3.org/2000/svg'>
        <path
          d='M0.226497 6L6 11.7735L11.7735 6L6 0.226497L0.226497 6ZM1315.77 6L1310 0.226497L1304.23 6L1310 11.7735L1315.77 6ZM6 7H1310V5H6V7Z'
          fill='#852D23'
          fillOpacity='0.25'
        />
      </svg> */}
        <div className={styles.monthHeader}>
          <svg
              onClick={() => setCurrentDate(new Date(currentDate.setMonth(currentDate.getMonth() - 1)))}
              width='37'
              height='37'
              viewBox='0 0 37 37'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
          >
            <path
                d='M16.5253 27.043L18.6436 24.9247L11.7629 18.0291L18.6436 11.1334L16.5253 9.01516L7.51138 18.0291L16.5253 27.043Z'
                fill='#291818'
            />
          </svg>
          <div className={styles.monthTitle}>{format(currentDate, 'MMMM yyyy')}</div>
          <svg
              onClick={() => setCurrentDate(new Date(currentDate.setMonth(currentDate.getMonth() + 1)))}
              width='36'
              height='36'
              viewBox='0 0 36 36'
              fill='none'
              xmlns='http://www.w3.org/2000/svg'
          >
            <path
                d='M19.4988 26.998L17.3838 24.883L24.2538 17.998L17.3838 11.113L19.4988 8.99805L28.4988 17.998L19.4988 26.998Z'
                fill='#291818'
            />
          </svg>
        </div>
        <div className={styles.calendarGrid}>
          {WEEKDAYS.map((day) => {
            return (
                <div key={day} className={styles.weekday}>
                  {day}
                </div>
            );
          })}
          {Array.from({ length: startingDayIndex }).map((_, index) => {
            return <div key={`empty-${index}`} className={styles.emptyDay} />;
          })}
          {daysInMonth.map((day, index) => {
            const dateKey = format(day, 'yyyy-MM-dd');
            const todaysEvents = eventsByDate[dateKey] || [];
            return (
                <div key={index} className={styles.day}>
                  <div className={styles.dayNumber}>{format(day, 'd')}</div>
                  {todaysEvents.map((event) => {
                    return (
                        <div
                            className={styles.eventWrapper}
                            onMouseEnter={() => showTooltip(event.id)}
                            onMouseLeave={hideTooltip}
                        >
                          <Link to={`/events/${event.id}`}>
                            <div
                                key={event.id}
                                className={styles.event}
                                dangerouslySetInnerHTML={{ __html: event?.localizations[0]?.htmlTitle }}
                            ></div>
                            {event.id === tooltip && (
                                <div className={styles.tooltip}>
                                  <div
                                      className={styles.title}
                                      dangerouslySetInnerHTML={{ __html: event?.localizations[0]?.htmlTitle }}
                                  />
                                  <div className={styles.time}>{format(event.eventStartDate, 'h:mm a')}</div>
                                  <div
                                      className={styles.description}
                                      dangerouslySetInnerHTML={{ __html: event?.localizations[0]?.htmlShortContent }}
                                  />
                                </div>
                            )}
                          </Link>
                        </div>
                    );
                  })}
                </div>
            );
          })}
          {Array.from({ length: 6 - endingDayIndex }).map((_, index) => {
            return <div key={`empty-${index}`} className={styles.emptyDay} />;
          })}
        </div>
      </div>
  );
};

export default EventCalendar;
