import React, {useEffect, useState} from 'react';
import Slider from 'react-slick';
import languageService from '../service/languageService';
import styles from './mainSlider.module.scss';
import {motion} from 'framer-motion';
import AgeConfirmationService from '../service/AgeConfirmationService';
import AgeConfirmationPopup from '../ageverification/AgeConfirmationPopup';
import {fetchSliderContent} from "../service/sliderService";
import {Link} from "react-router-dom";

const MainSlider = () => {
    const acceptLanguageHeader = languageService.getLanguage(navigator.language || 'en-US');
    const language = localStorage.getItem('language') || acceptLanguageHeader;
    const [mainSlider, setMainSliderContent] = useState([]);
    const [currentSlide, setCurrentSlide] = useState(0);
    const [isAgeConfirmed, setIsAgeConfirmed] = useState(AgeConfirmationService.isAgeConfirmed());
    const [showPopup, setShowPopup] = useState(!isAgeConfirmed);

    useEffect(() => {
        const fetchSliderData = async () => {
            const sliderData = await fetchSliderContent(); // Fetch slider content including image URLs
            if (sliderData) {
                setMainSliderContent(sliderData.data);
            }
        };
        fetchSliderData();
    }, []);


    const getMenuText = (menuKey) => {
        const locale = language || 'en';
        const menuTexts = {
            en: {
                learn: 'Learn More',
            },
            hy: {
                learn: 'Իմացեք ավելին',
            },
        };

        return menuTexts[locale][menuKey] || menuTexts.en[menuKey];
    };

    const settings = {
        infinite: true,
        speed: 2000,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        arrows: false,
        beforeChange: (currentSlide, nextSlide) => {
            setCurrentSlide(nextSlide);
        },
    };

    const handleAgeConfirmation = () => {
        setIsAgeConfirmed(true);
        setShowPopup(false);
    };

    const handleRejectAge = () => {
        setShowPopup(false);
    };

    return (
        <div className={styles.mainSlider}>
            {!isAgeConfirmed && showPopup && (
                <AgeConfirmationPopup onConfirm={handleAgeConfirmation} onReject={handleRejectAge} />
            )}
            {!isAgeConfirmed && (
                <div className={styles.ageVerificationOverlay} /> // Render the overlay
            )}

            <div className={styles.imageSlider}>
                <Slider {...settings}>
                    {mainSlider.map((slideData, index) => (
                        <motion.div
                            key={slideData.sliderEnum}
                            id={`slide-${index}`}
                            className={styles.sliderItem}
                            style={{
                                filter: index === currentSlide ? 'blur(0px)' : 'blur(10px)',
                                transition: 'filter 2s ease-in-out',
                            }}
                        >
                            <div key={slideData.sliderEnum} className={styles.sliderItem} preload="auto">
                                <video id={`video${index}`} loop muted autoPlay controls className={styles.mainSlider_video} >
                                    <source src={slideData.sliderMedia} />
                                </video>
                                <img src={slideData.sliderMedia} alt='slider' />
                                <div className={styles.sliderContentOverlay} />
                                <div className={styles.sliderContentContainer}>
                                    <div className={styles.sliderContentWrapper}>
                                        <div className={styles.mainSliderContentSectionTitle}>
                                            <div dangerouslySetInnerHTML={{ __html: slideData?.sliderLocalizations[0]?.htmlContent }} />
                                        </div>
                                        <div className={styles.contentButton}>
                                            <Link to={slideData.link.toLowerCase()} className={styles.buttonText}>
                                                {getMenuText('learn')}
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </motion.div>
                    ))}
                </Slider>
            </div>
        </div>
    );
};

export default MainSlider;