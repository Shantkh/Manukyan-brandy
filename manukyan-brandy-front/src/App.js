import React, { useEffect, useState } from 'react';
import { Routes, Route } from 'react-router-dom';

import './App.scss';
import Layout from './pages/Layout';
import Home from './pages/Home';
import Tours from './pages/Tours';
import Events from './pages/Events';
import About from './pages/About';
import Holani from './pages/Holani';
import Anamor from './pages/Anamor';
import Wines from './pages/Wines';
import Factory from './pages/Factory';
import EventDetail from './components/eventDetail/EventDetail';
import ContactUs from './components/cotcatsUs/ContactUs';
import AdminLayout from './admin/components/AdminLayout';
import LoginPage from './admin/components/auth/LoginPage';
import Protected from './admin/components/Protected';
import Dashboard from './admin/components/dashboard/Dashboard';
import Uploads from './admin/components/uploadesComponent/UploadsPage';
import BookingComponent from './components/bookingtours/BookingComponent';
import 'react-quill/dist/quill.snow.css';
import { getEnums } from './components/service/getEnums';
import CreateTour from "./admin/components/tours/CreateTour";
import ToursComponent from "./admin/components/tours/ToursComponent";
import ToursUpdateComponent from "./admin/components/tours/ToursUpdateComponent";
import TourBookingComponent from "./admin/components/toursBooking/ToursBooking";
import SubscribersComponent from "./admin/components/subscribers/SubscribersComponent";
import EventsCreateComponent from "./admin/components/events/EventsCreateComponent";
import PartnerCreateComponent from "./admin/components/partners/PartnerCreateComponent";
import EventsManagementComponent from "./admin/components/events/EventsManagementComponent";
import EventUpdateComponent from "./admin/components/events/EventUpdateComponent";
import PartnersManagementComponent from "./admin/components/partners/PartnersManagementComponent";
import PartnersUpdateComponent from "./admin/components/partners/PartnersUpdateComponent";
import ProductCreateComponent from "./admin/components/prooduct/ProductCreateComponent";
import ProductManagementComponent from "./admin/components/prooduct/ProductManagementComponent";
import ProductUpdateComponent from "./admin/components/prooduct/ProductUpdateComponent";
import BrandCreateComponent from "./admin/components/brand/BrandCreateComponent";
import BrandNameComponent from "./admin/components/brand/BrandNameComponent";
import BrandNameUpdateComponent from "./admin/components/brand/BrandNameUpdateComponent";
import Cellar from "./pages/Cellar";
import Bottle from "./pages/Bottle";
import HomeSlider from "./admin/components/homeslider/homeSlider";
import CellarsCreateComponent from "./admin/components/Cellars/CellarsCreateComponent";
import BottlingCreateComponent from "./admin/components/bottling/BottlingCreateComponent";
import CreatingSildingBanner from "./admin/components/banners/CreatingSildingBanner";
import CreateBanner from "./admin/components/banners/CreateBanner";
import InfoComponent from "./admin/components/InfoComponent/InfoComponent";
import BookingToursPersonalComponent from "./components/bookingtourspresonal/BookingToursPersonalComponent";

function App() {

  const [sectionsEnums, setSectionsEnums] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const data = await getEnums();

      if (data) {
        setSectionsEnums(data);
      }
    };
    fetchData();
  }, []);

  return (
      <div className='App'>
        {/* Assuming 'App' is the main container class */}
        <Routes>
          <Route path='/' element={<Layout />}>
            <Route index element={<Home />} />
            <Route path='events' element={<Events />} />
            <Route path='events/:id' element={<EventDetail />} />
            <Route path='tours' element={<Tours />} />
            <Route path='/tours/booking/:id' element={<BookingComponent />} />
            <Route path='/tours/booking/personal/:id' element={<BookingToursPersonalComponent />} />
            <Route path='about' element={<About />} />
            <Route path='factory' element={<Factory />} />
            <Route path='terroir' element={<Factory />} />
            <Route path='vineyards' element={<Factory />} />
            <Route path='fermentation' element={<Factory />} />
            <Route path='cellars' element={<Factory />} />
            <Route path='factory/cellars' element={<Cellar />} />
            <Route path='factory/bottling' element={<Bottle />} />
            <Route path='bottling' element={<Factory />} />
            <Route path='contactus' element={<ContactUs />} />
            <Route path='wines' element={<Wines />} />
            <Route path='wines/holani' element={<Holani />} />
            <Route path='wines/anamor' element={<Anamor />} />
          </Route>
          <Route path='admin'>
            <Route path='logIn' element={<LoginPage />} />
            <Route element={<AdminLayout />}>
              <Route element={<Protected />}>
                <Route index element={<Dashboard />} />
                <Route path='create/homeSlider' element={<HomeSlider/>}/>

                <Route path='create/homeFactory' element={<CreatingSildingBanner data={'HOME_FACTORY'}/>}/>
                <Route path='create/homeEvent' element={<CreatingSildingBanner data={'HOME_EVENTS'}/>}/>
                <Route path='create/homeTour' element={<CreatingSildingBanner data={'HOME_TOURS'}/>}/>
                <Route path='create/homeBanner' element={<CreateBanner data={'HOME_BANNER'}/>}/>
                <Route path='uploads' element={<Uploads />} />
                <Route path='create/factoryMainBanner' element={<CreateBanner  data={'FACTORY_MAIN_BANNER'}/>}/>
                <Route path='create/factorySecondBanner' element={<CreateBanner  data={'FACTORY_SECOND_BANNER'}/>}/>
                <Route path='create/factoryWineYards' element={<CreatingSildingBanner  data={'FACTORY_VINEYARDS'}/>}/>
                <Route path='create/factoryMiddleBanner' element={<CreateBanner data={'FACTORY_MIDDLE_BANNER'}/>}/>
                <Route path='create/factoryFermentation' element={<CreatingSildingBanner data={'FACTORY_FERMENTATION'}/>}/>
                <Route path='create/factoryCellars' element={<CreatingSildingBanner data={'FACTORY_CELLARS'}/>}/>
                <Route path='create/factoryBottling' element={<CreatingSildingBanner data={'FACTORY_BOTTLING'}/>}/>
                <Route path='create/factoryBottomBanner' element={<CreateBanner data={'FACTORY_BOTTOM_BANNER'}/>}/>
                <Route path='create/factory/info' element={<InfoComponent data={'factory'}/>}/>
                <Route path='tours/create' element={<CreateTour />} />
                <Route path='tours/management' element={<ToursComponent />} />
                <Route path='tours/update' element={<ToursUpdateComponent />} />
                <Route path='tours/booking' element={<TourBookingComponent />} />
                <Route path='subscribers' element={<SubscribersComponent />} />
                <Route path='product/create' element={<ProductCreateComponent />} />
                <Route path='product/management' element={<ProductManagementComponent />} />
                <Route path='product/update' element={<ProductUpdateComponent />} />
                <Route path='product/wineMainBanner' element={<CreateBanner data={'WINE_MAIN_BANNER'}/>}/>
                <Route path='product/wineMiddleBanner' element={<CreateBanner data={'WINE_MIDDLE_BANNER'}/>}/>
                <Route path='product/info' element={<InfoComponent data={'wines'}/>}/>
                <Route path='events/create' element={<EventsCreateComponent />} />
                <Route path='events/management' element={<EventsManagementComponent />} />
                <Route path='events/update' element={<EventUpdateComponent />} />
                <Route path='partner/create' element={<PartnerCreateComponent />} />
                <Route path='partner/management' element={<PartnersManagementComponent />} />
                <Route path='partner/update' element={<PartnersUpdateComponent />} />
                <Route path='brand/create' element={<BrandCreateComponent />} />
                <Route path='cellar/create' element={<CellarsCreateComponent />} />
                <Route path='cellar/mainBanner' element={<CreateBanner data={'CELLAR_MAIN_BANNER'}/>}/>
                <Route path='cellar/middleBanner' element={<CreateBanner data={'CELLAR_MIDDLE_BANNER'}/>}/>
                <Route path='cellar/info' element={<InfoComponent data={'cellars'}/>}/>
                <Route path='bottling/create' element={<BottlingCreateComponent />} />
                <Route path='bottling/mainBanner' element={<CreateBanner data={'BOTTLING_MAIN_BANNER'}/>}/>
                <Route path='bottling/middleBanner' element={<CreateBanner data={'BOTTLING_MIDDLE_BANNER'}/>}/>
                <Route path='bottling/info' element={<InfoComponent data={'bottling'}/>}/>
                <Route path='anamor/create' element={<BrandCreateComponent data={'anamor'}/>} />
                <Route path='anamor/anamorMainBanner' element={<CreateBanner data={'ANAMOR_MAIN_BANNER'}/>}/>
                <Route path='anamor/anamorLowerBanner' element={<CreateBanner data={'ANAMOR_BOTTOM_BANNER'}/>}/>
                <Route path='brand/names/create' element={<BrandNameComponent />} />
                <Route path='brand/names/management' element={<BrandNameUpdateComponent />} />
                <Route path='holani/create' element={<BrandCreateComponent data={'holani'}/>} />
                <Route path='holani/holaniMainBanner' element={<CreateBanner data={'HOLANY_MAIN_BANNER'}/>}/>
                <Route path='holani/holaniLowerBanner' element={<CreateBanner data={'HOLANY_INFO_BANNER'}/>}/>
                {/*<Route path='create'>*/}
                {/*  {sectionsEnums &&*/}
                {/*    sectionsEnums.data?.map((sectionEnum) => {*/}
                {/*      return <Route key={sectionEnum} path={sectionEnum} element={<CreateSliderAndTextSection />} />;*/}
                {/*    })}*/}
                {/*
                      <Route path='FACTORY_MIDDLE_BANNER' element={<CreateSliderAndTextSection />} />
                      <Route path='FACTORY_BOTTOM_BANNER' element={<CreateSliderAndTextSection />} />
                      */}
                {/*</Route>*/}
              </Route>
            </Route>
          </Route>
        </Routes>
      </div>
  );
}

export default App;
