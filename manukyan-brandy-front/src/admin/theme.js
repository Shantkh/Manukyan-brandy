import { createTheme } from '@mui/material';

export const theme = createTheme({
  palette: {
    primary: { main: '#750027' },
    secondary: { main: '#75002799' },
    mode: 'light',
  },
  components: {
    MuiDrawer: {
      styleOverrides: {
        root: {
          backgroundColor: 'lightgray',
          height: 'auto !important',
        },
      },
    },
  },
});