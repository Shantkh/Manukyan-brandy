import React, { useState } from 'react';
import { EditorState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

const RichTextEditor = ({ value, onChange }) => {
    const [editorState, setEditorState] = useState(() => EditorState.createEmpty());

    const onEditorStateChange = (newEditorState) => {
        setEditorState(newEditorState);
        const htmlContent = convertEditorStateToHtml(newEditorState);
        onChange(htmlContent);
    };

    const convertEditorStateToHtml = (editorState) => {
        const contentState = editorState.getCurrentContent();
        return JSON.stringify(contentState);
    };

    return (
        <Editor
            editorState={editorState}
            onEditorStateChange={onEditorStateChange}
            wrapperClassName="wrapper-class"
            editorClassName="editor-class"
            toolbarClassName="toolbar-class"
            toolbar={{
                options: ['inline', 'blockType', 'fontSize', 'fontFamily', 'list', 'textAlign', 'colorPicker', 'link', 'embedded', 'emoji', 'image', 'remove', 'history'],
                inline: {
                    options: ['bold', 'italic', 'underline', 'strikethrough'],
                },
                blockType: {
                    inDropdown: true,
                },
                fontSize: {
                    options: [8, 9, 10, 11, 12, 14, 18, 24, 30, 36, 48, 60, 72, 96],
                },
                fontFamily: {
                    options: ['Arial', 'Georgia', 'Impact', 'Tahoma', 'Times New Roman', 'Verdana'],
                },
                textAlign: {
                    inDropdown: true,
                },
                colorPicker: {
                    colors: ['rgb(0,0,0)', 'rgb(255,0,0)', 'rgb(0,255,0)', 'rgb(0,0,255)'],
                },
                link: {
                    defaultTargetOption: '_self',
                },
                history: {
                    inDropdown: false,
                },
            }}
        />
    );
};

export default RichTextEditor;
