import React from 'react';

const HtmlContent = ({ htmlString }) => {
    // Create a DOM element to parse the HTML string
    const parser = new DOMParser();
    const htmlDoc = parser.parseFromString(htmlString, 'text/html');

    // Extract the body content of the parsed HTML document
    const bodyContent = htmlDoc.body.innerHTML;

    return <div dangerouslySetInnerHTML={{ __html: bodyContent }} />;
};

export default HtmlContent;
