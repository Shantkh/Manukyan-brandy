import React from 'react';
import {useState} from 'react';
import {
    Box,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    Grid,
    InputLabel,
    Paper,
    Typography,
} from '@mui/material';
import FilesMultiselect from '../banners/FileMultiselect';
import {useLocation} from 'react-router-dom';
import InputToHtml from '../inputToHtml/InputToHtml';
import {toast} from "react-toastify";



const CreateSliderAndTextSection = () => {
    const [armenianContent, setArmenianContent] = useState('');
    const [englishContent, setEnglishContent] = useState('');
    const [selectedFiles, setSelectedFiles] = useState([]);
    const [open, setOpen] = useState(false);
    const location = useLocation();
    const pathParts = location.pathname.split('/');
    const sectionEnum = pathParts[pathParts.length - 1];

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleSubmit = async () => {

        const media = selectedFiles.map((file) => {
            return file

        });

        const data = JSON.stringify({
            "sliderMedia": media,
            "localizations": [
                {
                    "locale": "en",
                    "htmlContent": englishContent,
                },
                {
                    "locale": "hy",
                    "htmlContent": armenianContent,
                },
            ]
        })

        try {
            const headers = new Headers();
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
            headers.append('Content-Type', 'application/json');

            const response = await fetch(`admin/sections/save?page=${sectionEnum}`, {
                method: 'POST',
                headers: headers,
                body: data
            });
            const json = await response.json();
            if (response.ok) {
                toast.success("Page created.")
            }
        } catch (error) {
            toast.error(error.message);
            console.error(error);
        }
    }

    let sectionTitle = sectionEnum.toLowerCase().split('_');
    sectionTitle = sectionTitle.map((word, index) => sectionTitle[index] = word.charAt(0).toUpperCase() + word.slice(1));
    sectionTitle = sectionTitle.join(' ');

    return (
        <>
            <Paper elevation={3} sx={{marginRight: '15%', marginLeft: '15%'}}>
                <Box sx={{padding: 5}}>
                    <Typography variant='h6' gutterBottom sx={{paddingBottom: 5}}>
                        Create {sectionTitle} Content and Slider Section
                    </Typography>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <label htmlFor='english-content'>English Content</label>
                            <InputToHtml id='english-content' value={englishContent} setValue={setEnglishContent}
                                         placeholder='Write ENGLISH nontent here...'/>
                        </Grid>
                        <Grid item xs={12}>
                            <label htmlFor='armenian-content'>Armenian Content</label>
                            <InputToHtml id='armenian-content' value={armenianContent} setValue={setArmenianContent}
                                         placeholder='Write ARMENIAN nontent here...'/>
                        </Grid>
                        <Grid item xs={12} sm={3}>
                            <InputLabel
                                sx={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                    fontWeight: 700,
                                }}
                            >
                                Select Slider Images
                            </InputLabel>
                        </Grid>
                        <Grid item xs={12} sm={9}>
                            <FormControl fullWidth size='small'>
                                <Button onClick={handleClickOpen} variant='outlined'>
                                    Select Files
                                </Button>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12} sm={5}/>
                        <Grid item xs={12} sm={4}>
                            <Button onClick={handleSubmit} variant='contained'>
                                Save
                            </Button>
                        </Grid>
                    </Grid>
                </Box>
            </Paper>
            <Dialog
                open={open}
                onClose={handleClose}
                scroll={'paper'}
                aria-labelledby="scroll-dialog-title"
                aria-describedby="scroll-dialog-description"
                maxWidth='md'
            >
                <DialogTitle id="scroll-dialog-title">Select files</DialogTitle>
                <DialogContent dividers={true}>
                    <FilesMultiselect selectedFiles={selectedFiles} setSelectedFiles={setSelectedFiles}/>
                </DialogContent>
                <DialogActions sx={{justifyContent: 'center'}}>
                    <Button onClick={handleClose} variant='outlined' sx={{fontSize: 'large', my: '10px'}}>
                        Done
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
};

export default CreateSliderAndTextSection;
