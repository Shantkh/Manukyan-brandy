import React, { useEffect, useState } from 'react';

const TourBookingList = ({ data }) => {
    const [selectedMonths, setSelectedMonths] = useState([]);
    const [filteredData, setFilteredData] = useState([]);

    // Extract all unique months from tours and bookings
    const months = Array.from(new Set(data.flatMap(({ tour }) => (tour ? tour.tourMonth : []))));

    const handleCheckboxChange = (e) => {
        const month = e.target.value;
        if (selectedMonths.includes(month)) {
            setSelectedMonths(selectedMonths.filter(m => m !== month));
        } else {
            setSelectedMonths([...selectedMonths, month]);
        }
    };

    useEffect(() => {
        if (!data || data.length === 0) {
            // If data is empty, display a message
            setFilteredData([]);
        } else if (selectedMonths.length === 0) {
            setFilteredData(data);
        } else {
            const filtered = data.filter(({ tour }) =>
                tour && selectedMonths.includes(tour.tourMonth)
            );
            setFilteredData(filtered);
        }
    }, [selectedMonths, data]);

    const getTime = (dateToFix) => {
        const date = new Date(dateToFix);

        const year = date.getFullYear();
        const month = String(date.getMonth() + 1).padStart(2, '0');
        const day = String(date.getDate()).padStart(2, '0');
        return `${year}-${month}-${day}`;
    }

    return (
        <div>
            {(!data || data.length === 0) ? (
                <div>No data available</div>
            ) : (
                <>
                    <div className="checkbox-container">
                        {months.map((month, index) => {
                            const toursForMonth = filteredData.filter(({ tour }) => tour && tour.tourMonth === month);
                            const totalBookings = toursForMonth.reduce((acc, curr) => {
                                return acc + curr.bookings.length;
                            }, 0);
                            return (
                                <label key={month} className="checkbox-label-tour">
                                    <input
                                        type="checkbox"
                                        value={month}
                                        onChange={handleCheckboxChange}
                                        checked={selectedMonths.includes(month)}
                                    />
                                    <span className="checkbox-text">{month}</span>
                                    <span className="event-count">{totalBookings}</span>
                                </label>
                            );
                        })}
                    </div>
                    <div>
                        {filteredData.map(({ tour, bookings }) => (
                            <div key={tour.id} className="tour-card-booking">
                                <div className="tour-card-booking-title">
                                    <h3>{tour.tourMonth}</h3>
                                    <p>Date: {tour.dateOfTour}</p>
                                    <p>Day: {tour.tourDay}</p>
                                    <p>Start hour: {tour.tourHour}</p>
                                </div>
                                <div className="tour-container-booking">
                                    {bookings
                                        .filter(booking => booking.tourId === tour.id) // Filter bookings for this tour
                                        .map(booking => (
                                            <div key={booking.id} className="booking-booking">
                                                <p><strong className="padding-color">Order date:</strong><span className="data-padder"> {getTime(booking.createdOn)}</span> </p>
                                                <p><strong className="padding-color">Number of visitors:</strong><span className="data-padder"> {booking.attendees}</span> </p>
                                                <p><strong className="padding-color">Visitor name:</strong><span className="data-padder"> {booking.name + " " + booking.surname}</span> </p>
                                                <p><strong className="padding-color">Email:</strong><span className="data-padder"> {booking.email}</span> </p>
                                                <p><strong className="padding-color">Phone number:</strong><span className="data-padder">{booking.phone}</span></p>
                                                <p><strong className="padding-color">Language of the tour:</strong><span className="data-padder">{booking.tourLanguage}</span> </p>
                                                <p><strong className="padding-color">Amount to pay:</strong><span className="amount">{booking.totalAmount} AMD</span></p>
                                            </div>
                                        ))}
                                </div>
                            </div>
                        ))}
                    </div>
                </>
            )}
        </div>
    );
};

export default TourBookingList;
