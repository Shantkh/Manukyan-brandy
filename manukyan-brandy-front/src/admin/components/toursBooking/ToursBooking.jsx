import React, {useEffect, useState} from 'react';
import TourBookingList from "./TourBookingList";




const TourBookingComponent = ({data}) => {
    const [tourBookingData, setTourBookingData] = useState([]);

    const fetchData = async () => {
        try {
            const headers = new Headers();
            const token = localStorage.getItem('accessToken');
            headers.append( "Authorization", 'Bearer ' + token);
            headers.append("Content-Type", "application/json");
            const response = await fetch( `admin/tours/book/`, {
                method: 'POST',
                headers: headers,
                body: JSON.stringify({
                    pageNumber: 0,
                    contentSize: 100,
                    sortDirection: 'asc',
                    sortElement: 'tourId'
                })
            });
            const data = await response.json();
            setTourBookingData((data.data));
        } catch (error) {
            console.error('Error fetching booking results:', error);
        }
    };


    useEffect(() => {
        fetchData();
    }, []);
    return(
        <TourBookingList data={tourBookingData}/>
    )

}
export default TourBookingComponent;