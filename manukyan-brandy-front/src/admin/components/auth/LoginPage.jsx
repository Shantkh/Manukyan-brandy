import * as React from 'react';
import styles from './loginPage.module.scss';
import { useState } from 'react';
import { Avatar, Box, Button, Container, TextField, ThemeProvider, Typography } from '@mui/material';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { theme } from '../../theme';
import authProvider from '../../authProvider';
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import REACT_APP_BASE_URL from "../../../config";

const LoginPage = () => {
  const [forgotToggle, setForgotToggle] = useState(false);
  const [newPassToggle, setNewPassToggle] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const navigate = useNavigate()

  const [newPasswordInput, setNewPasswordInput] = useState({
    code: '',
    password: '',
    confirmPassword: '',
  });

  const [error, setError] = useState({
    code: 'Enter the code that you received in the email.',
    password: 'Enter your new password.',
    confirmPassword: 'Reenter your new password.',
  });

  const [email, setEmail] = useState('');

  const handleLogin = async (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const success = await authProvider.login({
      username: data.get('username'),
      password: data.get('password'),
    });

    if(success) {
      navigate("/admin/dashboard")
    }
  };

  const handleForgotToggle = () => {
    setForgotToggle((state) => !state);
  };

  const handleNewPasswordToggle = () => {
    setNewPassToggle((state) => !state);
  };

  const handleForgotPasswordSubmit = async (event) => {
    event.preventDefault();

    try {
      setIsLoading(true);

      const forgotPasswordHeaders = new Headers();
      forgotPasswordHeaders.append('Content-Type', 'application/json');

      const response = await fetch(`api/auth/forgot-password`, {
        method: 'POST',
        headers: forgotPasswordHeaders,
        body: JSON.stringify({ email: email }),
      });

      if (!response.ok) {
        throw new Error(`Error! status: ${response.status}`);
      }

      const json = await response.json();
      localStorage.setItem('codeId', json.data.id);
      localStorage.setItem('email', json.data.recipient);
      setForgotToggle(false);
      setNewPassToggle(true);
      toast('The code is sent to your email address.');
    } catch (error) {
      toast('The Email address doesn\'t match the one linked to this account, please enter the correct email address.');
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  const onInputChange = (e) => {
    const { name, value } = e.target;
    setNewPasswordInput((prev) => ({
      ...prev,
      [name]: value,
    }));
    validateInput(e);
  };

  const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&.])[A-Za-z\d@$!%*?&.]{8,}$/;

  const validateInput = (e) => {
    let { name, value } = e.target;
    setError((prev) => {
      const stateObj = { ...prev, [name]: '' };

      switch (name) {
        case 'code':
          if (!value) {
            stateObj[name] = 'Enter the code that you received in the email.';
          }
          break;

        case 'password':
          if (!value) {
            stateObj[name] = 'Enter your new password.';
          } else if (!passwordRegex.test(value)) {
            stateObj[name] =
                'Password should include at least \n1 Uppercase letter, 1 Lowercase Letter,\n1 Number, 1 Symbol,\nAnd have at least 8 characters.';
          } else if (newPasswordInput.confirmPassword && value !== newPasswordInput.confirmPassword) {
            stateObj[name] = 'Password and Confirm Password does not match.';
          } else {
            stateObj[name] = newPasswordInput.confirmPassword ? '' : error.confirmPassword;
          }
          break;

        case 'confirmPassword':
          if (!value) {
            stateObj[name] = 'Reenter your new password.';
          } else if (newPasswordInput.password && value !== newPasswordInput.password) {
            stateObj[name] = 'Password and Confirm Password does not match.';
          }
          break;

        default:
          break;
      }

      return stateObj;
    });
  };

  const handleNewPasswordSubmit = async (event) => {
    event.preventDefault();

    try {
      setIsLoading(true);

      const forgotPasswordHeaders = new Headers();
      forgotPasswordHeaders.append('Content-Type', 'application/json');

      const requestBody = {
        codeId: localStorage.getItem('codeId'),
        code: newPasswordInput.code,
        email: localStorage.getItem('email'),
        newPassword: newPasswordInput.password,
      };

      const response = await fetch(`api/auth/new-credentials`, {
        method: 'POST',
        headers: forgotPasswordHeaders,
        body: JSON.stringify(requestBody),
      });

      if (!response.ok) {
        throw new Error(`Error! status: ${response.status}`);
      }

      localStorage.removeItem('codeId');
      localStorage.removeItem('email');
      setNewPassToggle(false);
      toast('You have successfully changed your password!');
    } catch (error) {
      toast('Please check if you\'ve entered the correct code');
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
      <ThemeProvider theme={theme}>
        <div className={styles.loginContainer}>
          <Container component='main' maxWidth='xs'>
            {/* <CssBaseline /> */}
            <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                  margin: 'auto',
                  backgroundColor: 'white',
                  padding: '20px',
                  borderRadius: '8px'
                }}
            >
              <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                <LockOutlinedIcon />
              </Avatar>

              {!isLoading && !forgotToggle && !newPassToggle && (
                  <>
                    <Typography component='h1' variant='h5'>
                      Sign in
                    </Typography>
                    <Box component='form' onSubmit={handleLogin} noValidate sx={{ mt: 1, width: '90%' }}>
                      <TextField
                          margin='normal'
                          required
                          variant='filled'
                          fullWidth
                          id='username'
                          label='Username'
                          name='username'
                          autoComplete='username'
                          autoFocus
                      />
                      <TextField
                          margin='normal'
                          required
                          variant='filled'
                          fullWidth
                          name='password'
                          label='Password'
                          type='password'
                          id='password'
                          autoComplete='current-password'
                      />
                      <Button type='submit' fullWidth variant='contained' sx={{ mt: 3, mb: 2 }}>
                        Sign In
                      </Button>
                      <div onClick={handleForgotToggle} className={styles.forgotToggle}>
                        Forgot Password
                      </div>
                    </Box>
                  </>
              )}


              {!isLoading && forgotToggle && (
                  <>
                    <Typography component='h1' variant='h5'>
                      Forgot Password
                    </Typography>
                    <form onSubmit={handleForgotPasswordSubmit} className={styles.forgotForm}>
                      <TextField
                          id='filled-basic'
                          label='Email'
                          variant='filled'
                          type='email'
                          value={email}
                          required
                          helperText='The Code will be sent to this email address'
                          onChange={(e) => {
                            setEmail(e.target.value);
                          }}
                      />
                      <Button variant='contained' type='submit'>
                        Send Reset Code
                      </Button>
                      <Button variant='outlined' onClick={handleForgotToggle}>
                        Back
                      </Button>
                    </form>
                  </>
              )}
              {!isLoading && newPassToggle && (
                  <>
                    <Typography component='h1' variant='h5'>
                      New Password
                    </Typography>
                    <form onSubmit={handleNewPasswordSubmit} className={styles.forgotForm}>
                      <TextField
                          label='Code'
                          name='code'
                          type='number'
                          value={newPasswordInput.code}
                          required
                          helperText={error.code}
                          InputLabelProps={{
                            shrink: true,
                          }}
                          onChange={onInputChange}
                          onBlur={validateInput}
                      />
                      <TextField
                          label='New Password'
                          name='password'
                          type='password'
                          value={newPasswordInput.password}
                          required
                          helperText={error.password}
                          onChange={onInputChange}
                          onBlur={validateInput}
                      />
                      <TextField
                          label='Confirm New Password'
                          name='confirmPassword'
                          type='password'
                          value={newPasswordInput.confirmPassword}
                          required
                          helperText={error.confirmPassword}
                          onChange={onInputChange}
                          onBlur={validateInput}
                      />
                      <Button variant='contained' type='submit'>
                        Change Password
                      </Button>
                      <Button variant='outlined' onClick={handleNewPasswordToggle}>
                        Back
                      </Button>
                    </form>
                  </>
              )}
              {isLoading && <div className={styles.loading}>Loading...</div>}
            </Box>
          </Container>
          <ToastContainer
              position="bottom-center"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
              theme="light"
          />
        </div>
      </ThemeProvider>
  );
};

export default LoginPage;
