import * as React from 'react';
import { Box, Card, Typography } from '@mui/material';

import logo from './Manukyan_brandy_logo.svg';

const Welcome = () => {
    return (
        <Card
            sx={{
                background: theme =>
                    `linear-gradient(135deg, ${theme.palette.primary.light} 0%, ${theme.palette.primary.main} 50%, ${theme.palette.secondary.main} 100%)`,
                color: theme => theme.palette.primary.contrastText,
                padding: '20px',
                marginTop: 2,
                marginBottom: '1em',
                width: '100%',
            }}
        >
            <Box display="flex">
                <Box flex="1">
                    <Typography variant="h4" component="h2" gutterBottom>
                        Welcome To Manukyan Brandy Admin
                    </Typography>
                    <Box maxWidth="40em">
                        <Typography variant="body1" component="p" gutterBottom>
                            Here you can make changes to the Manukyan Brandy website.
                        </Typography>
                    </Box>
                </Box>
                <Box
                    display={{ xs: 'none', sm: 'none', md: 'block' }}
                    sx={{
                        background: `url(${logo}) top right / cover`,
                        marginLeft: 'auto',
                    }}
                    width="7.62em"
                    height="6em"
                    overflow="hidden"
                />
            </Box>
        </Card>
    );
};

export default Welcome;