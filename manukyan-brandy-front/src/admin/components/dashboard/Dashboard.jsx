import React, { useState, useEffect } from 'react';
import styles from './dashboard.module.scss';
import SimpleCard from './SimpleCard';
import TodayIcon from '@mui/icons-material/Today';
import DateRangeIcon from '@mui/icons-material/DateRange';
import EventNoteIcon from '@mui/icons-material/EventNote';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import PublicIcon from '@mui/icons-material/Public';
import AnalyticsIcon from '@mui/icons-material/Analytics';
import { useNavigate } from 'react-router-dom';
import Welcome from './Welcome';
import ListCard from './ListCard';
import { handleUnauthorized } from '../../errorHelper';



const DashboardComponent = () => {
  const [data, setData] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const headers = new Headers();
        const token = localStorage.getItem('accessToken');
        headers.append( "Authorization", 'Bearer ' + token);
        headers.append("Content-Type", "application/json");
        const response = await fetch(  `admin/static/home`, {
          method: 'POST',
          headers: headers,
          body: {
            "pageNumber": 0,
            "contentSize": 25,
            "sortDirection": "desc",
            "sortElement": "id"
          }
        });
        if (!handleUnauthorized(response.status)) {
          navigate('/admin/login');
        }
        const json = await response.json();
        setData(json.data);
      } catch (error) {
        console.error(error);
      }
    };
    fetchData();
  }, []);

  return (
    <>
      {data ? (
        <div className={styles.container}>
          <Welcome />
          <div className={styles.visitorsContainer}>
            <SimpleCard icon={TodayIcon} title={'Visits Today'} subtitle={data.statisticsResult.usersToday} />
            <SimpleCard
              icon={DateRangeIcon}
              title={'Visits Last 7 Days'}
              subtitle={data.statisticsResult.usersLast7Days}
            />
            <SimpleCard
              icon={EventNoteIcon}
              title={'Visits Last 15 Days'}
              subtitle={data.statisticsResult.usersLast15Days}
            />
            <SimpleCard
              icon={CalendarMonthIcon}
              title={'Visits Last 30 Days'}
              subtitle={data.statisticsResult.usersLast30Days}
            />
          </div>
          <div className={styles.staticsContainer}>
            {!!data.countries.length && <ListCard
              icon={PublicIcon}
              title={'Visits From Countries'}
              propName={'country'}
              propValueName={'visitors'}
              data={data.countries}
            />}
            {!!data.pages.length && <ListCard
              icon={AnalyticsIcon}
              title={'Page Visits'}
              propName={'page'}
              propValueName={'pageCount'}
              data={data.pages}
            />}
          </div>
        </div>
      ) : (
        <div>Loading data</div>
      )}
    </>
  );
};

export default DashboardComponent;
