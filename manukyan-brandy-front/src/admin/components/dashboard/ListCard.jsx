import * as React from 'react';
import { createElement } from 'react';
import { Card, Box, Typography } from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

const ListCard = ({ icon, title, propName, propValueName, data }) => {

  return (
  <Card
    sx={{
      minHeight: 52,
      display: 'flex',
      flexDirection: 'column',
      flex: '1',
      '& a': {
        textDecoration: 'none',
        color: 'inherit',
      },
    }}
  >
    <Box
      sx={{
        position: 'relative',
        overflow: 'hidden',
        padding: '16px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        '& .icon': {
          color: 'primary.main',
        },
        '&:before': {
          position: 'absolute',
          top: '50%',
          left: 0,
          display: 'block',
          content: `''`,
          height: '200%',
          aspectRatio: '1',
          transform: 'translate(-30%, -60%)',
          borderRadius: '50%',
          backgroundColor: 'secondary.main',
          opacity: 0.15,
        },
      }}
    >
      <Box width='3em' className='icon'>
        {createElement(icon, { fontSize: 'large' })}
      </Box>
      <Box textAlign='right'>
        <Typography
          sx={{
            color: 'primary.main',
          fontWeight: '500',
          fontSize: '18px',
          }}
        >{title}</Typography>
      </Box>
    </Box>
    <Box>
      <TableContainer component={Paper}>
          <Table sx={{ minWidth: 400 }} aria-label="simple table">
            <TableBody>
              {data.map((item) => (
                <TableRow
                  key={item[propName]}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {item[propName]}              
                  </TableCell>
                  <TableCell align="right">{item[propValueName]}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
    </Box>
  </Card>
)};

export default ListCard;
