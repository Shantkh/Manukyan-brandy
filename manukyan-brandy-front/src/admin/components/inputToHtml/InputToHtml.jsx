import React from 'react'
import ReactQuill from 'react-quill';

const modules = {
  toolbar: [
    [{ 'header': [1,2,3,4,5,6,false] }],
    [{size: []}],
    ['bold', 'italic', 'underline', 'strike'],
    ['clean']
  ],
  clipboard: {
    matchVisual: true,
  }
}

const InputToHtml = ({value, setValue, placeholder,error }) => {
  return (
    <div className={error ? 'input-to-html error' : 'input-to-html'}>
      <ReactQuill value={value} onChange={setValue} theme='snow' modules={modules} placeholder={placeholder} />
      {error && <p className="error-message">{error}</p>}
    </div>
  )
}

export default InputToHtml