
export const HoursOfTheDay = {
  HOUR_0: '0',
  HOUR_9: '09:00',
  HOUR_10: '10:00',
  HOUR_11: '11:00',
  HOUR_12: '12:00',
  HOUR_13: '13:00',
  HOUR_14: '14:00',
  HOUR_15: '15:00',
  HOUR_16: '16:00',
  HOUR_17: '17:00',
  HOUR_18: '18:00',
};