import React, {useEffect, useState} from 'react';
import {WeekDaysEnum} from "./WeekDaysEnum";
import {ToastContainer} from 'react-toastify';
import {monthsEnum} from "./monthsEnum";
import {HoursOfTheDay} from "./HoursOfTheDay";
import './createTourStule.css'
import {toast} from 'react-toastify';
import {
    Box,
    Button,
    Dialog, DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    FormHelperText,
    Grid,
    InputLabel, Paper
} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import FilesMultiselect from "../banners/FileMultiselect";



const ToursUpdateComponent = () => {
    const [selectedFiles, setSelectedFiles] = useState([]);
    const [tourData, setTourData] = useState(null);
    const [englishTitle, setEnglishTitle] = useState('');
    const [englishId, setEnglishId] = useState('');
    const [armenianId, setArmenianId] = useState('');
    const [armenianTitle, setArmenianTitle] = useState('');
    const [englishShortContent, setEnglishShortContent] = useState('');
    const [armenianShortContent, setArmenianShortContent] = useState('');
    const [englishLongContent, setEnglishLongContent] = useState('');
    const [armenianLongContent, setArmenianLongContent] = useState('');
    const [selectedDay, setSelectedDay] = useState(null);
    const [selectedMonth, setSelectedMonth] = useState(null);
    const [tourDateData, setTourDateData] = useState();
    const [selectImageError, setSelectImageError] = useState(true);
    const [open, setOpen] = useState(false);
    const durationOptions = [0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0];
    const [availableDates, setAvailableDates] = useState([]);

    useEffect(() => {

        const fetchTourData = async () => {
            const tourId = localStorage.getItem('editingTourId');
            const headers = new Headers();
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
            headers.append('Content-Type', 'application/json');
            try {
                const response = await fetch(`admin/tours/${tourId}`, {
                    method: 'GET',
                    headers: headers,
                });
                const data = await response.json();
                setTourData(data.data);
                setSelectedDay(data.data.tourDay); // Set selected day
                setSelectedMonth(data.data.tourMonth); // Set selected month
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchTourData();
    }, []);


    useEffect(() => {
        generateAvailableDates(); // Generate available dates when component mounts
    }, [tourData]);

    useEffect(() => {
        const fetchTourData = async () => {
            const tourId = localStorage.getItem('editingTourId');
            const headers = new Headers();
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
            headers.append('Content-Type', 'application/json');
            try {
                const response = await fetch(  `admin/tours/${tourId}`, {
                    method: 'GET',
                    headers: headers,
                });
                const data = await response.json();

                setTourDateData(data.data.dateOfTour);
                setTourData(data.data);

            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };

        fetchTourData();
    }, []);

    useEffect(() => {
        if (tourData) {
            const englishLocalization = tourData.tourLocalization.find(localization => localization.locale === 'en');
            const armenianLocalization = tourData.tourLocalization.find(localization => localization.locale === 'hy');

            if (englishLocalization) {
                setEnglishId(englishLocalization.id);
                setEnglishTitle(englishLocalization.htmlTitle);
                setEnglishShortContent(englishLocalization.htmlShortContent);
                setEnglishLongContent(englishLocalization.htmlLongContent);
            }

            if (armenianLocalization) {
                setArmenianId(armenianLocalization.id);
                setArmenianTitle(armenianLocalization.htmlTitle);
                setArmenianShortContent(armenianLocalization.htmlShortContent);
                setArmenianLongContent(armenianLocalization.htmlLongContent);
            }
            setSelectedFiles(tourData.tourImages);
        }
    }, [tourData]);

    const generateAvailableDates = () => {
        if (!tourData || !tourData.tourMonth || !tourData.tourDay) {
            return;
        }

        const currentDate = new Date();
        const currentYear = currentDate.getFullYear();
        const selectedMonthIndex = monthsEnum.findIndex(month => month.key === tourData.tourMonth);
        const enumValueToString = Object.keys(WeekDaysEnum);
        const selectedDayIndex = enumValueToString.findIndex(day => day === tourData.tourDay) + 1;

        const dates = [];
        const startDate = new Date(currentYear, selectedMonthIndex, 1);
        const endDate = new Date(currentYear, selectedMonthIndex + 1, 0);

        for (let date = startDate; date <= endDate; date.setDate(date.getDate() + 1)) {
            if ((date.getDay()) === selectedDayIndex) {
                const formattedDate = new Date(date);
                dates.push({
                    date: formattedDate,
                    checked: false
                });
            }
        }

        // Check if the date from the backend matches with any of the available dates
        const backendDate = new Date(tourData.dateOfTour);
        const backendDateFormatted = `${backendDate.getMonth() + 1}/${backendDate.getDate()}/${backendDate.getFullYear()}`;
        const matchingDate = dates.find(date => {
            const formattedDate = `${date.date.getMonth() + 1}/${date.date.getDate()}/${date.date.getFullYear()}`;
            return formattedDate === backendDateFormatted;
        });

        if (matchingDate) {
            matchingDate.checked = true;
        }

        setAvailableDates(dates);
    };


    useEffect(() => {
        if (!tourData || !tourData.tourMonth || !tourData.tourDay) return;

        generateAvailableDates();
    }, [tourData]);

    if (!tourData) {
        return <p>Loading...</p>;
    }

    const handleCheckboxChange = (e) => {
        const {value, checked} = e.target;
        if (checked) {
            setTourData(prevState => ({
                ...prevState,
                tourDay: value
            }));
        }
    };

    const handleCheckboxMonthChange = (e, month) => {
        const {value, checked} = e.target;
        if (checked) {
            setTourData(prevState => ({
                ...prevState,
                tourMonth: value
            }));
        }
    };

    const groupDaysIntoPairs = () => {
        const daysArray = Object.entries(WeekDaysEnum);
        const pairs = [];
        for (let i = 0; i < daysArray.length; i += 2) {
            const pair = daysArray.slice(i, i + 2);
            pairs.push(pair);
        }
        return pairs;
    };

    const groupMonthsIntoRows = () => {
        const rows = [];
        for (let i = 0; i < monthsEnum.length; i += 3) {
            rows.push(monthsEnum.slice(i, i + 3));
        }
        return rows;
    };

    const handleChange = (e) => {
        const {name, value} = e.target;
        setTourData({
            ...tourData,
            [name]: value,
        });
    };

    const validateForm = () => {
        if (tourData.tourDay.length === 0) {
            alert('Please select at least one tour day.');
            return false;
        }

        // Validate Tour Month
        if (tourData.tourMonth.length === 0) {
            alert('Please select at least one tour month.');
            return false;
        }

        // Validate Tour Start Hour
        if (!tourData.tourHour.trim()) {
            alert('Please select the tour start hour.');
            return false;
        }

        // Validate Duration
        if (tourData.duration <= 0) {
            alert('Please select a valid tour duration.');
            return false;
        }

        // Validate Price
        if (tourData.price <= 0) {
            alert('Please enter a valid tour price.');
            return false;
        }

        // Validate Titles
        if (!englishTitle.trim()) {
            alert('Please enter English title.');
            return false;
        }
        if (!armenianTitle.trim()) {
            alert('Please enter Armenian title.');
            return false;
        }

        // Validate Short Contents
        if (!englishShortContent.trim()) {
            alert('Please enter English short content.');
            return false;
        }
        if (!armenianShortContent.trim()) {
            alert('Please enter Armenian short content.');
            return false;
        }

        // Validate Long Contents
        if (!englishLongContent.trim()) {
            alert('Please enter English long content.');
            return false;
        }
        if (!armenianLongContent.trim()) {
            alert('Please enter Armenian long content.');
            return false;
        }
        if (selectedFiles.length === 0) {
            setSelectImageError("please select Image.");
            return false;
        }
        if (selectedFiles.length > 1) {
            setSelectImageError("please select one image only.");
            return false;
        }

        return true;
    };

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const removeImage = (indexToRemove) => {
        setSelectedFiles((prevFiles) => prevFiles.filter((_, index) => index !== indexToRemove));
    };


    const handleSubmit = async (e) => {
        e.preventDefault();
        if (!validateForm()) {
            return;
        }

        const tourLocalization = [
            {
                id: englishId,
                locale: 'en',
                htmlLongContent: englishLongContent,
                htmlShortContent: englishShortContent,
                htmlTitle: englishTitle,
            },
            {
                id: armenianId,
                locale: 'hy',
                htmlLongContent: armenianLongContent,
                htmlShortContent: armenianShortContent,
                htmlTitle: armenianTitle,
            }
        ];
        const checkedDate = availableDates.find(date => date.checked);
        let formattedCheckedDate = null;
        if (checkedDate) {
            const nextDay = new Date(checkedDate.date);
            nextDay.setDate(nextDay.getDate() + 1); // Add one day
            formattedCheckedDate = nextDay.toISOString().split('T')[0];
        }

        const formData = {
            tourLocalization,
            tourDay: tourData.tourDay,
            tourMonth: tourData.tourMonth,
            tourHour: HoursOfTheDay[tourData.tourHour],
            duration: tourData.duration,
            price: tourData.price,
            dateOfTour: formattedCheckedDate,
            tourId: localStorage.getItem('editingTourId'),
        };

        try {
            const headers = new Headers();
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
            headers.append('Content-Type', 'application/json');
            const response = await fetch(`admin/tours/update`, {
                method: 'PUT',
                headers: headers,
                body: JSON.stringify(formData),
            });

            if (response.ok) {
                toast.success('Tour updated successfully');

            } else {
                toast.error('Error updating tour');
            }
        } catch (error) {
            console.error('Error sending data to backend:', error);
            toast.error('Error updating tour');
        }
    };

    const handleDateSelection = (index) => {
        const updatedDates = [...availableDates];
        updatedDates.forEach((date, i) => {
            date.checked = i === index;
        });
        setAvailableDates(updatedDates);
    };

    return (
        <div className="form-container">
            <form onSubmit={handleSubmit}>
                {/* Tour Day section */}
                <div className="form-row">
                    <div className="form-group">
                        <label className="form-label">Tour Day: <span
                            className='spans'>Can not select multiple days</span></label>
                        <div className="checkbox-group">
                            {groupDaysIntoPairs().map((pair, index) => (
                                <div key={index} className="checkbox-row">
                                    {pair.map(([key, value]) => (
                                        <div key={key} className="checkbox-item">
                                            <input
                                                type="checkbox"
                                                id={key}
                                                name="tourDay"
                                                value={key}
                                                checked={tourData.tourDay === key}
                                                onChange={handleCheckboxChange}
                                            />
                                            <label htmlFor={key} className="checkbox-label">{value}</label>
                                        </div>
                                    ))}
                                </div>
                            ))}
                        </div>
                    </div>
                </div>

                {/* Tour Month section */}
                <div className="form-row">
                    <div className="form-group">
                        <label className="form-label">Tour Month: <span
                            className='spans'>Can select multiple months</span></label>
                        <div className="checkbox-group">
                            {groupMonthsIntoRows().map((row, rowIndex) => (
                                <div key={rowIndex} className="checkbox-row">
                                    {row.map(({key, value}) => (
                                        <div key={key} className="checkbox-item">
                                            <input
                                                type="checkbox"
                                                id={key}
                                                name="tourMonth"
                                                value={key}
                                                checked={tourData.tourMonth.includes(key)}
                                                onChange={(e) => handleCheckboxMonthChange(e, key)}
                                            />
                                            <label htmlFor={key} className="checkbox-label">{value}</label>
                                        </div>
                                    ))}
                                </div>
                            ))}
                        </div>
                    </div>
                </div>

                {/* Available Dates section */}
                <div className="form-container">
                    <div className="form-group">
                        <p>Available Dates:</p>
                        <ul>
                            {availableDates.map((date, index) => (
                                <li key={index}>
                                    <input
                                        type="checkbox"
                                        id={`date_${index}`}
                                        checked={date.checked}
                                        onChange={() => handleDateSelection(index)}
                                    />
                                    <label htmlFor={`date_${index}`}>
                                        {date.date.toLocaleDateString()}
                                    </label>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>

                {/* Tour Start Hour and Duration section */}
                <div className="form-row">
                    <div className="form-group">
                        <label htmlFor="tourHour" className="form-label">Tour Start Hour: </label>
                        <select
                            id="tourHour"
                            name="tourHour"
                            className="form-select"
                            value={Object.keys(HoursOfTheDay).find(key => HoursOfTheDay[key] === tourData.tourHour)}
                            onChange={handleChange}
                        >
                            {Object.entries(HoursOfTheDay).map(([key, value]) => (
                                <option key={key} value={key}>
                                    {value}
                                </option>
                            ))}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="duration" className="form-label">
                            Duration:<span className='spans'>Duration of the tour 0.5 is half hour.</span>
                        </label>
                        <select
                            id="duration"
                            name="duration"
                            className="form-select"
                            value={tourData.duration}
                            onChange={handleChange}
                        >
                            {durationOptions.map(option => (
                                <option key={option} value={option}>
                                    {option}
                                </option>
                            ))}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="price" className="form-label">Price:</label>
                        <input
                            type="number"
                            id="price"
                            name="price"
                            className="form-input"
                            value={tourData.price}
                            onChange={handleChange}
                        />
                    </div>
                </div>

                {/* Tour Title section */}
                <div className="form-row-text">
                    <div className="form-group-text" data-id={tourData.tourLocalization[0].id}>
                        <label htmlFor="tourTitleArmenian" className="form-label">Tour Title Armenian:</label>
                        <div className="text-input-container">
                            <input
                                type="text"
                                id="tourTitleArmenian"
                                className="text-input"
                                value={armenianTitle}
                                onChange={(e) => setArmenianTitle(e.target.value)}
                            />
                        </div>
                    </div>
                    <div className="form-group-text" data-id={tourData.tourLocalization[1].id}>
                        <label htmlFor="tourTitleEnglish" className="form-label">Tour Title English:</label>
                        <div className="text-input-container">
                            <input
                                type="text"
                                id="tourTitleEnglish"
                                className="text-input"
                                value={englishTitle}
                                onChange={(e) => setEnglishTitle(e.target.value)}
                            />
                        </div>
                    </div>
                </div>

                {/* Tour Short content section */}
                <div className="form-row-text">
                    <div className="form-group-text">
                        <label htmlFor="tourShortArmenian" className="form-label">Tour Short content Armenian:</label>
                        <div className="text-input-container">
                    <textarea
                        type="text"
                        id="tourShortArmenian"
                        className="text-input"
                        value={armenianShortContent}
                        onChange={(e) => setArmenianShortContent(e.target.value)}
                    />
                        </div>
                    </div>
                    <div className="form-group-text">
                        <label htmlFor="tourShortEnglish" className="form-label">Tour Short content English:</label>
                        <div className="text-input-container">
                    <textarea
                        type="text"
                        id="tourShortEnglish"
                        className="text-input"
                        value={englishShortContent}
                        onChange={(e) => setEnglishShortContent(e.target.value)}
                    />
                        </div>
                    </div>
                </div>

                {/* Tour Long content section */}
                <div className="form-row-text">
                    <div className="form-group-text">
                        <label htmlFor="tourLongArmenian" className="form-label">Tour Long content Armenian:</label>
                        <div className="text-input-container">
                    <textarea
                        type="text"
                        id="tourLongArmenian"
                        className="text-input"
                        value={armenianLongContent}
                        onChange={(e) => setArmenianLongContent(e.target.value)}
                    />
                        </div>
                    </div>
                    <div className="form-group-text">
                        <label htmlFor="tourLongEnglish" className="form-label">Tour Long content English:</label>
                        <div className="text-input-container">
                    <textarea
                        type="text"
                        id="tourLongEnglish"
                        className="text-input"
                        value={englishLongContent}
                        onChange={(e) => setEnglishLongContent(e.target.value)}
                    />
                        </div>
                    </div>
                </div>
                <Paper elevation={3} sx={{marginRight: '15%', marginLeft: '15%'}}>
                    <Box sx={{padding: 5}}>
                        <Grid container spacing={2}>
                            {selectedFiles.map((image, index) => (
                                <Grid item key={index} sx={{display: 'flex', alignItems: 'center'}}>
                                    <img src={image} alt={`Image ${index}`}
                                         style={{width: '100px', height: '100px', objectFit: 'cover'}}/>
                                    <IconButton onClick={() => removeImage(index)}>
                                        <DeleteIcon/>
                                    </IconButton>
                                </Grid>
                            ))}
                        </Grid>
                        <Box sx={{padding: 5}}>
                            <Grid item xs={12} sm={3}>
                                <InputLabel>Select Slider Images</InputLabel>
                            </Grid>
                            <Grid item xs={12} sm={9}>
                                <FormControl fullWidth size='small'>
                                    <Button onClick={handleClickOpen} variant='outlined' id="images">
                                        Select Files
                                    </Button>
                                </FormControl>
                                {selectImageError && <FormHelperText error>{selectImageError}</FormHelperText>}
                            </Grid>
                        </Box>
                        <Dialog
                            open={open}
                            onClose={handleClose}
                            scroll={'paper'}
                            aria-labelledby="scroll-dialog-title"
                            aria-describedby="scroll-dialog-description"
                            maxWidth='md'
                        >
                            <DialogTitle id="scroll-dialog-title">Select files</DialogTitle>
                            <DialogContent dividers={true}>
                                <FilesMultiselect selectedFiles={selectedFiles} setSelectedFiles={setSelectedFiles}/>
                            </DialogContent>
                            <DialogActions sx={{justifyContent: 'center'}}>
                                <Button onClick={handleClose} variant='outlined'>
                                    Done
                                </Button>
                            </DialogActions>
                        </Dialog>
                        {/*<Grid item xs={12} sm={4} sx={{textAlign: 'right'}}>*/}
                        {/*    <Button onClick={handleSubmit} variant='contained'*/}
                        {/*            disabled={selectedFiles.length === 0}>*/}
                        {/*        Submit*/}
                        {/*    </Button>*/}
                        {/*</Grid>*/}
                    </Box>
                </Paper>
                {/* Submit button */}
                <div className="form-row-button">
                    <button type="submit" className="form-submit">Submit</button>
                </div>
            </form>
            <ToastContainer/>
        </div>

    );
};

export default ToursUpdateComponent;
