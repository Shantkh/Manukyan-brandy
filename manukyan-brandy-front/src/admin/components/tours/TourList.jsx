import React, {useState, useEffect} from 'react';
import {toast, ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './TourList.css';
import { useNavigate } from "react-router-dom";


const TourList = ({data, fetchDataCallback}) => {
    const [selectedMonths, setSelectedMonths] = useState([]);
    const [filteredData, setFilteredData] = useState(data);
    const [deleteConfirmationTourId, setDeleteConfirmationTourId] = useState(null);
    const months = Array.from(new Set(data.map(tour => tour.tourMonth)));
    let navigate = useNavigate();

    const handleCheckboxChange = (e) => {
        const month = e.target.value;
        if (selectedMonths.includes(month)) {
            setSelectedMonths(selectedMonths.filter(m => m !== month));
        } else {
            setSelectedMonths([...selectedMonths, month]);
        }
    };

    useEffect(() => {
        if (selectedMonths.length === 0) {
            setFilteredData(data);
        } else {
            const filtered = data.filter(tour => selectedMonths.includes(tour.tourMonth));
            setFilteredData(filtered);
        }
    }, [selectedMonths, data]);


    const openDeleteConfirmationPopup = (tourId) => {
        setDeleteConfirmationTourId(tourId);
    };

    const closeDeleteConfirmationPopup = () => {
        setDeleteConfirmationTourId(null);
    };


    const deleteItem = (tourId) => {
        const headers = new Headers();
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
        headers.append('Content-Type', 'application/json');
        fetch(`admin/tours/delete/${tourId}`, {
            method: 'DELETE',
            headers: headers,
        })
            .then(response => {
                if (!response.ok) {
                    throw new Error('Failed to delete item');
                }
                toast.success('Item deleted successfully');
                closeDeleteConfirmationPopup();
                fetchDataCallback();
            })
            .catch(error => {
                console.error('Error deleting item:', error);
                toast.error('Error deleting item');
                closeDeleteConfirmationPopup();
            });
    };

    const handleEditClick = (tourId) => {
        localStorage.setItem('editingTourId', tourId);
        navigate("/admin/tours/update")
    };

    return (
        <div>
            <div className="checkbox-container">
                {months.map((month, index) => {
                    const eventsForMonth = filteredData.filter(e => e.tourMonth === month);
                    return (
                        <label key={month} className="checkbox-label-tour">
                            <input
                                type="checkbox"
                                value={month}
                                onChange={handleCheckboxChange}
                                checked={selectedMonths.includes(month)}
                            />
                            <span className="checkbox-text">{month}</span>
                            <span className="event-count">{eventsForMonth.length}</span>
                        </label>
                    );
                })}
            </div>
            <div className="tour-container">
                {filteredData.map(tour => (
                    <div key={tour.id} className="tour-card-update">
                        <div className="tour-details-update">
                            <div className="info-title-update">Date: <span className='info-title-span'> {tour.dateOfTour}</span></div>
                            <div className="info-title-update">Day: <span className='info-title-span'> {tour.tourDay}</span></div>
                            <div className="info-title-update">Month: <span className='info-title-span'>  {tour.tourMonth}</span></div>
                            <div className="info-title-update">Hour: <span className='info-title-span'> {tour.tourHour}</span></div>
                            <div className="info-title-update">Duration: <span className='info-title-span'> {tour.duration} hours</span></div>
                            <div className="info-title-update">Price: <span className='info-title-span'> {tour.price}</span></div>
                            <br/>
                            <div className="language-content">
                                <div className="content">
                                    <div className="info-title-update">Title: <span className='info-title-span'>{tour.tourLocalization.find(loc => loc.locale === 'en').htmlTitle}</span></div>
                                    <div className="info-title-update">Short content: <span className='info-title-span'>{tour.tourLocalization.find(loc => loc.locale === 'en').htmlShortContent}</span></div>
                                    <div className="info-title-update">Long content: <span className='info-title-span'>{tour.tourLocalization.find(loc => loc.locale === 'en').htmlLongContent}</span></div>
                                </div>
                            </div>
                            <br/>
                            <div className="language-content">
                                <div className="content">
                                    <div className="content">
                                        <div className="info-title-update">Վերնագիր: <span className='info-title-span'>{tour.tourLocalization.find(loc => loc.locale === 'hy').htmlTitle}</span></div>
                                        <div className="info-title-update">Կարճ բովանդակություն: <span className='info-title-span'>{tour.tourLocalization.find(loc => loc.locale === 'hy').htmlShortContent}</span></div>
                                        <div className="info-title-update">Երկար բովանդակություն: <span className='info-title-span'>{tour.tourLocalization.find(loc => loc.locale === 'hy').htmlLongContent}</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="button-container">
                            <button className="edit-button" onClick={() => handleEditClick(tour.id)}>Edit</button>
                            <button className="delete-button"
                                    onClick={() => openDeleteConfirmationPopup(tour.id)}>Delete
                            </button>
                        </div>
                        {deleteConfirmationTourId === tour.id && (
                            <div className="popup-delete">
                                <div className="popup-content-delete">
                                    <p>Are you sure you want to delete this item?</p>
                                    <div className="button-container-delete">
                                        <button className="delete-button"
                                                onClick={closeDeleteConfirmationPopup}>Cancel
                                        </button>
                                        <button className="confirm-button-delete"
                                                onClick={() => deleteItem(tour.id)}>Confirm
                                        </button>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                ))}
            </div>
            <ToastContainer/>
        </div>
    );
};

export default TourList;
