export const monthsEnum = [
    { key: 'JANUARY', value: 'January' },
    { key: 'FEBRUARY', value: 'February' },
    { key: 'MARCH', value: 'March' },
    { key: 'APRIL', value: 'April' },
    { key: 'MAY', value: 'May' },
    { key: 'JUNE', value: 'June' },
    { key: 'JULY', value: 'July' },
    { key: 'AUGUST', value: 'August' },
    { key: 'SEPTEMBER', value: 'September' },
    { key: 'OCTOBER', value: 'October' },
    { key: 'NOVEMBER', value: 'November' },
    { key: 'DECEMBER', value: 'December' }
];