import React, {useEffect, useState} from 'react';
import './toursComponentStyle.css';
import TourList from "./TourList";


const ToursComponent = ({data }) => {
    const [toursData, setToursData] = useState([]);
    const [paginationWithSorting, setPaginationWithSorting] = useState({
        pageNumber: 0,
        contentSize: 20,
        sortDirection: 'asc',
        sortElement: 'tourMonth'
    });

    const fetchData = async () => {
        try {
            const response = await fetch(   `api/tours/all/pagination`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(paginationWithSorting)
            });
            const data = await response.json();
            setToursData(data.data);
        } catch (error) {
            console.error('Error fetching data:', error);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);


    if (!toursData || toursData.length === 0) {
        return <div>No tours available</div>;
    }

    return(
        <>
            <div>
                {toursData.length === 0 ? (
                    <div>No tours available</div>
                ) : (
                    <TourList data={toursData} fetchDataCallback={fetchData} />
                )}
            </div>
        </>
    )
}

export default ToursComponent;