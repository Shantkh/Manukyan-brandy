import React, {useEffect, useState} from 'react';
import './createTourStule.css';
import {HoursOfTheDay} from './HoursOfTheDay';
import {WeekDaysEnum} from "./WeekDaysEnum";
import {monthsEnum} from "./monthsEnum";
import {toast, ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import {
    Box,
    Button,
    Dialog, DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    FormHelperText,
    Grid,
    InputLabel, Paper
} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import FilesMultiselect from "../banners/FileMultiselect";



const CreateTour = () => {
    const [selectedFiles, setSelectedFiles] = useState([]);
    const [selectedDays, setSelectedDays] = useState([]);
    const durationOptions = [0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0];
    const [englishTitle, setEnglishTitle] = useState('');
    const [armenianTitle, setArmenianTitle] = useState('');
    const [englishShortContent, setEnglishShortContent] = useState('');
    const [armenianShortContent, setArmenianShortContent] = useState('');
    const [englishLongContent, setEnglishLongContent] = useState('');
    const [armenianLongContent, setArmenianLongContent] = useState('');
    const [isOneTimeTour, setIsOneTimeTour] = useState(false);
    const [selectedDate, setSelectedDate] = useState(null);
    const [selectImageError, setSelectImageError] = useState(true);

    const handleOneTimeTourChange = (e) => {
        setIsOneTimeTour(e.target.checked);
    };

    const handleDateChange = (date) => {
        setSelectedDate(date);
    };

    const [tourData, setTourData] = useState({
        tourDay: [],
        tourMonth: [],
        tourHour: '',
        duration: 0.0,
        price: 0,
    });

    const handleCheckboxChange = (event) => {
        const {value, checked} = event.target;
        if (checked) {
            setSelectedDays([...selectedDays, value]);
        } else {
            setSelectedDays(selectedDays.filter(day => day !== value));
        }
    };

    const handleChange = (e) => {
        const {name, value} = e.target;
        setTourData({
            ...tourData,
            [name]: value,
        });
    };


    const handleSubmit = async (e) => {
        e.preventDefault();
        if (!validateForm()) {
            return;
        }
        const tourLocalization = [
            {
                locale: 'en',
                htmlLongContent: englishLongContent,
                htmlShortContent: englishShortContent,
                htmlTitle: englishTitle,
            },
            {
                locale: 'hy',
                htmlLongContent: armenianLongContent,
                htmlShortContent: armenianShortContent,
                htmlTitle: armenianTitle,
            }
        ];

        const toursRequest = {
            tourLocalization,
            tourDay: selectedDays,
            tourMonth: tourData.tourMonth,
            tourHour: tourData.tourHour,
            duration: parseFloat(tourData.duration),
            price: parseFloat(tourData.price),
            tourDate: selectedDate,
            image: selectedFiles
        }


        try {
            const headers = new Headers();
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
            headers.append('Content-Type', 'application/json');
            let url =  `admin/tours/save`;
            if (isOneTimeTour) {
                url = `admin/tours/save/one`;
            }
            const response = await fetch(url, {
                method: 'POST',
                headers: headers,
                body: JSON.stringify(toursRequest)
            });

            if (!response.ok) {
                throw new Error('Failed to save tour data');
            }

            const responseData = await response.json();
            toast.success('Tour data saved successfully', {
                autoClose: 3000, // 3 seconds
                onClose: () => window.location.reload() // Reload page after toast is closed
            });
        } catch (error) {
            console.error('Error saving tour data:', error.message);
            toast.error('Failed to save tour data');
        }
    };

    const validateForm = () => {
        if (selectedDays.length === 0 && !isOneTimeTour) {
            alert('Please select at least one tour day.');
            return false;
        }

        // Validate Tour Month
        if (tourData.tourMonth.length === 0 && !isOneTimeTour) {
            alert('Please select at least one tour month.');
            return false;
        }

        // Validate Tour Start Hour
        if (!tourData.tourHour.trim()) {
            alert('Please select the tour start hour.');
            return false;
        }

        // Validate Duration
        if (tourData.duration <= 0) {
            alert('Please select a valid tour duration.');
            return false;
        }

        // Validate Price
        if (tourData.price <= 0) {
            alert('Please enter a valid tour price.');
            return false;
        }

        // Validate Titles
        if (!englishTitle.trim()) {
            alert('Please enter English title.');
            return false;
        }
        if (!armenianTitle.trim()) {
            alert('Please enter Armenian title.');
            return false;
        }

        // Validate Short Contents
        if (!englishShortContent.trim()) {
            alert('Please enter English short content.');
            return false;
        }
        if (!armenianShortContent.trim()) {
            alert('Please enter Armenian short content.');
            return false;
        }

        // Validate Long Contents
        if (!englishLongContent.trim()) {
            alert('Please enter English long content.');
            return false;
        }
        if (!armenianLongContent.trim()) {
            alert('Please enter Armenian long content.');
            return false;
        }
        if (selectedFiles.length === 0) {
            setSelectImageError("please select Image.");
            return false;
        }
        if(selectedFiles.length > 1){
            setSelectImageError("please select one image only.");
            return false;
        }
        return true;
    };

    const removeImage = (indexToRemove) => {
        setSelectedFiles((prevFiles) => prevFiles.filter((_, index) => index !== indexToRemove));
    };

    const groupDaysIntoPairs = () => {
        const daysArray = Object.entries(WeekDaysEnum);
        const pairs = [];
        for (let i = 0; i < daysArray.length; i += 2) {
            const pair = daysArray.slice(i, i + 2);
            pairs.push(pair);
        }
        return pairs;
    };
    const [open, setOpen] = useState(false);
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const groupMonthsIntoRows = () => {
        const rows = [];
        for (let i = 0; i < monthsEnum.length; i += 3) {
            rows.push(monthsEnum.slice(i, i + 3));
        }
        return rows;
    };

    const handleCheckboxMonthChange = (event, selectedMonth) => {
        const isChecked = event.target.checked;
        const updatedMonths = isChecked
            ? [...tourData.tourMonth, selectedMonth] // Add the selected month
            : tourData.tourMonth.filter(month => month !== selectedMonth); // Remove the selected month
        setTourData({...tourData, tourMonth: updatedMonths});
    };

    return (
        <div className="form-container">
            <form onSubmit={handleSubmit}>
                <div className="form-row">
                    <div className="form-group">
                        <input
                            type="checkbox"
                            id="oneTimeTour"
                            name="oneTimeTour"
                            checked={isOneTimeTour}
                            onChange={handleOneTimeTourChange}
                        />
                        <label htmlFor="oneTimeTour" className="form-label-one">
                            One-Time Tour
                        </label>
                    </div>
                    <div className="form-group">
                        {isOneTimeTour && (
                            <div>
                                <label htmlFor="tourDate" className="form-label-one">
                                    Tour Date:
                                </label>
                                <DatePicker
                                    id="tourDate"
                                    selected={selectedDate}
                                    onChange={handleDateChange}
                                    dateFormat="MM/dd/yyyy"
                                />
                            </div>
                        )}

                    </div>
                </div>
                {!isOneTimeTour && (
                    <div className="form-row">
                        <div className="form-group">
                            <label className="form-label">Tour Day: <span
                                className='spans'>Can select multiple days</span></label>
                            <div className="checkbox-group">
                                {groupDaysIntoPairs().map((pair, index) => (
                                    <div key={index} className="checkbox-row">
                                        {pair.map(([key, value]) => (
                                            <div key={key} className="checkbox-item">
                                                <input
                                                    type="checkbox"
                                                    id={key}
                                                    name="tourDay"
                                                    value={key}
                                                    checked={selectedDays.includes(key)}
                                                    onChange={handleCheckboxChange}
                                                />
                                                <label htmlFor={key} className="checkbox-label">{value}</label>
                                            </div>
                                        ))}
                                    </div>
                                ))}
                            </div>

                        </div>
                        <div className="form-group">

                            <label className="form-label">Tour Month: <span
                                className='spans'>Can not select multiple months</span></label>

                            <div className="checkbox-group">
                                {groupMonthsIntoRows().map((row, rowIndex) => (
                                    <div key={rowIndex} className="checkbox-row">
                                        {row.map(({key, value}) => (
                                            <div key={key} className="checkbox-item">
                                                <input
                                                    type="checkbox"
                                                    id={key}
                                                    name="tourMonth"
                                                    value={key}
                                                    checked={tourData.tourMonth.includes(key)}
                                                    onChange={(e) => handleCheckboxMonthChange(e, key)} // Pass the key to handleCheckboxChange
                                                />
                                                <label htmlFor={key} className="checkbox-label">{value}</label>
                                            </div>
                                        ))}
                                    </div>
                                ))}
                            </div>

                        </div>
                    </div>
                )}
                <div className="form-row">
                    <div className="form-group">

                        <label htmlFor="tourHour" className="form-label">Tour Start Hour: </label>

                        <select id="tourHour" name="tourHour" className="form-select" value={tourData.tourHour}
                                onChange={handleChange}>
                            {Object.entries(HoursOfTheDay).map(([key, value]) => (
                                <option key={key} value={value}>
                                    {value}
                                </option>
                            ))}
                        </select>

                    </div>
                    <div className="form-group">
                        <label htmlFor="duration" className="form-label">
                            Duration:<span className='spans'>Duration of the tour 0.5 is half hour.</span>
                        </label>

                        <select
                            id="duration"
                            name="duration"
                            className="form-select"
                            value={tourData.duration}
                            onChange={handleChange}
                        >
                            {durationOptions.map(option => (
                                <option key={option} value={option}>
                                    {option}
                                </option>
                            ))}
                        </select>

                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="price" className="form-label">Price:</label>

                    <input type="number" id="price" name="price" className="form-input" value={tourData.price}
                           onChange={handleChange}/>

                </div>
                <div className="form-row-text">
                    <div className="form-group-text">
                        <label htmlFor="tourTitleArmenian" className="form-label">Tour Title Armenian:</label>

                        <div className="text-input-container">
                            <input type="text" id="tourTitleArmenian" className="text-input" value={armenianTitle}
                                   onChange={(e) => setArmenianTitle(e.target.value)}/>

                        </div>
                    </div>
                    <div className="form-group-text">
                        <label htmlFor="tourTitleEnglish" className="form-label">Tour Title English:</label>

                        <div className="text-input-container">
                            <input type="text" id="tourTitleEnglish" className="text-input" value={englishTitle}
                                   onChange={(e) => setEnglishTitle(e.target.value)}/>

                        </div>
                    </div>
                </div>
                <div className="form-row-text">
                    <div className="form-group-text">
                        <label htmlFor="tourShortArmenian" className="form-label">Tour Short content Armenian:</label>
                        <div className="text-input-container">
                            <textarea type="text" id="tourShortArmenian" className="text-input"
                                      value={armenianShortContent}
                                      onChange={(e) => setArmenianShortContent(e.target.value)}/>
                        </div>

                    </div>
                    <div className="form-group-text">
                        <label htmlFor="tourShortEnglish" className="form-label">Tour Short content English:</label>

                        <div className="text-input-container">
                            <textarea type="text" id="tourShortEnglish" className="text-input"
                                      value={englishShortContent}
                                      onChange={(e) => setEnglishShortContent(e.target.value)}/>
                        </div>

                    </div>
                </div>
                <div className="form-row-text">
                    <div className="form-group-text">
                        <label htmlFor="tourLongArmenian" className="form-label">Tour Long content Armenian:</label>

                        <div className="text-input-container">
                            <textarea type="text" id="tourLongArmenian" className="text-input"
                                      value={armenianLongContent}
                                      onChange={(e) => setArmenianLongContent(e.target.value)}/>

                        </div>
                    </div>
                    <div className="form-group-text">
                        <label htmlFor="tourLongEnglish" className="form-label">Tour Long content English:</label>

                        <div className="text-input-container">
                            <textarea type="text" id="tourLongEnglish" className="text-input" value={englishLongContent}
                                      onChange={(e) => setEnglishLongContent(e.target.value)}/>
                        </div>

                    </div>
                </div>
                <Paper elevation={3} sx={{marginRight: '15%', marginLeft: '15%'}}>
                    <Box sx={{padding: 5}}>
                        <Grid container spacing={2}>
                            {selectedFiles.map((image, index) => (
                                <Grid item key={index} sx={{display: 'flex', alignItems: 'center'}}>
                                    <img src={image} alt={`Image ${index}`}
                                         style={{width: '100px', height: '100px', objectFit: 'cover'}}/>
                                    <IconButton onClick={() => removeImage(index)}>
                                        <DeleteIcon/>
                                    </IconButton>
                                </Grid>
                            ))}
                        </Grid>
                        <Box sx={{padding: 5}}>
                            <Grid item xs={12} sm={3}>
                                <InputLabel>Select Slider Images</InputLabel>
                            </Grid>
                            <Grid item xs={12} sm={9} >
                                <FormControl fullWidth size='small'>
                                    <Button onClick={handleClickOpen} variant='outlined' id="images">
                                        Select Files
                                    </Button>
                                </FormControl>
                                {selectImageError && <FormHelperText error>{selectImageError}</FormHelperText>}
                            </Grid>
                        </Box>
                        <Dialog
                            open={open}
                            onClose={handleClose}
                            scroll={'paper'}
                            aria-labelledby="scroll-dialog-title"
                            aria-describedby="scroll-dialog-description"
                            maxWidth='md'
                        >
                            <DialogTitle id="scroll-dialog-title">Select files</DialogTitle>
                            <DialogContent dividers={true}>
                                <FilesMultiselect selectedFiles={selectedFiles} setSelectedFiles={setSelectedFiles}/>
                            </DialogContent>
                            <DialogActions sx={{justifyContent: 'center'}}>
                                <Button onClick={handleClose} variant='outlined'>
                                    Done
                                </Button>
                            </DialogActions>
                        </Dialog>
                        {/*<Grid item xs={12} sm={4} sx={{textAlign: 'right'}}>*/}
                        {/*    <Button onClick={handleSubmit} variant='contained'*/}
                        {/*            disabled={selectedFiles.length === 0}>*/}
                        {/*        Submit*/}
                        {/*    </Button>*/}
                        {/*</Grid>*/}
                    </Box>
                </Paper>

                <div className="form-row-button">
                    <button type="submit" className="form-submit">Submit</button>
                </div>
            </form>
            <ToastContainer/>
        </div>
    );
}

export default CreateTour;