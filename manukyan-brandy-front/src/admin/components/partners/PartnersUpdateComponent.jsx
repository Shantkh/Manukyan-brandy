import React, {useEffect, useState} from "react";
import {FilePond} from "react-filepond";
import {Grid} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import {toast} from "react-toastify";



const PartnersUpdateComponent = () => {
    const [isDisabledUpload, setIsDisabledUpload] = useState(true);
    const [partnerData, setPartnerData] = useState({
        partnerNameEn: '',
        partnerNameHy: '',
        logo: null,
    });
    const [error, setError] = useState('');

    const handleInputChange = (e) => {
        const {name, value} = e.target;
        setPartnerData(prevData => ({
            ...prevData,
            [name]: value
        }));
    };

    const handleFileChange = (files) => {
        if (files.length > 0) {
            setPartnerData(prevData => ({
                ...prevData,
                logo: files[0].file
            }));
            setIsDisabledUpload(false);
        }
    };

    const removeImage = () => {
        setPartnerData(prevData => ({
            ...prevData,
            logo: null
        }));
        setIsDisabledUpload(false);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const BASE_API = `partner/update`;
        try {
            if (isDisabledUpload) {
                const headers = new Headers();
                headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
                headers.append('Content-Type', 'application/json');
                const partners = {
                    id: partnerData.id,
                    createdOn: partnerData.createdOn,
                    logo: partnerData.logo,
                    partnerNameEn: partnerData.partnerNameEn,
                    partnerNameHy: partnerData.partnerNameHy
                };
                const endpoint = BASE_API;

                const response = await fetch(endpoint, {
                    method: 'PUT',
                    headers: headers,
                    body: JSON.stringify(partners)
                });
                if (!response.ok) {

                    throw new Error('Failed to update partner');
                }
                const data = await response.json();

            } else {
                const headers = new Headers();
                headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));


                const formData = new FormData();
                formData.append('file', partnerData.logo);
                formData.append('partners', JSON.stringify({
                    id: localStorage.getItem("partnerId"),
                    partnerNameEn: partnerData.partnerNameEn,
                    partnerNameHy: partnerData.partnerNameHy
                }));
                const endpoint = `admin/partner/update/image`;

                const response = await fetch(endpoint, {
                    method: 'PUT',
                    headers: headers,
                    body: formData
                });
                if (!response.ok) {
                    throw new Error('Failed to update partner');
                }
                const data = await response.json();
            }
            toast.success("Partner updated successfully");
        } catch (error) {
            console.error('Error updating partner:', error.message);
        }
    };

    const fetchEventData = async () => {
        const partnerId = localStorage.getItem('partnerId');
        const headers = new Headers();
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
        headers.append('Content-Type', 'application/json');

        try {
            const response = await fetch(`admin/partner/${partnerId}`, {
                method: 'GET',
                headers: headers,
            });

            if (!response.ok) {
                throw new Error('Failed to fetch event data');
            }
            const data = await response.json();
            setPartnerData(data.data);
        } catch (error) {
            console.error('Error fetching event data:', error.message);
        }
    };

    useEffect(() => {
        fetchEventData();
    }, []);

    return (
        <div className="container">
            <form onSubmit={handleSubmit} encType="multipart/form-data">
                <div className="form-group">
                    <label htmlFor="partnerNameEn">Partner Name (English)</label>
                    <input type="text" id="partnerNameEn" name="partnerNameEn" value={partnerData.partnerNameEn}
                           onChange={handleInputChange} required/>
                </div>
                <div className="form-group">
                    <label htmlFor="partnerNameHy">Partner Name (Armenian)</label>
                    <input type="text" id="partnerNameHy" name="partnerNameHy" value={partnerData.partnerNameHy}
                           onChange={handleInputChange} required/>
                </div>
                <div className="form-group">
                    <label htmlFor="partnerImage">Partner Image</label>
                    <Grid container spacing={2}>
                        {partnerData.logo && (
                            <Grid item sx={{display: 'flex', alignItems: 'center'}}>
                                <img
                                    src={partnerData.logo}
                                    alt="Selected Image"
                                    style={{width: '100px', height: '100px', objectFit: 'cover'}}
                                />
                                <IconButton onClick={removeImage}>
                                    <DeleteIcon/>
                                </IconButton>
                            </Grid>
                        )}
                    </Grid>
                    {!partnerData.logo && (
                        <FilePond
                            allowMultiple={false}
                            maxFiles={1}
                            onupdatefiles={handleFileChange}
                            disabled={isDisabledUpload}
                        />
                    )}
                </div>
                {error && <div className="error">{error}</div>}
                <button type="submit">Submit</button>
            </form>
        </div>
    )
}

export default PartnersUpdateComponent;
