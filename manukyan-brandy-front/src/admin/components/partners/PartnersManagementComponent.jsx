import React, {useState, useEffect} from 'react';
import './PartnersStyle.css';
import {toast, ToastContainer} from "react-toastify";
import {useNavigate} from "react-router-dom";




const PartnersManagementComponent = () => {
    const [partners, setPartners] = useState([]);
    const [error, setError] = useState('');
    const [deleteConfirmationEventId, setDeleteConfirmationEventId] = useState(null);
    let navigate = useNavigate();

    const BASE_API =   `api/partners/all`;
    const fetchPartners = async () => {
        try {
            const response = await fetch(BASE_API);
            if (!response.ok) {
                throw new Error('Failed to fetch partners');
            }
            const data = await response.json();
            setPartners(data.data);
        } catch (error) {
            console.error('Error fetching partners:', error);
            setError('Failed to fetch partners');
        }
    };


    useEffect(() => {
        fetchPartners();
    }, []);

    const openDeleteConfirmationPopup = (eventId) => {
        setDeleteConfirmationEventId(eventId);
    };

    const deleteItem = async (partnerId) => {
        try {
            const headers = new Headers();
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
            headers.append('Content-Type', 'application/json');

            const response = await fetch(  `admin/partner/delete/${partnerId}`, {
                method: 'DELETE',
                headers: headers,
            });
            closeDeleteConfirmationPopup();
            if (response.ok) {
                toast.success('Partner deleted successfully');
                fetchPartners();
            } else {
                throw new Error('Failed to delete partner');
            }
        } catch (error) {
            console.error('Error deleting partner:', error);
            toast.error('Failed to delete partner');
        }
    };

    const closeDeleteConfirmationPopup = () => {
        setDeleteConfirmationEventId(null);
    };

    const handleEditClick = (eventId) => {
        localStorage.setItem('partnerId', eventId);
        navigate("/admin/partner/update")
    };

    return (
        <div className="tour-container">
            {partners.map((event) => (
                <div key={event.id} className="tour-card">
                    <div className="partner-details">
                            <img src={event.logo} alt="Partner Logo" className="partner-logo" />
                        <div >
                            <div className="content-partner">
                                <div className="info-title-partner">English:</div>
                                {event.partnerNameHy.replace(/<\/?[^>]+(>|$)/g, "")}
                            </div>
                        </div>
                        <div>
                            <div className="content-partner">
                                <div className="info-title-partner">Հայերեն:</div>
                                {event.partnerNameEn.replace(/<\/?[^>]+(>|$)/g, "")}
                            </div>
                        </div>
                        <div className="button-container">
                            {/*<button className="edit-button" onClick={() => handleEditClick(event.id)}>Edit</button>*/}
                            <button className="delete-button" onClick={() => openDeleteConfirmationPopup(event.id)}>Delete</button>

                        </div>
                        {deleteConfirmationEventId === event.id && (
                            <div className="popup-delete">
                                <div className="popup-content-delete">
                                    <p>Are you sure you want to delete this item?</p>
                                    <div className="button-container-delete">
                                        <button
                                            className="delete-button"
                                            onClick={closeDeleteConfirmationPopup}
                                        >
                                            Cancel
                                        </button>
                                        <button
                                            className="confirm-button-delete"
                                            onClick={() => deleteItem(event.id)}
                                        >
                                            Confirm
                                        </button>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            ))}
            <ToastContainer />
        </div>
    );
};

export default PartnersManagementComponent;