import 'filepond/dist/filepond.min.css';
import React, { useState } from 'react';
import { FilePond } from 'react-filepond';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import {toast, ToastContainer} from "react-toastify";
import {
    Box,
    Button,
    Dialog, DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    FormHelperText,
    Grid,
    InputLabel, Paper
} from "@mui/material";
import InputToHtml from "../inputToHtml/InputToHtml";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import FilesMultiselect from "../banners/FileMultiselect";



const PartnerCreateComponent = () => {

    const [open, setOpen] = useState(false);
    const [selectedFiles, setSelectedFiles] = useState([]);
    const [selectImageError, setSelectImageError] = useState(true);
    const [englishContent, setEnglishContent] = useState('');
    const [armenianContent, setArmenianContent] = useState('');

    const [englishContentError, setEnglishContentError] = useState('');
    const [armenianContentError, setArmenianContentError] = useState('');

    const validateForm = () => {
        if (!englishContent) {
            setArmenianContentError("Please fill in all required fields.");
            setSelectImageError('');
            return false;
        }
        if (!armenianContent) {
            setArmenianContentError("Please fill in all required fields.");
            setEnglishContentError('');
            setSelectImageError('');
            return false;
        }
        if (selectedFiles.length === 0 ) {
            setSelectImageError('Please select at least one image.');
            return false;
        }

        return true;
    };


    const handleSubmit = async (e) => {
        e.preventDefault();
        if (!validateForm()) {
            return;
        }

        const partnerData = {
            partnerNameHy: armenianContent,
            partnerNameEn: englishContent,
            logo: selectedFiles[0] // Assuming selectedFiles is an array of File objects
        };

        try {

            const response = await fetch(`api/partners/new`, {
                method: 'POST',
                body: JSON.stringify(partnerData),
                headers: {
                    'Authorization': 'Bearer ' + localStorage.getItem('accessToken'),
                    'Content-Type': 'application/json'
                }
            });

            if (!response.ok) {
                throw new Error('Failed to save partner');
            }

            toast.success('Partner saved successfully:');
        } catch (error) {
            console.error('Error saving partner:', error);
        }
    };

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const removeImage = (indexToRemove) => {
        setSelectedFiles((prevFiles) => prevFiles.filter((_, index) => index !== indexToRemove));
    };

    return (
        <Paper elevation={3} sx={{marginRight: '15%', marginLeft: '15%'}}>
            <Box sx={{padding: 5}}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <label htmlFor='english-first-content'>English Content</label>
                        <InputToHtml id='english-first-content' value={englishContent}
                                     setValue={setEnglishContent}
                                     placeholder='Write ENGLISH content here...' />
                        {englishContentError && <FormHelperText error>{englishContentError}</FormHelperText>}
                    </Grid>
                    <Grid item xs={12}>
                        <label htmlFor='armenian-first-content'>Armenian Content</label>
                        <InputToHtml id='armenian-first-content' value={armenianContent}
                                     setValue={setArmenianContent}
                                     placeholder='Write ARMENIAN content here...' />
                        {armenianContentError && <FormHelperText error>{armenianContentError}</FormHelperText>}

                    </Grid>
                </Grid>

                <Grid container spacing={2}>
                    {selectedFiles.map((image, index) => (
                        <Grid item key={index} sx={{display: 'flex', alignItems: 'center'}}>
                            <img src={image} alt={`Image ${index}`}
                                 style={{width: '100px', height: '100px', objectFit: 'cover'}}/>
                            <IconButton onClick={() => removeImage(index)}>
                                <DeleteIcon/>
                            </IconButton>
                        </Grid>
                    ))}
                </Grid>
                <Box sx={{padding: 5}}>
                    <Grid item xs={12} sm={3}>
                        <InputLabel>Select Slider Images</InputLabel>
                    </Grid>
                    <Grid item xs={12} sm={9}>
                        <FormControl fullWidth size='small'>
                            <Button onClick={handleClickOpen} variant='outlined' id="images">
                                Select Files
                            </Button>
                        </FormControl>
                        {selectImageError && <FormHelperText error>{selectImageError}</FormHelperText>}
                    </Grid>
                </Box>
                <Dialog
                    open={open}
                    onClose={handleClose}
                    scroll={'paper'}
                    aria-labelledby="scroll-dialog-title"
                    aria-describedby="scroll-dialog-description"
                    maxWidth='md'
                >
                    <DialogTitle id="scroll-dialog-title">Select files</DialogTitle>
                    <DialogContent dividers={true}>
                        <FilesMultiselect selectedFiles={selectedFiles} setSelectedFiles={setSelectedFiles}/>
                    </DialogContent>
                    <DialogActions sx={{justifyContent: 'center'}}>
                        <Button onClick={handleClose} variant='outlined'>
                            Done
                        </Button>
                    </DialogActions>
                </Dialog>
                <Grid item xs={12} sm={4} sx={{textAlign: 'right'}}>
                    <Button onClick={handleSubmit} variant='contained'
                            disabled={selectedFiles.length === 0}>
                        Submit
                    </Button>
                </Grid>
                <ToastContainer/>
            </Box>
        </Paper>
    );
};

export default PartnerCreateComponent;
