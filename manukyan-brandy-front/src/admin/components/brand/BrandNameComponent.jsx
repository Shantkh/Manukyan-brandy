import React, {useState} from 'react';
import {toast, ToastContainer} from 'react-toastify';
import {Box, Button, Grid, Paper} from '@mui/material';
import InputToHtml from "../inputToHtml/InputToHtml";



const BrandCreateComponent = () => {
    const [englishBrandName, setEnglishBrandName] = useState('');
    const [armenianBrandName, setArmenianBrandName] = useState('');
    const [htmlTitleError, setHtmlTitleError] = useState('');

    const validateForm = () => {
        if(englishBrandName.length > 20 || armenianBrandName.length > 20 || !englishBrandName || !armenianBrandName){
            return false;
        }
        return true;
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (!validateForm()) {
            return;
        }
        try {
            const headers = new Headers();
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
            headers.append('Content-Type', 'application/json'); // Set content type to JSON

            const payload = {
                brandNameEnglish : englishBrandName,
                brandNameArmenian : armenianBrandName,
            };

            const response = await fetch( `admin/brand/name/save`, {
                method: 'POST',
                headers: headers,
                body: JSON.stringify(payload)
            });

            if (response.ok) {
                toast.success("New band created Successfully.")
                setEnglishBrandName('');
                setArmenianBrandName('');
            } else {
                toast.warn("The brand already exists");
                console.error('Form submission failed:', response.statusText);
            }
        } catch (error) {
            console.error('Error submitting form:', error);
        }
    };

    return (
        <Paper elevation={3} sx={{ marginRight: '15%', marginLeft: '15%' }}>
            <Box sx={{padding: 5}}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <label htmlFor='english-brand'>English Brand Name</label>
                        <InputToHtml id='english-brand' value={englishBrandName}
                                     setValue={setEnglishBrandName}
                                     placeholder='Write ENGLISH content here...' error={htmlTitleError}/>
                    </Grid>
                    <Grid item xs={12}>
                        <label htmlFor='armenian-brand'>Armenian Brand Name</label>
                        <InputToHtml id='armenian-brand' value={armenianBrandName}
                                     setValue={setArmenianBrandName}
                                     placeholder='Write ARMENIAN content here...' error={htmlTitleError}/>
                    </Grid>
                </Grid>
                <Grid item xs={12} sm={4} sx={{ textAlign: 'right' }}>
                    <Button onClick={handleSubmit} variant='contained' >
                        Submit
                    </Button>
                </Grid>
                <ToastContainer/>
            </Box>
        </Paper>
    );
}

export default BrandCreateComponent;
