import React, {useEffect, useState} from 'react';
import {toast, ToastContainer} from 'react-toastify';
import {Box, Button, Grid, Paper} from '@mui/material';



const BrandNameUpdateComponent = () => {

    const [brands, setBrands] = useState([]);

    const fetchBrands = async () => {
        try {
            const response = await fetch( `admin/brand/name/get`);
            if (response.ok) {
                const data = await response.json();
                setBrands(data.data);

            } else {
                console.error('Failed to fetch brands');
            }
        } catch (error) {
            console.error('Error fetching brands:', error);
        }
    };

    useEffect(() => {
        fetchBrands();
    }, []);

    const handleSubmit = async (id) => {
        try {
            const response = await fetch(`admin/brand/name/delete/${id}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            if (response.ok) {
                toast.success("Brand deleted successfully");
                // After successful deletion, update the brands list by removing the deleted brand
                setBrands(prevBrands => prevBrands.filter(brand => brand.id !== id));
            } else {
                console.error('Failed to delete brand');
                toast.error("Failed to delete brand");
            }
        } catch (error) {
            console.error('Error deleting brand:', error);
            toast.error("Error deleting brand");
        }
    };

    return (
        <Box sx={{ margin: '0 auto', width: '80%' }}>
            {brands.map((brand) => (
                <Paper key={brand.id} elevation={3} sx={{ padding: '20px', margin: '20px 0' }}>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <label>English Brand Name</label>
                            <div dangerouslySetInnerHTML={{ __html: brand.brandNameEnglish }} />
                        </Grid>
                        <Grid item xs={12}>
                            <label>Armenian Brand Name</label>
                            <div dangerouslySetInnerHTML={{ __html: brand.brandNameArmenian }} />
                        </Grid>
                        <Grid item xs={12} sx={{ textAlign: 'right' }}>
                            <Button onClick={() => handleSubmit(brand.id)} variant='contained'>
                                Delete
                            </Button>
                        </Grid>
                    </Grid>
                </Paper>
            ))}
            <ToastContainer />
        </Box>
    )
};

export default BrandNameUpdateComponent;