import React from 'react';
import { Outlet } from 'react-router-dom';
import NavBar from './navbar/NavBar';
import { Box, ThemeProvider } from '@mui/material';
import { theme } from '../theme';
import './layout.scss'

const AdminLayout = () => {
  return (
    <Box sx={{ display: 'flex' }}>
      <ThemeProvider theme={theme}>
        <NavBar>
          <Outlet />
        </NavBar>
      </ThemeProvider>
    </Box>
  );
};

export default AdminLayout;
