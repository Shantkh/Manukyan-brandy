import { useState } from 'react';
import authProvider from '../authProvider';
import { Outlet, Navigate } from "react-router-dom";

const Protected = () => {

  let [isLoggedIn, setIsLoggedIn] = useState(true);
  const checkLogin = async () => {
    const res = await authProvider.checkAuth();
    setIsLoggedIn(res);
  }
  checkLogin();
  
  if (!isLoggedIn) {
    return <Navigate to={'admin/login'} replace />;
  } else {
    return <Outlet />;
  }

}

export default Protected

