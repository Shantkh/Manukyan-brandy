import {
    Box,
    Button,
    FormHelperText,
    Grid, Paper, Typography
} from "@mui/material";
import InputToHtml from "../inputToHtml/InputToHtml";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import {toast, ToastContainer} from "react-toastify";
import React, {useState} from "react";




const InfoComponent = (event) => {
    const [armenianContent, setArmenianContent] = useState('');
    const [armenianContentError, setArmenianContentError] = useState('');
    const [englishContent, setEnglishContent] = useState('');
    const [englishContentError, setEnglishContentError] = useState('');
    const [linkTo, setLinkTo] = useState('');
    const [linkToError, setLinkToError] = useState('');

    const handleLinkChange = (event) => {
        const value = event.target.value;
        setLinkTo(value);
    }

    const validateForm = () => {
        if (!englishContent) {
            setEnglishContentError("Please fill in all required fields.");
            setArmenianContentError('');
            setLinkToError('');
            return false;
        }
        if (!armenianContent) {
            setArmenianContentError("Please fill in all required fields.");
            setEnglishContentError('');
            setLinkToError('');
            return false;
        }
        if (!linkTo) {
            setLinkToError("PLease select link.")
            setEnglishContentError('');
            setArmenianContentError('');
            return false;
        }

        return true;
    };

    const handleSubmit = async () => {

        if (!validateForm()) {
            return;
        }

        const data = JSON.stringify({
            "link": linkTo,
            "infoEn": englishContent,
            "infoHy": armenianContent,
            "showOn": event.data
        })

        try {
            const headers = new Headers();
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
            headers.append('Content-Type', 'application/json');

            const response = await fetch(`api/info/save`, {
                method: 'POST',
                headers: headers,
                body: data
            });
            if (response.ok) {
                toast.success(" Info created.")
            }
        } catch (error) {
            toast.error(error.message);
        }
    }

    return (
        <>
            <Paper elevation={3} sx={{marginRight: '15%', marginLeft: '15%'}}>
                <Box sx={{padding: 5}}>
                    <Typography variant='h6' gutterBottom sx={{paddingBottom: 5}}>
                        Create info Content and Slider Section
                    </Typography>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <label htmlFor='english-content'>English Content</label>
                            <InputToHtml id='english-content' value={englishContent} setValue={setEnglishContent}
                                         placeholder='Write ENGLISH nontent here...'/>
                            {englishContentError && <FormHelperText error>{englishContentError}</FormHelperText>}
                        </Grid>
                        <Grid item xs={12}>
                            <label htmlFor='armenian-content'>Armenian Content</label>
                            <InputToHtml id='armenian-content' value={armenianContent} setValue={setArmenianContent}
                                         placeholder='Write ARMENIAN nontent here...'/>
                            {armenianContentError && <FormHelperText error>{armenianContentError}</FormHelperText>}
                        </Grid>

                        <Grid item xs={12}>
                            <Select
                                value={linkTo}
                                onChange={handleLinkChange}
                                displayEmpty
                                fullWidth
                                inputProps={{'aria-label': 'Select Section'}}
                            >
                                <MenuItem value="">Select Redirection</MenuItem>
                                <MenuItem value="FACTORY">Factory</MenuItem>
                                <MenuItem value="TOURS">Tours</MenuItem>
                                <MenuItem value="WINES">Wines</MenuItem>
                                <MenuItem value="EVENTS">Events</MenuItem>
                                <MenuItem value="CONTACT">ContactUs</MenuItem>
                            </Select>
                            {linkToError && <FormHelperText error>{linkToError}</FormHelperText>}
                        </Grid>
                    </Grid>
                    <Grid item xs={12} sm={5}/>
                    <Grid item xs={12} sm={4} sx={{textAlign: 'left'}}>
                        <Button onClick={handleSubmit} variant='contained'>
                            Save
                        </Button>
                    </Grid>
                </Box>
            </Paper>
            <ToastContainer/>
        </>
    )
}

export default InfoComponent;