import React, {useEffect, useState} from "react";
import {toast, ToastContainer} from "react-toastify";
import {
    Box,
    Button,
    Dialog, DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    FormHelperText,
    Grid,
    InputLabel,
    Paper
} from "@mui/material";
import InputToHtml from "../inputToHtml/InputToHtml";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import FilesMultiselect from "../banners/FileMultiselect";



const BottlingCreateComponent = () =>{
    const [englishContent, setEnglishContent] = useState('');
    const [armenianContent, setArmenianContent] = useState('');
    const [englishBrandName, setEnglishBrandName] = useState('');
    const [armenianBrandName, setArmenianBrandName] = useState('');
    const [englishContentError, setEnglishContentError] = useState('');
    const [armenianContentError, setArmenianContentError] = useState('');
    const [dropDownValueError, setDropDownValueError] = useState('');

    const [open, setOpen] = useState(false);
    const [selectedFiles, setSelectedFiles] = useState([]);
    const [dropDownValue, setDropDownValue] = useState('');
    const [imageVisible, setImageVisible] = useState('disable');
    const [brands, setBrands] = useState([]);
    const [selectImageError, setSelectImageError] = useState(true);

    const validateForm = () => {
        if (!englishContent) {
            setEnglishContentError("Please fill in all required fields.");
            setArmenianContentError('');
            setSelectImageError('');
            setDropDownValueError('');
            return false;
        }
        if (!armenianContent) {
            setArmenianContentError("Please fill in all required fields.");
            setEnglishContentError('');
            setSelectImageError('');
            setDropDownValueError('');
            return false;
        }
        if (!dropDownValue) {
            setDropDownValueError("Please fill in all required fields.");
            setArmenianContentError('')
            setEnglishContentError('');
            setSelectImageError('');
            return false;
        }
        if (selectedFiles.length === 0 && imageVisible !== 'disable') {
            setSelectImageError('Please select at least one image.');
            setDropDownValueError('');
            setArmenianContentError('')
            setEnglishContentError('');
            return false;
        }

        return true;
    };


    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleChange = (event) => {
        const value = event.target.value;
        if (value.toLowerCase() === 'title') {
            setImageVisible('disable');
            setSelectedFiles([]);
        } else {
            setImageVisible('');
        }
        setEnglishBrandName(value);
        setDropDownValue(value);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const selectedBrand = brands.find(brand => brand.brandNameEnglish === englishBrandName);
        if (!validateForm()) {
            return;
        }

        if(selectedBrand === null){
            toast.error("Please select a brand.");
            return
        }

        try {
            const headers = new Headers();
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
            headers.append('Content-Type', 'application/json'); // Set content type to JSON

            const payload = {
                armenianContent: armenianContent,
                englishContent: englishContent,
                bottlingSection: dropDownValue,
                images: selectedFiles
            };


            const response = await fetch(`admin/bottling/save`, {
                method: 'POST',
                headers: headers,
                body: JSON.stringify(payload) // Convert payload to JSON string
            });

            if (response.ok) {
                toast.success("New section created Successfully.")
                setEnglishContent('');
                setArmenianContent('');
                setEnglishBrandName('');
                setArmenianBrandName('');
                setDropDownValue('');
                setSelectedFiles([]);
            } else {
                toast.warn("The data already exist.")
                console.error('Form submission failed:', response.statusText);
            }
        } catch (error) {
            console.error('Error submitting form:', error);
        }
    };

    const removeImage = (indexToRemove) => {
        setSelectedFiles((prevFiles) => prevFiles.filter((_, index) => index !== indexToRemove));
    };

    return (
        <Paper elevation={3} sx={{marginRight: '15%', marginLeft: '15%'}}>
            <Box sx={{padding: 5}}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <label htmlFor='english-first-content'>English Content</label>
                        <InputToHtml id='english-first-content' value={englishContent}
                                     setValue={setEnglishContent}
                                     placeholder='Write ENGLISH content here...' />
                        {englishContentError && <FormHelperText error>{englishContentError}</FormHelperText>}
                    </Grid>
                    <Grid item xs={12}>
                        <label htmlFor='armenian-first-content'>Armenian Content</label>
                        <InputToHtml id='armenian-first-content' value={armenianContent}
                                     setValue={setArmenianContent}
                                     placeholder='Write ARMENIAN content here...'/>
                        {armenianContentError && <FormHelperText error>{armenianContentError}</FormHelperText>}
                    </Grid>
                </Grid>

                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Select
                            value={dropDownValue}
                            onChange={handleChange}
                            displayEmpty
                            fullWidth
                            inputProps={{'aria-label': 'Select Section'}}
                        >
                            <MenuItem value="">Select Section</MenuItem>
                            <MenuItem value="Title">Title</MenuItem>
                            <MenuItem value="Upper Section">Upper Section</MenuItem>
                            <MenuItem value="Lower Section">Lower Section</MenuItem>
                        </Select>
                        {dropDownValueError && <FormHelperText error>{dropDownValueError}</FormHelperText>}
                    </Grid>
                </Grid>

                <Grid container spacing={2}>
                    {selectedFiles.map((image, index) => (
                        <Grid item key={index} sx={{display: 'flex', alignItems: 'center'}}>
                            <img src={image} alt={`Image ${index}`}
                                 style={{width: '100px', height: '100px', objectFit: 'cover'}}/>
                            <IconButton onClick={() => removeImage(index)}>
                                <DeleteIcon/>
                            </IconButton>
                        </Grid>
                    ))}
                </Grid>
                <Box sx={{padding: 5}}>
                    <Grid item xs={12} sm={3}>
                        <InputLabel>Select Slider Images</InputLabel>
                    </Grid>
                    <Grid item xs={12} sm={9} sx={{display: imageVisible === 'disable' ? 'none' : 'block'}}>
                        <FormControl fullWidth size='small'>
                            <Button onClick={handleClickOpen} variant='outlined' id="images">
                                Select Files
                            </Button>
                        </FormControl>
                        {selectImageError && <FormHelperText error>{selectImageError}</FormHelperText>}
                    </Grid>
                </Box>
                <Dialog
                    open={open}
                    onClose={handleClose}
                    scroll={'paper'}
                    aria-labelledby="scroll-dialog-title"
                    aria-describedby="scroll-dialog-description"
                    maxWidth='md'
                >
                    <DialogTitle id="scroll-dialog-title">Select files</DialogTitle>
                    <DialogContent dividers={true}>
                        <FilesMultiselect selectedFiles={selectedFiles} setSelectedFiles={setSelectedFiles}/>
                    </DialogContent>
                    <DialogActions sx={{justifyContent: 'center'}}>
                        <Button onClick={handleClose} variant='outlined'>
                            Done
                        </Button>
                    </DialogActions>
                </Dialog>
                <Grid item xs={12} sm={4} sx={{textAlign: 'right'}}>
                    <Button onClick={handleSubmit} variant='contained'
                            disabled={selectedFiles.length === 0 && imageVisible === ''}>
                        Submit
                    </Button>
                </Grid>
                <ToastContainer/>
            </Box>
        </Paper>
    );
}

export default BottlingCreateComponent;