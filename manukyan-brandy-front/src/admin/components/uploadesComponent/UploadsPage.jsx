import PropTypes from 'prop-types';
import { useEffect, useRef, useState } from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogTitle from '@mui/material/DialogTitle';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import { FilePond, registerPlugin } from 'react-filepond';
import FilePondPluginImageExifOrientation from 'filepond-plugin-image-exif-orientation';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond/dist/filepond.min.css';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css';



const CustomTabPanel = (props) => {
  const { children, value, index, ...other } = props;

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box sx={{ display: 'flex', paddingTop: '20px' }}>{children}</Box>}
    </div>
  );
};

CustomTabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

export default function Uploads() {
  const [value, setValue] = useState(0);
  const [files, setFiles] = useState([]);
  const [data, setData] = useState(null);
  const [open, setOpen] = useState(false);
  const [deleteTemp, setDeleteTemp] = useState('');

  const handleClickOpen = (e) => {
    const originalUrl = e.target.value;
    const filename = originalUrl.substring(originalUrl.lastIndexOf('/') + 1);
    setDeleteTemp(filename);
    setOpen(true);
  };

  const handleClose = () => {
    setDeleteTemp('');
    setOpen(false);
  };



  const fetchFilesData = async () => {
    try {
      const headers = new Headers();

      const response = await fetch(  `api/files`, {
        method: 'GET',
        headers: headers,
      });
      const json = await response.json();
      setData(json.data);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchFilesData();
  }, []);

  const handleDelete = () => {
    const fetchData = async () => {
      try {
        const headers = new Headers();
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
        const response = await fetch(  `admin/files/${deleteTemp}`, {
          method: 'DELETE',
          headers: headers,
        });

        if (!response.ok) {
          throw new Error('Error: Item is not deleted!');
        }
        setDeleteTemp('');
        setOpen(false);

        // notify('Item was successfully deleted.', { autoHideDuration: 5000 });
        fetchFilesData();
      } catch (error) {
        // notify(error, { autoHideDuration: 5000 });
        console.error(error);
      } finally {
        setOpen(false);
      }
    };
    fetchData();
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  registerPlugin(FilePondPluginImageExifOrientation, FilePondPluginImagePreview);

  const filePondRef = useRef(null);

  const handleFileProcessed = () => {
    if (filePondRef.current) {
      setTimeout(() => {
        filePondRef.current.removeFiles();
      }, 3000);
      fetchFilesData();
    }
  };

  const styleUploadPanel = {
    width: '100%',
    padding: '20px',
    flexWrap: 'wrap',
    '@media (max-width: 900px)': {
      width: '100%',
      flexWrap: 'wrap',
    },
  };

  return (
    <>
      <Box sx={{ width: '100%', display: 'flex', flexWrap: 'wrap' }}>
        <Box sx={styleUploadPanel}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <Tabs value={value} onChange={handleChange} aria-label='basic tabs example'>
              <Tab label='Images' {...a11yProps(0)} />
              <Tab label='Documents' {...a11yProps(1)} />
              <Tab label='Videos' {...a11yProps(2)} />
            </Tabs>
          </Box>
          <CustomTabPanel value={value} sx={{ display: 'flex', flexWrap: 'wrap' }} index={0}>
            <Box sx={{ display: 'flex', flexWrap: 'wrap' }}>
              {data && data.hasOwnProperty('images')
                ? data.images.map((img) => {
                    const imgUrl = img;
                    return (
                      <Card key={img} sx={{ width: 220, margin: '10px' }}>
                        <CardMedia sx={{ height: 180 }} image={imgUrl} title={imgUrl} />
                        <CardContent>
                          <Typography sx={{ fontSize: '12px' }} component='div'>
                            {img}
                          </Typography>
                        </CardContent>
                        <CardActions>
                          <Button value={img} variant='outlined' onClick={handleClickOpen} startIcon={<DeleteIcon />}>
                            Delete
                          </Button>
                        </CardActions>
                      </Card>
                    );
                  })
                : 'There are no images yet.'}
            </Box>
          </CustomTabPanel>
          <CustomTabPanel value={value} sx={{ display: 'flex', flexWrap: 'wrap' }} index={1}>
            <Box sx={{ display: 'flex', flexWrap: 'wrap' }}>
              {data && data.hasOwnProperty('documents')
                ? data.documents.map((doc) => {
                    const imgUrl = doc;
                    return (
                      <Card key={doc} sx={{ width: 220, margin: '10px' }}>
                        <CardMedia sx={{ height: 120 }} image={imgUrl} title={imgUrl} />
                        <CardContent>
                          <Typography sx={{ fontSize: '12px' }} component='div'>
                            {doc}
                          </Typography>
                        </CardContent>
                        <CardActions>
                          <Button value={doc} variant='outlined' onClick={handleClickOpen} startIcon={<DeleteIcon />}>
                            Delete
                          </Button>
                        </CardActions>
                      </Card>
                    );
                  })
                : 'There are no documents yet.'}
            </Box>
          </CustomTabPanel>
          <CustomTabPanel value={value} index={2}>
            <Box sx={{ display: 'flex', flexWrap: 'wrap' }}>
              {data && data.hasOwnProperty('videos')
                ? data.videos.map((video) => {
                    const imgUrl =  video;
                    return (
                      <Card key={video} sx={{ width: 220, margin: '10px' }}>
                        <CardMedia sx={{ height: 120 }} image={imgUrl} title={imgUrl} component='video' autoPlay />
                        <CardContent>
                          <Typography sx={{ fontSize: '12px' }} component='div'>
                            {video}
                          </Typography>
                        </CardContent>
                        <CardActions>
                          <Button value={video} variant='outlined' onClick={handleClickOpen} startIcon={<DeleteIcon />}>
                            Delete
                          </Button>
                        </CardActions>
                      </Card>
                    );
                  })
                : 'There are no videos yet.'}
            </Box>
          </CustomTabPanel>
          <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby='alert-dialog-title'
            aria-describedby='alert-dialog-description'
          >
            <DialogTitle id='alert-dialog-title'>{`Are you sure you want to delete ${deleteTemp}?`}</DialogTitle>
            <DialogActions>
              <Button onClick={handleClose}>Close</Button>
              <Button onClick={handleDelete}>Delete</Button>
            </DialogActions>
          </Dialog>
        </Box>
        <Box sx={styleUploadPanel}>
          <FilePond
            files={files}
            onupdatefiles={setFiles}
            ref={filePondRef}
            allowMultiple={true}
            onprocessfiles={() => handleFileProcessed()}
            server={{
              url:  `upload`,
              method: 'POST',
              process: {
                headers: {
                  Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
                },
              },
            }}
            name='files'
          />
        </Box>
      </Box>
    </>
  );
}
