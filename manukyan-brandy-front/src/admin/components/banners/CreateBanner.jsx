import React, {useEffect, useState} from "react";
import {useLocation} from "react-router-dom";
import {toast, ToastContainer} from "react-toastify";
import {
    Box,
    Button, Dialog, DialogActions, DialogContent, DialogTitle,
    FormControl,
    FormHelperText,
    Grid,
    InputLabel,
    MenuItem,
    Paper,
    Select,
    Typography
} from "@mui/material";
import InputToHtml from "../inputToHtml/InputToHtml";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import FilesMultiselect from "./FileMultiselect";



const CreateBanner = (pageBanner) => {
    const [bannerName, setBannerName] = useState('');
    const [armenianContent, setArmenianContent] = useState('');
    const [armenianContentError, setArmenianContentError] = useState('');
    const [englishContent, setEnglishContent] = useState('');
    const [englishContentError, setEnglishContentError] = useState('');
    const [selectedFiles, setSelectedFiles] = useState([]);
    const [open, setOpen] = useState(false);
    const location = useLocation();
    const pathParts = location.pathname.split('/');
    const sectionEnum = pathParts[pathParts.length - 1];
    const [selectImageError, setSelectImageError] = useState('');

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    useEffect(() => {
        const bannerTitle = pageBanner.data;
        if (bannerTitle !== bannerName) {
            setBannerName(bannerTitle);
            setArmenianContent('');
            setEnglishContent('');
            setEnglishContentError('');
            setArmenianContentError('');
            setSelectedFiles([]);
            setSelectImageError('');
        }
    }, [pageBanner.data, bannerName]);

    const validateForm = () => {
        if (!englishContent) {
            setEnglishContentError("Please fill in all required fields.");
            setArmenianContentError('');
            setSelectImageError('');
            return false;
        }
        if (!armenianContent) {
            setArmenianContentError("Please fill in all required fields.");
            setEnglishContentError('');
            setSelectImageError('');
            return false;
        }

        if (selectedFiles.length === 0) {
            setSelectImageError("Please select at least one image.");
            setEnglishContentError('');
            setArmenianContentError('');
            return false;
        }
        setSelectImageError('');
        return true;
    };
    const handleSubmit = async () => {

        if (!validateForm()) {
            return;
        }

        const media = selectedFiles.map((file) => {
                return file
        });

        const data = JSON.stringify({
            "sliderMedia": media,  "localizations": [{
                "locale": "en", "htmlContent": englishContent,
            }, {
                "locale": "hy", "htmlContent": armenianContent,
            },]
        })

        try {
            const headers = new Headers();
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
            headers.append('Content-Type', 'application/json');
            const BASE_API =   `admin/sections/save?page=${pageBanner.data}`;
            const response = await fetch(BASE_API, {
                method: 'POST', headers: headers, body: data
            });
            if (response.ok) {
                toast.success(pageBanner.data + " Banner created.")
            }
        } catch (error) {
            toast.error(error.message);
        }
    }

    let sectionTitle = sectionEnum.toLowerCase().split('_');
    sectionTitle = sectionTitle.map((word, index) => sectionTitle[index] = word.charAt(0).toUpperCase() + word.slice(1));
    sectionTitle = sectionTitle.join(' ');

    const removeImage = (indexToRemove) => {
        setSelectedFiles((prevFiles) => prevFiles.filter((_, index) => index !== indexToRemove));
    };


    return (<>
        <Paper elevation={3} sx={{marginRight: '15%', marginLeft: '15%'}}>
            <Box sx={{padding: 5}}>
                <Typography variant='h6' gutterBottom sx={{paddingBottom: 5}}>
                    Create {sectionTitle} Content and Slider Section
                </Typography>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <label htmlFor='english-content'>English Content</label>
                        <InputToHtml id='english-content' value={englishContent} setValue={setEnglishContent}
                                     placeholder='Write ENGLISH nontent here...'/>
                        {englishContentError && <FormHelperText error>{englishContentError}</FormHelperText>}
                    </Grid>
                    <Grid item xs={12}>
                        <label htmlFor='armenian-content'>Armenian Content</label>
                        <InputToHtml id='armenian-content' value={armenianContent} setValue={setArmenianContent}
                                     placeholder='Write ARMENIAN nontent here...'/>
                        {armenianContentError && <FormHelperText error>{armenianContentError}</FormHelperText>}
                    </Grid>

                    <Grid container spacing={2}>
                        {selectedFiles.map((image, index) => (
                            <Grid item key={index} sx={{display: 'flex', alignItems: 'center'}}>
                                <img src={image} alt={`Image ${index}`}
                                     style={{width: '100px', height: '100px', objectFit: 'cover'}}/>
                                <IconButton onClick={() => removeImage(index)}>
                                    <DeleteIcon/>
                                </IconButton>
                            </Grid>))}
                    </Grid>
                    <Grid item xs={12} sm={3}>
                        <InputLabel
                            sx={{
                                display: 'flex', justifyContent: 'center', fontWeight: 700,
                            }}
                        >
                            Select Slider Images
                        </InputLabel>
                    </Grid>
                    <Grid item xs={12} sm={9}>
                        <FormControl fullWidth size='small'>
                            <Button onClick={handleClickOpen} variant='outlined'>
                                Select Files
                            </Button>
                        </FormControl>
                        {selectImageError && <FormHelperText error>{selectImageError}</FormHelperText>}
                    </Grid>
                    <Grid item xs={12} sm={5}/>
                    <Grid item xs={12} sm={4}>
                        <Button onClick={handleSubmit} variant='contained'>
                            Save
                        </Button>
                    </Grid>
                </Grid>
            </Box>
        </Paper>
        <Dialog
            open={open}
            onClose={handleClose}
            scroll={'paper'}
            aria-labelledby="scroll-dialog-title"
            aria-describedby="scroll-dialog-description"
            maxWidth='md'
        >
            <DialogTitle id="scroll-dialog-title">Select files</DialogTitle>
            <DialogContent dividers={true}>
                <FilesMultiselect selectedFiles={selectedFiles} setSelectedFiles={setSelectedFiles}/>
            </DialogContent>
            <DialogActions sx={{justifyContent: 'center'}}>
                <Button onClick={handleClose} variant='outlined' sx={{fontSize: 'large', my: '10px'}}>
                    Done
                </Button>
            </DialogActions>
        </Dialog>
        <ToastContainer/>
    </>);
};

export default CreateBanner;