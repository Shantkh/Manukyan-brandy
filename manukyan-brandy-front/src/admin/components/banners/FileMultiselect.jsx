import PropTypes from 'prop-types';
import { useEffect, useRef, useState } from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import { FilePond, registerPlugin } from 'react-filepond';
import FilePondPluginImageExifOrientation from 'filepond-plugin-image-exif-orientation';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import 'filepond/dist/filepond.min.css';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.min.css';


const CustomTabPanel = (props) => {
  const { children, value, index, ...other } = props;

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box sx={{ display: 'flex', paddingTop: '20px' }}>{children}</Box>}
    </div>
  );
};

CustomTabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const primaryColor = '#9B2242';
const secondaryColor = 'rgba($primaryColor, 0.2)'

export default function FilesMultiselect( {selectedFiles, setSelectedFiles} ) {
  const [value, setValue] = useState(0);
  const [files, setFiles] = useState([]);
  const [data, setData] = useState(null);

  const handleSelect = (e) => {
    setSelectedFiles(selectedFiles => {
      if(e.target.value === undefined)
        return [...selectedFiles]
      if(selectedFiles.includes(e.target.value)) {
        if(selectedFiles.length === 1) {
          return [];
        }
        return selectedFiles.filter(file => file !== e.target.value)
      } else {

        return [...selectedFiles, e.target.value]
      }
    });
  };

  const fetchFilesData = async () => {
    try {
      const headers = new Headers();
      headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));

      const response = await fetch(  `api/files`, {
        method: 'GET',
        headers: headers,
      });
      const json = await response.json();
      setData(json.data);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchFilesData();
  }, []);


  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  registerPlugin(FilePondPluginImageExifOrientation, FilePondPluginImagePreview);

  const filePondRef = useRef(null);

  const handleFileProcessed = () => {
    if (filePondRef.current) {
      setTimeout(() => {
        filePondRef.current.removeFiles();
      }, 3000);
      fetchFilesData();
    }
  };

  const styleUploadPanel = {
    width: '100%',
    padding: '20px',
    flexWrap: 'wrap',
    '@media (max-width: 900px)': {
      width: '100%',
      flexWrap: 'wrap',
    },
  };

  return (
    <>
      <Box sx={{ width: '100%', display: 'flex', flexWrap: 'wrap' }}>
        <Box sx={styleUploadPanel}>
          <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
            <Tabs value={value} onChange={handleChange} aria-label='basic tabs example'>
              <Tab label='Images' {...a11yProps(0)} />
              <Tab label='Documents' {...a11yProps(1)} />
              <Tab label='Videos' {...a11yProps(2)} />
            </Tabs>
          </Box>
          <CustomTabPanel value={value} sx={{ display: 'flex', flexWrap: 'wrap' }} index={0}>
            <Box sx={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
              {data && data.hasOwnProperty('images')
                ? data.images.map((img) => {
                    const imgUrl = img;
                    return (
                      <Card key={img} sx={{ width: 220, margin: '10px' }}>
                        <CardMedia sx={{ height: 180 }} image={imgUrl} title={img} />
                        <CardContent>
                          <Typography sx={{ fontSize: '12px' }} component='div'>
                            {img}
                          </Typography>
                        </CardContent>
                        <CardActions>
                          <Button value={imgUrl} variant='outlined' onClick={handleSelect} startIcon={ selectedFiles && selectedFiles.includes(imgUrl) ? <CheckCircleOutlineIcon /> : <RadioButtonUncheckedIcon /> }
                            sx={{ color: selectedFiles.includes(imgUrl) ? 'white' : primaryColor, background: selectedFiles.includes(imgUrl) ? primaryColor : 'white',
                             '&:hover': { color: primaryColor }}}
                          >
                            Select
                          </Button>
                        </CardActions>
                      </Card>
                    );
                  })
                : 'There are no images yet.'}
            </Box>
          </CustomTabPanel>
          <CustomTabPanel value={value} sx={{ display: 'flex', flexWrap: 'wrap' }} index={1}>
            <Box sx={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
              {data && data.hasOwnProperty('documents')
                ? data.documents.map((doc) => {
                    const docURL = doc;
                    return (
                      <Card key={doc} sx={{ width: 220, margin: '10px' }}>
                        <CardMedia sx={{ height: 120 }} image={docURL} title={doc} />
                        <CardContent>
                          <Typography sx={{ fontSize: '12px' }} component='div'>
                            {doc}
                          </Typography>
                        </CardContent>
                        <CardActions>
                          <Button docURL={docURL} variant='outlined' onClick={handleSelect} startIcon={ selectedFiles && selectedFiles.includes(docURL) ? <CheckCircleOutlineIcon /> : <RadioButtonUncheckedIcon /> } 
                            sx={{ color: selectedFiles.includes(docURL) ? 'white' : primaryColor, background: selectedFiles.includes(docURL) ? primaryColor : 'white',
                             '&:hover': { color: primaryColor }}}                          >
                            Select
                          </Button>
                        </CardActions>
                      </Card>
                    );
                  })
                : 'There are no documents yet.'}
            </Box>
          </CustomTabPanel>
          <CustomTabPanel value={value} index={2}>
            <Box sx={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
              {data && data.hasOwnProperty('videos')
                ? data.videos.map((video) => {
                    const videoURL = video;
                    return (
                      <Card key={video} sx={{ width: 220, margin: '10px' }}>
                        <CardMedia sx={{ height: 120 }} image={videoURL} title={video} component='video' autoPlay />
                        <CardContent>
                          <Typography sx={{ fontSize: '12px' }} component='div'>
                            {video}
                          </Typography>
                        </CardContent>
                        <CardActions>
                          <Button value={videoURL} variant='outlined' onClick={handleSelect} startIcon={ selectedFiles && selectedFiles.includes(videoURL) ? <CheckCircleOutlineIcon /> : <RadioButtonUncheckedIcon /> } 
                            sx={{ color: selectedFiles.includes(videoURL) ? 'white' : primaryColor, background: selectedFiles.includes(videoURL) ? primaryColor : 'white',
                             '&:hover': { color: primaryColor }}}                          >
                            Select
                          </Button>
                        </CardActions>
                      </Card>
                    );
                  })
                : 'There are no videos yet.'}
            </Box>
          </CustomTabPanel>
        </Box>
        <Box sx={styleUploadPanel}>
          <FilePond
            files={files}
            onupdatefiles={setFiles}
            ref={filePondRef}
            allowMultiple={true}
            onprocessfiles={() => handleFileProcessed()}
            server={{
              url: `upload`,
              method: 'POST',
              process: {
                headers: {
                  Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
                },
              },
            }}
            name='files'
          />
        </Box>
      </Box>
    </>
  );
}
