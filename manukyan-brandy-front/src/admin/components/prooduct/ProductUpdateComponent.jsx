import {useEffect, useState} from "react";
import InputToHtml from "../inputToHtml/InputToHtml";
import FilesMultiselect from "../banners/FileMultiselect";
import {
    Box,
    Button,
    Dialog, DialogActions, DialogContent,
    DialogTitle,
    FormControl,
    Grid,
    InputLabel,
    Paper,
    TextField,
    Typography
} from "@mui/material";
import {LocalizationProvider} from "@mui/x-date-pickers/LocalizationProvider";
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import MenuItem from "@mui/material/MenuItem";
import {toast, ToastContainer} from "react-toastify";
import Select from '@mui/material/Select';
import { FormHelperText } from '@mui/material';



const ProductUpdateComponent =() =>{
    const [brands, setBrands] = useState([]);

    const [productData, setProductData] = useState(null);
    const [open, setOpen] = useState(false);
    const [englishTitle, setEnglishTitle] = useState('');
    const [armenianTitle, setArmenianTitle] = useState('');
    const [englishDescription, setEnglishDescription] = useState('');
    const [armenianDescription, setArmenianDescription] = useState('');
    const [englishBrand, setEnglishBrand] = useState('');
    const [armenianBrand, setArmenianBrand] = useState('');
    const [englishProductType, setEnglishProductType] = useState('');
    const [armenianProductType, setArmenianProductType] = useState('');
    const [selectedFiles, setSelectedFiles] = useState([]);
    const [htmlTitleError, setHtmlError] = useState('');
    const [htmlImageTitleError] = useState('');
    const [htmlDateTitleError, setDateHtmlTitleError] = useState('');
    const [dateYear, setDateYear] = useState(null);
    const currentYear = new Date().getFullYear();
    const years = Array.from({ length: 150 }, (_, index) => currentYear - index);
    const [dropDownError] = useState('');

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };


    const handleBrandDropdownChange = (event) => {
        const selectedBrandName = event.target.value;
        setEnglishBrand(selectedBrandName);
        const selectedBrandObject = brands.find(brand => brand.brandNameEnglish === selectedBrandName);

        const armenianBrandName = selectedBrandObject ? selectedBrandObject.brandNameArmenian : '';
        setArmenianBrand(armenianBrandName);
    };

    const removeImage = (indexToRemove) => {
        setSelectedFiles((prevFiles) => prevFiles.filter((_, index) => index !== indexToRemove));
    };
    const BASE_API = `admin/brand/name/get`;
    const fetchBrands = async () => {
        try {
            const response = await fetch(BASE_API);
            if (response.ok) {
                const data = await response.json();
                setBrands(data.data);

            } else {
                console.error('Failed to fetch brands');
                toast.error("Failed to fetch brands");
            }
        } catch (error) {
            console.error('Error fetching brands:', error);
            toast.error("Error fetching brands");
        }
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            if (!productData) {
                throw new Error('Product data is not available');
            }

            const formData = {
                id: productData.id,
                createdOn: productData.createdOn,
                productImages: selectedFiles,
                pageTitle: [
                    { id: productData.pageTitle[0].id, locale: 'en', htmlContent: englishTitle },
                    { id: productData.pageTitle[1].id, locale: 'hy', htmlContent: armenianTitle }
                ],
                brand: [
                    { id: productData.brandName.id,  brandNameEnglish: englishBrand, brandNameArmenian : armenianBrand },
                ],
                description: [
                    { id: productData.description[0].id, locale: 'en', htmlContent: englishDescription },
                    { id: productData.description[1].id, locale: 'hy', htmlContent: armenianDescription }
                ],
                productDate: dateYear,
                productType: [
                    { id: productData.productType[0].id, locale: 'en', htmlContent: englishProductType },
                    { id: productData.productType[1].id, locale: 'hy', htmlContent: armenianProductType }
                ]
            };

            const response = await fetch(`admin/product/update`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(formData)
            });

            if (!response.ok) {
                throw new Error('Failed to update product');
            }

            toast.success('Product updated successfully', {
                autoClose: 3000,
            });
        } catch (error) {
            console.error('Error updating product:', error.message);
            toast.error('Failed to update product. Please try again later.');
        }
    };


    const handleYearChange = (event) => {
        setDateYear(event.target.value);
        setDateHtmlTitleError('');
    };


    const fetchProductData = async () => {
        const eventId = localStorage.getItem('editingProductId');
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        try {
            const response = await fetch(`api/products/${eventId}`, {
                method: 'GET',
                headers: headers,
            });
            if (!response.ok) {
                throw new Error('Failed to fetch product data');
            }
            const data = await response.json();

            const {
                id,
                createdOn,
                productImages,
                pageTitle,
                brandName,
                description,
                productDate,
                productType
            } = data.data;

            setProductData({
                id: id,
                createdOn: createdOn,
                productImages: productImages,
                pageTitle: pageTitle,
                brandName: brandName,
                description: description,
                productDate: productDate,
                productType: productType
            });

            setEnglishTitle(pageTitle.find(item => item.locale === 'en').htmlContent);
            setArmenianTitle(pageTitle.find(item => item.locale === 'hy').htmlContent);

            setEnglishDescription(description.find(item => item.locale === 'en').htmlContent);
            setArmenianDescription(description.find(item => item.locale === 'hy').htmlContent);

            setEnglishBrand(brandName.brandNameEnglish);
            setArmenianBrand(brandName.brandNameArmenian);

            setEnglishProductType(productType.find(item => item.locale === 'en').htmlContent);
            setArmenianProductType(productType.find(item => item.locale === 'hy').htmlContent);

            setSelectedFiles(productImages);
            setDateYear(productDate);
        } catch (error) {
            console.error('Error fetching product data:', error.message);
        }
    };

    useEffect(() => {
    }, [englishBrand]);

    useEffect(() => {
        fetchProductData();
        fetchBrands();
    });


    return(
        <Paper elevation={3} sx={{marginRight: '15%', marginLeft: '15%'}}>
            <Box sx={{padding: 5}}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <label htmlFor='english-content'>English PageTitle</label>
                        <InputToHtml id='english-content' value={englishTitle} setValue={setEnglishTitle}
                                     placeholder='Write ENGLISH content here...' error={htmlTitleError}/>
                    </Grid>
                    <Grid item xs={12}>
                        <label htmlFor='armenian-content'>Armenian PageTitle</label>
                        <InputToHtml id='armenian-content' value={armenianTitle} setValue={setArmenianTitle}
                                     placeholder='Write ARMENIAN content here...' error={htmlTitleError}/>
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <label htmlFor='english-content'>English Description</label>
                        <InputToHtml id='english-content' value={englishDescription}
                                     setValue={setEnglishDescription}
                                     placeholder='Write ENGLISH content here...' error={htmlTitleError}/>
                    </Grid>
                    <Grid item xs={12}>
                        <label htmlFor='armenian-content'>Armenian Description</label>
                        <InputToHtml id='armenian-content' value={armenianDescription}
                                     setValue={setArmenianDescription} placeholder='Write ARMENIAN content here...'
                                     error={htmlTitleError}/>
                    </Grid>
                </Grid>


                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <label htmlFor='english-content'>English ProductType</label>
                        <InputToHtml id='english-content' value={englishProductType}
                                     setValue={setEnglishProductType}
                                     placeholder='Write ENGLISH content here...' error={htmlTitleError}/>
                    </Grid>
                    <Grid item xs={12}>
                        <label htmlFor='armenian-content'>Armenian ProductType</label>
                        <InputToHtml id='armenian-content' value={armenianProductType}
                                     setValue={setArmenianProductType}
                                     placeholder='Write ARMENIAN content here...' error={htmlTitleError}/>
                    </Grid>
                </Grid>
                <label htmlFor='date-content'>Product Year</label>
                <Grid item xs={12} sm={6}>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <MenuItem value="" disabled></MenuItem>
                        <TextField
                            id="dateYear"
                            select
                            value={dateYear}
                            onChange={handleYearChange}
                            fullWidth
                            error={!!htmlDateTitleError}
                            helperText={htmlDateTitleError}
                        >
                            {years.map((year) => (
                                <MenuItem key={year} value={year}>
                                    {year}
                                </MenuItem>
                            ))}
                        </TextField>
                    </LocalizationProvider>
                </Grid>
                <Grid item xs={12}>
                    <InputLabel htmlFor='brand-select'>Select Brand</InputLabel>
                    <Select
                        id="brand-select"
                        value={englishBrand.replace(/<\/?[^>]+(>|$)/g, "")}
                        onChange={handleBrandDropdownChange}
                        fullWidth
                        error={!!htmlDateTitleError}
                        inputProps={{ 'aria-label': 'Select Brand' }}
                    >
                        <MenuItem value={englishBrand.replace(/<\/?[^>]+(>|$)/g, "")} disabled>{englishBrand.replace(/<\/?[^>]+(>|$)/g, "")}</MenuItem>
                        {brands.map((brand, index) => (
                            <MenuItem key={index} value={brand.brandNameEnglish}>
                                {brand.brandNameEnglish.replace(/<\/?[^>]+(>|$)/g, "")}
                            </MenuItem>
                        ))}
                    </Select>
                    {dropDownError && <FormHelperText error>{dropDownError}</FormHelperText>}
                </Grid>

                <Grid container spacing={2}>
                    {selectedFiles.map((image, index) => (
                        <Grid item key={index} sx={{ display: 'flex', alignItems: 'center' }}>
                            <img src={image} alt={`Image ${index}`} style={{ width: '100px', height: '100px', objectFit: 'cover' }} />
                            <IconButton onClick={() => removeImage(index)}>
                                <DeleteIcon />
                            </IconButton>
                        </Grid>
                    ))}
                </Grid>


                <Paper elevation={3} sx={{marginRight: '15%', marginLeft: '15%'}}>

                    <Box sx={{padding: 5}}>
                        <Grid item xs={12} sm={3}>
                            <InputLabel
                                sx={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                    fontWeight: 700,
                                }}
                            >
                                {htmlImageTitleError && (
                                    <Typography variant="body2" color="error">
                                        {htmlImageTitleError}
                                    </Typography>
                                )}
                                Select Slider Images
                            </InputLabel>
                        </Grid>
                        <Grid item xs={12} sm={9}>
                            <FormControl fullWidth size='small'>
                                <Button onClick={handleClickOpen} variant='outlined' id="images">
                                    Select Files
                                </Button>
                            </FormControl>
                        </Grid>
                    </Box>
                </Paper>
                <Dialog
                    open={open}
                    onClose={handleClose}
                    scroll={'paper'}
                    aria-labelledby="scroll-dialog-title"
                    aria-describedby="scroll-dialog-description"
                    maxWidth='md'
                >
                    <DialogTitle id="scroll-dialog-title">Select files</DialogTitle>
                    <DialogContent dividers={true}>
                        <FilesMultiselect selectedFiles={selectedFiles} setSelectedFiles={setSelectedFiles}/>
                    </DialogContent>
                    <DialogActions sx={{justifyContent: 'center'}} error={htmlImageTitleError}>
                        <Button onClick={handleClose} variant='outlined' sx={{fontSize: 'large', my: '10px'}}>
                            Done
                        </Button>
                    </DialogActions>
                </Dialog>
                <Grid item xs={12} sm={4} sx={{textAlign: 'right'}}>
                    <Button onClick={handleSubmit} variant='contained'>
                        Submit
                    </Button>
                </Grid>
                <ToastContainer/>
            </Box>
        </Paper>
    )
}

export default ProductUpdateComponent;