
import React, {useEffect, useState} from "react";
import ProductList from "./ProductList";



const ProductManagementComponent =() =>{
    const [productData, setProductData] = useState([]);
    useEffect(() => {
        fetchData();
    }, []);
    const fetchData = async () => {

        try {
            const headers = new Headers();
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
            headers.append('Content-Type', 'application/json');

            const response = await fetch(`api/products/all`, {
                method: 'GET',
                headers: headers,
            });

            if (!response.ok) {
                throw new Error('Failed to fetch events');
            }

            const data = await response.json();
            setProductData(data.data);
        } catch (error) {
            console.error('Error fetching products:', error.message);
        }
    };

    return (
        <>
            <div>
                {productData.length === 0 ? (
                    <div>No Products available</div>
                ) : (
                    <ProductList data={productData} fetchDataCallback={fetchData} />
                )}
            </div>
        </>
    )
}

export default ProductManagementComponent;