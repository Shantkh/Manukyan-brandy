import {toast, ToastContainer} from "react-toastify";
import React, {useState} from "react";
import {useNavigate} from "react-router-dom";
import './ProductStyle.css';



const ProductList = ({data, fetchDataCallback}) => {
    const [selectedYears, setSelectedYears] = useState([]);
    const [deleteConfirmationEventId, setDeleteConfirmationEventId] = useState(null);
    let navigate = useNavigate();


    const openDeleteConfirmationPopup = (eventId) => {
        setDeleteConfirmationEventId(eventId);
    };

    const closeDeleteConfirmationPopup = () => {
        setDeleteConfirmationEventId(null);
    };

    const handleEditClick = (productId) => {
        localStorage.setItem('editingProductId', productId);
        navigate("/admin/product/update")
    };

    const getYearFromDate = (dateString) => {
        const date = new Date(dateString);
        return date.toLocaleString("default", {year: "numeric"});
    };

    const uniqueYears = Array.from(
        new Set(data.map((event) => getYearFromDate(event.productDate)))
    );

    const deleteItem = async (productId) => {
        try {
            const headers = new Headers();
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
            headers.append('Content-Type', 'application/json');

            const response = await fetch(`admin/product/delete/${productId}`, {
                method: 'DELETE',
                headers: headers
            });

            if (!response.ok) {
                throw new Error('Failed to delete event');
            }

            closeDeleteConfirmationPopup();
            fetchDataCallback();
            toast.success('Event deleted successfully', {
                autoClose: 3000, // 3 seconds
            });
        } catch (error) {
            console.error('Error deleting event:', error.message);
            toast.error('Failed to delete event', {
                autoClose: 3000, // 3 seconds
            });
        }
    }

    const handleCheckboxChange = (event) => {
        const {value, checked} = event.target;
        setSelectedYears((prevSelectedMonths) =>
            checked
                ? [...prevSelectedMonths, value]
                : prevSelectedMonths.filter((month) => month !== value)
        );
    };

    return (
        <div>
            <div className="checkbox-container">
                {uniqueYears.map((year, index) => {
                    const eventsForYear = data.filter(
                        (event) => getYearFromDate(event.productDate) === year
                    );
                    return (
                        <label key={year} className="checkbox-label-tour">
                            <input
                                type="checkbox"
                                value={year}
                                onChange={handleCheckboxChange}
                                checked={selectedYears.includes(year)}
                            />
                            <span className="checkbox-text">{year}</span>
                            <span className="event-count">{eventsForYear.length}</span>
                        </label>
                    );
                })}
            </div>
            <div className="product-container">
                {data
                    .filter(
                        (event) =>
                            selectedYears.length === 0 ||
                            selectedYears.includes(
                                getYearFromDate(event.productDate)
                            )
                    )
                    .map((event) => (
                        <div key={event.id} className="products-card">
                            <div className="info-title-product">Product year:  <span className="info-title-product-span">{event.productDate}</span></div>
                            <hr/>
                            <div className="product-details-update">
                                <div className="content">
                                    <div className="info-title-product">Title:
                                        <span className="info-title-product-span" dangerouslySetInnerHTML={{
                                            __html: event.pageTitle.find((loc) => loc.locale === "en").htmlContent,
                                        }}/>
                                    </div>
                                    <div className="info-title-product">Brand:
                                        <span className="info-title-product-span"
                                            dangerouslySetInnerHTML={{
                                                __html: event.brandName.brandNameEnglish
                                            }}
                                        />
                                    </div>
                                    <div className="info-title-product">Description:
                                        <span className="info-title-product-span"
                                            dangerouslySetInnerHTML={{
                                                __html: event.description.find(
                                                    (loc) => loc.locale === "en"
                                                ).htmlContent,
                                            }}
                                        />
                                    </div>
                                    <div className="info-title-product">Product type:
                                        <span className="info-title-product-span"
                                            dangerouslySetInnerHTML={{
                                                __html: event.productType.find(
                                                    (loc) => loc.locale === "en"
                                                ).htmlContent,
                                            }}
                                        />
                                    </div>
                                </div>
                               <hr/>
                                <div className="product-details-update">
                                    <div className="content">
                                        <div className="info-title-product">Վերնագիր:
                                        <span className="info-title-product-span"
                                            dangerouslySetInnerHTML={{
                                                __html: event.pageTitle.find(
                                                    (loc) => loc.locale === "hy"
                                                ).htmlContent,
                                            }}
                                        />
                                        </div>
                                        <div className="info-title-product">Ապրանքանիշը:
                                        <span className="info-title-product-span"
                                            dangerouslySetInnerHTML={{
                                                __html: event.brandName.brandNameArmenian
                                            }}
                                        /></div>
                                        <div className="info-title-product">Երկար բովանդակություն:
                                        <span className="info-title-product-span"
                                            dangerouslySetInnerHTML={{
                                                __html: event.description.find(
                                                    (loc) => loc.locale === "hy"
                                                ).htmlContent,
                                            }}
                                        /></div>
                                        <div className="info-title-product">Երկար բովանդակություն:
                                        <span className="info-title-product-span"
                                            dangerouslySetInnerHTML={{
                                                __html: event.productType.find(
                                                    (loc) => loc.locale === "hy"
                                                ).htmlContent,
                                            }}
                                        /></div>
                                    </div>
                                </div>
                            </div>
                            <div className="tproduct-images">
                                {event.productImages.map((image, index) => (
                                    <div key={index} className="product-container">
                                        <img
                                            src={image}
                                            alt={`Product ${index}`}
                                            className="product-image"
                                        />
                                    </div>
                                ))}
                            </div>
                            <div className="button-container">
                                <button
                                    className="edit-button"
                                    onClick={() => handleEditClick(event.id)}
                                >
                                    Edit
                                </button>
                                <button
                                    className="delete-button"
                                    onClick={() =>
                                        openDeleteConfirmationPopup(event.id)
                                    }
                                >
                                    Delete
                                </button>
                            </div>
                            {deleteConfirmationEventId === event.id && (
                                <div className="popup-delete">
                                    <div className="popup-content-delete">
                                        <p>Are you sure you want to delete this item?</p>
                                        <div className="button-container-delete">
                                            <button
                                                className="delete-button"
                                                onClick={closeDeleteConfirmationPopup}
                                            >
                                                Cancel
                                            </button>
                                            <button
                                                className="confirm-button-delete"
                                                onClick={() => deleteItem(event.id)}
                                            >
                                                Confirm
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            )}
                        </div>
                    ))}
            </div>
            <ToastContainer/>
        </div>

    );
};


export default ProductList;