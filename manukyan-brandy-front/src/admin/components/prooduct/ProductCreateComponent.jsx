import React, { useState, useEffect } from "react";
import {
    Box,
    Button,
    Dialog, DialogActions, DialogContent,
    DialogTitle,
    FormControl,
    Grid,
    InputLabel,
    Paper,
    TextField,
    Typography
} from "@mui/material";
import InputToHtml from "../inputToHtml/InputToHtml";
import {LocalizationProvider} from "@mui/x-date-pickers/LocalizationProvider";
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";
import FilesMultiselect from "../banners/FileMultiselect";
import {toast, ToastContainer} from "react-toastify";
import MenuItem from "@mui/material/MenuItem";
import Select from '@mui/material/Select';




const ProductCreateComponent = () => {
    const [brands, setBrands] = useState([]);
    const [selectedBrand, setSelectedBrand] = useState('');
    const [selectedBrandIndex, setSelectedBrandIndex] = useState(null); // Define selectedBrandIndex
    const [open, setOpen] = useState(false);
    const [englishTitle, setEnglishTitle] = useState('');
    const [armenianTitle, setArmenianTitle] = useState('');
    const [englishDescription, setEnglishDescription] = useState('');
    const [armenianDescription, setArmenianDescription] = useState('');
    const [englishProductType, setEnglishProductType] = useState('');
    const [armenianProductType, setArmenianProductType] = useState('');
    const [selectedFiles, setSelectedFiles] = useState([]);
    const [htmlTitleError, setHtmlTitleError] = useState('');
    const [htmlImageTitleError, setHtmlImageTitleError] = useState('');
    const [htmlDateTitleError, setDateHtmlTitleError] = useState('');
    const [dateYear, setDateYear] = useState(null);
    const [loading, setLoading] = useState(true);
    const currentYear = new Date().getFullYear();
    const [englishBrandName, setEnglishBrandName] = useState('');
    const [armenianBrandName, setArmenianBrandName] = useState('');

    const years = Array.from({ length: 150 }, (_, index) => currentYear - index);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const removeImage = (indexToRemove) => {
        setSelectedFiles((prevFiles) => prevFiles.filter((_, index) => index !== indexToRemove));
    };

    useEffect(() => {
        fetchBrands();
    }, []);


    const fetchBrands = async () => {
        try {
            const response = await fetch( `admin/brand/name/get`);
            if (response.ok) {
                const data = await response.json();
                setBrands(data.data);
            } else {
                console.error('Failed to fetch brands');
                toast.error("Failed to fetch brands");
            }
        } catch (error) {
            console.error('Error fetching brands:', error);
            toast.error("Error fetching brands");
        } finally {
            setLoading(false);
        }
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        if(selectedBrand === null){
            toast.error("Please select a brand.");
            return
        }
        const headers = new Headers();
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
        headers.append('Content-Type', 'application/json'); // Set content type to JSON

        try {
            const selectedBrandData = brands.find(brand => brand.brandNameEnglish === englishBrandName);
            const startTimestamp = dateYear;

            const titleLocalizations = [
                { locale: 'en', htmlContent: englishTitle },
                { locale: 'hy', htmlContent: armenianTitle }
            ];

            const descriptionLocalizations = [
                { locale: 'en', htmlContent: englishDescription },
                { locale: 'hy', htmlContent: armenianDescription }
            ];

            const typeLocalizations = [
                { locale: 'en', htmlContent: englishProductType },
                { locale: 'hy', htmlContent: armenianProductType }
            ];



            const formData = {
                productImages: selectedFiles,
                pageTitle: titleLocalizations,
                brandName: selectedBrandData,
                description: descriptionLocalizations,
                productDate: startTimestamp,
                productType: typeLocalizations
            };

            const response = await fetch( `admin/product/save`, {
                method: 'POST',
                headers: headers,
                body: JSON.stringify(formData)
            });

            if (!response.ok) {
                throw new Error('Failed to save product');
            }

            toast.success('Product saved successfully', {
                autoClose: 3000,
                onClose: () => window.location.reload()
            });
        } catch (error) {
            console.error('Error saving product:', error.message);
            toast.error('Failed to save product. Please try again later.');
        }
    };


    const handleYearChange = (event) => {
        setDateYear(event.target.value);
        setDateHtmlTitleError('');
    };

    const handleBrandDropdownChange = (event) => {
        const selectedBrandName = event.target.value;
        const selectedIndex = brands.findIndex(brand => brand.brandNameEnglish === selectedBrandName);
        setSelectedBrand(selectedBrandName);
        setSelectedBrandIndex(selectedIndex);
    };


    useEffect(() => {
        // Fetch brands from the server when the component mounts
        fetchBrands();
    }, []);

    return (
        <Paper elevation={3} sx={{marginRight: '15%', marginLeft: '15%'}}>
            <Box sx={{padding: 5}}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <label htmlFor='english-content'>English PageTitle</label>
                        <InputToHtml id='english-content' value={englishTitle} setValue={setEnglishTitle}
                                     placeholder='Write ENGLISH content here...' error={htmlTitleError}/>
                    </Grid>
                    <Grid item xs={12}>
                        <label htmlFor='armenian-content'>Armenian PageTitle</label>
                        <InputToHtml id='armenian-content' value={armenianTitle} setValue={setArmenianTitle}
                                     placeholder='Write ARMENIAN content here...' error={htmlTitleError}/>
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <label htmlFor='english-content'>English Description</label>
                        <InputToHtml id='english-content' value={englishDescription}
                                     setValue={setEnglishDescription}
                                     placeholder='Write ENGLISH content here...' error={htmlTitleError}/>
                    </Grid>
                    <Grid item xs={12}>
                        <label htmlFor='armenian-content'>Armenian Description</label>
                        <InputToHtml id='armenian-content' value={armenianDescription}
                                     setValue={setArmenianDescription} placeholder='Write ARMENIAN content here...'
                                     error={htmlTitleError}/>
                    </Grid>
                </Grid>

                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <label htmlFor='english-content'>English ProductType</label>
                        <InputToHtml id='english-content' value={englishProductType}
                                     setValue={setEnglishProductType}
                                     placeholder='Write ENGLISH content here...' error={htmlTitleError}/>
                    </Grid>
                    <Grid item xs={12}>
                        <label htmlFor='armenian-content'>Armenian ProductType</label>
                        <InputToHtml id='armenian-content' value={armenianProductType}
                                     setValue={setArmenianProductType}
                                     placeholder='Write ARMENIAN content here...' error={htmlTitleError}/>
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <FormControl fullWidth>
                            <InputLabel>Select Brand Name</InputLabel>
                            <Select
                                value={englishBrandName}
                                onChange={(e) => setEnglishBrandName(e.target.value)}
                                displayEmpty
                                fullWidth
                                inputProps={{'aria-label': 'Select Brand Name'}}
                            >
                                {brands.map((brand) => (
                                    <MenuItem key={brand.id}
                                              value={brand.brandNameEnglish}>{brand.brandNameEnglish.replace(/<\/?[^>]+(>|$)/g, "")}</MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                </Grid>
                <label htmlFor='date-content'>Product Year</label>
                <Grid item xs={12} sm={6}>
                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                        <TextField
                            id="dateYear"
                            label="Select a Year"
                            select
                            value={dateYear}
                            onChange={handleYearChange}
                            fullWidth
                            error={!!htmlDateTitleError}
                            helperText={htmlDateTitleError}
                        >
                            {years.map((year) => (
                                <MenuItem key={year} value={year}>
                                    {year}
                                </MenuItem>
                            ))}
                        </TextField>
                    </LocalizationProvider>
                </Grid>
                <Grid container spacing={2}>
                    {selectedFiles.map((image, index) => (
                        <Grid item key={index} sx={{ display: 'flex', alignItems: 'center' }}>
                            <img src={image} alt={`Image ${index}`} style={{ width: '100px', height: '100px', objectFit: 'cover' }} />
                            <IconButton onClick={() => removeImage(index)}>
                                <DeleteIcon />
                            </IconButton>
                        </Grid>
                    ))}
                </Grid>

                <Paper elevation={3} sx={{marginRight: '15%', marginLeft: '15%'}}>

                    <Box sx={{padding: 5}}>
                        <Grid item xs={12} sm={3}>
                            <InputLabel
                                sx={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                    fontWeight: 700,
                                }}
                            >
                                {htmlImageTitleError && (
                                    <Typography variant="body2" color="error">
                                        {htmlImageTitleError}
                                    </Typography>
                                )}
                                Select Slider Images
                            </InputLabel>
                        </Grid>
                        <Grid item xs={12} sm={9}>
                            <FormControl fullWidth size='small'>
                                <Button onClick={handleClickOpen} variant='outlined' id="images">
                                    Select Files
                                </Button>
                            </FormControl>
                        </Grid>
                    </Box>
                </Paper>
                <Dialog
                    open={open}
                    onClose={handleClose}
                    scroll={'paper'}
                    aria-labelledby="scroll-dialog-title"
                    aria-describedby="scroll-dialog-description"
                    maxWidth='md'
                >
                    <DialogTitle id="scroll-dialog-title">Select files</DialogTitle>
                    <DialogContent dividers={true}>
                        <FilesMultiselect selectedFiles={selectedFiles} setSelectedFiles={setSelectedFiles}/>
                    </DialogContent>
                    <DialogActions sx={{justifyContent: 'center'}} error={htmlImageTitleError}>
                        <Button onClick={handleClose} variant='outlined' sx={{fontSize: 'large', my: '10px'}}>
                            Done
                        </Button>
                    </DialogActions>
                </Dialog>
                <Grid item xs={12} sm={4} sx={{textAlign: 'right'}}>
                    <Button onClick={handleSubmit} variant='contained'>
                        Submit
                    </Button>
                </Grid>
                <ToastContainer/>
            </Box>
        </Paper>
    )
}

export default ProductCreateComponent;