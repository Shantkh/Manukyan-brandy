import React, {useEffect, useState} from "react";
import EventList from "./EventList";


const EventsManagementComponent = () => {

    const [eventData, setEventData] = useState([]);
    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
        const paginationBody = {
            pageNumber: 0,
            contentSize: 15,
            sortDirection: "asc",
            sortElement: "eventStartDate"
        };

        try {
            const headers = new Headers();
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
            headers.append('Content-Type', 'application/json');

            const response = await fetch( `api/events/show`, {
                method: 'POST',
                headers: headers,
                body: JSON.stringify(paginationBody)
            });

            if (!response.ok) {
                throw new Error('Failed to fetch events');
            }

            const data = await response.json();
            setEventData(data.data);
        } catch (error) {
            console.error('Error fetching events:', error.message);
        }
    };
    return (
        <>
            <div>
                {eventData.length === 0 ? (
                    <div>No events available</div>
                ) : (
                    <EventList data={eventData} fetchDataCallback={fetchData} />
                )}
            </div>
        </>
    )
}

export default EventsManagementComponent;