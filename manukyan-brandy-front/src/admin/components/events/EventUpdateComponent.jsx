import React, {useState, useEffect} from 'react';
import {toast, ToastContainer} from 'react-toastify';
import './EventsCreateStyle.css';
import {
    Box,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    Grid,
    InputLabel,
    Paper, TextField, Typography
} from '@mui/material';
import FilesMultiselect from '../banners/FileMultiselect';
import InputToHtml from "../inputToHtml/InputToHtml";

import {AdapterDayjs} from '@mui/x-date-pickers/AdapterDayjs';
import {LocalizationProvider} from '@mui/x-date-pickers/LocalizationProvider';
import {DateTimeField} from "@mui/x-date-pickers";
import dayjs from "dayjs";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";




const EventUpdateComponent = () => {
    const [eventData, setEventData] = useState(null);
    const [open, setOpen] = useState(false);
    const [englishTitle, setEnglishTitle] = useState('');
    const [armenianTitle, setArmenianTitle] = useState('');
    const [armenianTitleKey, setArmenianTitleKey] = useState('');
    const [englishTitleKey, setEnglishTitleKey] = useState('');
    const [englishShortContent, setEnglishShortContent] = useState('');
    const [armenianShortContent, setArmenianShortContent] = useState('');
    const [englishLongContent, setEnglishLongContent] = useState('');
    const [armenianLongContent, setArmenianLongContent] = useState('');
    const [selectedFiles, setSelectedFiles] = useState([]);
    const [htmlTitleError, setHtmlTitleError] = useState('');
    const [htmlImageTitleError, setHtmlImageTitleError] = useState('');
    const [htmlDateTitleError, setDateHtmlTitleError] = useState('');
    const [startDate, setStartDate] = useState(null);
    const [endDate, setEndDate] = useState(null);

    useEffect(() => {
        if (eventData && eventData.localizations && eventData.sliderMedia) {
            const englishLoc = eventData.localizations.find(loc => loc.locale === "en");
            const armenianLoc = eventData.localizations.find(loc => loc.locale === "hy");
            const sliderMedia = eventData.sliderMedia || [];

            setEnglishTitle(englishLoc ? englishLoc.htmlTitle : "");
            setArmenianTitle(armenianLoc ? armenianLoc.htmlTitle : "");

            setArmenianTitleKey(armenianLoc ? armenianLoc.id : "");
            setEnglishTitleKey(englishLoc ? englishLoc.id : "");

            setEnglishShortContent(englishLoc ? englishLoc.htmlShortContent : "");
            setArmenianShortContent(armenianLoc ? armenianLoc.htmlShortContent : "");
            setEnglishLongContent(englishLoc ? englishLoc.htmlLongContent : "");
            setArmenianLongContent(armenianLoc ? armenianLoc.htmlLongContent : "");
            setStartDate(new Date(eventData.eventStartDate));
            setEndDate(new Date(eventData.eventEndDate));
            setSelectedFiles(sliderMedia);
        }
    }, [eventData]);

    const fetchEventData = async () => {
        const eventId = localStorage.getItem('editingEventId');
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        try {
            const response = await fetch(`api/events/get/${eventId}`, {
                method: 'GET',
                headers: headers,
            });

            if (!response.ok) {
                throw new Error('Failed to fetch event data');
            }
            const data = await response.json();
            setEventData(data.data);
        } catch (error) {
            console.error('Error fetching event data:', error.message);
        }
    };

    useEffect(() => {
        fetchEventData();
    }, []);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const convertToJDate = (date) =>{
        return  dayjs(date);
    };

    const validateForm = () => {
        let isValid = true;

        if (!englishTitle || !armenianTitle || !englishShortContent || !armenianShortContent || !englishLongContent || !armenianLongContent) {
            isValid = false;
            setHtmlTitleError('All fields are required');
            return isValid;
        }

        if (!startDate) {
            isValid = false;
            setDateHtmlTitleError('Please select a start date.');
            return isValid;
        }

        if (!endDate) {
            isValid = false;
            setDateHtmlTitleError('Please select an end date.');
            return isValid;
        }

        if (endDate < startDate) {
            isValid = false;
            setDateHtmlTitleError('End date cannot be before start date.');
            return isValid;
        }

        if (selectedFiles.length === 0) {
            isValid=false;
            setHtmlImageTitleError('No images selected.')
            return isValid;
        }

        setHtmlTitleError('');
        setDateHtmlTitleError('');
        setHtmlImageTitleError('');
        return isValid;
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        let media = [];

        if (validateForm()) {
            media = selectedFiles.map((file) => {
                if (file.includes(process.env.REACT_APP_ADMIN_IMAGES)) {
                    return file;
                } else {
                    return process.env.REACT_APP_ADMIN_IMAGES + file;
                }
            });

            const startTimestamp = dayjs(startDate).valueOf();
            const endTimestamp = dayjs(endDate).valueOf();

            const localizations = [
                {
                    id: englishTitleKey,
                    locale: 'en',
                    htmlLongContent: englishLongContent,
                    htmlShortContent: englishShortContent,
                    htmlTitle: englishTitle
                },
                {
                    id: armenianTitleKey,
                    locale: 'hy',
                    htmlLongContent: armenianLongContent,
                    htmlShortContent: armenianShortContent,
                    htmlTitle: armenianTitle
                }
            ];

            const formData = {
                id:  localStorage.getItem('editingEventId'),
                isEventVisible: true,
                eventStartDate: startTimestamp,
                eventEndDate: endTimestamp,
                sliderMedia: media,
                localizations: localizations,
            };

            const endpoint = `admin/events/update`;

            const headers = new Headers();
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
            headers.append('Content-Type', 'application/json');

            try {
                const response = await fetch(endpoint, {
                    method: 'PUT',
                    headers: headers,
                    body: JSON.stringify(formData),
                });

                if (!response.ok) {
                    throw new Error('Failed to save event');
                }

                toast.success('Tour data saved successfully', {
                    autoClose: 3000, // 3 seconds
                    onClose: () => window.location.reload() // Reload page after toast is closed
                });
            } catch (error) {
                console.error('Error saving event:', error.message);
            }
        } else {
        }
    };
    const removeImage = (indexToRemove) => {
        setSelectedFiles((prevFiles) => prevFiles.filter((_, index) => index !== indexToRemove));
    };

    const handleStartDateChange = (date) => {
        setStartDate(date);
    };

    const handleEndDateChange = (date) => {
        setEndDate(date);
    };

    return (
        <Paper elevation={3} sx={{marginRight: '15%', marginLeft: '15%'}}>
            <Box sx={{padding: 5}}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <label htmlFor='english-content'>English Content</label>
                        <InputToHtml id='english-content' value={englishTitle || ''} setValue={setEnglishTitle}
                                     placeholder='Write ENGLISH content here...' error={htmlTitleError}/>
                    </Grid>
                    <Grid item xs={12}>
                        <label htmlFor='armenian-content'>Armenian Content</label>
                        <InputToHtml id='armenian-content' value={armenianTitle || ''} setValue={setArmenianTitle}
                                     placeholder='Write ARMENIAN content here...' error={htmlTitleError}/>
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <label htmlFor='english-content'>English Short Content</label>
                        <InputToHtml id='english-content' value={englishShortContent || ''}
                                     setValue={setEnglishShortContent}
                                     placeholder='Write ENGLISH content here...' error={htmlTitleError}/>
                    </Grid>
                    <Grid item xs={12}>
                        <label htmlFor='armenian-content'>Armenian Short Content</label>
                        <InputToHtml id='armenian-content' value={armenianShortContent || ''}
                                     setValue={setArmenianShortContent} placeholder='Write ARMENIAN content here...'
                                     error={htmlTitleError}/>
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <label htmlFor='english-content'>English long Content</label>
                        <InputToHtml id='english-content' value={englishLongContent }
                                     setValue={setEnglishLongContent}
                                     placeholder='Write ENGLISH content here...' error={htmlTitleError}/>
                    </Grid>
                    <Grid item xs={12}>
                        <label htmlFor='armenian-content'>Armenian long Content</label>
                        <InputToHtml id='armenian-content' value={armenianLongContent || ''}
                                     setValue={setArmenianLongContent}
                                     placeholder='Write ARMENIAN content here...' error={htmlTitleError}/>
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={12} sm={6}>
                        <label htmlFor='date-content'>Start Date</label>
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                            <DateTimeField
                                id="startDate"
                                value={convertToJDate(startDate)}
                                onChange={(date) => handleStartDateChange(date)}
                                placeholder='Select start date...'
                                renderInput={(params) => <TextField {...params} fullWidth/>}
                                error={htmlDateTitleError}
                            />
                        </LocalizationProvider>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <label htmlFor='date-content'>End Date</label>
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                            <DateTimeField
                                id="endDate"
                                value={convertToJDate(endDate)}
                                onChange={(date) => handleEndDateChange(date)}
                                placeholder='Select end date...'
                                renderInput={(params) => <TextField {...params} fullWidth/>}
                                error={htmlDateTitleError}
                            />
                        </LocalizationProvider>
                    </Grid>
                </Grid>
                <Grid container spacing={2}>
                    {selectedFiles.map((image, index) => (
                        <Grid item key={index} sx={{ display: 'flex', alignItems: 'center' }}>
                            <img src={image} alt={`Image ${index}`} style={{ width: '100px', height: '100px', objectFit: 'cover' }} />
                            <IconButton onClick={() => removeImage(index)}>
                                <DeleteIcon />
                            </IconButton>
                        </Grid>
                    ))}
                </Grid>
                <Paper elevation={3} sx={{marginRight: '15%', marginLeft: '15%'}}>

                    <Box sx={{padding: 5}}>
                        <Grid item xs={12} sm={3}>
                            <InputLabel
                                sx={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                    fontWeight: 700,
                                }}
                            >
                                {htmlImageTitleError && (
                                    <Typography variant="body2" color="error">
                                        {htmlImageTitleError}
                                    </Typography>
                                )}
                                Select Slider Images
                            </InputLabel>
                        </Grid>
                        <Grid item xs={12} sm={9}>
                            <FormControl fullWidth size='small'>
                                <Button onClick={handleClickOpen} variant='outlined' id="images">
                                    Select Files
                                </Button>
                            </FormControl>
                        </Grid>
                    </Box>
                </Paper>
                <Dialog
                    open={open}
                    onClose={handleClose}
                    scroll={'paper'}
                    aria-labelledby="scroll-dialog-title"
                    aria-describedby="scroll-dialog-description"
                    maxWidth='md'
                >
                    <DialogTitle id="scroll-dialog-title">Select files</DialogTitle>
                    <DialogContent dividers={true}>
                        <FilesMultiselect selectedFiles={selectedFiles} setSelectedFiles={setSelectedFiles}/>
                    </DialogContent>
                    <DialogActions sx={{justifyContent: 'center'}} error={htmlImageTitleError}>
                        <Button onClick={handleClose} variant='outlined' sx={{fontSize: 'large', my: '10px'}}>
                            Done
                        </Button>
                    </DialogActions>
                </Dialog>
                <Grid item xs={12} sm={4} sx={{textAlign: 'right'}}>
                    <Button onClick={handleSubmit} variant='contained'>
                        Submit
                    </Button>
                </Grid>
                <ToastContainer/>
            </Box>
        </Paper>
    );
};

export default EventUpdateComponent;