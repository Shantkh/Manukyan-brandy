import React, { useState } from "react";
import { toast, ToastContainer } from "react-toastify";
import {useNavigate} from "react-router-dom";



const EventList = ({ data, fetchDataCallback }) => {
    const [selectedMonths, setSelectedMonths] = useState([]);
    const [deleteConfirmationEventId, setDeleteConfirmationEventId] = useState(null);
    let navigate = useNavigate();

    const getMonthFromDate = (dateString) => {
        const date = new Date(dateString);
        return date.toLocaleString("default", { month: "long" });
    };

    const handleCheckboxChange = (event) => {
        const { value, checked } = event.target;
        setSelectedMonths((prevSelectedMonths) =>
            checked
                ? [...prevSelectedMonths, value]
                : prevSelectedMonths.filter((month) => month !== value)
        );
    };

    const handleEditClick = (eventId) => {
        localStorage.setItem('editingEventId', eventId);
        navigate("/admin/events/update")
    };

    const openDeleteConfirmationPopup = (eventId) => {
        setDeleteConfirmationEventId(eventId);
    };

    const closeDeleteConfirmationPopup = () => {
        setDeleteConfirmationEventId(null);
    };

    const uniqueMonths = Array.from(
        new Set(data.map((event) => getMonthFromDate(event.eventStartDate)))
    );

    const deleteItem = async (eventId) => {
        try {
            const headers = new Headers();
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
            headers.append('Content-Type', 'application/json');

            const response = await fetch(  `admin/events/delete/${eventId}`, {
                method: 'DELETE',
                headers: headers
            });

            if (!response.ok) {
                throw new Error('Failed to delete event');
            }

            closeDeleteConfirmationPopup();
            fetchDataCallback();
            toast.success('Event deleted successfully', {
                autoClose: 3000, // 3 seconds
            });
        } catch (error) {
            console.error('Error deleting event:', error.message);
            toast.error('Failed to delete event', {
                autoClose: 3000, // 3 seconds
            });
        }
    };

    return (
        <div>
            <div className="checkbox-container">
                {uniqueMonths.map((month, index) => {
                    const eventsForMonth = data.filter(
                        (event) => getMonthFromDate(event.eventStartDate) === month
                    );
                    return (
                        <label key={month} className="checkbox-label-tour">
                            <input
                                type="checkbox"
                                value={month}
                                onChange={handleCheckboxChange}
                                checked={selectedMonths.includes(month)}
                            />
                            <span className="checkbox-text">{month}</span>
                            <span className="event-count">{eventsForMonth.length}</span>
                        </label>
                    );
                })}
            </div>
            <div className="tour-container">
                {data
                    .filter(
                        (event) =>
                            selectedMonths.length === 0 ||
                            selectedMonths.includes(
                                getMonthFromDate(event.eventStartDate)
                            )
                    )
                    .map((event) => (
                        <div key={event.id} className="tour-card">
                            <div className="tour-details">
                                <div className="info-group">
                                    <div className="info-title">Start Date:</div>
                                    <div>
                                        {new Date(event.eventStartDate).toLocaleString()}
                                    </div>
                                </div>
                                <div className="info-group">
                                    <div className="info-title">End Date:</div>
                                    <div>{new Date(event.eventEndDate).toLocaleString()}</div>
                                </div>
                                <div className="language-content">
                                    <div className="content">
                                        <div className="info-title">English:</div>
                                        <div className="info-title">Title:</div>
                                        <div
                                            dangerouslySetInnerHTML={{
                                                __html: event.localizations.find(
                                                    (loc) => loc.locale === "en"
                                                ).htmlTitle,
                                            }}
                                        />
                                        <div className="info-title">Short content:</div>
                                        <div
                                            dangerouslySetInnerHTML={{
                                                __html: event.localizations.find(
                                                    (loc) => loc.locale === "en"
                                                ).htmlShortContent,
                                            }}
                                        />
                                        <div className="info-title">Long content:</div>
                                        <div
                                            dangerouslySetInnerHTML={{
                                                __html: event.localizations.find(
                                                    (loc) => loc.locale === "en"
                                                ).htmlLongContent,
                                            }}
                                        />
                                    </div>
                                </div>
                                <div className="language-content">
                                    <div className="content">
                                        <div className="info-title">Հայերեն:</div>
                                        <div className="info-title">Վերնագիր:</div>
                                        <div
                                            dangerouslySetInnerHTML={{
                                                __html: event.localizations.find(
                                                    (loc) => loc.locale === "hy"
                                                ).htmlTitle,
                                            }}
                                        />
                                        <div className="info-title">Կարճ բովանդակություն:</div>
                                        <div
                                            dangerouslySetInnerHTML={{
                                                __html: event.localizations.find(
                                                    (loc) => loc.locale === "hy"
                                                ).htmlShortContent,
                                            }}
                                        />
                                        <div className="info-title">Երկար բովանդակություն:</div>
                                        <div
                                            dangerouslySetInnerHTML={{
                                                __html: event.localizations.find(
                                                    (loc) => loc.locale === "hy"
                                                ).htmlLongContent,
                                            }}
                                        />
                                    </div>
                                </div>
                                <div className="button-container">
                                    <button
                                        className="edit-button"
                                        onClick={() => handleEditClick(event.id)}
                                    >
                                        Edit
                                    </button>
                                    <button
                                        className="delete-button"
                                        onClick={() =>
                                            openDeleteConfirmationPopup(event.id)
                                        }
                                    >
                                        Delete
                                    </button>
                                </div>
                                {deleteConfirmationEventId === event.id && (
                                    <div className="popup-delete">
                                        <div className="popup-content-delete">
                                            <p>Are you sure you want to delete this item?</p>
                                            <div className="button-container-delete">
                                                <button
                                                    className="delete-button"
                                                    onClick={closeDeleteConfirmationPopup}
                                                >
                                                    Cancel
                                                </button>
                                                <button
                                                    className="confirm-button-delete"
                                                    onClick={() => deleteItem(event.id)}
                                                >
                                                    Confirm
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                    ))}
            </div>
            <ToastContainer />
        </div>
    );
};

export default EventList;
