import React, {useState} from 'react';
import {toast, ToastContainer} from 'react-toastify';
import './EventsCreateStyle.css';
import {
    Box,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    Grid,
    InputLabel,
    Paper, TextField, Typography
} from '@mui/material';
import FilesMultiselect from '../banners/FileMultiselect';
import InputToHtml from "../inputToHtml/InputToHtml";
import {AdapterDayjs} from '@mui/x-date-pickers/AdapterDayjs';
import {LocalizationProvider} from '@mui/x-date-pickers/LocalizationProvider';
import {DateTimeField} from "@mui/x-date-pickers";
import dayjs from "dayjs";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";




const EventsCreateComponent = () => {
    const [open, setOpen] = useState(false);
    const [englishTitle, setEnglishTitle] = useState('');
    const [armenianTitle, setArmenianTitle] = useState('');
    const [englishShortContent, setEnglishShortContent] = useState('');
    const [armenianShortContent, setArmenianShortContent] = useState('');
    const [englishLongContent, setEnglishLongContent] = useState('');
    const [armenianLongContent, setArmenianLongContent] = useState('');
    const [selectedFiles, setSelectedFiles] = useState([]);
    const [htmlTitleError, setHtmlTitleError] = useState('');
    const [htmlImageTitleError, setHtmlImageTitleError] = useState('');
    const [htmlDateTitleError, setDateHtmlTitleError] = useState('');
    const [startDate, setStartDate] = useState(null);
    const [endDate, setEndDate] = useState(null);

    const validateForm = () => {
        let isValid = true;

        if (!englishTitle || !armenianTitle || !englishShortContent || !armenianShortContent || !englishLongContent || !armenianLongContent) {
            isValid = false;
            setHtmlTitleError('All fields are required');
            return isValid;
        }

        if (!startDate) {
            isValid = false;
            setDateHtmlTitleError('Please select a start date.');
            return isValid;
        }

        if (!endDate) {
            isValid = false;
            setDateHtmlTitleError('Please select an end date.');
            return isValid;
        }

        if (endDate < startDate) {
            isValid = false;
            setDateHtmlTitleError('End date cannot be before start date.');
            return isValid;
        }

        if (selectedFiles.length === 0) {
            isValid=false;
            setHtmlImageTitleError('No images selected.')
            return isValid;
        }

        setHtmlTitleError('');
        setDateHtmlTitleError('');
        setHtmlImageTitleError('');
        return isValid;
    };

    const handleSubmit = async (e) => {

        e.preventDefault();

        if (validateForm()) {
            const media = selectedFiles.map((file) => {
                return file
            });

            const startTimestamp = dayjs(startDate).valueOf();
            const endTimestamp = dayjs(startDate).valueOf();

            const localizations = [
                {
                    locale: 'en',
                    htmlLongContent: englishLongContent,
                    htmlShortContent: englishShortContent,
                    htmlTitle: englishTitle
                },
                {
                    locale: 'hy',
                    htmlLongContent: armenianLongContent,
                    htmlShortContent: armenianShortContent,
                    htmlTitle: armenianTitle
                }
            ];

            const formData = {
                isEventVisible: true,
                startDate: startTimestamp,
                endDate: endTimestamp,
                sliderMedia: media,
                localizations: localizations
            };
            const queryParams = new URLSearchParams({
                startDate: startTimestamp,
                endDate: endTimestamp,
            });

            const headers = new Headers();
            headers.append('Authorization', 'Bearer ' + localStorage.getItem('accessToken'));
            headers.append('Content-Type', 'application/json');

            try {
                const response = await fetch(  `admin/events/save?${queryParams}`, {
                    method: 'POST',
                    headers: headers,
                    body: JSON.stringify(formData),
                });

                if (!response.ok) {
                    throw new Error('Failed to save event');
                }

                toast.success('Tour data saved successfully', {
                    autoClose: 3000, // 3 seconds
                    onClose: () => window.location.reload() // Reload page after toast is closed
                });
            } catch (error) {
                console.error('Error saving event:', error.message);
            }
        }
    };

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const removeImage = (indexToRemove) => {
        setSelectedFiles((prevFiles) => prevFiles.filter((_, index) => index !== indexToRemove));
    };

    return (
        <Paper elevation={3} sx={{marginRight: '15%', marginLeft: '15%'}}>
            <Box sx={{padding: 5}}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <label htmlFor='english-content'>English Content</label>
                        <InputToHtml id='english-content' value={englishTitle} setValue={setEnglishTitle}
                                     placeholder='Write ENGLISH content here...' error={htmlTitleError}/>
                    </Grid>
                    <Grid item xs={12}>
                        <label htmlFor='armenian-content'>Armenian Content</label>
                        <InputToHtml id='armenian-content' value={armenianTitle} setValue={setArmenianTitle}
                                     placeholder='Write ARMENIAN content here...' error={htmlTitleError}/>
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <label htmlFor='english-content'>English Short Content</label>
                        <InputToHtml id='english-content' value={englishShortContent}
                                     setValue={setEnglishShortContent}
                                     placeholder='Write ENGLISH content here...' error={htmlTitleError}/>
                    </Grid>
                    <Grid item xs={12}>
                        <label htmlFor='armenian-content'>Armenian Short Content</label>
                        <InputToHtml id='armenian-content' value={armenianShortContent}
                                     setValue={setArmenianShortContent} placeholder='Write ARMENIAN content here...'
                                     error={htmlTitleError}/>
                    </Grid>
                </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <label htmlFor='english-content'>English long Content</label>
                        <InputToHtml id='english-content' value={englishLongContent}
                                     setValue={setEnglishLongContent}
                                     placeholder='Write ENGLISH content here...' error={htmlTitleError}/>
                    </Grid>
                    <Grid item xs={12}>
                        <label htmlFor='armenian-content'>Armenian long Content</label>
                        <InputToHtml id='armenian-content' value={armenianLongContent}
                                     setValue={setArmenianLongContent}
                                     placeholder='Write ARMENIAN content here...' error={htmlTitleError}/>
                    </Grid>
                </Grid>

                <Grid container spacing={3}>
                    <Grid item xs={12} sm={6}>
                        <label htmlFor='date-content'>Start Date</label>
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                            <DateTimeField
                                id="startDate"
                                value={startDate}
                                onChange={(date) => setStartDate(date)}
                                placeholder='Select start date...'
                                renderInput={(params) => <TextField {...params} fullWidth/>}
                                error={htmlDateTitleError}
                            />
                        </LocalizationProvider>
                    </Grid>
                    <Grid item xs={12} sm={6}>
                        <label htmlFor='date-content'>End Date</label>
                        <LocalizationProvider dateAdapter={AdapterDayjs}>
                            <DateTimeField
                                id="endDate"
                                value={endDate}
                                onChange={(date) => setEndDate(date)}
                                placeholder='Select end date...'
                                renderInput={(params) => <TextField {...params} fullWidth/>}
                                error={htmlDateTitleError}
                            />
                        </LocalizationProvider>
                    </Grid>

                </Grid>
                <Grid container spacing={2}>
                    {selectedFiles.map((image, index) => (
                        <Grid item key={index} sx={{ display: 'flex', alignItems: 'center' }}>
                            <img src={image} alt={`Image ${index}`} style={{ width: '100px', height: '100px', objectFit: 'cover' }} />
                            <IconButton onClick={() => removeImage(index)}>
                                <DeleteIcon />
                            </IconButton>
                        </Grid>
                    ))}
                </Grid>

                <Paper elevation={3} sx={{marginRight: '15%', marginLeft: '15%'}}>

                    <Box sx={{padding: 5}}>
                        <Grid item xs={12} sm={3}>
                            <InputLabel
                                sx={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                    fontWeight: 700,
                                }}
                            >
                                {htmlImageTitleError && (
                                    <Typography variant="body2" color="error">
                                        {htmlImageTitleError}
                                    </Typography>
                                )}
                                Select Slider Images
                            </InputLabel>
                        </Grid>
                        <Grid item xs={12} sm={9}>
                            <FormControl fullWidth size='small'>
                                <Button onClick={handleClickOpen} variant='outlined' id="images">
                                    Select Files
                                </Button>
                            </FormControl>
                        </Grid>
                    </Box>
                </Paper>
                <Dialog
                    open={open}
                    onClose={handleClose}
                    scroll={'paper'}
                    aria-labelledby="scroll-dialog-title"
                    aria-describedby="scroll-dialog-description"
                    maxWidth='md'
                >
                    <DialogTitle id="scroll-dialog-title">Select files</DialogTitle>
                    <DialogContent dividers={true}>
                        <FilesMultiselect selectedFiles={selectedFiles} setSelectedFiles={setSelectedFiles}/>
                    </DialogContent>
                    <DialogActions sx={{justifyContent: 'center'}} error={htmlImageTitleError}>
                        <Button onClick={handleClose} variant='outlined' sx={{fontSize: 'large', my: '10px'}}>
                            Done
                        </Button>
                    </DialogActions>
                </Dialog>
                <Grid item xs={12} sm={4} sx={{textAlign: 'right'}}>
                    <Button onClick={handleSubmit} variant='contained'>
                        Submit
                    </Button>
                </Grid>
                <ToastContainer/>
            </Box>
        </Paper>
    )
}

export default EventsCreateComponent;
