import React, {useEffect, useState} from 'react';
import './SubscribersStyle.css';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';
import {Button} from 'primereact/button';
import {toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

const SubscribersComponent = () => {
    const [subscribers, setSubscribers] = useState([]);
    const [loading, setLoading] = useState(false);

    const fetchData = async () => {
        try {
            const headers = new Headers();
            const token = localStorage.getItem('accessToken');
            headers.append("Authorization", 'Bearer ' + token);
            headers.append("Content-Type", "application/json");
            const response = await fetch( `admin/subscribe/list`, {
                method: 'GET',
                headers: headers
            });
            const data = await response.json();
            setSubscribers(data.data);
        } catch (error) {
            console.error('Error fetching booking results:', error);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const getTime = (timestamp) => {
        const date = new Date(timestamp);
        return `${date.getFullYear()}-${String(date.getMonth() + 1).padStart(2, '0')}-${String(date.getDate()).padStart(2, '0')}`;
    };

    const removeSubscriber = async (subscriberId) => {

        try {
            const headers = new Headers();
            const token = localStorage.getItem('accessToken');
            headers.append("Authorization", 'Bearer ' + token);
            headers.append("Content-Type", "application/json");
            const response = await fetch(`admin/subscribe/ids`, {
                method: 'DELETE',
                headers: headers,
                body: JSON.stringify([subscriberId])
            });
            if (response.ok) {
                toast("Subscriber deleted successfully");
                fetchData();
            } else {
                toast("Failed to delete subscriber");
            }
        } catch (error) {
            console.error('Error deleting subscriber:', error);
            toast("Failed to delete subscriber");
        }
    };


    const renderDeleteButton = (rowData) => {
        return (
            <Button onClick={() => removeSubscriber(rowData.id)} icon="pi pi-trash" className="delete-button">
                <i className="fa fa-trash"></i> Delete
            </Button>
        );
    };

    const handleDownload  = () => {
        setLoading(true); // Show loader

        fetch(`admin/subscribe/download`)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Failed to download CSV');
                }
                return response.blob();
            })
            .then(blob => {
                const url = window.URL.createObjectURL(blob);
                const a = document.createElement('a');
                a.href = url;
                a.download = 'subscribers.csv';
                document.body.appendChild(a);
                a.click();
                window.URL.revokeObjectURL(url);
            })
            .catch(error => {
                console.error('Error downloading CSV:', error);
            })
            .finally(() => {
                setLoading(false); // Hide loader
            });
    };

    return (
        <div className="subscribers-container">
            <div className="button-suc">
                <button className="download-button" disabled={loading} onClick={handleDownload}>
                    <i className="pi pi-download"></i>
                    {loading ? 'Downloading...' : 'Download CSV'}
                </button>
            </div>
            <DataTable value={subscribers} paginator rows={25}
                       tableClassName="p-datatable-striped" tableStyle={{minWidth: '50rem'}}>
                <Column field={(rowData) => getTime(rowData.createdOn)} header="Subscriber Since" sortable
                        style={{width: '45%', padding: '20px 20px', marginRight: '20px'}}/>
                <Column field="email" header="Email" sortable
                        style={{width: '50%', padding: '20px 20px', marginRight: '20px'}}/>
                <Column header="DELETE" body={renderDeleteButton}
                        style={{minWidth: '5%', padding: '20px 20px', marginRight: '20px'}}/>
            </DataTable>
        </div>
    );
};

export default SubscribersComponent;
