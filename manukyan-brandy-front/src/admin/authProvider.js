
const authProvider = {
  login: async ({ username, password }) => {

    const signInHeaders = new Headers();
    signInHeaders.append("Content-Type", "application/json");

    const requestOptions = {
      method: 'POST',
      headers: signInHeaders,
      body: JSON.stringify({ username, password }),
    };

    try {
      const response = await fetch(  `api/auth/signin`, requestOptions);

      // if (!response.ok) {
      //   throw new Error('Wrong username or password.');
      // }

      const responseData = await response.json();

      localStorage.setItem('accessToken', responseData.data.accessToken);
      localStorage.setItem('refreshToken', responseData.data.refreshToken);
      localStorage.setItem('permissions', responseData.data.roles[0]);

      return true;
    } catch (error) {
      console.error('Authentication error:', error);
      // throw new Error('Authentication failed' + error);
    }
  },

  checkAuth: async () => {
    if(localStorage.getItem('accessToken')) {
      return true
    } else if(localStorage.getItem('refreshToken')){
      try {
        const refreshHeaders = new Headers();
        refreshHeaders.append("Content-Type", "application/json");

        const refreshToken = localStorage.getItem('refreshToken');
        const requestOptions = {
          method: 'POST',
          headers: refreshHeaders,
          body: JSON.stringify({"token": refreshToken}),
          redirect: 'follow'
        };

        const response = await fetch( `api/auth/refresh-token`, requestOptions);

        // if (!response.ok) {
        //   throw new Error('Authentication failed');
        // }

        const responseData = await response.json();

        localStorage.setItem('accessToken', responseData.data.accessToken);

        return true
      } catch (error) {
        console.error('Authentication error:', error);
        // throw new Error('Authentication failed');
      }
    } else return false
  },

  getPermissions: () => {
    const role = localStorage.getItem('permissions');
    return role ? Promise.resolve(role) : Promise.reject();
  },

  logout: async () => {
    const token = localStorage.getItem('accessToken');

    if (!token) {
      return Promise.resolve();
    }

    const requestOptions = {
      method: 'POST',
    };

    try {
      const response = await fetch(`api/auth/signout`, requestOptions);

      if (!response.ok) {
        throw new Error('Logout failed');
      }

      localStorage.removeItem('accessToken');
      localStorage.removeItem('refreshToken');

      return Promise.resolve();
    } catch (error) {
      console.error('Logout error:', error);
      throw new Error(error);
    }
  },

  checkError: (error) => {
    return Promise.resolve();
  },
};

export default authProvider;
