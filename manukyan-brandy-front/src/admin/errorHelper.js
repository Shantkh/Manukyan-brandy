import authProvider from './authProvider';

export const handleUnauthorized = (status) => {
  if (status === 401) {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
    return authProvider.checkAuth();
  }
  return true;
};