import HomeIcon from '@mui/icons-material/Home';
import UploadFileIcon from '@mui/icons-material/UploadFile';
import DashboardIcon from '@mui/icons-material/Dashboard';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import ManageAccounts from '@mui/icons-material/ManageAccounts';
import FactoryIcon from '@mui/icons-material/Factory';
import EventSeat from '@mui/icons-material/EventSeat';
import Subscriptions from '@mui/icons-material/Subscriptions';
import Tour from '@mui/icons-material/Tour';
import WineBar from '@mui/icons-material/WineBar';
import Celebration from '@mui/icons-material/Celebration';
import Feed from '@mui/icons-material/Feed';
import Grade from '@mui/icons-material/Grade';
import Handshake from '@mui/icons-material/Handshake';
import Liquor from '@mui/icons-material/Liquor';
import {OilBarrel, QrCodeScanner, Sanitizer} from "@mui/icons-material";

export const sectionsInfo = [
    {name: 'Dashboard', path: '/admin', icon: <DashboardIcon/>},
    {name: 'Uploads', path: '/admin/uploads', icon: <UploadFileIcon/>},
    {
        name: 'Home Page',
        icon: <HomeIcon/>,
        subitems: [
            {name: 'Main Slider', path: '/admin/create/homeSlider', icon: <AddCircleOutlineIcon/>},
            {name: 'Factory Section', path: '/admin/create/homeFactory', icon: <AddCircleOutlineIcon/>},
            {name: 'Events Section', path: '/admin/create/homeEvent', icon: <AddCircleOutlineIcon/>},
            {name: 'Tours Section', path: '/admin/create/homeTour', icon: <AddCircleOutlineIcon/>},
            {name: 'Banner', path: '/admin/create/homeBanner', icon: <AddCircleOutlineIcon/>},
        ],
    },
    {
        name: 'Factory Page',
        icon: <FactoryIcon/>,
        subitems: [
            {name: 'Factory Main Banner', path: '/admin/create/factoryMainBanner', icon: <AddCircleOutlineIcon/>},
            {name: 'Factory Map Banner', path: '/admin/create/factorySecondBanner', icon: <AddCircleOutlineIcon/>},
            {name: 'Factory Wine Yards', path: '/admin/create/factoryWineYards', icon: <AddCircleOutlineIcon/>},
            {name: 'Factory Middle Banner', path: '/admin/create/factoryMiddleBanner', icon: <AddCircleOutlineIcon/>},
            {name: 'Factory Fermentation', path: '/admin/create/factoryFermentation', icon: <AddCircleOutlineIcon/>},
            {name: 'Factory Cellars', path: '/admin/create/factoryCellars', icon: <AddCircleOutlineIcon/>},
            {name: 'Factory Bottling', path: '/admin/create/factoryBottling', icon: <AddCircleOutlineIcon/>},
            {name: 'Factory Bottom Banner', path: '/admin/create/factoryBottomBanner', icon: <AddCircleOutlineIcon/>},
            {name: 'Factory Info Banner', path: '/admin/create/factory/info', icon: <AddCircleOutlineIcon/>},
        ],
    },
    {
        name: 'Cellar Page',
        icon: <OilBarrel/>,
        subitems: [
            {name: 'Create Cellar Page', path: '/admin/cellar/create', icon: <AddCircleOutlineIcon/>},
            {name: 'Main Banner', path: '/admin/cellar/mainBanner', icon: <AddCircleOutlineIcon/>},
            {name: 'Middle Banner', path: '/admin/cellar/middleBanner', icon: <AddCircleOutlineIcon/>},
            {name: 'Cellar Info Banner', path: '/admin/cellar/info', icon: <AddCircleOutlineIcon/>},
        ],
    },
    {
        name: 'Bottling Page',
        icon: <Sanitizer/>,
        subitems: [
            {name: 'Create Bottling Page', path: '/admin/bottling/create', icon: <AddCircleOutlineIcon/>},
            {name: 'Main Banner', path: '/admin/bottling/mainBanner', icon: <AddCircleOutlineIcon/>},
            {name: 'Middle Banner', path: '/admin/bottling/middleBanner', icon: <AddCircleOutlineIcon/>},
            {name: 'Bottling Info Banner', path: '/admin/bottling/info', icon: <AddCircleOutlineIcon/>},
        ],
    },
    {
        name: 'Wine branding Page',
        icon: <Liquor/>,
        subitems: [
            {name: 'Create Brand Names', path: '/admin/brand/names/create', icon: <AddCircleOutlineIcon/>},
            {name: 'Brand Names Management', path: '/admin/brand/names/management', icon: <ManageAccounts/>}
        ],
    },
    {
        name: 'Wines Page',
        icon: <WineBar/>,
        subitems: [
            {name: 'Create Wine', path: '/admin/product/create', icon: <AddCircleOutlineIcon/>},
            {name: 'Wines Management', path: '/admin/product/management', icon: <ManageAccounts/>},
            {name: 'Wines Main Banner', path: '/admin/product/wineMainBanner', icon: <AddCircleOutlineIcon/>},
            {name: 'Wines Middle Banner', path: '/admin/product/wineMiddleBanner', icon: <AddCircleOutlineIcon/>},
            {name: 'Wines Info Banner', path: '/admin/product/info', icon: <AddCircleOutlineIcon/>},
        ],
    },
    {
        name: 'Holani Page',
        icon: <QrCodeScanner/>,
        subitems: [
            {name: 'Holani Page', path: '/admin/holani/create', icon: <AddCircleOutlineIcon/>},
            {name: 'Main Banner', path: '/admin/holani/holaniMainBanner', icon: <AddCircleOutlineIcon/>},
            {name: 'Lower Banner', path: '/admin/holani/holaniLowerBanner', icon: <AddCircleOutlineIcon/>}
        ],
    },
    {
        name: 'Anamor Page',
        icon: <QrCodeScanner/>,
        subitems: [
            {name: 'Anamor  Page', path: '/admin/anamor/create', icon: <AddCircleOutlineIcon/>},
            {name: 'Main Banner', path: '/admin/anamor/anamorMainBanner', icon: <AddCircleOutlineIcon/>},
            {name: 'Lower Banner', path: '/admin/anamor/anamorLowerBanner', icon: <AddCircleOutlineIcon/>}
        ],
    },
    {
        name: 'Tours Page',
        icon: <Tour/>,
        subitems: [
            {name: 'Create Tour', path: '/admin/tours/create', icon: <AddCircleOutlineIcon/>},
            {name: 'Tours Management', path: '/admin/tours/management', icon: <ManageAccounts/>}
        ],
    },
    {
        name: 'Events Page',
        icon: <Celebration/>,
        subitems: [
            {name: 'Create Event', path: '/admin/events/create', icon: <AddCircleOutlineIcon/>},
            {name: 'Events Management', path: '/admin/events/management', icon: <ManageAccounts/>}
        ],
    },
    {
        name: 'Partners Page',
        icon: <Handshake/>,
        subitems: [
            {name: 'Create partner', path: '/admin/partner/create', icon: <AddCircleOutlineIcon/>},
            {name: 'Partners Management', path: '/admin/partner/management', icon: <ManageAccounts/>},
        ],
    },
    {
        name: '',
        icon: '',
    },
    {
        name: 'Tours Booking page',
        icon: <EventSeat/>,
        subitems: [
            {name: 'Tours bookings', path: '/admin/tours/booking', icon: <Grade/>},
        ],
    },
    {
        name: 'Email Subscribers Page',
        icon: <Subscriptions/>,
        subitems: [
            {name: 'News Subscribers', path: '/admin/subscribers', icon: <Feed/>},
        ],
    },

];