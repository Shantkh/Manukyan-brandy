FROM openjdk:17-jdk-alpine
WORKDIR /app
COPY /src/tar/*.jar /app/my-app.jar

EXPOSE 8080
ENTRYPOINT ["java", "-jar", "my-app.jar"]


